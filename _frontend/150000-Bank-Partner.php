<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Japan Airlines (JAL)</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <div class="block slider-hero">
                <div class="slider__item">
                    <img src="assets/img/slidebanner-1.jpg" alt="">
                </div>
            </div>

            <div class="text-center">
                <h1 class="block--small text-blue">
                    ANZ Installment 0% Hingga 6 Bulan + No Surcharge
                </h1>
                <p>
                    Berlaku hingga 20 September 2018
                </p>
            </div>
            <hr>
            <article>
                <h4>Syarat dan Ketentuan</h4>
                <ul>
                    <li>
                        Vero, dolorum expedita qui. Rem vitae fuga, perspiciatis quos illo omnis sequi odio ipsam perferendis pariatur atque esse nihil minus maiores tenetur!
                    </li>
                    <li>
                        Quam eligendi nisi minus blanditiis soluta nesciunt esse, mollitia autem doloremque totam nemo quis nobis consectetur, ex est veniam, quos velit consequuntur.
                    </li>
                    <li>
                        Autem explicabo facilis a perspiciatis voluptatibus ex totam quam eveniet impedit illo doloribus, omnis in assumenda blanditiis harum natus dignissimos magnam. Similique.
                    </li>
                    <li>
                        Quibusdam numquam illo, enim voluptate, culpa minima pariatur itaque nihil dolore voluptas doloribus non consectetur fugit sapiente facilis excepturi quaerat quis praesentium.
                    </li>
                    <li>
                        Quo quasi sunt error soluta eum hic veniam aspernatur quas reprehenderit at? Fugiat hic veritatis maiores sunt nesciunt magni sequi numquam, harum.
                    </li>
                    <li>
                        Natus excepturi tempore vitae, minus ea, molestias ut autem distinctio dolorum fugiat rem commodi veniam accusantium reprehenderit consectetur quisquam sapiente in obcaecati.
                    </li>
                    <li>
                        Molestiae incidunt necessitatibus commodi aliquam error culpa eum provident rem nemo laboriosam dolorem exercitationem, voluptas voluptatem architecto mollitia placeat, quo animi voluptates.
                    </li>
                    <li>
                        Aspernatur consequatur cumque molestiae esse voluptate ipsum omnis eum repellat velit fuga voluptatum quisquam autem, obcaecati tempora quis optio impedit, maxime odio.
                    </li>
                    <li>
                        Deleniti, qui, eius. Aperiam dolorum, voluptatibus quam laborum iste quibusdam quae omnis dolor dicta laboriosam repudiandae error! Impedit ad atque quaerat, voluptatibus.
                    </li>
                </ul>
            </article>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
