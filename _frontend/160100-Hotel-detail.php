<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Hotel</a></li>
                <li><a href="#">Mandarin Oriental</a></li>
            </ul>
        </div>
    </div>


    <div class="container container--smaller">
        <div class="block--half text-right">
        <button class="btn btn--round btn--ghost-red toggle-trigger text-up" type="button" data-target="#research_hotel">
            <strong>Ganti Pencarian</strong>
        </button>
        </div>
    </div>
    <div class="fill-primary block--half toggle-panel toggle-panel--slideup" id="research_hotel" data-room="dev/room2.json">
    <!-- <div class="block--double fill-primary block--inset relative z-4"> -->
        <div class="container container--smaller">
            <div class="block--inset-v relative z-4">
            <form id="form-hotel" action="https://histravel.suitdev.com/hotel/search" class="form" data-research="dev/room2.json">
                <!-- <input type="hidden" name="city_id" value="339_ID"> -->
                <div class="bzg bzg--lite-gutter">
                    <div class="form__row bzg_c" data-col="l6">
                        <label for=""  class="sr-only"  >Destinasi Pilihan</label>
                        <select required name="city_id" id="city_id" class="form-input form-input--block select_2"
                            data-select="https://histravel.suitdev.com/hotel/destination"
                            data-placeholder="Dimana anda akan menginap?">
                            <option value="7355_SG">Singapore, Singapore</option>
                        </select>
                    </div>
                    <div class="form__row bzg_c" data-col="l6">

                        <div class="bzg bzg--lite-gutter pickdate-range">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for=""  class="sr-only"  >Check In</label>
                                <div class="input-iconic">
                                    <input required name="checkin_date" type="text" class="form-input form-input--block pikaRange pikaRange--start" id="chekin_start" placeholder="Tanggal Check In"
                                     value="2018/12/20"                                     >
                                    <label for="chekin_start" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for=""  class="sr-only"  >Check Out</label>
                                <div class="input-iconic">
                                    <input required name="checkout_date" type="text" class="form-input form-input--block pikaRange pikaRange--end" id="chekout_end" placeholder="Tanggal Check Out"
                                     value="2018/12/24"                                     >
                                    <label for="chekout_end" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- pickdate-range -->
                    </div>

                </div>

                <div class="bzg">
                    <div class="mb-small bzg_c" data-col="l6">
                        <label for="">Jumlah Kamar</label>
                        <div class="bzg">
                            <div class="bzg_c"data-col="l6">
                                <div class="flex a-center">
                                    <div class="fg-1">
                                        <div class="flex add-room">
                                            <input type="number" class="sr-only room-counter" min="1" max="8" value="1">
                                            <div class="fg-1 form-input form-input-viewer text-center">1</div>
                                            <button class="btn btn--plain" style="color: #fff;" type="button" data-action="minus">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                            <button class="btn btn--plain" style="color: #fff;" type="button" data-action="plus">
                                                <span class="fa fa-plus"></span>
                                            </button>

                                            <template id="roomContent">
                                                <div class="bzg guest-list">
                                                    <span class="bzg_c hidden">Kamar <span class="room-index"></span></span>
                                                    <div class="mb-small bzg_c" data-col="m6">
                                                        <div class="flex a-center">
                                                            <label for="" class="nowrap in-block mr-small w-30">
                                                                <span class="fa fa-male"></span>
                                                                <span class="fa fa-female"></span>
                                                            </label>
                                                            <div class="fg-1">
                                                                <select name="adult_guest[]" id="adult_guest" class="adult-guest form-input form-input--block form-input--person selectstyle" data-value="{{adults}}">
                                                                    <option value="1">1 Dewasa</option>
                                                                    <option value="2">2 Dewasa</option>
                                                                    <option value="3">3 Dewasa</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-small bzg_c" data-col="m6">
                                                        <div class="flex a-center">
                                                            <label for="" class="nowrap in-block mr-small w-30 text-center">
                                                                <span class="fa fa-child"></span>
                                                            </label>
                                                            <div class="fg-1">
                                                                <select name="child_guest[]" id="child_guest" class="form-input form-input--child form-input--block form-input--person selectstyle child-age-select" data-value="{{childs.length}}" data-ages='[{{childs}}]'>
                                                                    <option value="0">0 Anak</option>
                                                                    <option value="1">1 Anak</option>
                                                                    <option value="2">2 Anak</option>
                                                                    <option value="3">3 Anak</option>
                                                                    <option value="4">4 Anak</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-half bzg_c child-age-container">
                                                        <div class="">Umur Anak</div>
                                                        <div class="bzg bzg--small-gutter child-age__inputs">

                                                        </div>
                                                        <hr class="no-space">
                                                    </div>
                                                </div>
                                            </template>
                                            <!-- roomContent template -->
                                            <!-- <script>
                                                function childAgeInput(index, age, val = 0) {
                                                    return '<div class="mb-small bzg_c" data-col="s3">'+
                                                    '<select name="child_age['+index+']['+age+']" id="ChildAge['+index+']['+age+']" class="form-input form-input--block" data-value="'+val+'">'+ages(18)+
                                                    '</select>'+
                                                    '</div>'
                                                }
                                                function ages(age) {
                                                    var option;
                                                    option += '<option value="0">&lt;1 Tahun</option>'
                                                    for(var i = 1; i < age; i++ ) {
                                                        option += '<option value="'+i+'">'+i+' Tahun</option>'
                                                    }
                                                    return option
                                                }
                                            </script> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-small bzg_c guest-container" data-col="l6">
                        loading
                    </div>
                </div>
                <div class="bzg">&nbsp;</div>
                <div class="bzg">
                    <div class="form__row bzg_c" data-col="l6">
                        <div class="toggle-fyout-m" data-toggle>
                            <button class="btn btn-toggle btn--plain link-white">
                                <span class="fa fa-angle-down btn-icon"></span>
                                Pilihan Pencarian Tambahan
                            </button>
                            <div class="toggle-panel fill-pink">
                                <div class="block--inset">
                                    <div class="form__row">
                                        <label for="">Harga per malam</label>
                                        <div class="bzg">
                                            <div class="form__row bzg_c" data-col="s6">
                                                <div class="input-iconic--left fill-white border">
                                                    <label for="" class="label-icon">IDR</label>
                                                    <input name="price_min" type="number" class="form-input form-input--block text-right no-border" placeholder="minimum">
                                                </div>
                                            </div>
                                            <div class="form__row bzg_c" data-col="s6">
                                                <div class="input-iconic--left fill-white border">
                                                    <label for="" class="label-icon">IDR</label>
                                                    <input name="price_max" type="number" class="form-input form-input--block text-right no-border" placeholder="maksimum">
                                                </div>
                                            </div>
                                            <div class="form__row bzg_c" data-col="m5, l6">
                                                <label for="">Star Rating</label>
                                                <div class="add-rating">
                                                    <label class="btn-star" for="star-1">
                                                        <input name="star" value="1" class="btn-radio" type="radio" name="rating-star" id="star-1">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-2">
                                                        <input name="star" value="2" class="btn-radio" type="radio" name="rating-star" id="star-2">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-3">
                                                        <input name="star" value="3" class="btn-radio" type="radio" name="rating-star" id="star-3">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-4">
                                                        <input name="star" value="4" class="btn-radio" type="radio" name="rating-star" id="star-4">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-5">
                                                        <input name="star" value="5" class="btn-radio" type="radio" name="rating-star" id="star-5">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                </div>
                                                <!-- add-rating -->
                                            </div>
                                            <div class="form__row bzg_c" data-col="s7, l6">
                                                <label for="">Ketersediaan</label><br>
                                                <label for="available_hotel">
                                                    <input name="is_available_only" value="1" type="checkbox" id="available_hotel">
                                                    <span>
                                                        Hanya menampilkan hotel yang tersedia
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bzg_c" data-col="l6">
                        <div class="bzg">
                            <div class="bzg_c" data-col="l12">
                                <label for="nationality">Kewarganegaraan :</label>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="bzg_c" data-col="l6">
                                <div class="fg-1">
                                    <select name="NationalityID" id="nationality" class="form-input form-input--block selectstyle">
                                        <option value="75"  selected="selected" >Indonesia</option>
                                        <option value="86" >Singapura</option>
                                    </select>
                                </div>
                                <br>
                            </div>
                            <div class="bzg_c" data-col="l6">
                                <button class="btn btn--red btn--block">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            </div>
        </div>
    </div>
    <div class="container container--smaller">
        <div class="block bzg">
            <div class="block bzg_c" data-col="m7">
                <h1 class="h2 no-space">Mandarin Oriental</h1>
                <div class="rating rating--solid" data-rating="4.0" title="4.0">
                    <span class="star-rating fa fa-star"></span>
                    <span class="star-rating fa fa-star"></span>
                    <span class="star-rating fa fa-star"></span>
                    <span class="star-rating fa fa-star"></span>
                    <span class="star-rating fa fa-star"></span>
                </div>
                <div class="block text-blue">
                    <span class="fa fa-map-marker"></span>
                    Thamrin, Jakarta - Indonesia
                </div>
                <div class="block slide-w-thumb">
                    <div class="responsive-media media--3-2">
                        <div class="slide-view">
                            <div class="slide-view_item">
                                <figure class="no-space responsive-media media--3-2">
                                    <img data-lazy="assets/img/shinkansen-1.jpg" alt="">
                                </figure>
                            </div>
                            <div class="slide-view_item">
                                <figure class="no-space responsive-media media--3-2">
                                    <img data-lazy="http://via.placeholder.com/600x400/607d8b" alt="">
                                </figure>
                            </div>
                            <div class="slide-view_item">
                                <figure class="no-space responsive-media media--3-2">
                                    <img data-lazy="assets/img/shinkansen-2.jpg" alt="">
                                </figure>
                            </div>
                            <div class="slide-view_item">
                                <figure class="no-space responsive-media media--3-2">
                                    <img data-lazy="http://via.placeholder.com/601x401/f05a8e" alt="">
                                </figure>
                            </div>
                            <div class="slide-view_item">
                                <figure class="no-space responsive-media media--3-2">
                                    <img data-lazy="http://via.placeholder.com/601x400/009bbf/ffffff" alt="">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="slide-thumb">
                        <div class="space slide-thumb__item">
                            <figure class="responsive-media media--3-2">
                                <img src="http://via.placeholder.com/150x100" alt="">
                            </figure>
                        </div>
                        <div class="space slide-thumb__item">
                            <figure class="responsive-media media--3-2">
                                <img src="http://via.placeholder.com/150x101/607d8b/ffffff" alt="">
                            </figure>
                        </div>
                        <div class="space slide-thumb__item">
                            <figure class="responsive-media media--3-2">
                                <img src="http://via.placeholder.com/151x100" alt="">
                            </figure>
                        </div>
                        <div class="space slide-thumb__item">
                            <figure class="responsive-media media--3-2">
                                <img src="http://via.placeholder.com/151x101/f05a8e" alt="">
                            </figure>
                        </div>
                        <div class="space slide-thumb__item">
                            <figure class="responsive-media media--3-2">
                                <img src="http://via.placeholder.com/152x102/009bbf/ffffff" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <!-- Slides -->
            </div>
            <div class="block bzg_c" data-col="m5, l4" data-offset="l1">
                <div class="mb-small flex a-center line--small">
                    <span>
                        Mulai<br>dari:
                    </span>
                    <strong class="h1 no-space fg-1 text-pink text-center">IDR 2.530.000</strong>
                    <span>/night</span>
                </div>
                <div class="block">
                    <a href="#" class="btn btn--round btn--block btn--pink text-up">Pilih Kamar</a>
                </div>
                <div class="block map-container map-container--responsive border" data-marker="assets/img/marker-dot.png" data-map="dev/location.json">
                    <div class="map-area" id="his_map_2"></div>
                </div>
                <div class="block flex flex--fixed text-center text--huge">
                    <div class="line--small">
                        14:00<br>
                        Check in
                    </div>
                    <div class="line--small">
                        11:00<br>
                        Check out
                    </div>
                </div>
                <div class="block border border--round p-small fill-lightgrey">
                    <h3 class="mb-small text-center">Hotel Top Features</h3>
                    <hr class="mb-small">
                    <div class="mb-small bzg bzg--small-gutter text--smaller line--small">
                        <div class="mb-small bzg_c" data-col="s6">
                            <div class="flex a-center">
                                <img src="assets/img/icon-internet-blue.png" width="32" alt="" class="fs-0">
                                <span class="fg-1 pl-small">Free Internet</span>
                            </div>
                        </div>
                        <div class="mb-small bzg_c" data-col="s6">
                            <div class="flex a-center">
                                <img src="assets/img/icon-room-service.png" width="32" alt="" class="fs-0">
                                <span class="fg-1 pl-small">Room Service</span>
                            </div>
                        </div>
                        <div class="mb-small bzg_c" data-col="s6">
                            <div class="flex a-center">
                                <img src="assets/img/icon-frontdesk.png" width="32" alt="" class="fs-0">
                                <span class="fg-1 pl-small">24 Hour front desk</span>
                            </div>
                        </div>
                        <div class="mb-small bzg_c" data-col="s6">
                            <div class="flex a-center">
                                <img src="assets/img/icon-currency-exchange.png" width="32" alt="" class="fs-0">
                                <span class="fg-1 pl-small">Currency Exchange</span>
                            </div>
                        </div>
                        <div class="mb-small bzg_c" data-col="s6">
                            <div class="flex a-center">
                                <img src="assets/img/icon-ac.png" width="32" alt="" class="fs-0">
                                <span class="fg-1 pl-small">Air Conditioning</span>
                            </div>
                        </div>
                        <div class="mb-small bzg_c" data-col="s6">
                            <div class="flex a-center">
                                <img src="assets/img/icon-parking-car.png" width="32" alt="" class="fs-0">
                                <span class="fg-1 pl-small">Car Parking</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block fill-lightgrey p-1 border border--round" data-room="dev/room2.json">
            <div class="mb-small">
                From <span class="text-blue book-date--start">...</span> to <span class="text-blue book-date--end">...</span>,
                <span class="book-nights">.</span> nights,
                <span class="book-rooms">.</span> rooms,
                <span class="book-adults">.</span> adults,
                <span class="book-childs">.</span> children
            </div>
            <hr class="mb-small">
            <form action="http://localhost/his-travel/_frontend/160100-Hotel-detail.php?city_id=339_ID&checkin_date=2019%2F10%2F16&checkout_date=&rooms=5&adult_guest%5B%5D=1&child_guest%5B3%5D=3&child_age%5B3%5D%5B0%5D=0&child_age%5B3%5D%5B1%5D=2&child_age%5B3%5D%5B2%5D=15&child_guest%5B4%5D=3&child_age%5B4%5D%5B0%5D=0&child_age%5B4%5D%5B1%5D=2&child_age%5B4%5D%5B2%5D=15&child_guest%5B5%5D=3&child_guest%5B6%5D=3&child_guest%5B7%5D=0&child_guest%5B8%5D=0&child_guest%5B9%5D=0" data-research="dev/room.json">
                <!-- <input type="hidden" name="city_id" value="339_ID"> -->
                <div class="bzg">
                    <div class="mb-small bzg_c" data-col="m8, l5">
                        <div class="flex a-center">
                            <label for="" class="in-block mr-small">Dates</label>
                            <div class="bzg bzg--no-gutter pickdate-range" data-days="#range2_Days">
                                <div class="bzg_c" data-col="m6">
                                    <div class="input-iconic">
                                        <input type="text" class="form-input form-input--block pikaRange pikaRange--start" name="checkin_date" id="chekin_start2" placeholder="Check In">
                                        <!-- <label for="chekin_start2" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label> -->
                                    </div>
                                </div>
                                <div class="bzg_c" data-col="m6">
                                    <div class="input-iconic">
                                        <input type="text" class="form-input form-input--block pikaRange pikaRange--end" name="checkout_date" id="chekout_end2" placeholder="Check Out">
                                        <!-- <label for="chekout_end2" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label> -->
                                    </div>
                                </div>
                            </div>
                            <!-- pickdate-range -->
                            <small class="in-block ml-small nowrap">
                                <span id="range2_Days">0</span> nights
                            </small>
                        </div>
                    </div>
                    <!-- Start template add hotel guest -->
                    <div class="mb-small bzg_c" data-col="m4, l2">
                        <div class="flex a-center">
                            <label for="" class="in-block mr-small w-30">
                                <span class="fa fa-bed"></span>
                            </label>
                            <div class="fg-1">
                                <div class="flex add-room">
                                    <button class="btn btn--plain btn--plain-red" type="button" data-action="minus">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="sr-only room-counter" name="rooms" min="1" max="8" value="1">
                                    <div class="fg-1 form-input  form-input-viewer form-input--redline text-center">1</div>
                                    <button class="btn btn--plain btn--plain-red" type="button" data-action="plus">
                                        <span class="fa fa-plus"></span>
                                    </button>

                                    <template id="roomContent2">
                                        <div class="bzg guest-list">
                                            <span class="bzg_c hidden">Kamar <span class="room-index"></span></span>
                                            <div class="mb-small bzg_c" data-col="m6">
                                                <div class="flex a-center">
                                                    <label for="" class="nowrap in-block mr-small w-30">
                                                        <span class="fa fa-male"></span>
                                                        <span class="fa fa-female"></span>
                                                    </label>
                                                    <div class="fg-1">
                                                        <select name="adult_guest[]" id="adult_guest" class="adult-guest form-input form-input--block form-input--person selectstyle" data-value="{{adults}}">
                                                            <option value="1">1 adult</option>
                                                            <option value="2">2 adult</option>
                                                            <option value="3">3 adult</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-small bzg_c" data-col="m6">
                                                <div class="flex a-center">
                                                    <label for="" class="nowrap in-block mr-small w-30 text-center">
                                                        <span class="fa fa-child"></span>
                                                    </label>
                                                    <div class="fg-1">
                                                        <select name="child_guest[]" id="child_guest" class="form-input form-input--child form-input--block form-input--person selectstyle child-age-select" data-value="{{childs.length}}" data-ages='[{{childs}}]'>
                                                            <option value="0">0 child</option>
                                                            <option value="1">1 child</option>
                                                            <option value="2">2 childs</option>
                                                            <option value="3">3 childs</option>
                                                            <option value="4">4 childs</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-half bzg_c child-age-container">
                                                <div class="">Umur Anak</div>
                                                <div class="bzg bzg--small-gutter child-age__inputs">

                                                </div>
                                                <hr class="no-space">
                                            </div>
                                        </div>
                                    </template>
                                    <!-- roomContent template -->
                                    <script>
                                        function childAgeInput(index, age, val = 0) {
                                            return '<div class="mb-small bzg_c" data-col="s4">'+
                                            '<select name="child_age['+index+']['+age+']" id="ChildAge['+index+']['+age+']" class="form-input form-input--block" data-value="'+val+'">'+ages(18)+
                                            '</select>'+
                                            '</div>'
                                        }
                                        function ages(age) {
                                            var option;
                                            option += '<option value="0">&lt;1 th</option>'
                                            for(var i = 1; i < age; i++ ) {
                                                option += '<option value="'+i+'">'+i+' th</option>'
                                            }
                                            return option
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-small bzg_c guest-container" data-col="l5">
                        <div class="bzg guest-list">
                            <span class="bzg_c hidden">Kamar <span class="room-index">1</span></span>
                            <div class="mb-small bzg_c" data-col="m6">
                                <div class="flex a-center">
                                    <label for="" class="nowrap in-block mr-small w-30">
                                        <span class="fa fa-male"></span>
                                        <span class="fa fa-female"></span>
                                    </label>
                                    <div class="fg-1">
                                        <select name="adult_guest[0]" id="adult_guest_0" class="form-input form-input--block selectstyle">
                                            <option value="1">1 adult</option>
                                            <option value="2">2 adult</option>
                                            <option value="3">3 adult</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-small bzg_c" data-col="m6">
                                <div class="flex a-center">
                                    <label for="" class="nowrap in-block mr-small w-30 text-center">
                                        <span class="fa fa-child"></span>
                                    </label>
                                    <div class="fg-1">
                                        <select name="child_guest[0]" id="child_guest_0" data-index="0" class="form-input form-input--child form-input--block selectstyle child-age-select">
                                            <option value="0">0 child</option>
                                            <option value="1">1 child</option>
                                            <option value="2">2 childs</option>
                                            <option value="3">3 childs</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-half bzg_c child-age-container">
                                <div class="">Umur Anak</div>
                                <div class="bzg bzg--small-gutter child-age__inputs">

                                </div>
                                <hr class="no-space">
                            </div>
                        </div>
                    </div>
                    <div class="bzg_c text-center" data-col="m6, l3" data-offset="m6, l9">
                        <button class="btn btn--block btn--round btn--pink btn-research" type="button">
                            Ganti Pencarian
                        </button>
                    </div>
                    <!-- End template add hotel guest -->
                </div>
            </form>
        </div>

        <div class="items table-items">
            <header class="bzg bzg--no-gutter head-item text--larger">
                <div class="bzg_c" data-col="m6, l3">
                    <div class="p-small">
                        Rooms
                    </div>
                </div>
                <div class="bzg_c" data-col="m6, l3">
                    <div class="p-small">
                        Deal Includes
                    </div>
                </div>
                <div class="bzg_c" data-col="m6, l3">
                    <div class="p-small">
                        Price/Night
                    </div>
                </div>
                <div class="bzg_c" data-col="m6, l3">
                    <div class="p-small">
                        Total Price
                    </div>
                </div>
                <!-- <div class="bzg_c" data-col="m4, l2">
                    <div class="p-small">
                        Select Room
                    </div>
                </div> -->
            </header>
            <div class="mb-2 body-item list-room">

            </div>
            <div class="empty-state p-8 text-center fill-red mb-2">
                <span class="fa fa-exclamation-triangle"></span>
                <strong>Pencarian tidak ditemukan, silahkan datang besok lagi!</strong>
            </div>
            <template id="loader">
                <div class="loader">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </template>
            <template id="listRoom">
                {{#each RoomClasses.RoomClass}}

                {{#each Rooms.Room}}

                <hr class="no-space {{isExpired Penalty_Date 1}}">
                <div class="bzg bzg--no-gutter {{isExpired Penalty_Date 1}}" data-toggle>
                    <div class="bzg_c" data-col="m4, l3">
                        <div class="p-16">
                            {{RoomName}}<br>
                            <div class="mb-small width-100">
                                <img src="assets/img/icon-adults.png" alt="" width="32">
                                {{Adults}} Adults
                            </div>
                            {{#if Childs}}
                            <div class="mb-small width-100">
                                <img src="assets/img/icon-child.png" alt="" width="32">
                                {{Childs}} Childs
                            </div>
                            {{/if}}
                        </div>
                    </div>
                    <div class="bzg_c" data-col="m8, l3">
                        <div class="p-small">
                            <div class="flex flex--fixed text-center">
                                <div class="p-small line--1">
                                    <img src="assets/img/icon-flexible-cancelation.png" width="50" alt="">
                                    <small class="is-block">
                                        Flexible cancelation until {{formatDate Penalty_Date}}
                                    </small>
                                </div>
                                <div class="p-small line--1 status-icon-{{BBCode}}">
                                    <img class="icon-type-{{BBCode}}" src="assets/img/icon-breakfast.png" width="50" alt="">
                                    <small class="is-block">
                                        <span class="include">Include</span>
                                        <span class="exclude">Without</span>
                                        Breakfast
                                    </small>
                                </div>
                                <!-- <div class="p-small line--1">
                                    <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                    <small class="is-block">
                                        Internet available
                                    </small>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="bzg_c" data-col="x6, m6, l3">
                        <div class="p-16">
                            <strong class="text-larger">IDR {{toLocaleStr PricePerNight}}/night</strong>
                        </div>
                    </div>
                    <div class="bzg_c" data-col="x6, m6, l3">
                        <div class="p-16">
                            <strong class="text-larger">IDR {{toLocaleStr TotalPrice}}/night</strong>
                        </div>
                    </div>
                </div>
                {{/each}}
                <hr class="mb-half">
                <div class="mb bzg bzg--no-gutter">
                    <div class="bzg_c" data-col="m4, l3" data-offset="m8, l9">
                        <button class="btn btn--block btn--pink btn--round" type="button">book</button>
                    </div>
                </div>
                {{/each}}
            </template>
            <!-- <div class="mb-2 body-item"> -->
                <!-- <div class="top-item">
                    <h3 class="m-0 p-small text-center item-head-title">
                        <img src="http://via.placeholder.com/64.png/6eb5f9/ffffff?text=%" width="32" alt="">
                        Top Deal!
                    </h3>
                    <div class="bzg bzg--no-gutter" data-toggle>
                        <div class="bzg_c" data-col="m4, l3">
                            <div class="p-16">
                                Twin Room<br>
                                <div class="width-100">
                                    <img src="http://via.placeholder.com/64.png?text=64" alt="" width="32">
                                    2 Adult
                                </div>
                                <nav class="item-nav" >
                                    <button class="btn--plain btn-toggle" type="button">
                                        Room Facilities
                                        <span class="fa fa-angle-down"></span>
                                    </button>
                                </nav>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m8, l3">
                            <div class="p-small">
                                <div class="flex flex--fixed text-center">
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Flexible cancelation until 23 Apr
                                        </small>
                                    </div>
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Without Breakfast
                                        </small>
                                    </div>
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Internet available
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="x6, m4, l2">
                            <div class="p-16">
                                <strong class="text-larger">IDR 2.500.000/night</strong>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="x6, m4, l2">
                            <div class="p-16">
                                <strong class="text-larger">IDR 5.000.000/night</strong>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m4, l2">
                            <div class="p-16">
                                <a href="#" class="btn btn--block btn--round btn--red">Select Room</a>
                            </div>
                        </div>
                        <div class="bzg_c panel-toggle">
                            <div class="p-16">
                                <ul class="block list-check bzg">
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Car Parking
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Bar/Lounge
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        24 Hour front desk
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Loundry room
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Meeting facilities
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Wi-Fi internet in public area
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Car Parking
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Bar/Lounge
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        24 Hour front desk
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Loundry room
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Meeting facilities
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Wi-Fi internet in public area
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- top-item -->
                <!-- <div class="best-item">
                    <h3 class="m-0 p-small text-center item-head-title">
                        <img src="http://via.placeholder.com/64.png?text=bed" width="32" alt="">
                        Best Offer Room Only
                    </h3>
                    <div class="bzg bzg--no-gutter" data-toggle>
                        <div class="bzg_c" data-col="m4, l3">
                            <div class="p-16">
                                Twin Room<br>
                                <div class="width-100">
                                    <img src="http://via.placeholder.com/64.png?text=64" alt="" width="32">
                                    2 Adult
                                </div>
                                <nav class="item-nav">
                                    <button class="btn--plain btn-toggle" type="button">
                                        Room Facilities
                                        <span class="fa fa-angle-down"></span>
                                    </button>
                                </nav>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m8, l3">
                            <div class="p-small">
                                <div class="flex flex--fixed text-center">
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Flexible cancelation until 23 Apr
                                        </small>
                                    </div>
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Without Breakfast
                                        </small>
                                    </div>
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Internet available
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="x6, m4, l2">
                            <div class="p-16">
                                <strong class="text-larger">IDR 2.500.000/night</strong>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="x6, m4, l2">
                            <div class="p-16">
                                <strong class="text-larger">IDR 5.000.000/night</strong>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m4, l2">
                            <div class="p-16">
                                <a href="#" class="btn btn--block btn--round btn--red">Select Room</a>
                            </div>
                        </div>
                        <div class="bzg_c panel-toggle">
                            <div class="p-16">
                                <ul class="block list-check bzg">
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Car Parking
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Bar/Lounge
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        24 Hour front desk
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Loundry room
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Meeting facilities
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Wi-Fi internet in public area
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Car Parking
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Bar/Lounge
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        24 Hour front desk
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Loundry room
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Meeting facilities
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Wi-Fi internet in public area
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- best-item -->
                <!-- <div class="best-item">
                    <h3 class="m-0 p-small text-center item-head-title">
                        <img src="http://via.placeholder.com/64.png?text=bed" width="32" alt="">
                        Best Offer with Board
                    </h3>
                    <div class="bzg bzg--no-gutter" data-toggle>
                        <div class="bzg_c" data-col="m4, l3">
                            <div class="p-16">
                                Twin Room<br>
                                <div class="width-100">
                                    <img src="http://via.placeholder.com/64.png?text=64" alt="" width="32">
                                    2 Adult
                                </div>
                                <nav class="item-nav">
                                    <button class="btn--plain btn-toggle" type="button">
                                        Room Facilities
                                        <span class="fa fa-angle-down"></span>
                                    </button>
                                </nav>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m8, l3">
                            <div class="p-small">
                                <div class="flex flex--fixed text-center">
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Flexible cancelation until 23 Apr
                                        </small>
                                    </div>
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Without Breakfast
                                        </small>
                                    </div>
                                    <div class="p-small line--1">
                                        <img src="http://via.placeholder.com/100.png?text=100" width="50" alt="">
                                        <small class="is-block">
                                            Internet available
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="x6, m4, l2">
                            <div class="p-16">
                                <strong class="text-larger">IDR 2.500.000/night</strong>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="x6, m4, l2">
                            <div class="p-16">
                                <strong class="text-larger">IDR 5.000.000/night</strong>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m4, l2">
                            <div class="p-16">
                                <a href="#" class="btn btn--block btn--round btn--red">Select Room</a>
                            </div>
                        </div>
                        <div class="bzg_c panel-toggle">
                            <div class="p-16">
                                <ul class="block list-check bzg">
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Car Parking
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Bar/Lounge
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        24 Hour front desk
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Loundry room
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Meeting facilities
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Wi-Fi internet in public area
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Car Parking
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Bar/Lounge
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        24 Hour front desk
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Loundry room
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Meeting facilities
                                    </li>
                                    <li class="list-check__item bzg_c" data-col="m6, l4">
                                        Wi-Fi internet in public area
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- best-item -->
                <!-- <div class="text-center">
                    <a href="#" class="btn btn--round btn--grey t-bold">
                        See more bedrooms and board types
                    </a>
                </div> -->
            <!-- </div> -->
        </div>

        <h3 class="text-center p-small fill-lightgrey text-up"><span class="text-blue">Hotel Information</span></h3>
        <div class="block toggle" data-toggle="">
            <div class="panel-toggle">
                <h3 class="mb-small text-up text-blue">Hotel Facilities</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil eos eveniet laboriosam molestiae at maxime labore nam itaque earum, fuga, asperiores sit voluptatibus, veniam error quis voluptatum illum cumque repellendus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel repellendus quisquam, natus, totam exercitationem officiis! Vel eum laborum, omnis minima saepe enim sequi unde, dignissimos delectus pariatur vitae reprehenderit recusandae!
                </p>
                <h3 class="mb-small text-up text-blue">Hotel Location</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos ad voluptate, hic velit eveniet, iusto rerum in eaque quae. Pariatur autem aut eius dolore velit consequuntur laudantium dolor reprehenderit recusandae! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo nesciunt atque, tempora id nam blanditiis repellendus dolorum magni accusamus vitae quas tempore error eveniet labore consectetur iusto nemo perspiciatis soluta.
                </p>
                <h3 class="mb-small text-up text-blue">How to Get to Hotel</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi ducimus dolores eveniet sapiente, voluptate esse quo tempora alias odio veniam, consequatur veritatis eos, ea dicta dolorum impedit aliquid blanditiis dignissimos? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam excepturi, eum nostrum aliquam blanditiis, similique ipsum aliquid voluptate maxime non earum, optio sed. Accusamus officia earum deleniti impedit, laudantium nihil.
                </p>
                <h3 class="mb-small text-up text-blue">Taxes and Fees Information</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit nisi ex fugiat earum est tempora repudiandae nesciunt inventore porro excepturi laborum, saepe, debitis fugit accusamus veniam. Beatae commodi, et consectetur! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, hic! Architecto beatae nobis sapiente officiis animi minima quas odio, deserunt, consequuntur inventore perferendis. Reprehenderit pariatur deleniti repudiandae eligendi minima ex!
                </p>
            </div>
            <div class="text-center">
                <button class="btn btn-toggle btn--plain" type="button">
                    <span class="btn-text">View Less</span>
                    <span class="btn-icon fa fa-angle-up"></span>
                </button>
            </div>
        </div>

        <h3 class="text-center p-small fill-lightgrey text-up"><span class="text-blue">Amenities</span></h3>
        <h3 class="mb-small text-up text-blue">Hotel Facilities</h3>
        <hr class="mb-half">
        <ul class="block list-check bzg">
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Car Parking
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Bar/Lounge
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                24 Hour front desk
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Loundry room
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Meeting facilities
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Wi-Fi internet in public area
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Car Parking
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Bar/Lounge
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                24 Hour front desk
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Loundry room
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Meeting facilities
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Wi-Fi internet in public area
            </li>
        </ul>
        <h3 class="mb-small text-up text-blue">Room Facilities</h3>
        <hr class="mb-half">
        <ul class="mb-2 list-check bzg">
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Car Parking
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Bar/Lounge
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                24 Hour front desk
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Loundry room
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Meeting facilities
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Wi-Fi internet in public area
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Car Parking
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Bar/Lounge
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                24 Hour front desk
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Loundry room
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Meeting facilities
            </li>
            <li class="list-check__item bzg_c" data-col="m6, l4">
                Wi-Fi internet in public area
            </li>
        </ul>
        <hr>
        <div class="block">
            <div class="text-center">
                <h3 class="no-space text-blue text-up">
                    Not Quite You were Looking for?
                </h3>
                <h3>We have more room deals for you in Jakarta</h3>
            </div>
            <div class="inset-on-m">
                <div class="bzg cards cards--blue">
                    <?php for ($i=1; $i <= 4; $i++) { ?>
                    <div class="block bzg_c" data-col="m6, l3">
                        <div class="card__item">
                            <a href="#" class="is-block">
                                <figure class="item-img img-square fill-lightgrey">
                                    <img src="assets/img/img-preload.png" data-src="//placehold.it/420x420" alt="" class="item-heavy">
                                </figure>
                            </a>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-1">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="rating rating--smaller rating--solid" data-rating="3.0" title="3.0">
                                        <span class="star-rating fa fa-star"></span>
                                        <span class="star-rating fa fa-star"></span>
                                        <span class="star-rating fa fa-star"></span>
                                        <span class="star-rating fa fa-star"></span>
                                        <span class="star-rating fa fa-star"></span>
                                    </div>
                                    <div class="cf">
                                        <strong class="text--larger t-white in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
