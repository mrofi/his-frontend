<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Japan Airlines (JAL)</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <h1 class="sr-only">Japan Airlines (JAL)</h1>
            <div class="block slider-hero">
                <div class="slider__item">
                    <img src="assets/img/slidebanner-1.jpg" alt="">
                </div>
            </div>

            <div class="block fill-lightgrey block--inset">
                <h3 class="title-group">
                    <span class="title-icon fa fa-1-5x fa-star i--blue"></span>
                    <span class="title-text text-up t--larger">Deskripsi</span>
                </h3>
                    Japan Airlines merupakan salah satu Airlines terkenal dari Negara Jepang, yang berdiri pada tahun 1951, dengan kode IATA JL, dan kode ICAO JAL. Penerbangan pertama dengan Rute dari kota Tokyo, Jepang menuju kota San Francisco, America Serikat telah dimulai pada tanggal 2 Februari 1954. Berbagai tujuan penerbangan Internasional terdapat di Airlines ini, diantaranya yaitu Hong kong, New york, Paris, London, dan lain sebagainya.
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <h3 class="title-group">
                <span class="title-icon fa-1-5x his-pesawat i--blue"></span>
                <span class="title-text text-up t--larger">Tipe Pesawat Japan Airlines</span>
            </h3>
            <div class="block bzg bzg--small-gutter">
                <?php for ($i=1; $i <= 5; $i++) { ?>
                <div class="block bzg_c" data-col="m4">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up block--small"><a href="#" class="link-black">Boeing 767-300ER</a></h3>
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <h3 class="title-group">
                <span class="title-icon fa fa-1-5x fa-map-marker i--blue"></span>
                <span class="title-text text-up t--larger">Destinasi Japan Airlines</span>
            </h3>
            <div class="block bzg col-list">
                <dl class="bzg_c col-list__item" data-col="m6">
                    <dt>Jepang</dt>
                    <dd>
                        <ul>
                            <li>Nagoya</li>
                            <li>Osaka</li>
                            <li>Tokyo</li>
                            <li>Matsuyama</li>
                            <li>Chitose</li>
                            <li>Fukuoka</li>
                            <li>Oita</li>
                        </ul>
                    </dd>
                </dl>
                <dl class="bzg_c col-list__item" data-col="m6">
                    <dt>Jepang</dt>
                    <dd>
                        <ul>
                            <li>Nagoya</li>
                        </ul>
                    </dd>
                </dl>
                <dl class="bzg_c col-list__item" data-col="m6">
                    <dt>Jepang</dt>
                    <dd>
                        <ul>
                            <li>Nagoya</li>
                            <li>Osaka</li>
                            <li>Tokyo</li>
                            <li>Matsuyama</li>
                            <li>Chitose</li>
                            <li>Fukuoka</li>
                            <li>Oita</li>
                            <li>Nagoya</li>
                            <li>Osaka</li>
                            <li>Tokyo</li>
                            <li>Matsuyama</li>
                            <li>Chitose</li>
                            <li>Fukuoka</li>
                            <li>Oita</li>
                            <li>Nagoya</li>
                            <li>Osaka</li>
                            <li>Tokyo</li>
                            <li>Matsuyama</li>
                        </ul>
                    </dd>
                </dl>

                <dl class="bzg_c col-list__item" data-col="m6">
                    <dt>Jepang</dt>
                    <dd>
                        <ul>
                            <li>Nagoya</li>
                            <li>Osaka</li>
                            <li>Tokyo</li>
                            <li>Matsuyama</li>
                        </ul>
                    </dd>
                </dl>

                <dl class="bzg_c col-list__item" data-col="m6">
                    <dt>Jepang</dt>
                    <dd>
                        <ul>
                            <li>Fukuoka</li>
                            <li>Oita</li>
                        </ul>
                    </dd>
                </dl>
                <dl class="bzg_c col-list__item" data-col="m6">
                    <dt>Jepang</dt>
                    <dd>
                        <ul>
                            <li>Nagoya</li>
                        </ul>
                    </dd>
                </dl>
                <dl class="bzg_c col-list__item" data-col="m6">
                    <dt>Jepang</dt>
                    <dd>
                        <ul>
                            <li>Nagoya</li>
                        </ul>
                    </dd>
                </dl>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <h3 class="title-group">
                <span class="title-icon fa-1-5x his-seat i--blue"></span>
                <span class="title-text text-up t--larger">Kelas Kabin Japan Airlines</span>
            </h3>
            <div class="block bzg bzg--small-gutter">
                <?php for ($i=1; $i <= 4; $i++) { ?>
                <div class="block bzg_c" data-col="m4, l3">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up block--small"><a href="#" class="link-black t--smaller">Premium Economy Class</a></h3>
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <h3 class="title-group">
                <span class="title-icon fa-1-5x his-user-camera i--blue"></span>
                <span class="title-text text-up t--larger">Hidangan dalam Japan Airlines</span>
            </h3>
            <div class="block bzg bzg--small-gutter">
                <?php for ($i=1; $i <= 6; $i++) { ?>
                <div class="block bzg_c" data-col="m4">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up block--small"><a href="#" class="link-black">Vegetarian Cuisine</a></h3>
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
            </div>
            <hr>
        </div>
    </section>

    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
