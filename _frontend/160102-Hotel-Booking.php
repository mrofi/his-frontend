<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pemesanan</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <ol class="block list-nostyle text-center text-up steps">
            <li class="steps__item current">
                <span>Rincian Anda</span>
            </li>
            <li class="steps__item">
                <span>Pembayaran</span>
            </li>
            <li class="steps__item">
                <span>Selesai</span>
            </li>
        </ol>

        <div class="block--double bzg">
            <div class="bzg_c" data-col="m8">
                <h1 class="block--small">Rincian Anda</h1>
                <p class="t-strong t--larger">Silakan <a href="#">log-in</a> untuk mempercepat pengisian atau lanjut sebagai tamu</p>
                <form action="" class="form baze-form"
                data-empty="Input tidak boleh kosong"
                data-email="Alamat email tidak benar"
                data-number="Input harus berupa angka"
                data-numbermin="Angka minimal "
                data-numbermax="Angka maksimal">
                    <fieldset class="block">
                        <h2 class="text-up">Data Pemesan</h2>
                        <div class="form__row">
                            <label class="space-right t-strong" for="">Titel *</label>
                            <label for="title_mr" class="btn-label">
                                <input type="radio" id="title_mr" name="gender" data-dest="title_mr1">
                                <span class="btn-label__text">Mr.</span>
                            </label>
                            <label for="title_mrs" class="btn-label">
                                <input type="radio" id="title_mrs" name="gender" checked data-dest="title_mrs1">
                                <span class="btn-label__text">Mrs.</span>
                            </label>
                            <label for="title_ms" class="btn-label">
                                <input type="radio" id="title_ms" name="gender" data-dest="title_ms1">
                                <span class="btn-label__text">Ms.</span>
                            </label>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="oriFirstName" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" id="oriFirstName" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="oriLastName" class="t-strong">Nama Belakang *</label><br>
                                <input type="text" class="form-input form-input--block" id="oriLastName" required>
                            </div>
                        </div>
                        <div class="block--small bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nomor Telepon *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="+61">+61</option>
                                            <option value="+62" selected>+62</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x8">
                                        <input type="number" class="form-input form-input--block" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Email</label><br>
                                <input type="email" class="form-input form-input--block" required>
                            </div>
                        </div>
                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <hr>

                    <fieldset class="block copy-val">
                        <h2 class="text-up">Tamu 1</h2>
                        <div class="block block--inset-small fill-yellow t-strong">
                            <span class="text-red">* Perhatian</span><br>
                            Pastikan data tamu sudah sesuai dengan identitas paspor yang masih berlaku
                        </div>
                        <div class="form__row">
                            <label for="same_as" class="btn-label btn-copy">
                                <input type="checkbox" id="same_as" data-msg="Pastikan <b>Data Pemesan</b> sudah terisi Cuk!">
                                <span class="btn-label__text">Sama dengan pemesan</span>
                            </label>
                        </div>
                        <div class="form__row bzg">
                            <label class="bzg_c t-strong pt-small" data-col="m4, l3" for="">Titel *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <label for="title_mr1" class="btn-label">
                                    <input type="radio" id="title_mr1" name="gender1" checked data-origin="title_mr">
                                    <span class="btn-label__text">Mr.</span>
                                </label>
                                <label for="title_mrs1" class="btn-label">
                                    <input type="radio" id="title_mrs1" name="gender1" data-origin="title_mrs">
                                    <span class="btn-label__text">Mrs.</span>
                                </label>
                                <label for="title_ms1" class="btn-label">
                                    <input type="radio" id="title_ms1" name="gender1" checked data-origin="title_ms">
                                    <span class="btn-label__text">Ms.</span>
                                </label>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="destFirstName" class="t-strong bzg_c pt-small" data-col="m4, l3">Nama Depan *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <input type="text" class="form-input form-input--block" id="destFirstName" data-origin="oriFirstName" required>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Nama Belakang *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <input type="text" class="form-input form-input--block" id="destLastName" data-origin="oriLastName" required>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Tanggal Lahir *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4, l3">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1">1</option>
                                            <option value="31" selected>31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4, l3">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="12" selected>12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4, l3">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000" selected>2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Jenis Kelamin *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <label for="male2" class="btn-label">
                                    <input type="radio" id="male2" name="gender12" checked>
                                    <span class="btn-label__text">Laki-laki</span>
                                </label>
                                <label for="female2" class="btn-label">
                                    <input type="radio" id="female2" name="gender12">
                                    <span class="btn-label__text">Perempuan</span>
                                </label>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Jenis Identitas *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <label for="paspor1" class="btn-label">
                                    <input type="radio" id="paspor1" name="identity1" checked>
                                    <span class="btn-label__text">Paspor</span>
                                </label>
                                <label for="ktp1" class="btn-label">
                                    <input type="radio" id="ktp1" name="identity1">
                                    <span class="btn-label__text">KTP</span>
                                </label>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Nomor Paspor *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Kebangsaan *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Nomor Telepon *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4, l3">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="+61">+61</option>
                                            <option value="+62" selected>+62</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x8, l9">
                                        <input type="number" class="form-input form-input--block" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form__row bzg">
                            <label for="" class="t-strong bzg_c pt-small" data-col="m4, l3">Email *</label>
                            <div class="bzg_c" data-col="m8, l9">
                                <input type="email" class="form-input form-input--block" required>
                            </div>
                        </div>

                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <hr>
                    <div class="block block--inset-small border">
                        <label for="agree_term" class="btn-label">
                            <input type="checkbox" id="agree_term" required class="submit-activator">
                            <span class="btn-label__text">
                                Saya menyetujui seluruh <a href="#">Syarat dan Ketentuan</a> dan <a href="#">Kebijakan Privasi</a> yang berlaku di
                                H.I.S. Travel Indonesia
                            </span>
                        </label>
                    </div>
                    <div class="text-right">
                        <button class="btn btn--round btn--red t-strong text-up btn-submit" type="submit" disabled="disabled">Lanjutkan</button>
                    </div>
                </form>
            </div>
            <!-- left -->
            <div class="bzg_c" data-col="m4" data-sticky-container>
                <div class="sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="70">
                    <h3 class="no-space text-center text-up block--inset-small">Rincian Pesanan</h3>
                    <div class="mb">
                        <span>Kode Pesanan</span><br>
                        <div class="text-blue">XXXXXX XXXX XXXX</div>
                    </div>
                    <table class="mb table--border fill-lightblue text-center">
                        <tbody class="text-blue">
                            <tr>
                                <td colspan="2" class="text-up p-small">Mandarin Oriental</td>
                            </tr>
                            <tr>
                                <td class="p-small">
                                    Kamar
                                    <span class="text--larger">1 Kamar</span>
                                </td>
                                <td class="p-small">
                                    Durasi
                                    <span class="text--larger">12 Malam</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right p-small">Harga Kamar</td>
                                <td class="text-left p-small">
                                    <span class="text--larger">IDR 29.060.000</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="mb fill-lightgrey">
                        <h3 class="no-space bg-grey p-smaller text-blue">
                            <span class="fa fa-bed"></span>
                            HOTEL
                        </h3>
                        <div class="p-smaller text-blue">
                            <div class="mb-half text-center">
                                <img src="https://via.placeholder.com/200x150" alt=""><br>
                                <span class="t-yellow t--larger">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </span>
                            </div>
                            <h4 class="no-space">Mandarin Oriental Hotel</h4>
                            <small>
                                Jl. Pejaten Barat II No. 3A Jakarta Selatan 12510 Indonesia
                            </small>
                        </div>
                        <hr class="no-space">
                        <div class="p-smaller text-blue">
                            <table class="table--plain cell--top no-space">
                                <tbody class="text-blue">
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Tipe Kamar</strong></td>
                                        <td>Deluxe Room, no balcony, Single Bed</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Kamar</strong></td>
                                        <td>1 Kamar</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Tamu</strong></td>
                                        <td>Dewasa 1</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Kamar</strong></td>
                                        <td>1 Kamar</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Tamu</strong></td>
                                        <td>Dewasa 1</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Kamar</strong></td>
                                        <td>1 Kamar</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Tamu</strong></td>
                                        <td>Dewasa 1</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="mb fill-lightgrey">
                        <h3 class="no-space bg-grey p-smaller text-blue">
                            <span class="fa fa-money"></span>
                            Perincian Harga
                        </h3>
                        <div class="p-smaller text-blue">
                            <table class="table--plain cell--top no-space">
                                <tbody class="text-blue">
                                    <tr>
                                        <td class="nowrap pr-small" width="100%"><strong>Harga Kamar</strong></td>
                                        <td class="nowrap text-right">
                                            <span class="flex">
                                                <span>IDR</span>
                                                <span class="fg-1 pl-small">
                                                    25.000.000
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small" width="100%"><strong>Harga Kamar</strong></td>
                                        <td class="nowrap text-right">
                                            <span class="flex">
                                                <span>IDR</span>
                                                <span class="fg-1 pl-small">
                                                    5.000.000
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small" width="100%"><strong>Harga Kamar</strong></td>
                                        <td class="nowrap text-right">
                                            <span class="flex">
                                                <span>IDR</span>
                                                <span class="fg-1 pl-small">
                                                    200.000
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="fill-lightblue">
                                        <td colspan="2">
                                            <span class="flex p-small">
                                                <span class="text-blue">TOTAL</span>
                                                <strong class="fg-1 text-red t--larger text-right">IDR 30.200.000</strong>
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
