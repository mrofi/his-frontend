<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="responsive-media media--8-1">
        <img src="" data-src="//placehold.it/1600x200" alt="" class="item-heavy">
        <div class="absolute flex j-center a-bottom">
            <div class="breadcrumb-campaign t-strong ">
                <li>1. PILIH DESIGN</li>
                <li>2. BUAT RENCANA PERJALANAN</li>
                <li>3. PREVIEW</li>
                <li>4. SELESAI & BAGIKAN</li>
            </div>
            </div>
    </div>
    <section class="section-block mt">
        <div class="container container--smaller">
            <div class="flex j-center block--half">
                <div class="fill-pink-light border--round-big space-h-big block--inset-v ">
                    <span class="text-center t-strong t--large ">JEPANG</span>
                </div>
            </div>
            <span class="flex j-center t-strong t--larger block">HARI KE 3</span>
            <div class="card--solid block--inset-v block border">
                <div class="space-h">
                    <div class="bzg width-100">
                        <div class="bzg_c block" data-col="m5">
                            <div class="card--solid block--inset">
                                <figure class="fill-white no-space">
                                    <img src="" data-src="//placehold.it/350x350" alt="" class="item-heavy">
                                </figure>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m7">
                            <h1 class="text-blue">DAY 3</h1>
                            <?php for ($i=1; $i <= 5; $i++) { ?>
                            <div class="bzg block--half">
                                <div class="bzg_c" data-col="m4">
                                    <input class="form-input form-input--block" type="text" name="text" placeholder="Text">
                                </div>
                                <div class="bzg_c" data-col="m7">
                                    <input class="form-input form-input--block" type="text" name="text" placeholder="Text">
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block--half flex v-center--spread">
                <button class="btn btn--round btn--ghost-red-black btn-shadow" type="button">
                    <b class="text-up t-strong">KEMBALI</b>
                </button>
                <button class="btn btn--round btn--red btn-shadow" type="button">
                    <b class="text-up t-strong">SELANJUTNYA</b>
                </button>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
