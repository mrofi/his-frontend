<?php include '_partials/head.php'; ?>
<table class="table-export">
    <thead class="theader">
        <tr>
            <th class="text-left pt pl-2">
                <img src="assets/img/invoice-title.png" alt="INVOICE" width="180" class="pull-left-2 mb-small"><br>
                Invoice No: <strong>123456782548</strong><br>
                Reservation No: <strong>H-546782</strong><br>
                Date: <strong>25 September 2018</strong>
            </th>
            <th class="text-right pt pr-2">
                <img src="assets/img/logo-his-blue.png" alt="HIS" width="132"><br>
                H.I.S. Travel Wonderland<br>
                <em>
                    Midplaza 2 Building 11 Floor | Jl. Jend. Sudirman Kav.10-11 Jakarta, Indonesia<br>
                    Monday - Saturday 9:00 - 18:00 | Closed Sunday And Public Holidays
                </em>
            </th>
        </tr>
        <tr>
            <th colspan="2" class="no-space th-line">
                <img src="assets/img/line-gradient.png" alt="">
            </th>
        </tr>
    </thead>

    <tbody class="tmain">
        <tr>
            <td colspan="2" class="pt-larger pl-2 pr-2">
                <div class="block">
                    <table class="block">
                        <tbody>
                            <tr>
                                <td>
                                    <h1 class="no-space text-blue" style="font-size: 18pt">INVOICE</h1>
                                </td>
                                <td class="text-right" style="font-size: 16pt">
                                    <em class="text-blue no-space">Status: PAID</em>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h1 class="text-blue" style="font-size: 16pt">
                        Detail Pelanggan <i class="t-normal"  style="font-size: 14pt">Customer Details</i>
                    </h1>
                    <table class="border">
                        <tbody>
                            <tr>
                                <td class="p-small pb" valign="top">
                                    Nama:<br>
                                    <i>Name</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        Joel Hutasoid
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    Kode Pos:<br>
                                    <i>Postal Code</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        12510
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="p-small pb" valign="top">
                                    Nama:<br>
                                    <i>Name</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        Joel Hutasoid
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    Kode Pos:<br>
                                    <i>Postal Code</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        12510
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="block">
                    <h1 class="text-blue" style="font-size: 16pt">
                        Detail Penumpang/Peserta Tour <i class="text--smaller t-normal" style="font-size: 14pt">Passenger Details</i>
                    </h1>
                    <table class="border">
                        <tbody>
                            <tr>
                                <td class="p-small pb" valign="top">
                                    Nama:<br>
                                    <i>Name</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        Joel Hutasoid
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    Tanggal Lahir:<br>
                                    <i>Birthdate</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        25-Jul-1988
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    No. Paspor:<br>
                                    <i>Passport</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        012345678909123
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    Berlaku Paspor:<br>
                                    <i>Valid until</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        25-Jul-2021
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="p-small pb" valign="top">
                                    Nama:<br>
                                    <i>Name</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        Joel Hutasoid
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    Tanggal Lahir:<br>
                                    <i>Birthdate</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        25-Jul-1988
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    No. Paspor:<br>
                                    <i>Passport</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        012345678909123
                                    </strong>
                                </td>
                                <td class="p-small pb" valign="top">
                                    Berlaku Paspor:<br>
                                    <i>Valid until</i>
                                </td>
                                <td class="p-small pb" valign="top">
                                    <strong>
                                        25-Jul-2021
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mb-2">
                    <h1 class="text-blue" style="font-size: 16pt">
                        Detail Pembayaran <i class="text--smaller t-normal" style="font-size: 14pt">Payment Details</i>
                    </h1>
                    <table class="block table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item</th>
                                <th>Others</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                                <th>Payment Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4"></td>
                                <td></td>
                                <td>TOTAL</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="block table">
                        <thead>
                            <tr class="text-center">
                                <th>Invoice Amount</th>
                                <th>Paid Amount</th>
                                <th>Balance Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td valign="top"></td>
                                <td valign="top"></td>
                                <td valign="top"></td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="border p-small">
                        <div>
                            <strong>Reservation Number: </strong>
                        </div>
                        <div>
                            <strong>Payment Deadline: </strong>
                        </div>
                        <div>
                            <strong>Payment Method: </strong>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
    <tfoot class="tfooter">
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </tfoot>
</table>
<footer class="doc-footer">
    <img class="doc-footer_img" src="assets/img/doc-footer.png" alt="">
</footer>

</body>
</html>