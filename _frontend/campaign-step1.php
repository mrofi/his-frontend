<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="responsive-media media--8-1">
        <img src="" data-src="//placehold.it/1600x200" alt="" class="item-heavy">
        <div class="absolute flex j-center a-bottom">
            <ol class="breadcrumb-campaign t-strong ">
                <li class="is-active"><span>PILIH DESIGN</span></li>
                <li><span>BUAT RENCANA PERJALANAN</span></li>
                <li><span>PREVIEW</span></li>
                <li><span>SELESAI & BAGIKAN</span></li>
            </ol>
        </div>
    </div>
    <section class="section-block mt">
        <div class="container container--smaller">
            <div class="block--half t-strong">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                </p>
            </div>
            <div class="section-block container--smallest">
                <div class="bzg bzg--v-center add-country " id="add-country">
                    <!-- <div class="bzg_c" data-col="m4"> -->
                    <label for="jepang" class="btn-country width-100 bzg_c" data-col="x6, m4">
                        <input class="btn-radio" type="radio" name="country" id="jepang"/>
                        <div class="block--double">
                            <div class="card--solid card-top-round country-content" style="background: #ffabcd;">
                                <h3 class="text-center space bzg--v-center ">JEPANG</h3>
                                <figure>
                                    <img src="" data-src="//placehold.it/450x450" alt="" class="item-heavy img-full"  >
                                </figure>
                            </div>
                        </div>
                    </label>
                    <!-- </div> -->
                    <!-- <div class="bzg_c" data-col="m4"> -->
                        <label for="korea" class="btn-country width-100 bzg_c" data-col="x6, m4 ">
                            <input class="btn-radio" type="radio" name="country" id="korea"/>
                            <div class="block--double">
                                <div class="card--solid card-top-round country-content" style="background: #21c0bb;">
                                    <h3 class="text-center space">KOREA</h3>
                                    <figure>
                                        <img src="" data-src="//placehold.it/450x450" alt="" class="item-heavy img-full">
                                    </figure>
                                </div>
                            </div>
                        </label>
                    <!-- </div> -->
                    <!-- <div class="bzg_c" data-col="m4"> -->
                        <label for="thailand" class="width-100 btn-country bzg_c" data-col="x6, m4">
                            <input type="radio" name="country" id="thailand" class="btn-radio"/>
                            <div class="block--double">
                                <div class="card--solid card-top-round country-content" 
                                style="background: #f8d47c;">
                                    <h3 class="text-center space">THAILAND</h3>
                                    <figure>
                                        <img src="" data-src="//placehold.it/450x450" alt="" class="item-heavy img-full">
                                    </figure>
                                </div>
                            </div>
                        </label>
                    <!-- </div> -->
                    <!-- <div class="bzg_c" data-col="m4" data-offset="m2"> -->
                        <label for="malaysia" class="width-100 btn-country bzg_c" data-col="x-6, m4">
                            <input type="radio" name="country" id="malaysia" class="btn-radio"/>
                            <div class="block--double ">
                                <div class="card--solid card-top-round country-content" 
                                style="background: #baa3db;">
                                    <h3 class="text-center space">MALAYSIA</h3>
                                    <figure>
                                        <img src="" data-src="//placehold.it/450x450" alt="" class="item-heavy img-full">
                                    </figure>
                                </div>
                            </div>
                        </label>
                    <!-- </div> -->
                    <!-- <div class="bzg_c" data-col="m4"> -->
                        <label for="singapura" class="width-100 btn-country bzg_c" data-col="x6, m4">
                        <input type="radio" name="country" id="singapura" class="btn-radio" />
                            <div class="block--double">
                                <div class="card--solid card-top-round country-content" 
                                style="background: #d9beab;">
                                    <h3 class="text-center space">SINGAPURA</h3>
                                    <figure>
                                        <img src="" data-src="//placehold.it/450x450" alt="" class="item-heavy img-full">
                                    </figure>
                                </div>
                            </div>
                        </label>
                    <!-- </div> -->
                </div>
                <div class="block--half flex flex-col-center">
                    <button class="btn btn--round btn--red space btn-shadow" type="button">
                        <b class="text-up t-strong">BUAT SEKARANG</b>
                    </button>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
