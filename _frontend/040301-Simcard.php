<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Aktivitas</a></li>
                <li><a href="#">Optional Tour</a></li>
                <li><a href="#">Jepang</a></li>
                <li><a href="#">GO! GO! SIMCARD</a></li>
            </ul>
        </div>
    </div>

    
    <div class="container container--smaller">
        <h1 class="block--small">GO! GO! SIMCARD</h1>
        <div class="bzg">
            <div class="bzg_c" data-col="m8">
                <section class="section-block--smaller">
                    <div class="responsive-media media--3-2">
                        <div class="demihero-slider slide-default">
                            <figure class="slide__item no-space">
                                <img data-lazy="//placehold.it/900x600" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-1.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-2.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- demihero-slider -->
                    </div>
                </section>
                <section class="section--block one-page-nav-container">
                    <nav class="inpage-nav block fill--overlap fill-lightgrey ">
                        <ul class="list-nostyle navs--inline text-up one-page-nav" id="jrPassNav">
                            <li class="inpage-nav__item nav__item current">
                                <a href="#tour_point">Tour Point</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_itinerary">Itinerary</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_description">Deskripsi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_location">Lokasi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_summary">Rincian Tour</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#" class="external">Unduh Flyer</a>
                            </li>
                        </ul>
                    </nav>
                    <article>
                        <div class="block">
                            <?php include '_partials/optional-tour/tour_point.php'; ?>
                            <?php include '_partials/optional-tour/tour_itinerary.php'; ?>
                            <?php include '_partials/optional-tour/tour_description.php'; ?>
                            <?php include '_partials/optional-tour/tour_location.php'; ?>
                            <?php include '_partials/optional-tour/tour_summary.php'; ?>
                        </div>
                    </article>
                </section>
            </div>
            <div class="bzg_c" data-col="m4" data-sticky-container>
                <?php include '_partials/simcard/booking-card.php'; ?>
            </div>
        </div>
    </div>
    <?php include '_partials/related-product.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
