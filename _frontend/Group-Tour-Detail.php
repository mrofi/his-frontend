<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Tour</a></li>
                <li><a href="#">Group Tour</a></li>
                <li><a href="#">Jepang</a></li>
                <li><a href="#">6D4N Mono Sapporo &amp; Tokyo</a></li>
            </ul>
        </div>
    </div>


    <div class="container container--smaller">
        <h1 class="block--small">6D4N Mono Sapporo &amp; Tokyo</h1>
        <div class="block bzg">
            <div class="bzg_c" data-col="m7, l8">
                <section class="section-block--smaller">
                    <div class="responsive-media media--3-2">

                    </div>
                </section>
                <section class="section--block one-page-nav-container">

                </section>
                <?php for ($i=1; $i <= 9; $i++) { ?>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates amet ea corporis tempora, neque odio. Obcaecati voluptas nobis temporibus sit dolores nesciunt, nulla necessitatibus rem itaque fugit omnis atque suscipit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, delectus voluptates repellat molestiae harum sint voluptatem eaque, inventore est. Aliquam voluptas quibusdam laudantium at accusantium vero omnis quis. Iure, nihil.
                </p>
            <?php } ?>
            </div>
            <div class="bzg_c" data-col="m5, l4" data-sticky-container>
                <div class="cards cards--default" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="120">
                    <div class="card__item">
                        <div class="card-head cf block--inset fill-yellow">
                            <strong class="in-block">mulai dari</strong>
                            <strong class="t--large pull-right">
                                IDR 26.504.500
                            </strong>
                        </div>
                        <div class="block--inset card-content relative">
                            <form action="" class="form form--line book-form" data-get-items="">
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-pesawat"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            All Nipon Airlines<br>
                                            <img src="//placehold.it/60x25" alt="">
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-alarm"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            3 Hari 2 Malam
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-travel-bag"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            Optional Tour
                                        </strong>
                                    </div>
                                </div>
                                <!-- <div class="form__row">
                                    <div class="input-iconic-left block-half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="his-alarm"></span>
                                        </label>
                                        <select name="" id="" class="form-input form-input-block selectstyle"
                                        data-tmplist="#qtyUpdater" data-tmplsum="#sumUpdater"  data-path="type/" data-items="1" data-name="type">
                                            <option value="1" selected>Tipe 1</option>
                                            <option value="2">Tipe 2</option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="his-alarm"></span>
                                        </label>
                                        <select name="" id="" class="form-input form-input--block selectstyle"
                                        data-tmplist="#qtyUpdater" data-tmplsum="#sumUpdater" data-path="dev/" data-items="1" data-name="duration" autocomplete="off">
                                            <option value="0">Durasi 0</option>
                                            <option value="1">Durasi 1</option>
                                            <option value="2">Durasi 2</option>
                                            <option value="3">Durasi 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label>
                                        <input type="text" class="form-input form-input--block pickaDay"
                                        data-items="" data-path="dev/" data-tmplist="#qtyUpdater" data-tmplsum="#sumUpdater" autocomplete="off" 
                                        id="departure_start" placeholder="Tanggal Berangkat" data-name="date">
                                    </div>
                                </div>
                                <div class="">
                                    <div class="input-iconic--left block--half">
                                        <label for="" class="label-icon">
                                            <span class="his-user-group"></span>
                                        </label>
                                        <div class="pick-guest fg-1" id="pickJRpassPsg" data-toggle
                                        data-min="1" data-totalvat="#totalVat_753" data-totalsurcharge="#totalSur_753"
                                        data-inclusions="0" data-total="#product_753" data-totaldiscount="#totalDiscount_753"
                                        data-sum="#bookSummary">
                                            <button class="form-input form-input--block btn-toggle" type="button" aria-label="Pick Guest">
                                                <strong class="text-ellipsis">
                                                    Jumlah tamu: <span class="total-input--person" id="total_psgjr">0</span>
                                                    <span id="total_product" class="sr-only"></span>
                                                </strong>
                                            </button>
                                            <div class="toggle-panel t-black fill-white">
                                                <div class="block--inset-small item-container">
                                                    <div class="flex">
                                                        <span class="fa fa-warning mr-small pt-small"></span>
                                                        <span class="fg-1">
                                                            Please select date and duration first
                                                        </span>
                                                    </div>
                                                    <!-- <php include '_partials/products/product-book.php'; > -->

                                                </div>
                                            </div>
                                        </div>
                                        <!-- pick-guest -->
                                    </div>
                                </div>

                                <div id="bookSummary" class="book-sum">
                                    <table id="table-sum-container" class="table-total sum-container">
                                        <!-- <tbody class="">
                                            <tr style="display: table-row">
                                                <td>Dewasa- TWN/TRP <small>(<span class="item-total">1</span>)</small></td>
                                                <td class="text-right">Rp. <span id="dewasa_1">10,000,000</span></td>
                                            </tr>
                                            <tr>
                                                <td>Single- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_1">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- Twin Sharing <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_2">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_3">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_4">0</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Vat(1%) <small>(<span class="total-vat">1</span>)</small></td>
                                                <td class="text-right">Rp. <span id="totalVat_753" class="n-total">1,000,000</span></td>
                                            </tr>
                                            <tr>
                                                <td>Surcharge <small>(<span class="total-surcharge">1</span>)</small></td>
                                                <td class="text-right">Rp. <span id="totalSur_753" class="n-total">10,000</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="inclusion_1">
                                                        <input type="checkbox" class="check-inclusion" id="inclusion_1"
                                                        data-include="10" data-includetarget="#total_includes_1">
                                                        Include Something <small>(<span class="total-includes"></span>)</small>
                                                    </label>
                                                </td>
                                                <td class="text-right">Rp. <span id="total_includes_1" class="n-total">0</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><hr class="no-space"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <strong class="pull-left mr-small">TOTAL</strong>
                                                    <div class="text-right text-red t-larger t-strong">
                                                        Rp. <span id="product_753" class="grand-total n-total">11,010,000</span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                </div>
                                <hr class="mb-half">
                                <div class="mb-half">
                                    <a href="#">Syarat dan Ketentuan</a>
                                </div>
                                <button class="btn btn--round btn--block btn--red btn-submit" type="submit">
                                    <b class="text-up">Pesan Sekarang</b>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Produk Terkait
                </h2>
            </div>
            <div class="block content-section">
                <div class="bzg cards cards--blue slide-3">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square">
                                <!-- <img src="assets/img/img-preload.png" data-src="//placehold.it/400x400" alt="" class="item-heavy"> -->
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</main>

<template id="qtyUpdater">
    <fieldset class="over-y-auto">
    {{#each this}}
        <div class="block--half flex v-ct qty-input is-{{age}}">
            <label for="" class="fg-1 line--small">
                <strong>{{title}}</strong><br>
                IDR {{toLocaleStr price}}++
            </label>
            <div class="nospace">
                <button class="btn btn--plain btn--plain-red" type="button" disabled
                data-group="{{group}}"
                data-bed="{{#if extaraBed}}extrabed{{else}}nobed{{/if}}"
                data-type="{{type}}"
                data-action="minus"
                data-age="{{age}}">
                    <span class="fa fa-minus"></span>
                </button>

                {{#with input}}
                <input type="number" class="form-input form-input--redline text-center type-{{../age}}"
                value="{{val}}" min="{{min}}" max="{{max}}" 
                data-psg="{{#if count_id}}{{count_id}}{{else}}total_psgjr{{/if}}"
                data-group-name="{{../age}}{{../group}}"
                data-step="{{step}}"  data-step-item="{{step-item}}" data-price="{{../price}}"
                data-vat="{{vat}}" data-surcharge="{{surcharge}}" data-discount="{{discount}}"
                data-target="#product_{{../id}}" name="products[{{../id}}]">
                {{/with}}

                <button class="btn btn--plain btn--plain-red" type="button"
                data-group="{{group}}"
                data-bed="{{#if extaraBed}}extrabed{{else}}nobed{{/if}}"
                data-type="{{type}}"
                data-action="plus"
                data-age="{{age}}">
                    <span class="fa fa-plus"></span>
                </button>
            </div>
        </div>
        <hr class="block--half">
    {{/each}}
    </fieldset>
    <div class="text-right">
        <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
            Pilih
        </button>
    </div>
</template>

<script id="sumUpdater" type="text/html">
    <tbody>
    {{#each this}}
    <tr class="cell--top line--small" style="{{#if input.val}}display: table-row;{{/if}}">
        <td class="pb-smaller">
            {{title}} <small>(<span class="item-total">{{input.min}}</span>)</small>
        </td>
        <td class="pb-smaller text-right nowrap">Rp. <span id="product_{{id}}">{{toLocaleStr price}}</span></td>
    </tr>
    {{/each}}
    </tbody>
    <tfoot>
        <tr class="cell--top line--small" style="display: none;">
            <td class="pb-smaller">Vat(1%) <small>(<span class="total-vat">1</span>)</small></td>
            <td class="pb-smaller text-right">
                Rp. <span id="totalVat_753">
                    {{#each this}}
                        {{#if input.val}}
                            {{toLocaleStr input.vat}}
                        {{/if}}
                    {{/each}}
                </span>
            </td>
        </tr>
        <tr class="cell--top line--small" style="display: none;">
            <td class="pb-smaller">Surcharge <small>(<span class="total-surcharge">1</span>)</small></td>
            <td class="pb-smaller text-right">
                Rp. <span id="totalSur_753">
                    {{#each this}}
                        {{#if input.val}}
                            {{toLocaleStr input.surcharge}}
                        {{/if}}
                    {{/each}}
                </span>
            </td>
        </tr>
        <tr>
            <td class="pb-smaller">
                <label for="inclusion_1">
                    <input type="checkbox" class="check-inclusion" id="inclusion_1"
                    data-include="10" data-includetarget="#total_includes_1">
                    Include Something <small>(<span class="total-includes">0</span>)</small>
                </label>
            </td>
            <td class="pb-smaller text-right">Rp. <span id="total_includes_1">0</span></td>
        </tr>
        <tr>
            <td colspan="2"><hr class="no-space"></td>
        </tr>
        <tr style="display: none;">
            <td colspan="2">
                <strong class="pull-left mr-small">Diskon</strong>
                <div class="text-right t-strong">
                    (-) Rp. <span id="totalDiscount_753"></span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong class="pull-left mr-small">TOTAL</strong>
                <div class="text-right text-red t--larger t-strong">
                    Rp. <span id="product_753" class="grand-total n-total">0</span>
                </div>
            </td>
        </tr>
    </tfoot>
</script>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
