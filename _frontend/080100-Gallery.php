<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Gallery</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="">Universal Studios Japan</h1>
        <section class="section-block">
            <div class="block bzg">
                <div class="bzg_c" data-col="m6">
                    <!-- <div class="responsive-media"> -->
                        <div class="demihero-slider slide-default slide-push-arrow" data-slick='{"autoplay": false}'>
                            <div class="slide__item">
                                <div class="responsive-media">
                                    <iframe class="video-iframe" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" gesture="media" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="slide__item">
                                <div class="responsive-media">
                                    <iframe class="video-iframe" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" gesture="media" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
                <div class="bzg_c" data-col="m6">
                    <div class="slide-2 slide-off-arrow bzg bzg--small-gutter">
                        <div class="slide__item bzg_c" data-col="m6">
                            <figure class="responsive-media square fig-tag">
                                <img data-lazy="//placehold.it/480x480" alt="">
                                <figcaption>HIS Travel Indonesia</figcaption>
                            </figure>
                        </div>
                        <div class="slide__item bzg_c" data-col="m6">
                            <figure class="responsive-media square fig-tag">
                                <img data-lazy="//placehold.it/480x480" alt="">
                                <figcaption>HIS Travel Indonesia</figcaption>
                            </figure>
                        </div>
                        <div class="slide__item bzg_c" data-col="m6">
                            <figure class="responsive-media square fig-tag">
                                <img data-lazy="//placehold.it/480x480" alt="">
                                <figcaption>HIS Travel Indonesia</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <p>
                <strong>10 September 2017</strong>, Tak hanya maju di bidang  teknologi dan ekonomi, Jepang  didukung oleh kondisi alamnya  yang indah serta objek wisata  budaya yang selalu dilestarikan. Tak hanya maju di bidang  teknologi dan ekonomi, Jepang  didukung oleh kondisi alamnya  yang indah serta objek wisata  budaya yang selalu dilestarikan.Tak hanya maju di bidang  teknologi dan ekonomi, Jepang  didukung oleh kondisi alamnya  yang indah serta objek wisata  budaya yang selalu dilestarikan
            </p>
        </section>
        <hr>
        
        <div class="" data-list="dev/gallery-1.json" data-category="Gallery" data-next="" data-tmpl="#tmpList">
            
        </div>
        <div class="loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <div class="block all-data-loaded is-ended text-center" style="display: none;">
            All Data Loaded
        </div>
    </div>
</main>
<template id="tmpList">
    {{#each this}}
    {{#each data}}
    <section class="section-block">
        <h2 class="text-up">
            {{title}}
        </h2>
        <div class="slideGallery slide-off-arrow bzg bzg--small-gutter">
            {{#each gallery}}
            <div class="slide__item bzg_c" data-col="m3">
                <figure class="responsive-media square fig-tag">
                    <img data-lazy="{{images}}" alt="">
                    <figcaption>{{caption}}</figcaption>
                </figure>
            </div>
            {{/each}}
        </div>
    </section>
    <hr>
    {{/each}}
    {{/each}}
</template>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
