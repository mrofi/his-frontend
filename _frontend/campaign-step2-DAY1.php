<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="responsive-media media--8-1">
        <img src="" data-src="//placehold.it/1400x200" alt="" class="lazyload">
        <div class="absolute flex j-center a-bottom">
            <ol class="breadcrumb-campaign t-strong ">
                <li class="is-active"><span>PILIH DESIGN</span></li>
                <li class="is-active"><span>BUAT RENCANA PERJALANAN</span></li>
                <li><span>PREVIEW</span></li>
                <li><span>SELESAI & BAGIKAN</span></li>
            </ol>
        </div>
    </div>
    <section class="section-block mt">
        <div class="container container--smaller">
            <form action="" class="baze-form">
                <div class="flex j-center block--half">
                    <div class="fill-pink-light border--round-big space-h-big block--inset-v ">
                        <span class="text-center t-strong t--large ">JEPANG</span>
                    </div>
                </div>
                <span class="flex j-center t-strong t--larger block">HARI KE 1</span>
                <div class="card--solid block--inset-v block border">
                    <div class="space-h">
                        <div class="bzg">
                            <div class="bzg_c block" data-col="m6, l5">
                                <div class="card--solid block--inset">
                                    <div class="upload-cropp-wrapper">
                                        <div class="upload-cropp" 
                                        id="imgDayInputPreview" 
                                        data-store="#imgStore"
                                        data-maxsize="2"
                                        data-img="assets/img/upload-img.png"></div>
                                        <label for="imgDayInput" class="browse-trigger">
                                            <span class="btn-icon fa fa-pencil"></span>
                                        </label>
                                        <input type="file" class="sr-only input-browse" id="imgDayInput"
                                        accept="image/png, image/jpeg, image/jpg"
                                        data-preview="#imgDayInputPreview">
                                    </div>
                                </div>
                            </div>
                            <div class="bzg_c" data-col="l7">
                                <h1 class="text-blue">DAY 1</h1>
                                <input type="hidden" id="imgStore" name="image" >
                                <?php for ($i=1; $i <= 5; $i++) { ?>
                                <div class="bzg block--half">
                                    <div class="bzg_c" data-col="m4">
                                        <input class="form-input form-input--block text-center" type="time" name="time<?= $i ?>" 
                                        placeholder="--.--" required>
                                    </div>
                                    <div class="bzg_c" data-col="m8, l7">
                                        <input class="form-input form-input--block" type="text" name="schedule<?= $i ?>" placeholder="Text"
                                        minlength="50" required>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block--half flex v-center--spread">
                    <button class="btn btn--round btn--ghost-red-black btn-shadow" type="button">
                        <b class="text-up t-strong">KEMBALI</b>
                    </button>
                    <button class="btn btn--round btn--red btn-shadow" type="submit">
                        <b class="text-up t-strong">SELANJUTNYA</b>
                    </button>
                </div>
            </form>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/underFooter.php'; ?>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/headroom.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/baze.validate.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/object-fit-images.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/moment.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.jquery.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/lazysizes.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/croppie.min.js"></script>

<script>
    var cachePath = './';
</script>
<?php include '_partials/mainScripts.php'; ?>
