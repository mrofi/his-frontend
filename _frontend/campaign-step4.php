<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="responsive-media media--8-1">
        <img src="" data-src="//placehold.it/1600x200" alt="" class="item-heavy">
        <div class="absolute flex j-center a-bottom">
            <ol class="breadcrumb-campaign t-strong ">
                <li class="is-active"><span>PILIH DESIGN</span></li>
                <li class="is-active"><span>BUAT RENCANA PERJALANAN</span></li>
                <li class="is-active"><span>PREVIEW</span></li>
                <li class="is-active"><span>SELESAI & BAGIKAN</span></li>
            </ol>
        </div>
    </div>
    <section class="section-block mt">
        <div class="container container--smaller">
            <div class="block--half t-strong text-center">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into 
                    electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                     and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
            <div class="container--smallest">
                <div class="bzg block">
                    <div class="bzg_c" data-col="m8" data-offset="m2">
                        <figure class="square responsive-media media--square">
                            <img src="" data-src="//placehold.it/500x500" alt="" class="item-heavy">
                            <div class="flex j-end text--huge a-bottom">
                                <button class="text-red btn--plain">
                                    <span class="fa fa-download"></span>
                                </button>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="bzg">
                    <div class="bzg_c" data-col="m8" data-offset="m2">
                        <div class="block--half flex v-center--spread">
                            <button class="btn btn--round btn--ghost-red-black btn-shadow" type="button">
                                <b class="text-up t-strong">BUAT SEKARANG</b>
                            </button>
                            <button class="btn btn--round btn--red btn-shadow" type="button">
                                <b class="text-up t-strong">LIHAT SEMUA</b>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
