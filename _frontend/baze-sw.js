const cacheName         = 'HIS-cache-v1';
let paths
let pageLocation        = location;
let origin              = pageLocation.origin;
let pathname            = pageLocation.pathname.split('/')[1];
let stagingFrontend     = 'https://frontend.suitdev.com';
let local               = 'http://localhost';

if(origin === stagingFrontend) {
    paths = origin + '/' + pathname
} else if (origin === local) {
    paths = 'http://localhost/2019/his-travel/_frontend'
} else {
    paths = '.'
}

console.log(paths)

const offlinePageUrl    = `${paths}/offline.html`
const precacheResources = [
    offlinePageUrl,
    `${paths}/assets/css/offline.css`,
    `${paths}/assets/img/site-logo--new.png`,
    `${paths}/assets/img/icon-mail-white.png`,
    `${paths}/assets/img/icon-facebook-white.png`,
    `${paths}/assets/img/icon-twitter-white.png`,
    `${paths}/assets/img/icon-instagram-white.png`,
    `${paths}/assets/img/icon-youtube-white.png`
];

self.addEventListener('install', event => {
    console.log('Service worker install event!');
    event.waitUntil(
        caches.open(cacheName)
        .then(cache => {
            return cache.addAll(precacheResources);
        })
    );
});

self.addEventListener('activate', event => {
    console.log('Service worker activate event!');
});

self.addEventListener('fetch', event => {
    let isNavigate  = event.request.mode === 'navigate'
    let isGET       = event.request.method === 'GET'
    let isManual    = event.request.redirect === 'manual'
    // console.log('url : ', event.request)
    if(isManual) return;

    if (isNavigate || (isGET && event.request.headers.get('accept').includes('text/html'))) {
        event.respondWith (
            fetch(event.request.url).catch(error => {
                // Return the offline page
                return caches.match(offlinePageUrl);
            })
        );
    } else {
        // Respond with everything else if we can
        event.respondWith (
            caches.match(event.request)
            .then(cachedResponse => {
                if (cachedResponse) {
                    return cachedResponse;
                }
                return fetch(event.request);
            })
        );
    }
});


