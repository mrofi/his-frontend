<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Press Release</a></li>
                <li><a href="#">Cool Japan Travel Fair Kembali Hadir di Jakarta</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <div class="table-container">
        <table class="table-export">
            <thead class="theader">
                <tr>
                    <th class="text-left pt pl-2">
                        <img src="assets/img/receipt-title.png" alt="RECEIPT" width="180" class="pull-left-2 mb-small">
                    </th>
                    <th class="text-right pt pr-2">
                        <img src="assets/img/logo-his-blue.png" alt="HIS" width="132"><br>
                        H.I.S. Travel Wonderland<br>
                        <em>
                            Midplaza 2 Building 11 Floor | Jl. Jend. Sudirman Kav.10-11 Jakarta, Indonesia<br>
                            Monday - Saturday 9:00 - 18:00 | Closed Sunday And Public Holidays
                        </em>
                    </th>
                </tr>
                <tr>
                    <th colspan="2" class="no-space th-line">
                        <img src="assets/img/line-gradient.png" alt="">
                    </th>
                </tr>
            </thead>

            <tbody class="tmain">
                <tr>
                    <td colspan="2" class="pt-larger pl-2 pr-2">
                        <div class="mb-2">
                            <table class="t-strong">
                                <tbody>
                                    <tr>
                                        <td class="p-small" valign="top">
                                            Invoice No:
                                        </td>
                                        <td class="p-small" valign="top">
                                            <strong>
                                                0
                                            </strong>
                                        </td>
                                        <td class="p-small" valign="top">
                                            Receipt No:
                                        </td>
                                        <td class="p-small" valign="top">
                                            <strong>
                                                0
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="p-small" valign="top">
                                            Reservation No:
                                        </td>
                                        <td class="p-small" valign="top">
                                            <strong>
                                                0
                                            </strong>
                                        </td>
                                        <td class="p-small" valign="top">
                                            Date of Payment
                                        </td>
                                        <td class="p-small" valign="top">
                                            <strong>
                                                0
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="p-small" valign="top">
                                            Date of issued:
                                        </td>
                                        <td class="p-small" valign="top">
                                            <strong>
                                                0
                                            </strong>
                                        </td>
                                        <td class="p-small" valign="top">
                                            Payment Method:
                                        </td>
                                        <td class="p-small" valign="top">
                                            <strong>
                                                0
                                            </strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="mb-2">
                            <h1 class="text-blue">
                                Detail Pelanggan <i class="text--smaller t-normal">Customer Details</i>
                            </h1>
                            <table class="border">
                                <tbody>
                                    <tr>
                                        <td class="p-small pb" valign="top">
                                            <strong>Nama:</strong><br>
                                            <i>Name</i>
                                        </td>
                                        <td class="p-small pb" valign="top">
                                            <strong>
                                                Joel Hutasoid
                                            </strong>
                                        </td>
                                        <td class="p-small pb" valign="top">
                                            <strong>Kode Pos:</strong><br>
                                            <i>Postal Code</i>
                                        </td>
                                        <td class="p-small pb" valign="top">
                                            <strong>
                                                12510
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="p-small pb" valign="top">
                                            <strong>Nama:</strong><br>
                                            <i>Name</i>
                                        </td>
                                        <td class="p-small pb" valign="top">
                                            <strong>
                                                Joel Hutasoid
                                            </strong>
                                        </td>
                                        <td class="p-small pb" valign="top">
                                            <strong>MyH.I.S. Point:</strong><br>
                                            <i>Postal Code</i>
                                        </td>
                                        <td class="p-small pb" valign="top">
                                            <strong>
                                                12510
                                            </strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="mb-2">
                            <div class="block border p-1">
                                <div></div>
                                <table class="no-space">
                                    <tbody>
                                        <tr>
                                            <td class="text--large">Amount</td>
                                            <td>
                                                <span class="text--large">IDR</span>
                                                <span class="text--huge">7,320,000</span><br>
                                                <em>Seven Million Three Hundred And Twenty Thousand IDR</em>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <table class="block table">
                                <thead>
                                    <tr class="text-center">
                                        <th>No.</th>
                                        <th>Item</th>
                                        <th>Amount</th>
                                        <th>Qty</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Fuel Surcharge</td>
                                        <td class="text-right">IDR 2,000,000</td>
                                        <td class="text-right">1</td>
                                        <td class="text-right">IDR 2,000,000</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Fuel Surcharge</td>
                                        <td class="text-right">IDR 2,000,000</td>
                                        <td class="text-right">1</td>
                                        <td class="text-right">IDR 2,000,000</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="text-right">TOTAL</td>
                                        <td class="text-right">IDR 7,320,000</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
            <tfoot class="tfooter">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
