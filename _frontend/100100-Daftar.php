<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Mice &amp; Corp</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="block--small">Daftar Akun Baru</h1>
        <div class="block block--inset border">
            <form action="404.php" method="post" class="form baze-form"
            data-empty="Input tidak boleh kosong"
            data-email="Alamat email tidak benar"
            data-number="Input harus berupa angka"
            data-numbermin="Angka minimal "
            data-numbermax="Angka maksimal">
                <div class="form__row bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="" class="form-label"><strong>Jenis Kelamin</strong></label>
                    </div>
                    <div class="bzg_c" data-col="m8, l9">
                        <label for="female" class="btn-label">
                            <input type="radio" id="female" name="gender">
                            <span class="btn-label__text">Perempuan</span>
                        </label>
                        <label for="male" class="btn-label">
                            <input type="radio" id="male" name="gender" required>
                            <span class="btn-label__text">Laki-laki</span>
                        </label>
                    </div>
                </div>
                <!-- row -->
                <div class="bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="" class="form-label"><strong>Nama *</strong></label>
                    </div>
                    <div class="bzg_c" data-col="m8, l9">
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <input type="text" data-pattern="AaZz" pattern="[A-Za-z]+" 
                                class="form-input form-input--block validStrong" placeholder="First Name" 
                                data-char='[1]' minlength="1" required data-invalid="Isi hanya dengan alphabet">
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <input type="text" data-pattern="AaZz" pattern="[A-Za-z]+" 
                                class="form-input form-input--block validStrong" placeholder="Last Name" 
                                data-char='[1]' minlength="1" required data-invalid="Isi hanya dengan alphabet">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form__row bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="" class="form-label"><strong>Email *</strong></label>
                    </div>
                    <div class="bzg_c" data-col="m8, l9">
                        <input type="email" class="form-input form-input--block" required>
                    </div>
                </div>
                <!-- row -->
                <div class="form__row bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="" class="form-label"><strong>Username *</strong></label>
                    </div>
                    <div class="bzg_c" data-col="m8, l9">
                        <input type="text" class="form-input form-input--block validStrong" data-char='[6, 10]' minlength="6" maxlength="10" required data-invalid="Username terdiri dari kombinasi huruf, angka, dan karakter spesial">
                    </div>
                </div>
                <!-- row -->
                <div class="form__row bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="" class="form-label"><strong>Tanggal Lahir</strong></label>
                    </div>
                    <div class="bzg_c" data-col="m8, l9">
                        <div class="bzg">
                            <div class="bzg_c" data-col="x3, m4">
                                <select name="" class="form-input form-input--block selectstyle">
                                    <option value="1">1</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                            <div class="bzg_c" data-col="x5, m4">
                                <select name="" class="form-input form-input--block selectstyle">
                                    <option value="Januari">Januari</option>
                                    <option value="September">September</option>
                                </select>
                            </div>
                            <div class="bzg_c" data-col="x4">
                                <select name="" class="form-input form-input--block selectstyle">
                                    <option value="1990">1990</option>
                                    <option value="1980">1980</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->
                <div class="form__row bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="" class="form-label"><strong>Jenis Kelamin</strong></label>
                    </div>
                    <div class="bzg_c" data-col="m8, l9">
                        <label for="email_subscribe" class="btn-label">
                            <input type="radio" id="email_subscribe" name="gender">
                            <span class="btn-label__text">Saya ingin menerima penawaran melalui email*</span>
                        </label>
                    </div>
                </div>
                <!-- row -->
                <div class="form__row bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="originPassword" class="form-label"><strong>Kata Sandi *</strong></label>
                    </div>
                    <div class="bzg_c input-peeker" data-col="m8, l9">
                        <input type="password" id="originPassword" minlength="6" maxlength="30" data-char='[6, 30]' data-match='["origin", "#retypePassword"]' class="form-input form-input--block input-peek validStrong" required>
                        <button class="btn-peek" type="button" aria-label="Show" data-input="#originPassword">
                            <span class="fa fa-eye"></span>
                        </button>
                    </div>
                </div>
                <!-- row -->
                <div class="form__row bzg">
                    <div class="bzg_c" data-col="m4, l3">
                        <label for="retypePassword" class="form-label"><strong>Ketik ulang kata sandi *</strong></label>
                    </div>
                    <div class="bzg_c input-peeker" data-col="m8, l9">
                        <input type="password" id="retypePassword" minlength="6" maxlength="30" data-char='[6, 30]' data-match='["retype", "#originPassword"]' class="form-input form-input--block input-peek validStrong" required data-invalid="Password anda tidak cocok">
                        <button class="btn-peek" type="button" aria-label="Show" data-input="#retypePassword">
                            <span class="fa fa-eye"></span>
                        </button>
                    </div>
                </div>
                <!-- row -->
                
                <div class="bzg">
                    <div class="bzg_c" data-col="m4, l3"></div>
                    <div class="bzg_c" data-col="m8, l9">
                        <div class="block--half">
                            <strong>*Wajib Diisi</strong>
                        </div>
                        <div class="block--half">
                            <button class="btn btn--round btn--red btn-submit" type="submit">
                                <strong class="text-up">Daftar</strong>
                            </button>
                            <span class="in-block space-h">
                                atau
                            </span>
                            <button class="btn btn--round btn--facebook">
                                <span class="fa fa-facebook"></span>
                                <strong>Daftar dengan Facebook</strong>
                            </button>
                            <button class="btn btn--round btn--google">
                                <span class="fa fa-google"></span>
                                <strong>Daftar dengan Google</strong>
                            </button>
                        </div>
                        *Dengan membuat akun, Anda telah membaca, memahami &amp; menyetujui <a href="#">Kebijakan Privasi</a> dan <a href="#">Syarat &amp; Ketentuan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
