<?php include '_partials/head.php'; ?>
<table class="table-export">
    <thead class="theader">
        <tr>
            <th class="text-left pt pl-2">
                <img src="assets/img/invoice-title.png" alt="INVOICE" width="180" class="pull-left-2 mb-small"><br>
                Invoice No: <strong>123456782548</strong><br>
                Reservation No: <strong>H-546782</strong><br>
                Date: <strong>25 September 2018</strong>
            </th>
            <th class="text-right pt pr-2">
                <img src="assets/img/logo-his-blue.png" alt="HIS" width="132"><br>
                H.I.S. Travel Wonderland<br>
                <em>
                    Midplaza 2 Building 11 Floor | Jl. Jend. Sudirman Kav.10-11 Jakarta, Indonesia<br>
                    Monday - Saturday 9:00 - 18:00 | Closed Sunday And Public Holidays
                </em>
            </th>
        </tr>
        <tr>
            <th colspan="2" class="no-space th-line">
                <img src="assets/img/line-gradient.png" alt="">
            </th>
        </tr>
    </thead>

    <tbody class="tmain">
        <tr>
            <td colspan="2" class="pt-larger pl-2 pr-2">
                <div class="block">
                    <h1 class="text-blue" style="font-size: 16pt">
                        Detail Penerbangan
                    </h1>
                    <table class="border">
                        <tbody>
                            <tr>
                                <td class="p-1 nowrap" valign="top">
                                    <img src="//via.placeholder.com/60x60?text=GA" alt=""><br>
                                    <small>Garuda Indonesia</small><br>
                                    GD - 007<br>
                                    Subclass A
                                </td>
                                <td class="p-1" valign="top" style="width: 100%;">
                                    Tuesday, 29 September 2019
                                    <ul class="list-flight">
                                        <li>
                                            <time>
                                                <span>05:36</span>
                                                <span class="dot"><span></span></span>
                                            </time>
                                            <div class="desc">
                                                Jakarta (CGK)<br>
                                                Soekarno International Airport - Terminal 2D
                                            </div>
                                        </li>
                                        <li>
                                            <time>
                                                <span>05:36</span>
                                                <span class="dot"><span></span></span>
                                            </time>
                                            <div class="desc">
                                                Jakarta (CGK)<br>
                                                Soekarno International Airport - Terminal 2D
                                            </div>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="mb-2 line--small">
                        <tbody>
                            <tr>
                                <td valign="top">NOTE: </td>
                                <td style="width: 100%" valign="top">
                                    Semua waktu yang tertera adalah waktu bandara setempat.<br>
                                    Mohon lakikan check-in minimal 90 menit (domestik) atau 3 jam (internasional) sebelum berangkat.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="block">
                    <h1 class="text-blue" style="font-size: 16pt">
                        Detail Penumpang <i class="text--smaller t-normal" style="font-size: 14pt">Passenger Details</i>
                    </h1>
                    <table class="table table--white-body">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tital</th>
                                <th width="50%">Nama Penumpang</th>
                                <th>Rute</th>
                                <th class="nowrap">Bagasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Mr.</td>
                                <td>Brian Huge Warner</td>
                                <td class="nowrap">CGK - KUL</td>
                                <td class="nowrap">20 Kg</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Mr.</td>
                                <td>Brian Huge Warner</td>
                                <td class="nowrap">CGK - KUL</td>
                                <td class="nowrap">20 Kg</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Mr.</td>
                                <td>Brian Huge Warner</td>
                                <td class="nowrap">CGK - KUL</td>
                                <td class="nowrap">20 Kg</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </tbody>
    <tfoot class="tfooter">
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </tfoot>
</table>
<footer class="doc-footer">
    <img class="doc-footer_img" src="assets/img/doc-footer.png" alt="">
</footer>

</body>
</html>