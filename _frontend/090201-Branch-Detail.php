<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Hubungi Kami</a></li>
                <li><a href="#">Wonderland Dharmawangsa</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="block--small">HIS Travel Wonderland Dharmawangsa</h1>
        <section class="section-block">
            <div class="slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Informasi Cabang
                </h2>
                <div class="block--half fill-blue-yellow block--inset-small text-center">
                    <h3 class="no-space">HIS Travel Wonderland Dharmawangsa</h3>
                </div>
                <ul class="list-nostyle line--small">
                    <li class="bzg bzg--small-gutter">
                        <div class="block--half bzg_c" data-col="m3">
                            <div class="fill-blue full-v block--inset-small t--larger text-up text-center">Alamat</div>
                        </div>
                        <div class="block--half bzg_c" data-col="m9">
                            <div class="border full-v block--inset-small">FX Mall F2 unit #15,16<br>Jl. Jenderal Sudirman, Pintu Satu Senayan, Jakarta 10270</div>
                        </div>
                    </li>
                    <li class="bzg bzg--small-gutter">
                        <div class="block--half bzg_c" data-col="m3">
                            <div class="fill-blue full-v block--inset-small t--larger text-up text-center">Jam Buka</div>
                        </div>
                        <div class="block--half bzg_c" data-col="m9">
                            <div class="border full-v block--inset-small">Senin - Minggu : 10:00 - 21:00</div>
                        </div>
                    </li>
                    <li class="bzg bzg--small-gutter">
                        <div class="block--half bzg_c" data-col="m3">
                            <div class="fill-blue full-v block--inset-small t--larger text-up text-center">No. Tel</div>
                        </div>
                        <div class="block--half bzg_c" data-col="m3">
                            <div class="border full-v block--inset-small">
                                <a href="tel:021-2911-0901" class="t--larger btn-call">
                                    <strong class="btn-text">021-2911-0901</strong>
                                    <span class="btn btn--round btn--smaller btn--red">Call</span>
                                </a>
                            </div>
                        </div>
                        <div class="block--half bzg_c m-center" data-col="m3">
                            <!-- <div class="fill-blue full-v block--inset-small t--larger text-up text-center">No. FAX</div> -->
                            <a class="btn btn--round btn--red t--larger" 
                            href="https://wa.me/{{whatsapp}}" target="_blank">
                                Chat Now
                            </a>
                        </div>
                        <div class="block--half bzg_c" data-col="m3">
                            <!-- <div class="border full-v block--inset-small">
                                <a href="tel:021-2911-0901" class="t--larger">
                                    <strong>021-2911-0901</strong>
                                </a>
                            </div> -->
                        </div>
                    </li>
                    <li class="bzg bzg--small-gutter">
                        <div class="block--half bzg_c" data-col="m3">
                            <div class="fill-blue full-v block--inset-small t--larger text-up text-center">Email</div>
                        </div>
                        <div class="block--half bzg_c" data-col="m9">
                            <div class="border full-v block--inset-small"><a href="mailto:info.fxsenayan@his-world.com">info.fxsenayan@his-world.com</a></div>
                        </div>
                    </li>
                    <li class="bzg bzg--small-gutter">
                        <div class="block--half bzg_c" data-col="m3">
                            <div class="fill-blue full-v block--inset-small t--larger text-up text-center">Kartu Kredit</div>
                        </div>
                        <div class="block--half bzg_c" data-col="m9">
                            <div class="border full-v block--inset-small">Visa · Master · JCB (3%)</div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Map
                </h2>
                <div class="block--inset">
                    <div class="responsive-media media--3-1">
                        <iframe width="600" height="450" frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJldcA63PxaS4RMxZJ6AZjMdg&key=AIzaSyDT39m_nMmH9JsWdNQxMbizNX91nKgyUZ8" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h2 class="text-up in-block">
                    Layanan Kami
                </h2>
                <div class="bzg available-list">
                    <div class="block--half bzg_c" data-col="m6, l4">
                        <div class="list__item">
                            <div class="item-img"></div>
                            <div class="item-text">Tiket Pesawat</div>
                        </div>
                    </div>
                    <div class="block--half bzg_c" data-col="m6, l4">
                        <div class="list__item unavailable">
                            <div class="item-img"></div>
                            <div class="item-text">Group Tour</div>
                        </div>
                    </div>
                    <div class="block--half bzg_c" data-col="m6, l4">
                        <div class="list__item">
                            <div class="item-img"></div>
                            <div class="item-text">Tiket Pesawat</div>
                        </div>
                    </div>
                    <div class="block--half bzg_c" data-col="m6, l4">
                        <div class="list__item unavailable">
                            <div class="item-img"></div>
                            <div class="item-text">Group Tour</div>
                        </div>
                    </div>
                    <div class="block--half bzg_c" data-col="m6, l4">
                        <div class="list__item">
                            <div class="item-img"></div>
                            <div class="item-text">Tiket Pesawat</div>
                        </div>
                    </div>
                    <div class="block--half bzg_c" data-col="m6, l4">
                        <div class="list__item unavailable">
                            <div class="item-img"></div>
                            <div class="item-text">Group Tour</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h2 class="text-up in-block">
                    Informasi Produk
                </h2>
            </div>
            <div class="block bzg">
                <div class="bzg_c" data-col="x2">
                    <figure class="responsive-media square circle big-frame">
                        <img src="//placehold.it/200x200" alt="">
                    </figure>
                </div>
                <div class="bzg_c" data-col="x10">
                    <h4 class="t--larger block--small">
                        Tour Package : Group Tour klik <a href="#">di sini</a> | Individual Tour klik <a href="#">di sini</a> | Optional Tour klik <a href="#">di sini</a>
                    </h4>
                    <p>
                        Internasional : Jepang, Bangkok, Singapore, Malaysia, Hongkong, Korea, Eropa, dll.<br>
                        Domestik : Jogjakarta, Bali, Lombok, Bangka, Belitung, dll. <br>
                        Kami dapat mengatur liburan pribadi atau perjalanan bisnis.
                    </p>
                </div>
            </div>
            <div class="block bzg">
                <div class="bzg_c" data-col="x2">
                    <figure class="responsive-media square circle big-frame">
                        <img src="//placehold.it/200x200" alt="">
                    </figure>
                </div>
                <div class="bzg_c" data-col="x10">
                    <h4 class="t--larger block--small">
                        Tour Package : Group Tour klik <a href="#">di sini</a> | Individual Tour klik <a href="#">di sini</a> | Optional Tour klik <a href="#">di sini</a>
                    </h4>
                    <p>
                        Internasional : Jepang, Bangkok, Singapore, Malaysia, Hongkong, Korea, Eropa, dll.<br>
                        Domestik : Jogjakarta, Bali, Lombok, Bangka, Belitung, dll. <br>
                        Kami dapat mengatur liburan pribadi atau perjalanan bisnis.
                    </p>
                </div>
            </div>
            <div class="block bzg">
                <div class="bzg_c" data-col="x2">
                    <figure class="responsive-media square circle big-frame">
                        <img src="//placehold.it/200x200" alt="">
                    </figure>
                </div>
                <div class="bzg_c" data-col="x10">
                    <h4 class="t--larger block--small">
                        Tour Package : Group Tour klik <a href="#">di sini</a> | Individual Tour klik <a href="#">di sini</a> | Optional Tour klik <a href="#">di sini</a>
                    </h4>
                    <p>
                        Internasional : Jepang, Bangkok, Singapore, Malaysia, Hongkong, Korea, Eropa, dll.<br>
                        Domestik : Jogjakarta, Bali, Lombok, Bangka, Belitung, dll. <br>
                        Kami dapat mengatur liburan pribadi atau perjalanan bisnis.
                    </p>
                </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h2 class="text-up in-block">
                    Hubungi Kami
                </h2>
            </div>
            <div class="bzg">
                <div class="bzg_c" data-col="l6">
                    <form action="" class="form baze-form"
                    data-empty="Input tidak boleh kosong"
                    data-email="Alamat email tidak benar"
                    data-number="Input harus berupa angka"
                    data-numbermin="Angka minimal "
                    data-numbermax="Angka maksimal"
                    data-password="Password anda kurang aman">
                        <div class="form__row">
                            <label for="" class="form-label"><strong>Nama *</strong></label>
                            <input type="text" class="form-input form-input--block" required>
                        </div>
                        <!-- row -->
                        <div class="form__row">
                            <label for="" class="form-label"><strong>Email *</strong></label>
                            <input type="email" class="form-input form-input--block" required>
                        </div>
                        <!-- row -->
                        <div class="form__row">
                            <label for="" class="form-label"><strong>Telepon *</strong></label>
                            <input type="number" class="form-input form-input--block" required>
                        </div>
                        <!-- row -->
                        <div class="form__row">
                            <div class="text-blue">Cabang yang dituju : H.I.S. FX Mall Sudirmn</div>
                            <label for="" class="form-label"><strong>Pesan Anda *</strong></label>
                            <textarea name="" id="" rows="10" class="form-input form-input--block" required></textarea>
                        </div>
                        <!-- row -->
                        <button class="btn btn--round btn--red" type="submit">
                            <strong class="text-up">Kirim</strong>
                        </button>
                    </form>
                </div>
            </div>
        </section>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
