<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Aktivitas</a></li>
                <li><a href="#">Optional Tour</a></li>
                <li><a href="#">Visa</a></li>
            </ul>
        </div>
    </div>

    <section class="section-slide">
        <div class="container container--smaller">
            <h1 class="sr-only">Flight Promo</h1>
            <div class="slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="fill-primary block--inset">
                <form action="" class="form">
                    <div class="bzg bzg--lite-gutter">
                        <div class="bzg_c" data-col="m4">
                            <div class="form__row">
                                <select name="" id="" class="form-input form-input--block selectstyle">
                                    <option value="">Asal</option>
                                    <option value="JKT">Jakarta</option><option value="SUB">Surabaya</option><option value="BDO">Bandung</option><option value="UPG">Makassar</option><option value="DPS">Denpasar Bali</option>
                                </select>
                            </div>
                            <div class="form__row">
                                <select name="" id="" class="form-input form-input--block selectstyle">
                                    <option value="">Tujuan</option>
                                    <option value="TYO">TOKYO</option><option value="NRT">Tokyo, TOKYO NARITA (NRT)</option><option value="HND">Tokyo, TOKYO HANEDA (HND)</option><option value="OSA">OSAKA</option><option value="KIX">Osaka, OSAKA KANSAI INTERNATIONAL (KIX)</option><option value="NGO">NAGOYA</option><option value="FUK">Fukuoka</option><option value="SPK">SAPPORO CHITOSE</option><option value="AKJ">ASAHIKAWA</option><option value="HKD">HAKODATE</option><option value="OBO">OBIHIRO</option><option value="SDJ">Sendai</option><option value="AOJ">AOMORI</option><option value="MSJ">MISAWA</option><option value="AXT">AKITA</option><option value="ONJ">ODATE NOSHIRO</option><option value="HNA">HANAMAKI</option><option value="GAJ">YAMAGATA JUNMACHI</option><option value="SYO">SHONAI</option><option value="FKS">FUKUSHIMA</option><option value="IBR">IBARAKI</option><option value="KIJ">NIIGATA</option><option value="KMQ">KOMATSU</option><option value="TOY">TOYAMA</option><option value="QSZ">SHIZUOKA</option><option value="TAK">TAKAMATSU</option><option value="KCZ">KOCHI</option><option value="MYJ">MATSUYAMA</option><option value="TKS">TOKUSHIMA</option><option value="HIJ">Hiroshima</option><option value="OKJ">OKAYAMA</option><option value="UBJ">UBE</option><option value="YGJ">YONAGO MIHO</option><option value="IZO">IZUMO</option><option value="TTJ">TOTTORI</option><option value="IWJ">IWAMI</option><option value="KKJ">KITA KYUSHU KOKURA</option><option value="HSG">SAGA</option><option value="NGS">NAGASAKI</option><option value="OIT">OITA</option><option value="KMJ">KUMAMOTO</option><option value="KMI">MIYAZAKI</option><option value="KOJ">Kagoshima</option><option value="OKA">Okinawa</option>
                                </select>
                            </div>
                        </div>
                        <div class="bzg_c pickdate-range" data-col="m4">
                            <div class="form__row">
                                <div class="input-iconic">
                                    <input type="text" class="form-input form-input--block pikaRange pikaRange--start" id="flight_start" placeholder="Tanggal Berangkat">
                                    <label for="flight_start" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row">
                                <div class="input-iconic" id="trip_type">
                                    <input type="text" class="form-input form-input--block pikaRange pikaRange--end" id="flight_end" placeholder="Tanggal Kembali">
                                    <label for="flight_end" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m4">
                            <div class="form__row">
                                <select name="" class="form-input form-input--block selectstyle">
                                    <option value="">Kelas Penerbangan</option>
                                    <option value="">Ekonomi</option>
                                </select>
                            </div>
                            <div class="form__row">
                                <div class="pick-guest" id="pickFlightPsg" data-toggle>
                                    <button class="form-input form-input--block btn-toggle" type="button">
                                        <strong class="text-ellipsis">
                                            <span class="total-input--all" id="total_psg">1</span> Traveller(s)
                                        </strong>
                                    </button>
                                    <div class="toggle-panel t-black fill-white">
                                        <div class="block--inset-small">
                                            <div class="form__row flex v-ct qty-input">
                                                <label for="" class="fg-1">
                                                    <span class="total-input">1</span> Adult
                                                </label>
                                                <div class="">
                                                    <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                    <input type="number" class="form-input form-input--redline text-center input-adult" 
                                                    value="1" min="1" max="9" data-psg="total_psg">
                                                    <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <hr class="block--half">
                                            <div class="form__row flex v-ct qty-input">
                                                <label for="" class="fg-1">
                                                    <span class="total-input">0</span> Children
                                                </label>
                                                <div class="">
                                                    <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                    <input type="number" class="form-input form-input--redline text-center" 
                                                    value="0" min="0" max="9" data-psg="total_psg">
                                                    <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <hr class="block--half">
                                            <div class="form__row flex v-ct qty-input">
                                                <label for="" class="fg-1">
                                                    <span class="total-input">0</span> Infant
                                                </label>
                                                <div class="">
                                                    <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="infant" type="button">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                    <input type="number" class="form-input form-input--redline text-center" 
                                                    value="0" min="0" max="9" data-psg="total_psg">
                                                    <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="infant" type="button">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <hr class="block--half">
                                            <div class="text-right">
                                                <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                                    Pilih
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- pick-guest -->
                            </div>
                        </div>
                    </div>
                    <div class="cf">
                        <label for="sekali_jalan" class="form-label--inline">
                            <input type="radio" id="sekali_jalan" name="trip_type" data-trip="one_way">
                            <span class="btn-label__text">Sekali Jalan</span>
                        </label>
                        <label for="pulang_pergi" class="form-label--inline">
                            <input type="radio" id="pulang_pergi" name="trip_type" data-trip="round_trip">
                            <span class="btn-label__text">Pulang Pergi</span>
                        </label>
                        <div class="pull-right text-right">
                            <label for="langsung" class="form-label--inline">
                                <input type="checkbox" id="langsung">
                                <span class="btn-label__text">Hanya penerbangan langsung</span>
                            </label>
                            <button class="btn btn--round btn--red">
                                <strong class="text-up">Cari</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section class="section-block text-center">
        <div class="container container--smaller">
            <ul class="block--half list-nostyle list-inblock--small nav-filter">
                <li class="current">
                    <button class="btn btn--round btn--ghost-red-black" type="button">
                        <strong class="text-up">Recommended</strong>
                    </button>
                </li>
                <li>
                    <button class="btn btn--round btn--ghost-red-black" type="button">
                        <strong class="text-up">Most Popular</strong>
                    </button>
                </li>
            </ul>
            <hr class="block--half">
            <h3 class="text-up"><span class="t--larger">Filter by Destination</span></h3>
        </div>
        <nav class="fill-lightgrey block--inset-small">
            <div class="container container--small">
                <ul class="list-nostyle list-inblock--small nav-filter">
                    <li class="current"><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Asia</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Eropa</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Australia</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">America</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Timur Tengah</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Africa</strong></a></li>
                </ul>
            </div>
        </nav>
    </section>

    <section class="section-block">
        <div class="inset-on-m">
        <div class="container container--smaller">
            <div class="bzg">
                <div class="bzg_c" data-col="m6">
                    <figure>
                        <img class="block border" src="//placehold.it/800x320" alt="" class="img-full">
                        <figcaption class="block--inset fill-lightgrey">
                            <h3 class="text-up">
                                ANA-Value Fare Campaign!
                            </h3>
                            <table class="cell--top">
                                <tbody>
                                    <tr>
                                        <td>Date of Travel</td>
                                        <td>:</td>
                                        <td>
                                            21 OCT - 15 DEC 2018<br>
                                            21 OCT - 15 DEC 2018
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Date of Issue</td>
                                        <td>:</td>
                                        <td>
                                            21 OCT - 15 DEC 2018
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Terms &amp; Conditions</td>
                                        <td>:</td>
                                        <td>
                                            Min Stay : 3 Days(K, L class)<br>
                                            Min Stay : 3 Days(K, L class)<br>
                                            Min Stay : 3 Days(K, L class)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Flight</td>
                                    </tr>
                                    <tr>
                                        <td>Outbound Flight</td>
                                        <td>:</td>
                                        <td>
                                            NH836 CGK NRT<br>
                                            NH836 CGK NRT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Inbound Flight</td>
                                        <td>:</td>
                                        <td>
                                            NH836 CGK NRT<br>
                                            NH836 CGK NRT
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </figcaption>
                    </figure>
                </div>
                <div class="bzg_c" data-col="m6">
                    <div class="bzg bzg--small-gutter">
                        <div class="block bzg_c" data-col="m6">
                            <div class="block--inset-small full-v border fill-pink-light">
                                <table class="cell--top">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="fa fa-map-marker"></span>
                                            </td>
                                            <td>
                                                <ul class="block--half list-dash text-up t-strong">
                                                    <li>Tokyo</li>
                                                    <li>Osaka</li>
                                                    <li>Nagoya</li>
                                                    <li>Fukuoka</li>
                                                    <li>Tokyo</li>
                                                    <li>Osaka</li>
                                                    <li>Nagoya</li>
                                                    <li>Fukuoka</li>
                                                    <li>Osaka</li>
                                                    <li>Nagoya</li>
                                                    <li>Fukuoka</li>
                                                </ul>
                                                <h4 class="block--small text-red t--larger">IDR 4.850.000++*</h4>
                                                <p>
                                                    Economy Class<br>
                                                    Booking Class: K Class<br>
                                                    *Exclude Tax, Aviation &amp; VAT 1%
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="block--half text-center">
                                    <a href="#" class="btn btn--round btn--red text-up t-strong">
                                        Selengkapnya
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="block bzg_c" data-col="m6">
                            <div class="block--inset-small full-v border fill-pink-light">
                                <table class="cell--top">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="fa fa-map-marker"></span>
                                            </td>
                                            <td>
                                                <ul class="block--half list-dash text-up t-strong">
                                                    <li>Tokyo</li>
                                                    <li>Osaka</li>
                                                    <li>Nagoya</li>
                                                    <li>Fukuoka</li>
                                                    <li>Tokyo</li>
                                                    <li>Osaka</li>
                                                    <li>Nagoya</li>
                                                    <li>Fukuoka</li>
                                                </ul>
                                                <h4 class="block--small text-red t--larger">IDR 4.850.000++*</h4>
                                                <p>
                                                    Economy Class<br>
                                                    Booking Class: K Class<br>
                                                    *Exclude Tax, Aviation &amp; VAT 1%
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="block--half text-center">
                                    <a href="#" class="btn btn--round btn--red text-up t-strong">
                                        Selengkapnya
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="block bzg_c" data-col="m6">
                            <div class="block--inset-small full-v border fill-pink-light">
                                <table class="cell--top">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="fa fa-map-marker"></span>
                                            </td>
                                            <td>
                                                <ul class="block--half list-dash text-up t-strong">
                                                    <li>Tokyo</li>
                                                    <li>Osaka</li>
                                                    <li>Nagoya</li>
                                                    <li>Fukuoka</li>
                                                    <li>Tokyo</li>
                                                    <li>Osaka</li>
                                                    <li>Nagoya</li>
                                                    <li>Fukuoka</li>
                                                </ul>
                                                <h4 class="block--small text-red t--larger">IDR 4.850.000++*</h4>
                                                <p>
                                                    Economy Class<br>
                                                    Booking Class: K Class<br>
                                                    *Exclude Tax, Aviation &amp; VAT 1%
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="block--half text-center">
                                    <a href="#" class="btn btn--round btn--red text-up t-strong">
                                        Selengkapnya
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    
    <section class="section-block">
        <div class="container container--smaller">
            <hr>
            <div class="block section-head">
                <h2 class="no-space text-up">
                    <a href="#" class="link-black">Best Deal Product</a>
                </h2>
            </div>
            <div class="content-section inset-on-m">
                <div class="bzg cards cards--blue">
                    <?php for ($i=1; $i <= 4; $i++) { ?>
                    <div class="block bzg_c" data-col="s6, m3">
                        <div class="card__item">
                            <figure class="item-img img-square">
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/420x420" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <div class="text-center">
                                        <h3 class="text-up">
                                            Group Tour
                                        </h3>
                                        <a href="#" class="btn btn--round btn--ghost-red">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <span class="in-block">
                                        <span class="fa fa-globe"></span>
                                        Asia
                                    </span>
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
