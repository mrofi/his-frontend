<?php include '_partials/head.php'; ?>
<div class="sticky-footer-container broken-page">
<a href="#" class="site-logo">
    <img src="assets/img/logo-circle.png" alt="H.I.S">
</a>
<div class="page-bg">
    <img src="" data-src="assets/img/bg-broken-page-mobile.jpg" alt="" class="bg-portrait item-heavy">
    <img src="" data-src="assets/img/bg-broken-page-desktop.jpg" alt="" class="bg-landscape item-heavy">
</div>
<div class="page-content container text-center">
    <h1 class="title-code">404</h1>
    <p>We looked everywhere for this page.
    Are you sure the website URL is correct?
    Get in touch with the site owner.</p>
    <a href="#" class="btn btn--round btn--red">
        Back to home
    </a>

</div>

<?php include '_partials/scripts.php'; ?>
