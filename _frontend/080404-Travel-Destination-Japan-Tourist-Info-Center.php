<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destinations</a></li>
                <li><a href="#">Asia</a></li>
                <li><a href="#">Jepang</a></li>
                <li><a href="#">Tourist Information Center</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <section class="section-block">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
            </figure>
            <div class="text-center">
                <h1 class="h3">Japan Tourist Information Center</h1>
                <p class="t--larger">
                    Tourist Information Center merupakan pusat informasi bagi para wisatawan yang datang ke Luar Negeri. Kunjungi Tourist Information Center yang terdapat di berbagai lokasi di Jepang yang dioperasikan oleh H.I.S. Tours & Travel. Kami siap untuk membantu Anda. 
                </p>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h3 class="no-space text-up in-block">
                    <span class="title-text text-up t--larger">LOKASI H.I.S. TOURIST INFORMATION CENTER</span>
                </h3>
            </div>
            <div class="block post inset-on-m">
                <div class="block post__item bzg">
                    <div class="bzg_c block--half" data-col="m4">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m8">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger text-up">Tokyo</a>
                        </h3>
                        <div class="block">
                            <strong>Tokyo Tourist Information Center</strong>  berbasis di Shinjuku, Ginza dan Yurakucho yang dikenal sebagai jantung kota Tokyo. Reservasi tour dengan guide, hotel dan optional tour juga tersedia di pusat-pusat informasi kami.
                        </div>
                        <div class="block text-right">
                            <a href="#" class="btn btn--round btn--red text-up">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <div class="block post__item bzg">
                    <div class="bzg_c block--half" data-col="m4">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m8">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger text-up">Osaka</a>
                        </h3>
                        <div class="block">
                            <strong>Osaka Tourist Information Center</strong>, pusat informasi menempati seluruh lantai 8 dari “Shinsaibashi OPA Gedung Utama” yang terletak di jantung Shinsaibashi dan tidak hanya menawarkan jasa perjalanan saja tetapi juga terdapat toko populer bagi wisatawan asing (toko obat, tax fee shopping, kosmetik, souvenir, dan lainya).
                        </div>
                        <div class="block text-right">
                            <a href="#" class="btn btn--round btn--red text-up">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <div class="block post__item bzg">
                    <div class="bzg_c block--half" data-col="m4">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m8">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger text-up">Tokyo</a>
                        </h3>
                        <div class="block">
                            <strong>Tokyo Tourist Information Center</strong>  berbasis di Shinjuku, Ginza dan Yurakucho yang dikenal sebagai jantung kota Tokyo. Reservasi tour dengan guide, hotel dan optional tour juga tersedia di pusat-pusat informasi kami.
                        </div>
                        <div class="block text-right">
                            <a href="#" class="btn btn--round btn--red text-up">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <!-- item -->
            </div>
            <ol class="pagination text-right">
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-left"></span>
                    </a>
                </li>
                <li>
                    <a href="#" class="active">
                        1
                    </a>
                </li>
                <li>
                    <a href="#">
                        2
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-right"></span>
                    </a>
                </li>
            </ol>
        </section>
        <div class="block bzg">
            <div class="block--half bzg_c" data-col="m6">
                <a href="#">
                    <img src="assets/img/banner-jtd.jpg" alt="">
                </a>
            </div>
            <div class="block--half bzg_c" data-col="m6">
                <a href="#">
                    <img src="assets/img/banner-jrm.jpg" alt="">
                </a>
            </div>
        </div>
        <hr>
    </div>
    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
