<?php 
    $path = '../';
?>

<?php include $path.'_partials/head.php'; ?>
<?php include $path.'_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pemesanan</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <ol class="block list-nostyle text-center text-up steps">
            <li class="steps__item ">
                <span>Penerbangan</span>
            </li>
            <li class="steps__item">
                <span>Isi Data</span>
            </li>
            <li class="steps__item current">
                <span>Pembayaran</span>
            </li>
            <li class="steps__item">
                <span>Selesai</span>
            </li>
        </ol>

        <!-- <div class="block--double">
            <h1 class="block--small">Ringkasan</h1>
            <div class="block--inset border line--small">
                Terima kasih Anda telah melakukan reservasi melalui H.I.S. Travel Indonesia<br>
                Staff kami akan segera menghubungi Anda dalam 1 x 24 Jam Hari Kerja atau maksimal 2 x 24 Jam Hari Kerja. Reservasi Anda belum mengikat selama pembayaran belum diterima oleh pihak H.I.S. Travel Indonesia dan kami tidak bertanggung jawab atas perubahan harga ataupun kesalahan cetak selama proses reservasi belum final.<br>
                Harap catat dan simpan kode reservasi Anda untuk reservasi lebih lanjut.<br>
                Terima kasih
            </div>
        </div> -->

        <div class="block--double bzg">
            <div class="bzg_c" data-col="m8">
                <div class="block--double">
                    <h2 class="block--small text-up">Informasi Pemesan</h2>
                    <ul class="list-nostyle list-iconic t--larger">
                        <li>
                            <span class="item-icon fa fa-user"></span>
                            <div class="item-text">Mrs. AULIA PUSPA</div>
                        </li>
                        <li>
                            <span class="item-icon fa fa-envelope-open-o"></span>
                            <div class="item-text">
                                <a href="mailto:my.email@gmail.com" class="link-black">my.email@gmail.com</a>
                            </div>
                        </li>
                        <li>
                            <span class="item-icon fa fa-phone"></span>
                            <div class="item-text">
                                <a href="tel:+62-8123456789" class="link-black">+62-8123456789</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="block--double">
                    <!-- <h2 class="block--small text-up">Peserta Tour</h2> -->
                    <p class="t-strong">Detail Penumpang</p>
                    <div class="block--inset-small border">
                        <div class="title">Dewasa</div>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>Mrs. Aulia Puspa</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>27 Jan 1990</td>
                                </tr>
                                <tr>
                                    <td>No. Paspor</td>
                                    <td>:</td>
                                    <td>A123546</td>
                                </tr>
                                <tr>
                                    <td>Berlaku Paspor</td>
                                    <td>:</td>
                                    <td>31 Des 2022</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn--round btn--red text-up">Selesai</button>
                </div>
            </div>
            <!-- left -->
            <div class="bzg_c" data-col="m4">
                <table class="block-half table--summary table--fixed text-center">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-blue">
                                <strong>Jakarta (CGK)</strong>
                                <span class="fa fa-exchange"></span>
                                <strong>Tokyo (HND)</strong>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="line--small">
                                <small>Total IDR</small><br>
                                <strong class="text-red">IDR 36.000.000</strong>
                            </td>
                            <td class="line--small">
                                <small>Penumpang</small><br>
                                <strong>2 Orang</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey">
                        <span class="his-seat text-blue"></span>
                        <strong class="ml-small text-up text-blue">Kelas Penerbangan</strong>
                    </div>
                    <div class="p-small">
                        <strong>Kelas Ekonomi</strong>
                    </div>
                </div>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey">
                        <span class="fa fa-plane text-blue"></span>
                        <strong class="ml-small text-up text-blue">Penerbangan Pergi</strong>
                    </div>
                    <div class="p-small">
                        <div class="t-bold mb-small">11:46 - Jakarta (CGK)</div>
                        <div class="direct-container">
                            <div class="flex a-start">
                                <img src="//via.placeholder.com/72x52" alt="" width="36">
                                <div class="fg-1 ml-small">
                                    <strong>All Nippon Airways</strong><br>
                                    <small><span class="fa fa-calendar"></span> Minggu, 29 Sepetember 2019</small>
                                </div>
                            </div>
                        </div>
                        <div class="t-bold mb-small">17:46 - Tokyo (HND)</div>
                        <small class="text-grey">
                            <span class="fa fa-info-circle"></span>
                            Waktu diatas adalah waktu setempat
                        </small>
                    </div>
                </div>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey">
                        <span class="fa fa-plane fa-rotate-180 text-blue"></span>
                        <strong class="ml-small text-up text-blue">Penerbangan Pulang</strong>
                    </div>
                    <div class="p-small">
                        <div class="t-bold mb-small">11:46 - Jakarta (CGK)</div>
                        <div class="direct-container">
                            <div class="flex a-start">
                                <img src="//via.placeholder.com/72x52" alt="" width="36">
                                <div class="fg-1 ml-small">
                                    <strong>All Nippon Airways</strong><br>
                                    <small><span class="fa fa-calendar"></span> Minggu, 29 Sepetember 2019</small>
                                </div>
                            </div>
                        </div>
                        <div class="t-bold mb-small">17:46 - Tokyo (HND)</div>
                        <small class="text-grey">
                            <span class="fa fa-info-circle"></span>
                            Waktu diatas adalah waktu setempat
                        </small>
                    </div>
                </div>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey">
                        <span class="fa fa-money text-blue"></span>
                        <strong class="ml-small text-up text-blue">Rincian Harga</strong>
                    </div>
                    <div class="p-small">
                        <table class="table--line mb-0">
                            <tbody>
                                <tr>
                                    <td>Dewasa (x2)</td>
                                    <td class="text-right">IDR 30.000.000</td>
                                </tr>
                                <tr>
                                    <td>Anak (1x)</td>
                                    <td class="text-right">IDR 6.000.000</td>
                                </tr>
                                <tr class="t-bold">
                                    <td></td>
                                    <td class="text-right text-red">IDR 36.000.000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- - -->
            </div>
        </div>
        <div class="block--double text-center t-strong">
            <a href="110101-Booking-Print.php" class="btn btn--round btn--red text-up">Cetak</a>
            <button class="btn btn--round btn--red text-up">Selesai</button>
        </div>
    </div>
</main>

<?php include $path.'_partials/footer.php'; ?>
<?php include $path.'_partials/scripts.php'; ?>
