<div class="toggle-panel toggle-panel--slideup <?= isset($isActive) ? $isActive : '' ?>" id="research_booking">
    <div class="fill-primary block--inset">
        <form action="flight-result.php" class="form">
            <div class="bzg bzg--lite-gutter">
                <div class="form__row bzg_c" data-col="m10, l12" data-offset="m1">
                    <!-- <label for="sekali_jalan" class="form-label--inline">
                        <input type="radio" id="sekali_jalan" name="trip_type-home" data-trip="one_way">
                        <span class="btn-label__text">Sekali Jalan</span>
                    </label>
                    <label for="pulang_pergi" class="form-label--inline">
                        <input type="radio" id="pulang_pergi" name="trip_type-home" data-trip="round_trip" checked>
                        <span class="btn-label__text">Pulang Pergi</span>
                    </label> -->
                    <label for="sekali_jalan" class="form-label--inline btn-check btn-check--radio-solid">
                        <input type="radio" class="btn-check-input" id="sekali_jalan" 
                        name="trip_type-home" data-trip="one_way">
                        <span class="btn-check-text">Sekali Jalan</span>
                    </label>
                    <label for="pulang_pergi" class="form-label--inline btn-check btn-check--radio-solid">
                        <input type="radio" class="btn-check-input" id="pulang_pergi" 
                        name="trip_type-home" data-trip="round_trip" checked>
                        <span class="btn-check-text">Pulang Pergi</span>
                    </label>
                </div>
                <div class="form__row bzg_c" data-col="m10, l2" data-offset="m1">
                    <div class="input-iconic autoinput" data-input="airport-default.json">
                        <input type="text" class="form-input form-input--block" id="FlightOrigin"
                        data-input="airportSIN.json" data-container="#FlightOriginInput" autocomplete="off"
                        value="Jakarta (CGK)">
                        <input type="hidden" class="input-sender" name="origin">
                        <label for="FlightOrigin" class="label-icon">
                            <span class="fa fa-plane"></span>
                        </label>
                        <button class="btn-cancel text-white" type="button">
                            Cancel
                        </button>
                        <div class="search-quick-res line--smaller" id="FlightOriginInput">
                            <div class="loader">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form__row bzg_c" data-col="m10, l2" data-offset="m1">
                    <div class="input-iconic autoinput" data-input="airport-default.json">
                        <input type="text" class="form-input form-input--block" id="FlightDest"
                        data-input="airport.json" data-container="#FlightDestInput" autocomplete="off"
                        value="Tokyo (HND)">
                        <input type="hidden" class="input-sender" name="destination">
                        <label for="FlightDest" class="label-icon">
                            <span class="fa fa-plane fa-rotate-90"></span>
                        </label>
                        <div class="search-quick-res line--smaller" id="FlightDestInput">
                            
                        </div>
                        <button class="btn-cancel text-white" type="button">
                            Cancel
                        </button>
                    </div>
                </div>
                <div class="bzg_c" data-col="m10, l4" data-offset="m1">
                    <div class="bzg bzg--lite-gutter pickdate-range"
                    data-min-start="2020-03-09" 
                    data-max-end="2025-06-30"
                    data-container="dateFlightRange">
                        <div class="form__row bzg_c" data-col="l6">
                            <div class="input-iconic">
                                <input type="text" class="form-input form-input--block pikaRange pikaRange--start" id="flight_start" placeholder="Tanggal Berangkat"
                                name="departure_date">
                                <label for="flight_start" class="label-icon">
                                    <span class="fa fa-calendar"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form__row bzg_c" data-col="l6">
                            <div class="input-iconic" id="trip_type-home">
                                <input type="text" class="form-input form-input--block pikaRange pikaRange--end" id="flight_end" placeholder="Tanggal Kembali"
                                name="return_date">
                                <label for="flight_end" class="label-icon">
                                    <span class="fa fa-calendar"></span>
                                </label>
                            </div>
                        </div>
                        <div class="bzg_c picker-container" id="dateFlightRange">
                            <div class="mb-small text-center">
                                <button class="btn-cancel btn--ghost" type="button">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- pickdate-range -->
                </div>
                <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                    <div class="pick-guest" id="pickFlightPsg" data-toggle>
                        <button class="form-input form-input--block btn-toggle" type="button">
                            <strong class="text-ellipsis">
                                <span class="total-input--all" id="total_psg">1</span> Traveller(s)
                                <span class="additional-info" id="psg_add_info">, Ekonomi</span>
                            </strong>
                        </button>
                        <div class="toggle-panel t-black fill-white zi-20">
                            <div class="block--inset-small">
                                <div class="form__row flex v-ct qty-input">
                                    <label for="" class="fg-1 line--small">
                                        <span class="total-input">1</span> Adult<br>
                                        <small>(12 thn atau lebih)</small>
                                    </label>
                                    <div class="">
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="minus" type="button" data-age="adult" data-type="person">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                        <input type="number" class="form-input form-input--redline text-center input-adult" name="adult" 
                                        value="1" min="1" max="9" data-psg="total_psg" data-step-item="1" data-step="1">
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="plus" type="button" data-age="adult" data-type="person">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                                <hr class="block--half">
                                <div class="form__row flex v-ct qty-input">
                                    <label for="" class="fg-1 line--small">
                                        <span class="total-input">0</span> Children<br>
                                        <small>(12 thn atau lebih)</small>
                                    </label>
                                    <div class="">
                                        <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="child" type="button" data-type="person">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                        <input type="number" class="form-input form-input--redline text-center" name="child" 
                                        value="0" min="0" max="9" data-psg="total_psg" data-step-item="1" data-step="1">
                                        <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="child" type="button" data-type="person">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                                <hr class="block--half">
                                <div class="form__row flex v-ct qty-input">
                                    <label for="" class="fg-1 line--small">
                                        <span class="total-input">0</span> Infant<br>
                                        <small>(12 thn atau lebih)</small>
                                    </label>
                                    <div class="">
                                        <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="infant" type="button" data-type="person">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                        <input type="number" class="form-input form-input--redline text-center input-infant" name="infant" 
                                        value="0" min="0" max="9" data-psg="total_psg" data-step-item="1" data-step="1">
                                        <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="infant" type="button" data-type="person">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                                <hr class="block--half">
                                <div class="form__row">
                                    <label for="economy_check" class="btn-check btn-check--radio">
                                        <span class="text-up btn-check-text">Ekonomi</span>
                                        <input type="radio" class="btn-check-input store-val" 
                                        data-val="Ekonomi" data-target="psg_add_info"
                                        name="class_check" checked="checked" id="economy_check">
                                        <span class="input-icon"></span>
                                    </label>
                                </div>
                                <div class="form__row">
                                    <label for="premium_check" class="btn-check btn-check--radio">
                                        <span class="text-up btn-check-text">Premium Ekonomi</span>
                                        <input type="radio" class="btn-check-input store-val" 
                                        data-val="Premium Ekonomi" data-target="psg_add_info"
                                        name="class_check" id="premium_check">
                                        <span class="input-icon"></span>
                                    </label>
                                </div>
                                <div class="form__row">
                                    <label for="premium_check" class="btn-check btn-check--radio">
                                        <span class="text-up btn-check-text">Bisnis</span>
                                        <input type="radio" class="btn-check-input store-val" 
                                        data-val="Bisnis" data-target="psg_add_info"
                                        name="class_check" id="premium_check">
                                        <span class="input-icon"></span>
                                    </label>
                                </div>
                                <div class="text-right">
                                    <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                        Pilih
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- pick-guest -->
                </div>
                <div class="form__row bzg_c" data-col="m10, l1" data-offset="m1">
                    <button class="btn btn--red btn--block">Cari</button>
                </div>
            </div>
        </form>
    </div>
</div>