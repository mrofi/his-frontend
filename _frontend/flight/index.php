<?php 
    $path = '../';
    $isActive = 'is-active';
?>

<?php include $path.'_partials/head.php'; ?>
<?php include $path.'_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Penerbangan</a></li>
            </ul>
        </div>
    </div>
    
    <section class="section-block">
        <div class="container container--smaller" >
            <div class="block">
                <a href="#" class="is-block text-center">
                    <img src="//via.placeholder.com/1320x400" alt="">
                </a>
            </div>
            
            <div class="block">
                <?php include 'research-form.php'; ?>
            </div>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Promo TIket Pesawat
                </h2>
                <a href="#" class="btn--plain btn--plain-red text--red pull-right">
                    <b class="text-up">Lihat Lainnya</b>
                </a>
            </div>
            <div class="bzg content-section">
                <div class="block bzg_c" data-col="m4">
                    <a href="#" class="is-block">
                        <img class="img-full" src="//via.placeholder.com/300" alt="">
                    </a>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <a href="#" class="is-block">
                        <img class="img-full" src="//via.placeholder.com/300" alt="">
                    </a>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <a href="#" class="is-block">
                        <img class="img-full" src="//via.placeholder.com/300" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include $path.'_partials/footer.php'; ?>
</div>

<script>
    window.onload = function () {
        $('#btnBookFlights').click(function(e) {
            e.preventDefault();
            Site._dialog('Yakin ?', 'Title is', 'formFlight', 'form', 'OK', 'Tidak');
        });

        // setTimeout(function() {
        //     $('#searchSession').addClass('is-active')
        // }, 600000);
    }
</script>
<script>
    var cachePath = '../';
</script>
<?php include $path.'_partials/scripts2.php'; ?>
