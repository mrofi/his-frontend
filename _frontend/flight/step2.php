<?php 
    $path = '../';
?>

<?php include $path.'_partials/head.php'; ?>
<?php include $path.'_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pemesanan</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <ol class="block list-nostyle text-center text-up steps">
            <li class="steps__item ">
                <span>Penerbangan</span>
            </li>
            <li class="steps__item current">
                <span>Isi Data</span>
            </li>
            <li class="steps__item">
                <span>Pembayaran</span>
            </li>
            <li class="steps__item">
                <span>Selesai</span>
            </li>
        </ol>

        <div class="block--double bzg">
            <div class="bzg_c" data-col="m8">
                <h1 class="block--small">Rincian Anda</h1>
                <p class="t-strong t--larger">Silakan <a href="#">log-in</a> untuk mempercepat pengisian atau lanjut sebagai tamu</p>
                <form action="" id="formFlight" class="form baze-form"
                data-empty="Input tidak boleh kosong"
                data-email="Alamat email tidak benar"
                data-number="Input harus berupa angka"
                data-numbermin="Angka minimal "
                data-numbermax="Angka maksimal"
                data-flash-msg="Lengkapi dulu data pemesanannya!">
                    <fieldset class="block">
                        <h2 class="text-up">Data Pemesan</h2>
                        <div class="form__row">
                            <label class="space-right t-strong" for="">Titel *</label>
                            <label for="title_mr" class="btn-label">
                                <input type="radio" id="title_mr" name="gender" data-dest="title_mr1">
                                <span class="btn-label__text">Mr.</span>
                            </label>
                            <label for="title_mrs" class="btn-label">
                                <input type="radio" id="title_mrs" name="gender" checked data-dest="title_mrs1">
                                <span class="btn-label__text">Mrs.</span>
                            </label>
                            <label for="title_ms" class="btn-label">
                                <input type="radio" id="title_ms" name="gender" data-dest="title_ms1">
                                <span class="btn-label__text">Ms.</span>
                            </label>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="oriFirstName" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" id="oriFirstName" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="oriLastName" class="t-strong">Nama Belakang *</label><br>
                                <input type="text" class="form-input form-input--block" id="oriLastName" required>
                            </div>
                        </div>
                        <div class="block--small bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nomor Telepon *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="+61">+61</option>
                                            <option value="+62" selected>+62</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x8">
                                        <input type="number" class="form-input form-input--block" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Email</label><br>
                                <input type="email" class="form-input form-input--block" data-email-guest required>
                            </div>
                        </div>
                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <hr>

                    <fieldset class="block copy-val">
                        <h2 class="text-up">Tamu 1</h2>
                        <div class="block block--inset-small fill-yellow t-strong">
                            <span class="text-red">* Perhatian</span><br>
                            Pastikan data tamu sudah sesuai dengan identitas paspor yang masih berlaku
                        </div>
                        <div class="form__row">
                            <label for="same_as" class="btn-label btn-copy">
                                <input type="checkbox" id="same_as" data-msg="Pastikan <b>Data Pemesan</b> sudah terisi Cuk!">
                                <span class="btn-label__text">Sama dengan pemesan</span>
                            </label>
                        </div>
                        <div class="form__row">
                            <label class="space-right t-strong" for="">Titel *</label>
                            <label for="title_mr1" class="btn-label">
                                <input type="radio" id="title_mr1" name="gender1" checked data-origin="title_mr">
                                <span class="btn-label__text">Mr.</span>
                            </label>
                            <label for="title_mrs1" class="btn-label">
                                <input type="radio" id="title_mrs1" name="gender1" data-origin="title_mrs">
                                <span class="btn-label__text">Mrs.</span>
                            </label>
                            <label for="title_ms1" class="btn-label">
                                <input type="radio" id="title_ms1" name="gender1" checked data-origin="title_ms">
                                <span class="btn-label__text">Ms.</span>
                            </label>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="destFirstName" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" id="destFirstName" data-origin="oriFirstName" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Belakang *</label><br>
                                <input type="text" class="form-input form-input--block" id="destLastName" data-origin="oriLastName" required>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Tanggal Lahir *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1">1</option>
                                            <option value="31" selected>31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="12" selected>12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000" selected>2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Jenis Kelamin *</label><br>
                                <label for="male2" class="btn-label">
                                    <input type="radio" id="male2" name="gender12" checked>
                                    <span class="btn-label__text">Laki-laki</span>
                                </label>
                                <label for="female2" class="btn-label">
                                    <input type="radio" id="female2" name="gender12">
                                    <span class="btn-label__text">Perempuan</span>
                                </label>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nomor Paspor *</label><br>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Masa Berlaku Paspor *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1">1</option>
                                            <option value="31" selected>31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="12" selected>12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000" selected>2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <hr>
                    <fieldset class="block">
                        <h2 class="text-up">Tamu 2</h2>
                        <div class="form__row">
                            <label class="space-right t-strong" for="">Titel *</label>
                            <label for="title_mr13" class="btn-label">
                                <input type="radio" id="title_mr13" name="gender13" required>
                                <span class="btn-label__text">Mr.</span>
                            </label>
                            <label for="title_mrs13" class="btn-label">
                                <input type="radio" id="title_mrs13" name="gender13">
                                <span class="btn-label__text">Mrs.</span>
                            </label>
                            <label for="title_ms13" class="btn-label">
                                <input type="radio" id="title_ms13" name="gender13">
                                <span class="btn-label__text">Ms.</span>
                            </label>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Belakang *</label><br>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Tanggal Lahir *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1">1</option>
                                            <option value="31" selected>31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="12" selected>12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000" selected>2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Jenis Kelamin *</label><br>
                                <label for="male23" class="btn-label">
                                    <input type="radio" id="male23" name="gender123" checked>
                                    <span class="btn-label__text">Laki-laki</span>
                                </label>
                                <label for="female23" class="btn-label">
                                    <input type="radio" id="female23" name="gender123">
                                    <span class="btn-label__text">Perempuan</span>
                                </label>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Masa Berlaku Paspor *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1">1</option>
                                            <option value="31" selected>31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="12" selected>12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000" selected>2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <fieldset>
                        <h2 class="text-up">Metode Pembayaran</h2>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m4">
                                <div class="block--inset-smallest border full-v">
                                    <label for="pay-1" class="btn-label btn--block">
                                        <input type="radio" id="pay-1" name="pay-method" checked required>
                                        <div class="btn-label__tex">
                                            <!-- <img src="assets/img/atm-bersma.png" alt="" width="40"> -->
                                            <img src="assets/img/prima.png" alt="" width="40"><br>
                                            <!-- <img src="assets/img/alto.png" alt="" width="40"> -->
                                            Bank Transfer
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m4">
                                <div class="block--inset-smallest border full-v">
                                    <label for="pay-2" class="btn-label btn--block">
                                        <input type="radio" id="pay-2" name="pay-method">
                                        <div class="btn-label__tex">
                                            <img src="assets/img/visa.jpg" alt="" width="40"><br>
                                            <!-- <img src="assets/img/master-card.png" alt=""  width="40"> -->
                                            Visa/Master Card<br>
                                            <a href="0001-Home.php">info</a>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m4">
                                <div class="block--inset-smallest border full-v">
                                    <label for="pay-3" class="btn-label btn--block">
                                        <input type="radio" id="pay-3" name="pay-method" >
                                        <div class="btn-label__tex">
                                            <img src="assets/img/bca.png" alt="" width="40"><br>
                                            Virtual Account Bank Transfer
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <hr>
                    <fieldset>
                        <div class="block" data-voucher="../dev/voucher/" data-form="#voucher" data-total="#tableTotal">
                            <div class="flex form-voucher">
                                <input type="text" class="form-input form-input--block" placeholder="Masukkan Kode Voucher" data-name="code" id="inputVoucher">
                                <button class="btn btn--blue btn-apply" type="button" disabled="disabled">Gunakan</button>
                            </div>
                            <span class="msg-container"></span>
                            <button class="btn btn--round btn--pink text--smaller ml-half btn-change-voucher hidden" type="button" data-target="#inputVoucher">
                                Ganti voucher
                            </button>
                        </div>
                        <hr>
                    </fieldset>
                    <div class="block block--inset-small border">
                        <label for="agree_term" class="btn-label">
                            <input type="checkbox" id="agree_term" required class="submit-activator">
                            <span class="btn-label__text">
                                Saya menyetujui seluruh <a href="#">Syarat dan Ketentuan</a> dan <a href="#">Kebijakan Privasi</a> yang berlaku di
                                H.I.S. Travel Indonesia
                            </span>
                        </label>
                    </div>
                    <div class="text-right">
                        <button class="btn btn--round btn--red t-strong text-up btn-submit" type="button" disabled="disabled" onclick="Site._dialog('Yakin ?', 'Title is', 'formFlight', 'form', 'OK', 'Ojo')">
                            Lanjutkan
                        </button>
                    </div>
                </form>
                <form action="" id="voucher">
                    <!-- <input type="hidden" name="code" value="asdf123"> -->
                    <input type="hidden" name="booking_code" value="lkjv876">
                </form>
            </div>
            <!-- left -->
            <div class="bzg_c" data-col="m4">
                <table class="block-half table--summary table--fixed text-center">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-blue">
                                <strong>Jakarta (CGK)</strong>
                                <span class="fa fa-exchange"></span>
                                <strong>Tokyo (HND)</strong>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="line--small">
                                <small>Total IDR</small><br>
                                <strong class="text-red">IDR 36.000.000</strong>
                            </td>
                            <td class="line--small">
                                <small>Penumpang</small><br>
                                <strong>2 Orang</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey">
                        <span class="his-seat text-blue"></span>
                        <strong class="ml-small text-up text-blue">Kelas Penerbangan</strong>
                    </div>
                    <div class="p-small">
                        <strong>Kelas Ekonomi</strong>
                    </div>
                </div>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey flex">
                        <span class="fa fa-plane text-blue"></span>
                        <strong class="ml-small text-up text-blue fg-1">Penerbangan Pergi</strong>
                        <a href="#" class="btn-modal"
                        data-modal="#flightsChoosenConfirm">
                            Detail
                        </a>
                    </div>
                    <div class="p-small">
                        <div class="t-bold mb-small">11:46 - Jakarta (CGK)</div>
                        <div class="direct-container">
                            <div class="flex a-start">
                                <img src="//via.placeholder.com/72x52" alt="" width="36">
                                <div class="fg-1 ml-small">
                                    <strong>All Nippon Airways</strong><br>
                                    <small class="text-red t-bold"><span class="badge-red">1</span> Transit</small><br>
                                    <small><span class="fa fa-calendar"></span> Minggu, 29 Sepetember 2019</small>
                                </div>
                            </div>
                        </div>
                        <div class="t-bold mb-small">17:46 - Tokyo (HND)</div>
                        <div class="fill-softtblue pv-8 ph-12">
                            <small>2j 40m Transit di Manila (MNL)</small>
                        </div>
                        <small class="text-grey">
                            <span class="fa fa-info-circle"></span>
                            Waktu diatas adalah waktu setempat
                        </small>
                    </div>
                </div>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey">
                        <span class="fa fa-plane fa-rotate-180 text-blue"></span>
                        <strong class="ml-small text-up text-blue">Penerbangan Pulang</strong>
                    </div>
                    <div class="p-small">
                        <div class="t-bold mb-small">11:46 - Jakarta (CGK)</div>
                        <div class="direct-container">
                            <div class="flex a-start">
                                <img src="//via.placeholder.com/72x52" alt="" width="36">
                                <div class="fg-1 ml-small">
                                    <strong>All Nippon Airways</strong><br>
                                    <small class="text-red t-bold"><span class="badge-red">1</span> Transit</small><br>
                                    <small><span class="fa fa-calendar"></span> Minggu, 29 Sepetember 2019</small>
                                </div>
                            </div>
                        </div>
                        <div class="t-bold mb-small">17:46 - Tokyo (HND)</div>
                        <div class="fill-softtblue pv-8 ph-12">
                            <small>2j 40m Transit di Manila (MNL)</small>
                        </div>
                        <small class="text-grey">
                            <span class="fa fa-info-circle"></span>
                            Waktu diatas adalah waktu setempat
                        </small>
                    </div>
                </div>
                <!-- - -->
                <div class="block-half border">
                    <div class="p-small fill-lightgrey">
                        <span class="fa fa-money text-blue"></span>
                        <strong class="ml-small text-up text-blue">Rincian Harga</strong>
                    </div>
                    <div class="p-small">
                        <table class="table--line mb-0">
                            <tbody>
                                <tr>
                                    <td>Dewasa (x2)</td>
                                    <td class="text-right">IDR 30.000.000</td>
                                </tr>
                                <tr>
                                    <td>Anak (1x)</td>
                                    <td class="text-right">IDR 6.000.000</td>
                                </tr>
                                <tr class="t-bold">
                                    <td><strong>Total</strong></td>
                                    <td class="text-right text-red">IDR 36.000.000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- - -->
            </div>
        </div>
    </div>
</main>

<?php include $path.'_partials/footer.php'; ?>
</div>

<div class="dialog session-timeout" id="searchSession">
    <div class="dialog-overlay"></div>
    <div class="dialog-box">
        <header class="text-up">Halaman Kedaluarsa</header>
        <main>
            Hasil pencarian Anda telah kedaluarsa.<br>
            Silahkan klik Refresh untuk ulangi pencarian.
        </main>
        <footer class="text-right">
            <a href="./" class="btn btn--round btn--red btn-refresh">Cari lagi</a>
            <!-- <butoon class="btn btn--round btn-cancel">${no}</butoon> -->
        </footer>
    </div>
</div>

<div class="modal modal-book-confirm" id="flightsChoosenConfirm">
    <div class="overlay"></div>
    <div class="modal-container modal-container--large">
        <div class="modal-head flex a-start">
            <h3 class="title mb-0 fg-1">Detail Perjalanan</h3>
            <button class="btn-close" type="button" aria-label="Close">
                <span class="fa fa-times" aria-hidden="true"></span>
            </button>
        </div>
        <div class="modal-content" id="flightsDetailConfirm">


<div class="block bzg line--small">
    <div class="bzg_c" data-col="m6, l3">
        Pergi<br>
        <small class="text-grey">Rabu, 18 Maret 2020</small>
    </div>
    <div class="bzg_c mb-small" data-col="m6, l3">
        <div class="flex a-center">
            <img src="https://static.his-travel.co.id/files/airlinelogos/20191125073810-singapore-airlines.png" alt="" width="36">
            <div class="ml-small fg-1">
                Singapore Airlines<br><span class="text-grey">Economy</span>
            </div>
        </div>
    </div>
    <div class="bzg_c" data-col="s4, l2">
        11:15<br><span class="text-grey">Jakarta (CGK)</span>
    </div>
    <div class="bzg_c" data-col="s4, l2">
        01:00<br><span class="text-grey">Tokyo (HND)</span>
    </div>
    <div class="bzg_c" data-col="s4, l2">
        Total Durasi<br>
        <span class="text-grey">11j 45m, Langsung</span>
    </div>
</div>

<hr class="mb-small">
<div class="block bzg line--small">
    <div class="bzg_c" data-col="m6, l3">
        Pulang<br>
        <small class="text-grey">Jumat, 20 Maret 2020</small>
    </div>
    <div class="bzg_c mb-small" data-col="m6, l3">
        <div class="flex a-center">
            <img src="https://static.his-travel.co.id/files/airlinelogos/20191125073810-singapore-airlines.png" alt="" width="36">
            <div class="ml-small fg-1">
                Singapore Airlines<br><span class="text-grey">Economy</span>
            </div>
        </div>
    </div>
    <div class="bzg_c" data-col="s4, l2">
        22:50<br><span class="text-grey">Tokyo (HND)</span>
    </div>
    <div class="bzg_c" data-col="s4, l2">
        08:25<br><span class="text-grey">Jakarta (CGK)</span>
    </div>
    <div class="bzg_c" data-col="s4, l2">
        Total Durasi<br>
        <span class="text-grey">11j 35m, Langsung</span>
    </div>
</div>
</div>
        <div class="modal-footer">
            <div class="flex a-center">
                <nav class="fg-1">
                    <a href="#flightsDetailInfo" data-panel-target="#flightsDetailInfo">
                        Detail Penerbangan <span class="fa fa-caret-down"></span>
                    </a>
                    <a href="#pricesDetailInfo" data-panel-target="#pricesDetailInfo">
                        Detail Harga <span class="fa fa-caret-down"></span>
                    </a>
                </nav>
                <div class="book-action">
                    <div class="total-flight-price mb-0 h3 text-red ml-small mr-small">IDR <span>14.077.000</span></div>
                    <form action="" id="formFlight">
                        <input type="hidden" data-search="flight-request-6e9ebf73-834c-4d29-b4fe-5b221cb6265c" name="searchid" value="flight-request-6e9ebf73-834c-4d29-b4fe-5b221cb6265c">
                        <input type="hidden" data-flight="0-24,1-24" name="flightsid" class="is-changed" value="0-24,1-24">
                        <input type="hidden" data-ps="1,0,0" name="flightpassenger" value="1,0,0">
                        <button class="btn btn--round btn--red btn-book is-changed" type="button" data-search="flight-request-6e9ebf73-834c-4d29-b4fe-5b221cb6265c" data-flight="0-24,1-24" data-ps="1,0,0" id="btnBookFlights">
                            <strong>Book</strong>
                        </button>
                    </form>
                </div>
            </div>
            <div class="panel-content t--smaller" id="flightsDetailInfo">

    <h3 class="mb-0 text-blue">Berangkat</h3>
    
    <div class="flight-detail flight-detail--start">
        <time class="time">
            <strong>11:15</strong><br>
            18 Mar
        </time>
        <div class="content">
            Soekarno Hatta International Airport (Jakarta) (CGK)
        </div>
    </div>
    <div class="flight-detail flight-detail--duration">
        <time class="time">01j 45m</time>
        <div class="content">
            <div class="mb-half">
                <img src="https://static.his-travel.co.id/files/airlinelogos/20191125073810-singapore-airlines.png" alt="" width="36"> SQ-957, Economy<br>
                Dioperasikan oleh: Singapore Airlines
            </div><hr class="mb">
            <div class="mb-2">
            <div class="mb-small nowrap">
                <span class="his-koper"></span>
                Bagasi 30 K
            </div>
            
            </div>
            <div class="bzg">
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Model</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Denah Kursi</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Kursi</small><br>
                    <strong>Economy</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Jarak Antar Kursi</small><br>
                    <strong>-</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="flight-detail flight-detail--end">
        <time class="time">
            <strong>14:00</strong><br>
            18 Mar
        </time>
        <div class="content">
            Changi Airport (Singapore) (SIN)
        </div>
    </div>

<div class="mb-half block--inset-small fill-lightgrey">
    Transit for 03j 25m in Changi Airport (Singapore) (SIN)
</div>
    <div class="flight-detail flight-detail--start">
        <time class="time">
            <strong>17:25</strong><br>
            18 Mar
        </time>
        <div class="content">
            Changi Airport (Singapore) (SIN)
        </div>
    </div>
    <div class="flight-detail flight-detail--duration">
        <time class="time">06j 35m</time>
        <div class="content">
            <div class="mb-half">
                <img src="https://static.his-travel.co.id/files/airlinelogos/20191125073810-singapore-airlines.png" alt="" width="36"> SQ-630, Economy<br>
                Dioperasikan oleh: Singapore Airlines
            </div><hr class="mb">
            <div class="mb-2">
            <div class="mb-small nowrap">
                <span class="his-koper"></span>
                Bagasi 30 K
            </div>
            
            </div>
            <div class="bzg">
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Model</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Denah Kursi</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Kursi</small><br>
                    <strong>Economy</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Jarak Antar Kursi</small><br>
                    <strong>-</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="flight-detail flight-detail--end">
        <time class="time">
            <strong>01:00</strong><br>
            19 Mar
        </time>
        <div class="content">
            Haneda Airport (Tokyo) (HND)
        </div>
    </div>


    <h3 class="mb-0 text-blue">Pulang</h3>
    
    <div class="flight-detail flight-detail--start">
        <time class="time">
            <strong>22:50</strong><br>
            20 Mar
        </time>
        <div class="content">
            Haneda Airport (Tokyo) (HND)
        </div>
    </div>
    <div class="flight-detail flight-detail--duration">
        <time class="time">07j 35m</time>
        <div class="content">
            <div class="mb-half">
                <img src="https://static.his-travel.co.id/files/airlinelogos/20191125073810-singapore-airlines.png" alt="" width="36"> SQ-635, Economy<br>
                Dioperasikan oleh: Singapore Airlines
            </div><hr class="mb">
            <div class="mb-2">
            <div class="mb-small nowrap">
                <span class="his-koper"></span>
                Bagasi 30 K
            </div>
            
            </div>
            <div class="bzg">
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Model</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Denah Kursi</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Kursi</small><br>
                    <strong>Economy</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Jarak Antar Kursi</small><br>
                    <strong>-</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="flight-detail flight-detail--end">
        <time class="time">
            <strong>05:25</strong><br>
            21 Mar
        </time>
        <div class="content">
            Changi Airport (Singapore) (SIN)
        </div>
    </div>

<div class="mb-half block--inset-small fill-lightgrey">
    Transit for 02j 15m in Changi Airport (Singapore) (SIN)
</div>
    <div class="flight-detail flight-detail--start">
        <time class="time">
            <strong>07:40</strong><br>
            21 Mar
        </time>
        <div class="content">
            Changi Airport (Singapore) (SIN)
        </div>
    </div>
    <div class="flight-detail flight-detail--duration">
        <time class="time">01j 45m</time>
        <div class="content">
            <div class="mb-half">
                <img src="https://static.his-travel.co.id/files/airlinelogos/20191125073810-singapore-airlines.png" alt="" width="36"> SQ-952, Economy<br>
                Dioperasikan oleh: Singapore Airlines
            </div><hr class="mb">
            <div class="mb-2">
            <div class="mb-small nowrap">
                <span class="his-koper"></span>
                Bagasi 30 K
            </div>
            
            </div>
            <div class="bzg">
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Model</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Denah Kursi</small><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Kursi</small><br>
                    <strong>Economy</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <small>Jarak Antar Kursi</small><br>
                    <strong>-</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="flight-detail flight-detail--end">
        <time class="time">
            <strong>08:25</strong><br>
            21 Mar
        </time>
        <div class="content">
            Soekarno Hatta International Airport (Jakarta) (CGK)
        </div>
    </div>

</div>
            <div class="panel-content t--smaller" id="pricesDetailInfo">
<div class="bzg">
    <div class="bzg_c" data-col="m6"><table class=" table--zebra"><tbody>
        
<tr><td>Adult Fare <small>tax included</small> (x1)</td>
<td class="text-right">IDR 14.077.000</td></tr>



<tr><td><strong>Total Fare</strong></td><td class="text-right"><strong class="text-red">IDR 14.077.000</strong></td></tr>
    </tbody></table></div></div></div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {

        setTimeout(function() {
            $('#searchSession').addClass('is-active')
        }, 60000);
    }
</script>

<?php include $path.'_partials/scripts.php'; ?>
