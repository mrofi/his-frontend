<?php 
    $path = '../';
?>

<?php include $path.'_partials/head.php'; ?>
<?php include $path.'_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Penerbangan</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <ol class="block list-nostyle text-center text-up steps">
            <li class="steps__item current">
                <span>Penerbangan</span>
            </li>
            <li class="steps__item">
                <span>Isi Data</span>
            </li>
            <li class="steps__item">
                <span>Pembayaran</span>
            </li>
            <li class="steps__item">
                <span>Selesai</span>
            </li>
        </ol>
    </div>

    <section class="fill-lightgrey pt pb">
        <div class="container container--smaller">
            <div class="mb-small flex a-bottom">
                <div class="fg-1">
                    <h1 class="mb-small text-red text-up">
                        <span class="in-block line--small t--small">
                            Jakarta (CGK)
                            <span class="fa fa-exchange"></span>
                            Tokyo (HND)
                        </span>
                    </h1>
                    <div class="line--small t--smaller">
                        <h3 class="mb-small">Pilih Penerbangan</h3>
                        Jakarta, Soekarno Hatta (CGK) ke Tokyo, Haneda Airport (HND)<br>
                        Minggu, 6 Agustus 2019 | 1 Dewasa, 0 Anak, 0 Bayi | Ekonomi
                    </div>
                </div>
                <button class="btn btn--round btn--ghost-red toggle-trigger text-up" type="button" data-target="#research_booking">
                    <strong>Ganti Pencarian</strong>
                </button>
            </div>
        </div>
    </section>

    <div class="container container--smaller" data-flight="flight10.json" 
    data-transit="" 
    data-origin="#originRes" 
    data-return="#returnRes"
    data-loader="#flightLoader"
    data-airline="">
        
        
        <div class="block">
            
            <?php include 'research-form.php'; ?>

            <div class="nprogress-wrapper"></div>
        </div>

        <div class="bzg res-container-wrapper res--origin" id="originRes">
            <div class="bzg_c res-summary" data-col="l4">
                <div id="selectedFlight0" class="block selected-flight"></div>
                <!-- <div id="selectedFlight1" class="block selected-flight"></div> -->
            </div>
            <div class="bzg_c" data-col="">

                <form action="">
                <nav class="block row-sort">
                    <ul class="list-nostyle">
                        <li class="label-nav">
                            <span class="in-block">Urutkan:</span>
                        </li>
                        <li class="input-switch">
                            <label class="btn btn-sort is-desc" for="FlightStart_asc">
                                <input type="radio" name="sort" value="FlightStart_asc" id="FlightStart_asc">
                                Keberangkatan
                            </label>
                            <label class="btn btn-sort is-asc hidden" for="FlightStart_desc">
                                <input type="radio" name="sort" value="FlightStart_desc" id="FlightStart_desc">
                                Keberangkatan
                            </label>
                        </li>
                        <li class="input-switch">
                            <label class="btn btn-sort is-desc" for="FlightDuration_asc">
                                <input type="radio" name="sort" value="FlightDuration_asc" id="FlightDuration_asc">
                                Durasi
                            </label>
                            <label class="btn btn-sort is-asc hidden" for="FlightDuration_desc">
                                <input type="radio" name="sort" value="FlightDuration_desc" id="FlightDuration_desc">
                                Durasi
                            </label>
                        </li>
                        <li class="input-switch is-selected">
                            <label class="btn btn-sort is-desc hidden" for="TotalFare_asc">
                                <input type="radio" name="sort" value="TotalFare_asc" id="TotalFare_asc" checked="checked">
                                Harga
                            </label>
                            <label class="btn btn-sort is-asc" for="TotalFare_desc">
                                <input type="radio" name="sort" value="TotalFare_desc" id="TotalFare_desc">
                                Harga
                            </label>
                        </li>
                    </ul>
                    <ul class="list-nostyle">
                        <li class="hidden">
                            <input type="hidden" name="filterFlight" class="filter-store" id="filterStore">
                        </li>
                        <li class="label-nav">
                            <span class="in-block">Filter:</span>
                        </li>
                        <li data-toggle>
                            <button class="btn btn-filter btn-toggle" type="button" >
                                Waktu
                            </button>
                            <ul class="list-nostyle toggle-panel time-filter" 
                            id="FlightTimeGroup">
                                <li class="hidden">
                                    <input type="hidden" name="DepartureTimeGroup" class="filter-store" 
                                    id="DepartureTimeStore">
                                    <input type="hidden" name="ArrivalTimeGroup" class="filter-store" 
                                    id="ArivalTimeStore">
                                </li>
                                <li>
                                    <div class="bzg">
                                        <ul class="bzg_c mb-0 multi-filter" data-col="s6" data-input="#DepartureTimeGroup">
                                            <li>
                                                <label for="g0000_0060_0" class="btn-label">
                                                    <input type="checkbox" name="DepartureTimeGroup" class="input-check" id="g0000_0060_0" value="00.00-06.00">
                                                    <span>00.00 - 06.00</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label for="g0060_1200_0" class="btn-label">
                                                    <input type="checkbox" name="DepartureTimeGroup" class="input-check" id="g0060_1200_0" value="06.00-12.00">
                                                    <span>06.00 - 12.00</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label for="g1200_1800_0" class="btn-label">
                                                    <input type="checkbox" name="DepartureTimeGroup" class="input-check" id="g1200_1800_0" value="12.00-18.00">
                                                    <span>12.00 - 18.00</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label for="g1800_0000_0" class="btn-label">
                                                    <input type="checkbox" name="DepartureTimeGroup" class="input-check" id="g1800_0000_0" value="18.00-00.00">
                                                    <span>18.00 - 00.00</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <ul class="bzg_c mb-0 multi-filter" data-col="s6" data-input="#ArrivalTimeGroup">
                                            <li>
                                                <label for="g0000_0060_1" class="btn-label">
                                                    <input type="checkbox" name="ArrivalTimeGroup" class="input-check" id="g0000_0060_1" value="00.00-06.00">
                                                    <span>00.00 - 06.00</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label for="g0060_1200_1" class="btn-label">
                                                    <input type="checkbox" name="ArrivalTimeGroup" class="input-check" id="g0060_1200_1" value="06.00-12.00">
                                                    <span>06.00 - 12.00</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label for="g1200_1800_1" class="btn-label">
                                                    <input type="checkbox" name="ArrivalTimeGroup" class="input-check" id="g1200_1800_1" value="12.00-18.00">
                                                    <span>12.00 - 18.00</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label for="g1800_0000_1" class="btn-label">
                                                    <input type="checkbox" name="ArrivalTimeGroup" class="input-check" id="g1800_0000_1" value="18.00-00.00">
                                                    <span>18.00 - 00.00</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li data-toggle>
                            <button class="btn btn-filter btn-toggle" type="button" >
                                Transit
                            </button>
                            <ul class="list-nostyle toggle-panel transit-filter multi-filter" 
                            id="transit1" data-input="#transitStore">
                                <li class="hidden">
                                    <input type="hidden" name="FlightTransit" class="filter-store" id="transitStore">
                                </li>
                                <li>
                                    <label class="btn-label" for="all_transit_0">
                                        <input class="input-check input-check--all" type="checkbox" id="all_transit_0" 
                                        name="filter_transit_0"  value="-1" data-check="relCheck0">
                                        <span>Pilih Semua</span>
                                    </label>
                                </li>
                            </ul>
                        </li>
                        <li data-toggle>
                            <button class="btn btn-filter btn-toggle" type="button">
                                Maskapai
                            </button>
                            <ul class="list-nostyle toggle-panel airline-filter multi-filter" 
                            id="airplane1" data-input="#airlineStore">
                                <li class="hidden">
                                    <input type="hidden" name="FlightAirline" class="filter-store" id="airlineStore">
                                </li>
                                <li>
                                    <label class="btn-label" for="all_airline_0" title="Semua">
                                        <input class="input-check input-check--all" type="checkbox" id="all_airline_0" 
                                        name="airline_0" value="-1" data-index="-1" data-check="relCheckAirplane0">
                                        <span class="text-ellipsis">Pilih Semua</span>
                                    </label>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- NAV SORT -->
                </form>

                <div class="res-container">
                    
                </div>
                <!-- Result Container -->
                <!-- <div class="text-center">
                    <div class="loader" id="flightLoader">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div> -->

                <div class="inline-msg inline-msg--static hidden" id="flightNotFound">
                    <div class="container">
                        <div class="inline-msg-content">
                            <span class="fa fa-warning"></span> Flight not found!
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bzg res-container-wrapper res--return" id="returnRes">
            <div class="bzg_c res-summary" data-col="l4">
                <!-- <div id="selectedFlight0" class="block selected-flight"></div> -->
                <!-- <div id="selectedFlight1" class="block selected-flight"></div> -->
            </div>
            <div class="bzg_c" data-col="l8">
                <div class="block line--small t--smaller flight-search-head flex a-start">
                    <span class="fa fa-plane fa-2x fa-rotate-180 text-blue"></span>
                    <div class="ml-half fg-1">
                        <h3 class="mb-small">Pilih Penerbangan Pulang</h3>
                        Jakarta, Soekarno Hatta (CGK) ke Tokyo, Haneda Airport (HND)<br>
                        Minggu, 6 Agustus 2019 | 1 Dewasa, 0 Anak, 0 Bayi | Ekonomi
                    </div>
                </div>

                <form action="">
                <nav class="block row-sort">
                    <ul class="list-nostyle">
                        <li class="label-nav">
                            <span class="in-block">Urutkan:</span>
                        </li>
                        <li class="input-switch">
                            <label class="btn btn-sort is-desc" for="FlightStart_asc1">
                                <input type="radio" name="sort" value="FlightStart_asc" id="FlightStart_asc1">
                                Keberangkatan
                            </label>
                            <label class="btn btn-sort is-asc hidden" for="FlightStart_desc1">
                                <input type="radio" name="sort" value="FlightStart_desc" id="FlightStart_desc1">
                                Keberangkatan
                            </label>
                        </li>
                        <li class="input-switch">
                            <label class="btn btn-sort is-desc" for="FlightDuration_asc1">
                                <input type="radio" name="sort" value="FlightDuration_asc" id="FlightDuration_asc1">
                                Durasi
                            </label>
                            <label class="btn btn-sort is-asc hidden" for="FlightDuration_desc1">
                                <input type="radio" name="sort" value="FlightDuration_desc" id="FlightDuration_desc1">
                                Durasi
                            </label>
                        </li>
                        <li class="input-switch is-selected">
                            <label class="btn btn-sort is-desc hidden" for="TotalFare_asc1">
                                <input type="radio" name="sort" value="TotalFare_asc" id="TotalFare_asc1" checked="checked">
                                Harga
                            </label>
                            <label class="btn btn-sort is-asc" for="TotalFare_desc1">
                                <input type="radio" name="sort" value="TotalFare_desc" id="TotalFare_desc1">
                                Harga
                            </label>
                        </li>
                    </ul>
                    <ul class="list-nostyle">
                        <li class="label-nav">
                            <span class="in-block">Filter:</span>
                        </li>
                        <li data-toggle>
                            <button class="btn btn-filter btn-toggle" type="button" >
                                Transit
                            </button>
                            <ul class="list-nostyle toggle-panel transit-filter multi-filter" id="transit2" data-input="#transitStore1">
                                <li class="hidden">
                                    <input type="hidden" name="FlightTransit" class="filter-store" id="transitStore1">
                                </li>
                                <li>
                                    <label class="btn-label" for="all_transit_0">
                                        <input class="input-check input-check--all" type="checkbox" id="all_transit_1" name="filter_transit_1"  value="-1">
                                        <span>Pilih Semua</span>
                                    </label>
                                </li>
                            </ul>
                        </li>
                        <!-- <li data-toggle>
                            <button class="btn btn-filter btn-toggle" type="button">
                                Maskapai
                            </button>
                            <ul class="list-nostyle toggle-panel airline-filter" id="airplane2">
                                
                            </ul>
                        </li> -->
                    </ul>
                </nav>
                <!-- NAV SORT -->
                </form>
                <div class="res-container"></div>
            </div>
        </div>
    </div>
    
    <!-- <div class="container mb-2">
        <button data-target-content="#btn123" type="button" class="btn">btns</button>
        <div class="tip-content" id="btn123">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat facilis inventore, animi itaque perspiciatis, a consequatur odit, sed, autem minus voluptatum! Quibusdam nobis consequuntur provident doloribus culpa? Provident, natus, consectetur.</p>
        </div>
    </div>
    
    <div class="container clearfix mb-2">
        <button data-target-content="#btn1234" type="button" class="btn" style="float: right">btns</button>
        <div class="tip-content" id="btn1234">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat facilis inventore, animi itaque perspiciatis, a consequatur odit, sed, autem minus voluptatum! Quibusdam nobis consequuntur provident doloribus culpa? Provident, natus, consectetur.</p>
        </div>
    </div>
    <div class="container text-center">
        <button data-target-content="#btn1235" type="button" class="btn">btns</button>
        <div class="tip-content" id="btn1235">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat facilis inventore, animi itaque perspiciatis, a consequatur odit, sed, autem minus voluptatum! Quibusdam nobis consequuntur provident doloribus culpa? Provident, natus, consectetur.</p>
        </div>
    </div> -->
</main>

<?php include $path.'_partials/footer.php'; ?>
</div>
<div class="modal modal-book-confirm" id="flightsChoosenConfirm">
    <div class="overlay"></div>
    <div class="modal-container modal-container--large">
        <div class="modal-head flex a-start">
            <h3 class="title mb-0 fg-1">Detail Perjalanan</h3>
            <button class="btn-close" type="button" aria-label="Close">
                <span class="fa fa-times" aria-hidden="true"></span>
            </button>
        </div>
        <div class="modal-content" id="flightsDetailConfirm">
            
        </div>
        <div class="modal-footer">
            <div class="flex a-center">
                <nav class="fg-1">
                    <a href="#flightsDetailInfo" data-panel-target="#flightsDetailInfo">
                        Detail Penerbangan <span class="fa fa-caret-down"></span>
                    </a>
                    <a href="#pricesDetailInfo" data-panel-target="#pricesDetailInfo">
                        Detail Harga <span class="fa fa-caret-down"></span>
                    </a>
                </nav>
                <div class="book-action">
                    <div class="total-flight-price mb-0 h3 text-red ml-small mr-small">IDR <span></span></div>
                    <form action="" id="formFlight">
                        <input type="hidden" data-search="" name="searchid">
                        <input type="hidden" data-flight="" name="flightsid">
                        <input type="hidden" data-ps="" name="flightpassenger">
                        <button class="btn btn--round btn--red btn-book" type="button" data-search="" data-flight="" data-ps="" id="btnBookFlights">
                            <strong>Book</strong>
                        </button>
                    </form>
                </div>
            </div>
            <div class="panel-content t--smaller" id="flightsDetailInfo"></div>
            <div class="panel-content t--smaller" id="pricesDetailInfo"></div>
        </div>
    </div>
</div>

<!-- <div class="dialog session-timeout" id="searchSession" data-timeout="1"> -->
<div class="dialog session-timeout" id="searchSession">
    <div class="dialog-overlay"></div>
    <div class="dialog-box">
        <header class="text-up">Halaman Kedaluarsa</header>
        <main>
            Hasil pencarian Anda telah kedaluarsa.<br>
            Silahkan klik Refresh untuk ulangi pencarian.
        </main>
        <footer class="text-right">
            <button class="btn btn--round btn--red btn-refresh">Refresh</button>
            <!-- <butoon class="btn btn--round btn-cancel">${no}</butoon> -->
        </footer>
    </div>
</div>

<script>
    window.onload = function () {
        $('#btnBookFlights').click(function(e) {
            e.preventDefault();
            Site._dialog('Yakin ?', 'Title is', 'formFlight', 'form', 'OK', 'Tidak');
        });

        // setTimeout(function() {
        //     $('#searchSession').addClass('is-active')
        // }, 600000);
    }
</script>
<script>
    var cachePath = '../';
</script>
<?php include $path.'_partials/scripts2.php'; ?>
