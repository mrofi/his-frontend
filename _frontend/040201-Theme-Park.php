<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Aktivitas</a></li>
                <li><a href="#">Theme Park</a></li>
                <li><a href="#">Huis Ten Bosch</a></li>
            </ul>
        </div>
    </div>

    
    <div class="container container--smaller">
        <h1 class="block--small">Huis Ten Bosch</h1>
        <div class="bzg">
            <div class="bzg_c" data-col="m8">
                <section class="section-block--smaller">
                    <div class="responsive-media media--3-2">
                        <div class="demihero-slider slide-default">
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-1.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-2.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- demihero-slider -->
                    </div>
                </section>
                <section class="section--block one-page-nav-container">
                    <nav class="inpage-nav block fill--overlap fill-lightgrey ">
                        <ul class="list-nostyle navs--inline text-up one-page-nav" id="jrPassNav">
                            <li class="inpage-nav__item nav__item current">
                                <a href="#description">Deskripsi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#atraksi">Atraksi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#type_route">Tipe &amp; Rute</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#regional_pass">Regional Pass</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#faq">Faq</a>
                            </li>
                        </ul>
                    </nav>
                    <article>
                        <div class="block">
                            <?php include '_partials/activity/description.php'; ?>
                            <?php include '_partials/activity/atraksi.php'; ?>
                            <?php include '_partials/activity/how_to_use.php'; ?>
                            <?php include '_partials/activity/harga.php'; ?>
                            <?php include '_partials/activity/kebijakan.php'; ?>
                        </div>
                    </article>
                </section>
            </div>
            <div class="bzg_c" data-col="m4" data-sticky-container>
                <div class="cards sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="120">
                    <div class="card__item">
                        <div class="card-head cf block--inset fill-yellow">
                            <strong class="in-block">mulai dari</strong>
                            <strong class="t--large pull-right">
                                IDR 264.500
                            </strong>
                        </div>
                        <div class="block--inset">
                            <form action="" class="form form--line">
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-park"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            1 Day Pass
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label>
                                        <input type="text" class="form-input form-input--block pickaDay" id="departure_start" placeholder="Tanggal Berangkat">
                                    </div>
                                </div>
                                <div class="">
                                    <div class="input-iconic--left block--half">
                                        <label for="" class="label-icon">
                                            <span class="his-user-group"></span>
                                        </label>
                                        <div class="pick-guest fg-1" id="pickJRpassPsg" data-toggle>
                                            <button class="form-input form-input--block btn-toggle" type="button">
                                                <strong class="text-ellipsis">
                                                    <span class="total-input--all" id="total_psgjr">1</span> Traveller(s)
                                                </strong>
                                            </button>
                                            <div class="toggle-panel t-black fill-white">
                                                <div class="block--inset-small">
                                                    <div class="form__row flex v-ct qty-input">
                                                        <label for="" class="fg-1">
                                                            <span class="total-input">1</span> Adult
                                                        </label>
                                                        <div class="">
                                                            <button class="btn btn--plain btn--plain-red" data-action="minus" type="button" data-age="adult">
                                                                <span class="fa fa-minus"></span>
                                                            </button>
                                                            <input type="number" class="form-input form-input--redline text-center input-adult" 
                                                            value="1" min="1" max="6" data-psg="total_psgjr">
                                                            <button class="btn btn--plain btn--plain-red" data-action="plus" type="button" data-age="adult">
                                                                <span class="fa fa-plus"></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <hr class="block--half">
                                                    <div class="form__row flex v-ct qty-input">
                                                        <label for="" class="fg-1">
                                                            <span class="total-input">0</span> Children
                                                        </label>
                                                        <div class="">
                                                            <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                                <span class="fa fa-minus"></span>
                                                            </button>
                                                            <input type="number" class="form-input form-input--redline text-center" 
                                                            value="0" min="0" max="6" data-psg="total_psgjr">
                                                            <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                                <span class="fa fa-plus"></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <hr class="block--half">
                                                    <div class="form__row flex v-ct qty-input">
                                                        <label for="" class="fg-1">
                                                            <span class="total-input">0</span> Infant
                                                        </label>
                                                        <div class="">
                                                            <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="infant" type="button">
                                                                <span class="fa fa-minus"></span>
                                                            </button>
                                                            <input type="number" class="form-input form-input--redline text-center input-infant" 
                                                            value="0" min="0" max="9" data-psg="total_psg">
                                                            <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="infant" type="button">
                                                                <span class="fa fa-plus"></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <hr class="block--half">
                                                    <div class="text-right">
                                                        <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                                            Pilih
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- pick-guest -->
                                    </div>
                                </div>
                                <hr>
                                <button class="btn btn--round btn--block btn--red" type="submit">
                                    <b class="text-up">Pesan Sekarang</b>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Produk Terkait
                </h2>
            </div>
            <div class="block content-section">
                <div class="bzg cards cards--blue slide-3">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square">
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/400x400" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
