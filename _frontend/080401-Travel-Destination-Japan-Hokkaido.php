<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destination</a></li>
                <li><a href="#">Asia</a></li>
                <li><a href="#">Japan</a></li>
                <li><a href="#">Hokkaido</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <section class="section-block">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
            </figure>
            <div class="text-center">
                <h1 class="h3">Hokkaido</h1>
                <p class="t--larger">
                    Hokkaido merupakan prefektur terbesar di Jepang. Kota ini juga dikenal dengan musim dinginnya yang sangat panjang, diantara prefektur lainnya di Jepang, dan salah satu festival yang terkenal disaat musim dingin tiba adalah Sapporo Snow Festival. Selain terkenal dengan berbagai tempat wisata yang menarik, Prefektur Hokkaido juga sangat terkenal dengan berbagai hidangan Seafoodnya yang super lezat, terutama Kepitingnya yang memiliki ukuran yang lebih besar
                </p>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h3 class="no-space text-up in-block">
                    <span class="title-text text-up t--larger">Destinasi di Hokkaido</span>
                </h3>
            </div>
            <div class="block post inset-on-m">
                <div class="block post__item bzg">
                    <h3 class="bzg_c block--small">
                        <a href="#" class="link-black t--larger">Sapporo</a>
                    </h3>
                    <div class="bzg_c" data-col="m4">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m8">
                        <div class="block">
                            <strong>Jakarta, 23 Agustus 2017</strong> - Cool Japan Travel Fair merupakan acara travel fair tunggal yang diselenggarakan oleh H.I.S. Travel Indonesia. Event yang pertama kali diadakan pada 16 – 18 September 2016 ini berhasil mendapat sambutan positif dari masyarakat Jakarta dan sekitarnya. Hal ini terbukti dengan suksesnya acara ini mendatangkan tidak kurang dari 195.000 pengunjung selama 3 hari acara berlangsung.
                        </div>
                        <div class="block text-right">
                            <a href="#" class="btn btn--round btn--ghost-red text-up">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <div class="block post__item bzg">
                    <h3 class="bzg_c block--small">
                        <a href="#" class="link-black t--larger">Sapporo</a>
                    </h3>
                    <div class="bzg_c" data-col="m4">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m8">
                        <div class="block">
                            <strong>Jakarta, 23 Agustus 2017</strong> - Cool Japan Travel Fair merupakan acara travel fair tunggal yang diselenggarakan oleh H.I.S. Travel Indonesia. Event yang pertama kali diadakan pada 16 – 18 September 2016 ini berhasil mendapat sambutan positif dari masyarakat Jakarta dan sekitarnya. Hal ini terbukti dengan suksesnya acara ini mendatangkan tidak kurang dari 195.000 pengunjung selama 3 hari acara berlangsung.
                        </div>
                        <div class="block text-right">
                            <a href="#" class="btn btn--round btn--ghost-red text-up">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <!-- item -->
            </div>
            <ol class="pagination text-right">
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-left"></span>
                    </a>
                </li>
                <li>
                    <a href="#" class="active">
                        1
                    </a>
                </li>
                <li>
                    <a href="#">
                        2
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-right"></span>
                    </a>
                </li>
            </ol>
        </section>
        <div class="block bzg">
            <div class="block--half bzg_c" data-col="m6">
                <a href="#">
                    <img src="assets/img/banner-jtd.jpg" alt="">
                </a>
            </div>
            <div class="block--half bzg_c" data-col="m6">
                <a href="#">
                    <img src="assets/img/banner-jrm.jpg" alt="">
                </a>
            </div>
        </div>
        <hr>
    </div>
    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
