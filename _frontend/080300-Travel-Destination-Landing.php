<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destination</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <h1 class="sr-only">Travel Destination</h1>
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="text-center">
                <h2>SEMUA INFORMASI TEMPAT TERBAIK DI ASIA</h2>
                <p class="t--larger">
                    Jelajahi kota-kota terbaik di Asia, yang penuh dengan budaya, pemandangan alam dan sejarah yang mengesankan bersama H.I.S. Travel Indonesia.<br>
                    Temukan juga berbagai paket tour dari H.I.S. Travel Indonesia yang terdiri dari tempat wisata terkenal, makanan lokal yang lezat, serta transportasi yang nyaman.
                </p>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Pilih Destinasi
                </h2>
            </div>
            <div class="inset-on-m">
            <div class="bzg">
                <?php for ($i=1; $i <= 5; $i++) { ?>
                <div class="block bzg_c" data-col="m3, l4">
                    <div class="card__item card__item--overlayed-reverse">
                        <a href="#">
                            <figure class="item-img img-square">
                                <img src="assets/img/img-preload.png" data-src="https://his-travel.co.id/files/continentcovers/088f003833d523d9dccc529e929afdc7-20180405101544-america_thumbnail256x256.jpg" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <div class="text-center text-up">
                                        <h3 class="title no-space">Selengkapnya</h3>
                                    </div>
                                </div>
                                <figcaption>Australia</figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
