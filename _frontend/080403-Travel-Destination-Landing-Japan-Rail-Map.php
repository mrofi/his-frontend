<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destination</a></li>
                <li><a href="#">Asia</a></li>
                <li><a href="#">Jepang</a></li>
                <li><a href="#">Rail Map</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="text-center">
                <h1 class="sr-only">Japan Rail Map</h1>
                <h2>SEMUA INFORMASI TEMPAT TERBAIK DI ASIA</h2>
                <p class="t--larger">
                    Jelajahi kota-kota terbaik di Asia, yang penuh dengan budaya, pemandangan alam dan sejarah yang mengesankan bersama H.I.S. Travel Indonesia.<br>
                    Temukan juga berbagai paket tour dari H.I.S. Travel Indonesia yang terdiri dari tempat wisata terkenal, makanan lokal yang lezat, serta transportasi yang nyaman.
                </p>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Pilih Destinasi
                </h2>
            </div>
            <div class="inset-on-m">
                <div class="bzg cards cards--plain imgPopup">
                    <div class="block bzg_c" data-col="m6">
                        <div class="card__item card__item--overlayed-reverse">
                            <a href="assets/img/bg-broken-page-desktop.jpg">
                                <figure class="item-img img-3-2 fill-lightgrey">
                                    <img src="assets/img/img-preload.png" data-src="//placehold.it/420x280" alt="Hokkaido Rail Map" class="item-heavy">
                                    <div class="item-detail">
                                        <div class="text-center text-up">
                                            <h3 class="title no-space">
                                                <span class="fa fa-2x fa-search-plus"></span>
                                            </h3>
                                        </div>
                                    </div>
                                </figure>
                            </a>
                            <div class="item-text text-center">
                                <strong class="text-up">
                                    Hokkaido Rail Map
                                </strong>
                            </div>
                        </div>
                    </div>
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m6">
                        <div class="card__item card__item--overlayed-reverse">
                            <a href="assets/img/bg-why.jpg">
                                <figure class="item-img img-3-2 fill-lightgrey">
                                    <img src="assets/img/img-preload.png" data-src="//placehold.it/420x280" alt="Hokkaido Rail Map" class="item-heavy">
                                    <div class="item-detail">
                                        <div class="text-center text-up">
                                            <h3 class="title no-space">
                                                <span class="fa fa-2x fa-search-plus"></span>
                                            </h3>
                                        </div>
                                    </div>
                                </figure>
                            </a>
                            <div class="item-text text-center">
                                <strong class="text-up">
                                    Hokkaido Rail Map
                                </strong>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="block bzg_c" data-col="m6">
                        <div class="card__item card__item--overlayed-reverse">
                            <a href="assets/img/bg-broken-page-mobile.jpg">
                                <figure class="item-img img-3-2 fill-lightgrey">
                                    <img src="assets/img/img-preload.png" data-src="//placehold.it/420x280" alt="Tokyo Rail Map" class="item-heavy">
                                    <div class="item-detail">
                                        <div class="text-center text-up">
                                            <h3 class="title no-space">
                                                <span class="fa fa-2x fa-search-plus"></span>
                                            </h3>
                                        </div>
                                    </div>
                                </figure>
                            </a>
                            <div class="item-text text-center">
                                <strong class="text-up">
                                    Tokyo Rail Map 2
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="bzg">
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jtd.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jrm.jpg" alt="">
                    </a>
                </div>
            </div>
            <hr>
        </div>
    </section>
    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
