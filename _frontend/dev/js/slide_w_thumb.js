import * as _ from './utils'

export default {
	slide_w_thumb() {
		_.existJquery($('.slide-w-thumb')).then($slider => {
            let slideView   = '.slide-view'
			let $slideView  = $(slideView)
            let slideThumb  = '.slide-thumb'
            let $slideThumb = $(slideThumb)

            $slideView.slick({
                asNavFor: slideThumb,
                lazyLoad: 'ondemand',
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: _.slickPrev,
                nextArrow: _.slickNext
            })

            $slideThumb.slick({
                asNavFor: slideView,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                focusOnSelect: true,
                centerMode: true
            })
		}).catch(e=>{})
	}
}