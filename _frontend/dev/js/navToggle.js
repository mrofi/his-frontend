import * as _ from './utils'

export default {
	navToggle() {
		let dataToggle  = '[data-toggle]'
        _.existJquery($('[data-toggle]')).then($toggler => {
            let $current    = $('.current')
            let $overlay    = '<div class="nav-overlay"></div>'
            // console.log($toggler.length)
            // _.log(document.body.offsetHeight)

            // $toggler.off('click', '.btn-toggle:not(.is-disabled)', e => {
            //     btnToggle(e)
            //     e.preventDefault()
            // })

            $(document).on('click', '.btn-toggle:not(.is-disabled)', e => {
                btnToggle(e)
                // console.log(e)
                
            })

            function btnToggle(e) {
                // $toggler.removeClass(_.classes.isActive)
                let $this   = $(e.currentTarget)
                let $parent = $this.closest(dataToggle)
                let $isLabel = $this.attr('for')

                if(!$isLabel) {
                    e.preventDefault()
                }

                // _.log('click')
                
                
                if($parent.hasClass(_.classes.isActive)) {
                    $parent.removeClass(_.classes.isActive)
                    $('.nav-overlay').remove()
                    mainNavInactive($parent)
                    // console.log('has')
                } else {
                    $(dataToggle).removeClass(_.classes.isActive)
                    $parent.addClass(_.classes.isActive)
                    $parent.append($overlay)
                    mainNavActive($parent)
                    // console.log('No')
                }
                // $parent.addClass(_.classes.isActive)
                //     $parent.append($overlay)
                //     mainNavActive($parent)
            }

            $toggler.on('click', '.nav-overlay', e=> {
                closeToggle()
            })

            $toggler.on('click', '.btn-done', e=> {
                closeToggle()
            })

            _.$doc.keyup(function(e) {
                if(_.escKeyPress(e)) {
                    closeToggle()
                }
            })

            function closeToggle() {
                if($(dataToggle).hasClass(_.classes.isActive)) {
                    $(dataToggle).removeClass(_.classes.isActive)
                    $(dataToggle).find('.nav-overlay').remove()
                    mainNavInactive($(dataToggle))
                }
            }

            for (var i = 0; i < $current.length; i++) {
                let $this       = $current.eq(i)
                let $triggerTxt = $this.closest(dataToggle).find('.trigger-txt')
                let curTxt      = $this.children().text()

                $triggerTxt.text(curTxt)
            }

            function mainNavActive(element) {
                if(element.hasClass('lock-body')) {
                    $('body').addClass(_.classes.noScroll)
                }
            }

            function mainNavInactive(element) {
                if(element.hasClass('lock-body')) {
                    $('body').removeClass(_.classes.noScroll)
                }
            }
        }).catch(e=>{})
	}
}