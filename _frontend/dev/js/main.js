import * as _ from './utils'
import installApp from './installApp'
import activeStateMobile from './activeStateMobile'
import WPViewportFix from './windowsPhoneViewportFix'
import objectFitPolyfill from './objectFitPolyfill'
import headroom from './headroom'
import navToggle from './navToggle'
import toggleTarget from './toggleTarget'
import subMainNav from './subMainNav'
import heroSlider from './heroSlider'
import selectStyle from './selectStyle'
import datePick from './datePick'
import datePickRange from './datePickRange'
import tabNav from './tabNav'
import promoText from './promoText'
import whySlider from './whySlider'
import slide5 from './slide5'
import slide3 from './slide3'
import slide2 from './slide2'
import slide3max from './slide3-max'
import slideOnMobile from './slide-on-mobile'
import slide from './slide'
import slide_w_thumb from './slide_w_thumb'

import countStats from './countStats'
import slideVideo from './slideVideo'
import lazyLoad from './lazyLoad'
import accordeon from './accordeon'
import onePageNav from './onePageNav'
import btnToTop from './btnToTop'
import infiniteScroll from './infiniteScroll'
import flightSearch from './flightSearch'
import stickyElements from './stickyElements'
import tripType from './tripType'
import selectFilter from './selectFilter'
// import passengerPick from './passengerPick'
import qtyInput from './qtyInput-v2'
import qtyAjax from './qtyAjax'
import hisMap from './hisMap'
import bazeForm from './bazeFormValidate'
import hoverTab from './hoverTab'
import colList from './colList'
import offlineMode from './offlineMode'
import imgPopup from './imgPopup'
import dupliFormVal from './duplicateFormVal'
import addPerson from './addPerson'
import rating from './rating'
import preventMultipleSubmit from './preventMultipleSubmit'
import select2 from './select2'
import priceSlider from './priceSlider'
import videoEmbed from './videoEmbed'
import roomSearch from './roomSearch'
import scrollToElement from './scrollToElement'
import qrCode from './qrCode'
import getVoucher from './getVoucher'
import inputSwitch from './inputSwitch'
import multiFilter from './multiFilter'
import modal from './modal'
import sessionTimeout from './sessionTimeout'
import inputPeeker from './inputPeeker'
import fillUserData from './fillUserData'
import flightNavFilter from './flightNavFilter'
import autocomplete from './autocomplete'
import copyText from './copyText'
import addCountry from './addCountry'
import uploadCropp from './uploadCropp'
// import socialStory from './socialStory'

import _popMsg from './_popUpMsg'
import _dialog from './confirm'
import _showLoader from './_showLoader'
import _inlineMsg from './inlineMsg'

const App = {
    ...installApp,
    ...activeStateMobile,
    ...WPViewportFix,
    ...objectFitPolyfill,
    ...headroom,
    ...navToggle,
    ...toggleTarget,
    ...subMainNav,
    ...heroSlider,
    ...datePick,
    ...datePickRange,
    ...tabNav,
    ...promoText,
    ...whySlider,
    ...slide5,
    ...slide3,
    ...slide2,
    ...slide3max,
    ...slideOnMobile,
    ...slide,
    ...slide_w_thumb,

    ...selectStyle,
    ...slideVideo,
    ...lazyLoad,
    ...countStats,
    ...accordeon,
    ...onePageNav,
    ...btnToTop,
    ...hisMap,
    ...bazeForm,
    ...infiniteScroll,
    ...flightSearch,
    ...stickyElements,
    // ...tripType,
    ...selectFilter,
    ...hoverTab,
    // ...offlineMode,
    ...imgPopup,
    ...dupliFormVal,
    ...addPerson,
    ...rating,
    ...preventMultipleSubmit,
    ...priceSlider,
    ...select2,
    ...videoEmbed,
    ...roomSearch,
    ...scrollToElement,
    ...qrCode,
    ...getVoucher,
    ...inputSwitch,
    ...multiFilter,
    ...modal,
    ...sessionTimeout,
    ...inputPeeker,
    ...fillUserData,
    ...flightNavFilter,
    ...autocomplete,
    ...copyText,
    ...uploadCropp,
    // ...socialStory,
    // ...colList,
    // ...passengerPick,
    ...qtyInput,
    // ...qtyAjax,
    ..._popMsg,
    ..._dialog,
    ..._inlineMsg,
    ..._showLoader,
    ...addCountry
}

for (let fn in App) {
    if(fn[0] !== '_')
        App[fn]()
}

window.Site = App

export default App
