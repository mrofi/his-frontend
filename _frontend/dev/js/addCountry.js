import * as _ from './utils'

export default {
	country() {
		_.existJquery($('.add-country')).then($addCountry => {
			let $btnCountry 	= $('.btn-country')
			let radioCheck 	= '.btn-radio'

			$btnCountry.hover(
                function(){ 
                 $(this).addClass('hover');
                 $(this).prevAll().addClass('hover');
             },
             function(){ 
                 $(this).removeClass('hover');
                 $(this).prevAll().removeClass('hover');
             }
             )

            $addCountry.on('change', radioCheck, e => {
            	let $this 	= $(e.currentTarget)
                if($this.is(':checked')) {
                    $this.parent().siblings().removeClass('checked')
                    $this.parent().nextAll().removeClass('checked')
                    $this.parent().prevAll().addClass('checked')
                    $this.parent().addClass('checked')
                } else {
                    $this.parent().removeClass('checked')
                }
            })
        }).catch(e=>{})
	}
}