import * as _ from './utils'

export default {
	tripType() {
		_.existJquery($('[data-trip]')).then($tripType => {
			$tripType.on('change', e=> {
				let $this 		= $(e.currentTarget)
				let target  	= $this.attr('name')
				let $target 	= $('#'+target)
				let $targetInput= $target.find('input')
				let type 		= $this.data('trip')
				let oneWay 		= type === 'one_way'
				let roundTrip	= type === 'round_trip'
				let checked 	= $this.is(':checked')

				if(checked && oneWay) {
					$target.css('opacity', '0.5')
					$targetInput.attr('disabled', 'disabled');
					$targetInput.val(null)
				} else if(checked && roundTrip) {
					$target.css('opacity', '1')
					$targetInput.removeAttr('disabled');
				}
			})
		}).catch(e=>{})
	}
}