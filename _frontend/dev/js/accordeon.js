import * as _ from './utils'

export default {
	accordeon() {
		_.existJquery($('.accordeon')).then($accordeon => {
			$accordeon.on('click', '.accordeon-trigger', e => {
				let $this 	= $(e.currentTarget)
				let $item   = $this.parent()
				let $allItem= $item.siblings()

				if($item.hasClass(_.classes.isActive)) {
					$item.removeClass(_.classes.isActive)
				} else {
					$item.addClass(_.classes.isActive)
					$allItem.removeClass(_.classes.isActive)
				}
			})
		}).catch(e=>{})
	}
}