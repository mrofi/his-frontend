import * as _ from './utils'

export default {
	autocomplete() {
		console.log()
		let autoInput = '.autoinput'
		_.existJquery($(autoInput)).then($autoinput => {
			let keyDowns = 0
			let keyupTime
			let alphaSpace 	= /[a-zA-Z ]/g

			const markQuery = (q, str) => {
				if(str == null) return
				let lowerStr 	= str.toLowerCase()
				let lowerQ 		= q.toLowerCase()

				if(!lowerStr.includes(lowerQ)) {
					return str
				} else {
					let matchStart 	= lowerStr.indexOf(''+lowerQ+'')
					let matchEnd 	= matchStart + q.length - 1
					let beforeMatch = str.slice(0, matchStart)
					let matchText 	= str.slice(matchStart, matchEnd + 1)
					let afterMatch 	= str.slice(matchEnd + 1)
					let marked  	= `${beforeMatch.length ? `<span>${beforeMatch}</span>`:``}<mark>${matchText}</mark>${afterMatch.length ? `<span>${afterMatch}</span>`:``}`
					// console.log(q)
					return marked
				}
			}
			
			const templateItem = (data, q, i, inputID) => {
				let src
				let areaCode
				let locationInfo
				if (data.airportDisplay == null) {
					src = data.areaDisplay
					areaCode = src.iataCode
					locationInfo = `Semua Bandara di ${markQuery(q, src.location)}`
				} else {
					src = data.airportDisplay
					areaCode = src.areaCode
					locationInfo = markQuery(q, src.name)
				}
				
				return `<div class="sq-item" data-text="${src.location} (${data.code})" data-val="${data.code}" data-tab="${i}" data-field="#${inputID}">
                    ${markQuery(q, src.location)}, <span>${src.country}</span>
                    <div class="t--smaller text-grey">
                        <span>${markQuery(q, src.code)}</span> - ${locationInfo}
                    </div>
                </div>`
			}
			const templateRes = (data, q, inputID) => {
				let popular   = data.data.popularAirportOrArea
				let recommend = data.data.recommendationAirportOrArea
				let isNoResult= !popular.length && !recommend.length
				let popularCount = popular.length + 1
				let writeResult
				
				let noResult = `<div class="sq-item">Tidak ada hasil untuk '<strong>${q}</strong>'</div>`

				let hasResult = `<div class="sq-head">Kota atau Bandara Terpopuler</div>
					${popular.map((p, i) => `${templateItem(p, q, (i+1), inputID)}`).join('')}
					${recommend.length != 0 ? `
					<div class="sq-head">Rekomendasi Bandara</div>
					${recommend.map((r, i) => `${templateItem(r, q, (i + popularCount), inputID)}`).join('')}`:``}
				`

				if(isNoResult) {
					writeResult = noResult
				} else {
					writeResult = hasResult
				}

				return writeResult
			}
			
			const getData = (url, container, q, inputID) => {
				_.getJSON(url).then(data => {
					keyDowns = 0
					container.append(templateRes(data, q, inputID))
					container.parent(autoInput).addClass(_.classes.isActive)
				})
				.catch(_.noop())
			}

			const renderRes = (url, vals, container, inputID) => {
				clearTimeout(keyupTime)

				keyupTime = setTimeout(() => {
					// console.log(inputID)
					container.empty()
					// container.parent(autoInput).removeClass(_.classes.isActive)
					getData(url, container, vals, inputID)
				}, 500)
			}

			const setValue = (input, val, text, sqOpts) => {
				keyDowns = 0
				input.val(text)
				input.attr('value', text)
				sqOpts.empty()
				sqOpts.parent(autoInput).removeClass(_.classes.isActive)

				let $sender = input.siblings('.input-sender')
				$sender.val(val)
				$sender.attr('value', val)
			}

			$autoinput.on('click', '.btn-cancel', e => {
				let $this = $(e.currentTarget)
				e.preventDefault()

				console.log('cancel')

				$this.parent(autoInput).removeClass(_.classes.isActive)
			})

			$autoinput.on('click', 'input', e => {
				let $this 		= $(e.currentTarget)
				let $container 	= $($this.data('container'))
				let qSearchUrl  = $this.parent('.autoinput').data('input')
				let inputID 	= $this.attr('id')
				e.stopPropagation()

				$('.autoinput').removeClass(_.classes.isActive)

				// if(e.key !== 'ArrowDown' || e.key !== 'ArrowUp') {
					$this.select()
					renderRes(qSearchUrl, '', $container, inputID)
				// }
			})

			$autoinput.on('keyup input', 'input', e => {
				if(e.key === 'ArrowDown' || e.key === 'ArrowUp' || e.keyCode == 13) return
				let $this 		= $(e.currentTarget)
				let vals  		= $this.val()
				let $container 	= $($this.data('container'))
				let inputID 	= $this.attr('id')
				let qSearchUrl
				if(!vals.length) {
					qSearchUrl 	= $this.parent('.autoinput').data('input')
				} else {
					qSearchUrl 	= $this.data('input')
				}

				let newVals 	= vals.replace(/[^A-Za-z0-9]+|\s+/g, ' ').trim()

				let qSearch 	= {
					query : newVals
				}

				let newSearcUrl = `${qSearchUrl}?${$.param(qSearch)}`

				if(e.key !== 'ArrowDown' || e.key !== 'ArrowUp') {
					renderRes(newSearcUrl, vals, $container, inputID)
				}
			})

			$autoinput.on('keydown', 'input', e => {
				let $this = $(e.currentTarget)
				let target = $this.data('container')
				let $item = $(`${target} [data-tab]`)
				let item  = $item.length
				if (e.key === 'ArrowDown' && keyDowns < item) {
					keyDowns++
				}
				if (e.key === 'ArrowUp' && keyDowns > 1) {
					keyDowns--
				}
				
				$(`${target} [data-tab]`).removeClass(_.classes.isActive)
				$(`${target} [data-tab='${keyDowns}']`).addClass(_.classes.isActive)
			})

			$autoinput.on('click', '[data-tab]', e => {
				let $this 	= $(e.currentTarget)
				let vals 	= $this.data('val')
				let text 	= $this.data('text')
				let $input  = $($this.data('field'))
				let $sqItem = $($input.data('container'))
				e.stopPropagation()

				setValue($input, vals, text, $sqItem)
			})

			$autoinput.on('keydown', 'input', e => {
				if (e.keyCode == 13) {
					e.preventDefault()
					let $this = $(e.currentTarget)
					let target = $this.data('container')
					let $item = $(`${target} [data-tab].is-active`)
					let vals 	= $item.data('val')
					let text 	= $item.data('text')
					let $sqItem = $($this.data('container'))

					console.log(vals, text)

					setValue($this, vals, text, $sqItem)
				}
			})

			$('html').click(e => {

				if($('.autoinput').hasClass(_.classes.isActive)) {
					let $input = $('.autoinput.is-active .search-quick-res').siblings('[data-input]')
					let $sender = $('.autoinput.is-active .search-quick-res').siblings('.input-sender')

					$input.val($input.attr('value'))
					$sender.val($sender.attr('value'))
					$('.autoinput').removeClass(_.classes.isActive)
				}
			})
		}).catch(e=>{})
	}
}