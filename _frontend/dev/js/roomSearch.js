import * as _ from './utils'

export default {
    roomSearch() {
        let $guestContainer = $('.guest-container')
        let guestList   = '.guest-list'
        let $guestList  = $(guestList)
        let addRoom     = '.add-room'
        let room        = 1
        let $roomCounter= $('.room-counter')
        let $inputViewer= $('.form-input-viewer')
        let $emptyState = $('.empty-state')

        _.existJquery($(addRoom)).then($addroom => {
            let ids         = $guestList.find('.form-input').attr('id')
            let viewer      = $inputViewer.length

            $addroom.on('click', '.btn', e=>{
                let $this       = $(e.currentTarget)
                let action      = $this.attr('data-action')
                let inputSets  = $this.siblings('template').html()
                let $input      = $this.siblings('input')
                let $inputView  = $this.siblings('.form-input')
                let max         = $roomCounter.attr('max')
                let min         = $roomCounter.attr('min')
                let val         = $roomCounter.val()
                let btnPlus     = action === 'plus'
                let btnMinus    = action === 'minus'
                let limitMax    = val <= max - 1
                let limitMin    = val >= min + 1
                let $addedSet   = $(guestList)
                let $thisGuestContainer = $this.closest('[data-room]').find('.guest-container')


                if (btnPlus && limitMax) {
                    room++
                    $guestContainer.append(inputSets)
                    $guestContainer.find('[data-value="{{adults}}"]').attr('data-value', 1)
                    $guestContainer.find('[data-ages="[{{childs}}]"]').attr('data-ages', '[0]')
                } else if (btnMinus && limitMin) {
                    let added       = $addedSet.length
                    let otherViewer = (added/2) - 1
                    room--
                    // console.log(viewer, added, otherViewer)
                    // console.log(viewer > 1)
                    if(viewer > 1) {
                        $addedSet.eq(otherViewer).remove()
                    }
                    $addedSet.eq(-1).remove()
                }


                let $setInput       = $guestContainer.find(guestList)

                // for (var i = 0; i < $setInput.length; i++) {
                //     let $this       = $setInput.eq(i)
                //     let $input      = $this.find('.form-input')
                //     const indexName   = `[${i}]`
                //     const indexIDs    = `${i}`
                //     let curIndex    = $this.index()
                //     let lastIndex   = $setInput.length - 1

                //     if(curIndex == lastIndex && limitMax && !btnMinus) {
                //     for (var t = 0; t < $input.length; t++) {
                //         let $this = $input.eq(t)
                //         let names = $this.attr('name')
                //         $this.attr({
                //             name: '',
                //             id: ''
                //         })
                //         $this.attr({
                //             name: names+indexName,
                //             id: names+indexIDs
                //         })
                //     }}

                //     $input.selectric({
                //         forceRenderBelow: true
                //     })
                // }
                if(limitMax && !btnMinus) {
                    writeInputName($setInput)
                }

                $roomCounter.val(room)
                $inputViewer.text(room)
            })
        }).catch(e=>{})

        _.existJquery($('[data-room]')).then($dataRoom=>{
            let dataRoom    = $dataRoom.attr('data-room')
            let $startDate  = $dataRoom.find('.book-date--start')
            let $endDate    = $dataRoom.find('.book-date--end')
            let $bookNights = $dataRoom.find('.book-nights')
            let $bookRooms  = $dataRoom.find('.book-rooms')
            let $bookAdults = $dataRoom.find('.book-adults')
            let $bookChilds = $dataRoom.find('.book-childs')
            let $roomCounter= $dataRoom.find('.room-counter')
            let $inputDays  = $($dataRoom.find('[data-days]').attr('data-days'))
            let dateFormat  = 'YYYY/MM/DD'
            let $listRoomTmpl= $('#listRoom')
            let listRoom    = $listRoomTmpl.html()
            let $listRoom   = $('.list-room')
            let addroomtmpl= $dataRoom.find('#roomContent').html()

            Handlebars.registerHelper('expiredDate', (date, expired, options)=> {
                date = new Date(date).getFullYear()

                if (date > expired) return options.fn(this)
            })

            Handlebars.registerHelper('isExpired', (date, expired) => {
                date = new Date(date).getFullYear()

                if (date == expired) return new Handlebars.SafeString(`is-expired`)
            })

            Handlebars.registerHelper('toLocaleStr', (val)=>{
                val = Handlebars.escapeExpression(val)

                let toNumber = _.currencyIDformat(val)

                return new Handlebars.SafeString(
                    // Number(val).toLocaleString().split(',').join('.')
                    toNumber
                )
            })

            Handlebars.registerHelper('formatDate', (date)=>{
                date = Handlebars.escapeExpression(date)

                let day     = new Date(date).toDateString().split(' ')
                // let month   = date.

                return new Handlebars.SafeString(
                    // moment(date).format('MMM, Do')
                    `${day[1]} ${day[2]}`
                )
            })

            if($listRoomTmpl.length) {
                listRoom        = Handlebars.compile(listRoom)
            }
            // console.log(dateFormat)
            // console.log(dataRoom)
            addroomtmpl     = Handlebars.compile(addroomtmpl)

            renderList(dataRoom)

            $dataRoom.on('click', '.btn-research', e=>{
                let $this           = $(e.currentTarget)
                let $form           = $this.closest('form')
                let formData        = $form.serialize()
                let actionLocation  = $form.attr('action').split('?')
                let baseUrl         = actionLocation[0]
                let qSearch         = actionLocation[1]
                let decodeQsearch   = decode(qSearch)
                let dataMerge       = Object.assign(decode(qSearch), decode(formData))
                let dataParam       = $.param(dataMerge)
                let dataResearch    = baseUrl+'?'+dataParam
                // console.log(decode(qSearch))
                // console.log(decode(formData))
                // console.log(dataMerge)
                renderList(dataResearch)
                window.history.pushState('', '', dataResearch)
            })

            $dataRoom.on('change', '.child-age-select', e=>{
                let $this               = $(e.currentTarget)
                let val                 = Number($this.val())
                let $childAgeContainer  = $this.closest('.guest-list').find('.child-age-container')
                let $childAgeList       = $this.closest('.guest-list').find('.child-age__inputs')
                let selectIndex         = $this.data('index')
                // console.log(val)
                $childAgeList.empty()
                $childAgeContainer.removeClass(_.classes.isActive)
                if(val > 0) {
                    $childAgeContainer.addClass(_.classes.isActive)
                    for(let i = 0; i < val; i++) {
                        $childAgeList.append(childAgeInput(selectIndex,i))
                    }
                    $childAgeList.find('.form-input').selectric()
                }

                $this.attr('data-value', val)
            })

            $dataRoom.on('change', '.adult-guest', e=>{
                let $this               = $(e.currentTarget)
                let val                 = Number($this.val())
                $this.attr('data-value', val)
            })

            function renderList(data) {
                if(data){
                    let $loader     = $('#loader').html()
                    let adults      = []
                    let childs      = []

                    $listRoom.empty().append($loader)
                    _.getJSON(data).then(data => {
                        let roomClass       = data.RoomClasses.RoomClass
                        let isEmpty         = $.isEmptyObject(roomClass)
                        // console.log($.isEmptyObject(roomClass))
                        if(!roomClass || isEmpty) {
                            $listRoom.empty()
                            $emptyState.show()
                        }
                        // let dateRate    = data.RoomClasses.RoomClass.Rooms.Room[0].RoomPriceBreak
                        let startDate   = moment(data.CheckInDate)
                        let roomRequest = data.RoomRequest
                        let rooms       = roomRequest.length
                        let startingDate= startDate.format(dateFormat)
                        let $startingDate = $('.pikaRange--start')
                        let $endingDate = $('.pikaRange--end')

                        // let rooms       = data.RoomClasses.RoomClass.length.length
                        let endDate, endingDate

                        if(data.CheckOutDate.length) {
                            endDate     = moment(data.CheckOutDate)
                            endingDate  = endDate.format(dateFormat)
                            $endDate.html(endDate.format(dateFormat))
                        } else {
                            endDate = ''
                            endingDate = ''
                            $endDate.html('-')
                        }

                        room = rooms
                        // console.log(room)

                        $startDate.html(startDate.format(dateFormat))

                        $roomCounter.val(rooms)
                        $roomCounter.siblings('.form-input').html(rooms)

                        // console.log(chekin_start2)
                        $startingDate.attr('value', startingDate).addClass('is-ajax')
                        $startingDate.val(startingDate).trigger('change')
                        $endingDate.attr({value: endingDate})
                        $endingDate.val(endingDate).trigger('change')

                        // Get status nights, rooms, adults, childs
                        let bookNights = _.countNights(startingDate, endingDate)
                        roomRequest.map(a=>{
                            adults.push(a.adults)
                            childs.push(a.childs.length)
                        })
                        adults = adults.reduce(_.getTotalSum)
                        childs = childs.reduce(_.getTotalSum)

                        // let bookRooms  = rooms
                        $bookNights.html(bookNights)
                        $bookRooms.html(room)
                        $bookAdults.html(adults)
                        $bookChilds.html(childs)
                        $inputDays.html(bookNights)

                        if($listRoom.length) {
                            $listRoom.empty().append(listRoom(data))
                        }

                        if(rooms > 0) {
                            $guestContainer.empty()
                            // for (let i = 0; i < rooms-1; i++) {
                            //     $guestContainer.append(addroomtmpl)
                            //     let $adultGuest = $('.adult-guest')
                            // }

                            // roomRequest.map(r=>{
                            //     // console.log(r.adults)
                            //     $guestContainer.append(addroomtmpl)
                            //     // let $adultGuest = $('.adult-guest')
                            //     // $adultGuest.val(r.adults)
                            // })

                            roomRequest.forEach((value, index)=>{
                                // console.log(value.adults)
                                $guestContainer.append(addroomtmpl(value))
                            })

                            setTimeout(()=>{
                                let $setInput       = $guestContainer.find(guestList)
                                writeInputName($setInput)

                                for (var i = 0; i < $setInput.length; i++) {
                                    let $element = $setInput.eq(i)
                                    renderInputAge($element)
                                }
                            }, 250)
                        }
                    })
                    .catch(e=>{
                        $listRoom.empty()
                        $emptyState.show()
                    })
                }
            }
        }).catch(e=>{})

        function writeInputName(inputSet) {
            for (var i = 0; i < inputSet.length; i++) {
                let $this       = inputSet.eq(i)
                let $input      = $this.find('.child-age-select')
                let $inputAdult = $this.find('.adult-guest')
                let $selectric  = $this.find('.selectstyle')
                let $roomNumber = $this.find('.room-index')
                const indexName   = `[${i}]`
                const indexIDs    = `${i}`
                let curIndex    = $this.index()
                let lastIndex   = inputSet.length - 1

                $roomNumber.html(Number(indexIDs) + 1)

                for (let t = 0; t < $input.length; t++) {
                    let $this = $input.eq(t)
                    let names = $this.attr('name').split('[')[0]
                    let val   = $this.attr('data-value')
                    // console.log(names+indexName)
                    if(val === '{{childs.length}}') {
                        val = 0
                    }
                    $this.attr({
                        name: '',
                        id: ''
                    })
                    $this.attr({
                        name: names+indexName,
                        id: names+'_'+indexIDs,
                        'data-value': val,
                        'data-index': indexIDs
                    })
                    $this.val(val)
                }

                for (var a = 0; a < $inputAdult.length; a++) {
                    let $this = $inputAdult.eq(a)
                    let val   = $this.attr('data-value')
                    let names = $this.attr('name').split('[')[0]
                    $this.attr('name', '')
                    $this.attr({
                        'data-value': val,
                        name: names+indexName
                    })
                    $this.val(val)
                }
                // }

                $selectric.selectric({
                    forceRenderBelow: true
                })
            }
        }

        function renderInputAge($element) {
            let $child      = $element.find('.child-age-select')
            for (let c = 0; c < $child.length; c++) {
                // console.log(c)
                let $thisChild = $child.eq(c)
                let childs     = $thisChild.attr('data-value')
                let $childAgeContainer  = $thisChild.closest('.guest-list').find('.child-age-container')
                let $childAgeList       = $thisChild.closest('.guest-list').find('.child-age__inputs')
                let selectIndex         = $thisChild.data('index')
                let val                 = childs.length
                let ages                = $thisChild.data('ages')

                // console.log(ages.length)

                $childAgeList.empty()
                $childAgeContainer.removeClass(_.classes.isActive)
                if(val > 0 && ages.length) {
                    $childAgeContainer.addClass(_.classes.isActive)

                    ages.map((c, i)=>{
                        $childAgeList.append(childAgeInput(selectIndex,i,c))
                    })

                    setTimeout(()=>{
                        let $inputChild = $childAgeList.find('.form-input')

                        for (var a = 0; a < $inputChild.length; a++) {
                            let $this = $inputChild.eq(a)
                            let dataVal = $this.attr('data-value')
                            // console.log(dataVal)
                            $this.val(dataVal)
                        }
                        // $inputChild.find('.form-input').selectric()
                    }, 250)
                }
            }
        }

        function decode(param) {
            return JSON.parse('{"' + decodeURI(param).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"').replace(/%2F/g,'/') + '"}')
        }

        function getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }
    }
}
