import * as _ from './utils'

export default {
	subMainNav() {
		_.existJquery($('.sub-main-nav')).then($subnav => {
			for (var i = 0; i < $subnav.length; i++) {
				let $this 			= $subnav.eq(i)
				let $subnavTrigger  = $this.siblings('a')
				let caretNav 		= `<span class="icon-subnav fa fa-caret-down"></span>`

				$subnavTrigger.addClass('has-sub').append(caretNav)
			}
		}).catch(e=>{})
	}
}