import * as _ from './utils'

export default {
	croppier() {
    const readFile = (input, cropper, inputstore, maxsize) => {
      let fileSize = input.files[0].size

      if(fileSize > maxsize) {
        Site._popMsg('too Big Mister', 'danger')
        return
      }

      if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
          cropper.croppie('bind', {
            url: e.target.result
          })
        }
        cropper.parent('.upload-cropp-wrapper').addClass('is-ready')
        reader.readAsDataURL(input.files[0]);
      } else {
        return
      }
    }

    const getImgData = (cropper, inputstore) => {
      cropper.croppie('result', {
        type: 'base64',
        format: 'jpeg'
      }).then(res => {
        inputstore.val(res)
      })
    }

    _.existJquery($('.upload-cropp')).then($uploadCropp => {
      let imgPlaceholder = $uploadCropp.attr('data-img')
      let $inputBrowse   = $('.input-browse')
      let $croppier = $uploadCropp.croppie({
        viewport: {
          width: '100%',
          height: '100%'
        }
      })

      $croppier.croppie('bind', {
        url: imgPlaceholder
      })

      $inputBrowse.on('change', e => {
        let $this     = $(e.currentTarget)
        let $preview  = $($this.data('preview'))
        let $store    = $($preview.data('store'))
        let maxSize   = $preview.data('maxsize')

        maxSize = _.toBytes(maxSize)
        
        readFile(e.target, $croppier, $store, maxSize)
      })

      $croppier.on('update.croppie', (e, cropData)=> {
        let $this = $(e.currentTarget)
        let $store = $($this.data('store'))
        // console.log($store)

        $croppier.croppie('result', {
          type: 'base64',
          format: 'jpeg'
        }).then(res => {
          $store.val(res)
        })
      })

    }).catch(e=>{})
  }
}