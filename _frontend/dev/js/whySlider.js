import * as _ from './utils'

export default {
	whySlider() {
		_.existJquery($('.why-slide')).then($slider => {
			$slider.slick({
				slidesToShow: 2,
                waitForAnimate: false,
                arrows: false,
                dots: true,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
					}
				]
			})
		}).catch(e=>{})
	}
}