import * as _ from './utils'

export default {
	inputSwitch() {
		_.existJquery($('.input-switch')).then($switch => {
			$switch.on('change', 'input', e => {
				let $this 		= $(e.currentTarget)
				let $switcher 	= $this.closest('.input-switch')

				if($this.prop('checked', true)) {
					$this.parent().addClass('hidden')
					$this.parent().siblings().removeClass('hidden')
					$switcher.siblings().removeClass('is-selected')
					$switcher.addClass('is-selected')
				} else {
					$this.parent().removeClass('hidden')
				}
			})
		}).catch(e=>{})
	}
}