import * as _ from './utils'

export default {
    _dialog(desc, title, actiondata, type = 'url', yes = 'Yes', no = 'Cancel', btnHighlight = 'yes') {
    let dialogID = Math.floor(Math.random() * 1000)
    let content =  `
        <div class="dialog" id="${dialogID}">
            <div class="dialog-overlay"></div>
            <div class="dialog-box text-center">
                ${!title.length ? ``:`<header class="">
                <figure class="dialog-icon"></figure>
                <strong>${title}</strong></header>`}
                <main>${desc}</main>
                <footer class="">
                    <button class="btn btn--round ${btnHighlight === `yes` ? `btn--red`:``} btn-doaction">${yes}</button>
                    <butoon class="btn btn--round ${btnHighlight === `no` ? `btn--red`:``} btn-cancel">${no}</butoon>
                </footer>
            </div>
        </div>`
        _.$body.append(content)
        let $dialog = $('#'+dialogID)

        setTimeout(function() {
            // $dialog.removeClass()
            $dialog.addClass(_.classes.isActive)
        }, 500)

        $dialog.on('click', '.btn-doaction', e=> {
            let $this   = $(e.currentTarget)
            let $parents= $this.parents('.dialog')

            if(type === 'form') {
                $('#'+actiondata).submit()
            } else {
                window.location.href = actiondata
            }

            removeDialog($parents)
        })

        $dialog.on('click', '.btn-cancel', e=> {
            let $this   = $(e.currentTarget)
            let $parents= $this.parents('.dialog')
            removeDialog($parents)
        })

        $dialog.on('click', '.dialog-overlay', e=> {
            let $this   = $(e.currentTarget)
            let $parents= $this.parents('.dialog')
            removeDialog($parents)
        })

        _.$doc.keyup(function(e) {
            if(_.escKeyPress(e)) {
                if($dialog.hasClass(_.classes.isActive)) {
                    $dialog.removeClass(_.classes.isActive)
                    setTimeout(function() {
                        $dialog.remove()
                    })
                }
            }
        })

        function removeDialog($parent) {
            $parent.removeClass(_.classes.isActive)
            setTimeout(function() {
                $parent.remove()
            })
        }
    }
}
