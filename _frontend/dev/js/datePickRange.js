import * as _ from './utils'

export default {
	datePickRange() {

		_.existJquery($('.pickdate-range')).then($pickrange => {
			let pickRange 	= '.pikaRange'
			let rangeStart  = '.pikaRange--start'
			let rangeEnd    = '.pikaRange--end'

			for (var i = 0; i < $pickrange.length; i++) {
				let $this 	= $pickrange.eq(i)
				let dataContainer = $this.data('container')
				let pickerContainer = document.getElementById(dataContainer)
				let minStart= $this.data('min-start')
				let maxEnd	= $this.data('max-end')
				let start	= $this.find(rangeStart).attr('id')
				let $start  = $('#'+start)
				let end		= $this.find(rangeEnd).attr('id')
				let $end  	= $('#'+end)
				let stVal 	= $start.val()
				let nights  = 0
				let $nDays  = $($this.data('days'))
				let formatDate = 'YYYY/MM/DD'
				let returnTrip = true

				if(minStart !== undefined) minStart = new Date(minStart)
				else minStart = new Date()

				if(maxEnd !== undefined) maxEnd = new Date(maxEnd)
				else maxEnd = new Date(2020, 12, 31)

				if(!stVal.length && minStart === undefined) {
					stVal = moment().toDate()
				} else if (!stVal.length && minStart !== undefined ) {
					stVal = moment(minStart, formatDate).toDate()
				} else { 
					stVal = moment(stVal, formatDate).toDate()
				}
				$(pickRange).prop('readonly', true)

				$start.on('click', e => {
					e.preventDefault()
				})

				$end.on('click', e => {
					e.preventDefault()
				})

				// if(pickerContainer !== undefined) {
				// 	pickerContainer = pickerContainer
				// } else {
				// 	pickerContainer = ''
				// }

				let startDate,
			        endDate,
			        updateStartDate = () => {
			            startPicker.setStartRange(startDate);
			            endPicker.setStartRange(startDate);
			            endPicker.setMinDate(startDate);
			        },
			        updateEndDate = () => {
			            startPicker.setEndRange(endDate);
			            startPicker.setMaxDate(endDate);
			            endPicker.setEndRange(endDate);
			        },
			        startPicker = new Pikaday({
			            field 				: $start[0],
			            minDate 			: minStart,
			            maxDate 			: maxEnd,
			            defaultDate 		: stVal,
						setDefaultDate 		: true,
			            format 				: formatDate,
			            keyboardInput 		: false,
			            // blurFieldOnSelect 	: false,
			            container 			: pickerContainer,
			            reposition 			: false,
                        // numberOfMonths: 2,
                        onOpen: () => {
                            let startVal    = startPicker._o.field.value
                            startPicker.gotoDate(new Date(startVal))
                            pickerContainer.parentNode.classList.add(_.classes.isActive)
                            setTimeout(() => {
                            	startPicker.show()
                            }, 200)

                        },
			            onSelect: function() {
			                startDate = this.getDate()
			                updateStartDate()
			                countDay(startDate, endDate)

			                if(returnTrip) {
				                setTimeout(() => {
	                            	endPicker.show()
	                            }, 200)
                            } else {
                            	pickerContainer.parentNode.classList.remove(_.classes.isActive)
                            }
			            }
			        }),
			        endPicker = new Pikaday({
			            field 				: $end[0],
			            minDate 			: minStart,
			            maxDate 			: maxEnd,
			            format 				: formatDate,
			            keyboardInput 		: false,
			            // blurFieldOnSelect 	: false,
			            container 			: pickerContainer,
                        // numberOfMonths: 2,
			            onOpen: function() {
                            let endval = endPicker._o.field.value
                            let getStart = startPicker.getDate()

                            if(endval.length == 0) {
                                endPicker.gotoDate(getStart)
                            } else {
                                endPicker.gotoDate(new Date(endval))
                            }

                            pickerContainer.parentNode.classList.add(_.classes.isActive)
			            },
			            onSelect: function() {
			                endDate = this.getDate();
			                updateEndDate();
			                countDay(startDate, endDate)

			                pickerContainer.parentNode.classList.remove(_.classes.isActive)
			            },
			            onClose: () => {
			            	pickerContainer.parentNode.classList.remove(_.classes.isActive)
			            }
			        }),
			        _startDate = startPicker.getDate(),
			        _endDate = endPicker.getDate();

			    _startDate = stVal

		        if (_startDate) {
		            startDate = _startDate;
		            updateStartDate();
		        }

		        if (_endDate) {
		            endDate = _endDate;
		            updateEndDate();
		        }

		        $('#'+dataContainer).on('click', '.btn-cancel', e => {
		        	console.log('cancel')
		        	$('#'+dataContainer).parent().removeClass(_.classes.isActive)
		        })

		        $start.on('change', e=> {
		        	let $this = $(e.currentTarget)
		        	let newVal = $this.val()
                    let endIsDisabled = endPicker._o.field.disabled
                    let isAjax  = $this.hasClass('is-ajax')
                    // console.log(isAjax)

		        	startPicker.setStartRange(new Date(newVal))
		            endPicker.setStartRange(new Date(newVal))
		            endPicker.setMinDate(new Date(newVal))

                    if(!endIsDisabled && !isAjax) endPicker.show()

                    $this.removeClass('is-ajax')
                    // $end.focus()
		        })

		        $end.on('change', e=> {
		        	let $this = $(e.currentTarget)
		        	let newVal = $this.val()

		        	startPicker.setEndRange(new Date(newVal))
		            startPicker.setMaxDate(new Date(newVal))
		            endPicker.setEndRange(new Date(newVal))
		        })

		        function countDay (firstDay, lastDay) {
		        	let oneDay = 24*60*60*1000
		        	if(firstDay !== undefined && lastDay !== undefined) {
		        		nights = Math.round(Math.abs((firstDay.getTime() - lastDay.getTime())/(oneDay)))

		        		$nDays.text(nights)
		        	}
		        }

		        let $tripType 		= $('[data-trip]')

		        $tripType.on('change', e=> {
					let $this 		= $(e.currentTarget)
					let target  	= $this.attr('name')
					let $target 	= $('#'+target)
					let $targetInput= $target.find('input')
					let type 		= $this.data('trip')
					let oneWay 		= type === 'one_way'
					let roundTrip	= type === 'round_trip'
					let checked 	= $this.is(':checked')

					if(checked && oneWay) {
						$target.css('opacity', '0.5')
						$targetInput.attr('disabled', 'disabled');
						endPicker.setDate(null)
						startPicker.setEndRange(null)
			            startPicker.setMaxDate(null)

			            returnTrip = false
					} else if(checked && roundTrip) {
						$target.css('opacity', '1')
						$targetInput.removeAttr('disabled');
						returnTrip = true
					}

				})
			}
		}).catch(e=>{})
	}
}
