import * as _ from './utils'

export default {
	hoverTab() {
		_.existJquery($('.hover-tab')).then($hoverTab => {
            let $button = $hoverTab.find('a')
			$button.hover(function(e) {
                let $this  = $(e.currentTarget) 
                let $target= $($this.data('target'))

                $target.addClass(_.classes.isActive)
            }, function(e) {
                let $this  = $(e.currentTarget) 
                let $target= $($this.data('target'))
                $target.removeClass(_.classes.isActive)
            })
		}).catch(e=>{})
	}
}
            