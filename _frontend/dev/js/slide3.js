import * as _ from './utils'

export default {
	slide3() {
		_.existJquery($('.slide-3')).then($slider => {
			$slider.slick({
				slidesToShow: 3,
                waitForAnimate: false,
                prevArrow: _.slickPrev,
                nextArrow:  _.slickNext,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
					}
				]
			})
		}).catch(e=>{})
	}
}