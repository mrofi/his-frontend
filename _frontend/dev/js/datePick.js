import * as _ from './utils'

export default {
	datePick() {
		_.existJquery($('.pickaDay')).then($pickaday => {
			$pickaday.prop('readonly', true)

			for (var i = 0; i < $pickaday.length; i++) {
				let thisInputID = $pickaday.eq(i).attr('id')
				let $thisInput  = $('#'+thisInputID)
				let enabled_dates= $thisInput.data('date')
				let sticky 		= $thisInput.closest('.sticky')
				let items 		= $thisInput.attr('data-items')
				let selctedItem = null
				let startDate 	= $thisInput.data('start')
                let maxDates    = $thisInput.data('max-date')
				startDate		= moment(startDate).format('YYYY/MM/DD')

                maxDates === undefined ? maxDates = '2025-12-31' : maxDates
				// console.log(new Date(maxDates))

				let picker = new Pikaday({
					field: $thisInput[0],
					firstDay: 0,
					minDate: new Date(startDate),
					maxDate: new Date(maxDates),
					format: 'YYYY/MM/DD',
					keyboardInput: false,
					blurFieldOnSelect: false,
					disableDayFn: function (date) {
					    if(enabled_dates !== undefined) {
						    if ($.inArray(moment(date).format("YYYY-MM-DD"), enabled_dates) === -1) {
						    	// console.log(date)
						        return date;
						    }
					    }
					},
					onSelect: function() {
						if(items !== undefined) {
					      	let selectedDate = this.getDate()
					      	let selectedFormat= moment(selectedDate).format("YYYY-MM-DD")
					      	// selctedItem = selectedFormat
					      	// console.log(selctedItem)
					      	// console.log(`${selectedFormat}-${items}`)
				      	} else {
				      		// console.log('no-items')
					    }
				    },
                    onOpen: function() {
                        // console.log(picker.getDate())
                        picker.gotoDate(new Date(startDate))
                    }
				})

				if(sticky.length) {
					_.$win.on('scroll', e=> {
						picker.hide()
					})
				}

				$thisInput.on('change', e => {
					let $this = $(e.currentTarget)
				})
			}
		}).catch(e=>{})
	}
}
