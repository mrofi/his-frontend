import * as _ from './utils'

export default {
    heroSlider() {
        _.existJquery($('.slider-hero')).then($slider => {
            let $imgDual    = $('.img-dual')

            for (var i = 0; i < $imgDual.length; i++) {
                const $this     = $imgDual.eq(i)
                const imgSmall  = $this.data('small')
                const imgMedium = $this.data('medium')

                imgResponsive($this, imgSmall, imgMedium)

                $(window).resize(e=> {
                    imgResponsive($this, imgSmall, imgMedium)
                })
            }

            buildSlider()

            function imgResponsive($img, small, medium) {
                let screenWidth = $(window).width()
                let isMedium    = screenWidth > 768
                // console.log(screenWidth, isMedium)
                if(isMedium) {
                    $img.attr('src', medium)
                } else {
                    $img.attr('src', small)
                }
            }

            function buildSlider() {
    	        $slider.not('.slick-initialized').slick({
                    // lazyLoad: 'progressive',
                    // waitForAnimate: false,
                    prevArrow: '<button type="button" class="slick-prev" type="button"><span class="his-arrow-bold-left"></span></button>',
                    nextArrow:  '<button type="button" class="slick-next" type="button"><span class="his-arrow-bold-right"></span></button>',
                    appendArrows: $('.slide-hero-arrows'),
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 5000
                })
            }
    	}).catch(e=>{})
    }
}
