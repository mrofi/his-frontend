import * as _ from './utils'

export default {
    _inlineMsg(msg, container = '.site-header') {
        let content =  `
        <div class="inline-msg">
            <button class="btn-close" type="button" aria-label="Close">
                <span class="fa fa-close"></span>
            </button>
            <div class="container">
                <div class="inline-msg-content">${msg}</div>
            </div>
        </div>`

        $(container).append(content)
        let $inlineMsg = $('.inline-msg')

        $inlineMsg.on('click', '.btn-close', e=> {
            let $this   = $(e.currentTarget)
            let $parents= $this.parents('.inline-msg')
            $parents.remove()
        })
    }
}
