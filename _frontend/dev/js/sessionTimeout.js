import * as _ from './utils'

export default {
	sessionTimeout() {
		_.existJquery($('.session-timeout')).then($ses => {
			let dataTimout 		= $ses.data('timeout')

			if(dataTimout === undefined) return

			let sesInterval = setInterval(() => {
                let sessionTime = sessionStorage.getItem('flightSearch')
                let sesTime     = new Date(sessionTime).getTime()
                let curTime     = new Date().getTime()
                let diff        = _.getMinuteDuration(sesTime, curTime)

                if(diff >= dataTimout) {
                	clearInterval(sesInterval)
                	$ses.addClass(_.classes.isActive)
                	sessionStorage.removeItem('flightSearch')
                }
            }, 60000)
		}).catch(e=>{})
	}
}