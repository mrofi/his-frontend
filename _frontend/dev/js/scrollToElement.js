import * as _ from './utils'

export default {
	scrollToElement() {
		_.existJquery($('.sticky-trigger')).then($stickyTrigger => {
      let interval

      function sticky() {
          for (var i = 0; i < $stickyTrigger.length; i++) {
              let $this       = $stickyTrigger.eq(i)
              let $btn        = $this.find('.btn-sticky-trigger')
              let $target     = $($btn.attr('href'))
              let targetTop   = $target.offset().top
              let curWinPos   = _.$win.scrollTop()

              $this.on('click', '.btn-sticky-trigger', e=>{
                  e.preventDefault()
                  let $thisbtn  = $(e.currentTarget)
                  let $targetbtn= $($thisbtn.attr('href'))
                  $this.removeClass('is-fixed')
                  // console.log($targetbtn.offset().top, curWinPos)

                  $('html, body').animate({
                      scrollTop: $targetbtn.offset().top + curWinPos
                  }, 350, ()=>{
                    $targetbtn.focus()
                    if($targetbtn.is(':focus')) {
                      return false
                    } else {
                      $targetbtn.attr('tabindex','-1')
                      $targetbtn.focus()
                    }
                  })
              })

              _.$win.scroll(e=>{
                  clearTimeout(interval)

                  interval = setTimeout(()=>{                          
                    if(_.$win.scrollTop() > targetTop + curWinPos - 200) {
                        $this.removeClass('is-fixed')
                        return false
                    } else {
                        $this.addClass('is-fixed')
                    }
                    // console.log(_.$win.scrollTop(), targetTop + curWinPos - 200)
                    curWinPos   = _.$win.scrollTop()
                  }, 350)
              })
          }
      }

      window.onload = sticky
		}).catch(e=>{})
	}
}
            