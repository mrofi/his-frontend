// v 3
import * as _ from './utils'

export default {
	flightSearch() {
		Promise.all([
            _.existJquery($('[data-flight]'))
        ]).then(res => {
            let $container  	 = res[0]
            let flightData  	 = $container.attr('data-flight')
            let $originContainer = $('#originRes')
            let $returnContainer = $('#returnRes')
            // let $loader          = $($container.data('loader'))

            NProgress.start();

            $container.addClass('is-changed')
            renderFlightData(flightData, $originContainer, $returnContainer, $container)

        }).catch(e => {})
	}
}

let choosenFlight
let choosenFlight0 	= []
let choosenFlight1 	= []
let flightTransits 	= 0
let $flightModal 	= $('#flightsChoosenConfirm')
let btnBook 		= '#flightsChoosenConfirm .btn-book'
let inputSearchId   = '#flightsChoosenConfirm input[data-search]'
let inputFlightsId  = '#flightsChoosenConfirm input[data-flight]'
let inputPassenger  = '#flightsChoosenConfirm input[data-ps]'
let btnCloseModal 	= '#flightsChoosenConfirm .btn-close'
let overlayModal 	= '#flightsChoosenConfirm .overlay'
let pessangers      = []

const renderFlightData = (data, originContainer, returnContainer, allContainer) => {
	if(data) {
	_.getJSON(data)
    .then( (data) => {
        // loader.hide()
        NProgress.done();
        
        let resMsg          = data.Messages[0].Type.toLowerCase()
        let resFlight       = data.FlightIndexes[0].FlightDetails

        if(resMsg === 'warning' || resFlight.length == 0) {
            $('#flightNotFound').removeClass('hidden')
            return
        }
		let flightIndexes 	= data.FlightIndexes
        // console.log(flightIndexes)
        let allItems        = _.concated(flightIndexes, 'FlightDetails')
        let groupRef        = allItems.reduce((r, a) => {
                r[a.RefNumber] = [...r[a.RefNumber] || [], a]
                return r;
            }, {})
        // console.log(groupRef)
        const groupedRef    = Object.keys(groupRef).map(g => {
            let groupData   = groupRef[g]
            let id0         = groupData[0].Id
            let groupData_0 = groupData[0]
            let groupData_1 = groupData[1]
            let id1
            if(groupData_1 === undefined) {
                id1 = ''
            } else {
                id1 = groupData[1].Id
            }

            let formatData = 
                {
                    "RefNumber": groupData_0.RefNumber,
                    "groupId": id0+id1,
                    "id0": id0,
                    "id1": id1,
                    "FlightTransit": groupData_0.FlightTransit[0],
                    "FlightAirlineCode": groupData_0.FlightAirlineCode,
                    "TotalFare": groupData_0.TotalFare,
                    "FlightStart": groupData_0.FlightSequences[0].Departure.Time,
                    "FlightDuration": groupData_0.FlightDuration,
                    "dataFlight":groupData,
                    "isPromoted": groupData_0.Promoted,
                    "DepartureTimeGroup" : _.timeGroup(groupData_0.FlightSequences[0].Departure.Time),
                    "ArrivalTimeGroup" : _.timeGroup(groupData_0.FlightSequences[0].Arrival.Time)
                }
            console.log(groupData_0.FlightDuration, groupData_0.RefNumber)
            return formatData
        })

		let flightsOrigin 	= flightIndexes[0].FlightDetails
		let flightsReturn
		if (flightIndexes.length > 1) flightsReturn 	= flightIndexes[1].FlightDetails
		// console.log(flightsReturn)
		let $origin 		= originContainer.find('.res-container')
		let $return 		= returnContainer.find('.res-container')

		let $oriTransit 	= originContainer.find('.transit-filter')
		let $oriAirline 	= originContainer.find('.airline-filter')

		let airPlaneOrigin  = []
		let transitOrigin   = []

        // store data timestamp
        sessionStorage.setItem('flightSearch', new Date())
        // store data pessangers
        pessangers.push(data.ResultPassengers)
        // get transit count
		flightTransits      = flightIndexes.length

		flightsOrigin.map(oriDetail => {
			transitOrigin.push(oriDetail.FlightSequences.length - 1)

			oriDetail.FlightSequences.map(oriPlane => {
				airPlaneOrigin.push(oriPlane.Airline)
			})
		})

		transitOrigin 		= _.nub(transitOrigin)
		transitOrigin 		= transitOrigin.sort()

		airPlaneOrigin 		= _.nubBy(airPlaneOrigin, 'Code')
		airPlaneOrigin 		= airPlaneOrigin.sort(_.compareValues('Code'))
		flightsOrigin 		= flightsOrigin.sort(_.compareValues('TotalFare'))

		renderResItems(groupedRef, $origin, '0')

		renderFilterAirline(airPlaneOrigin, $oriAirline, '0')
		renderFilterTransit(transitOrigin, $oriTransit, '0')

		renderSelectedFlight(groupedRef, originContainer, 0)

		// editSelectedFlight(allContainer, originContainer)

		filterItems(groupedRef, originContainer, $origin, 0)

		// $('#flightsChoosenConfirm .btn-book').attr('data-search', data.ResultID)
		$(btnBook).attr({
			'data-search' : data.ResultID,
			'data-ps': data.ResultPassengers
		})

        $(inputSearchId).val(data.ResultID).attr('data-search', data.ResultID)
        $(inputPassenger).val(data.ResultPassengers).attr('data-ps', data.ResultPassengers)

		_.$doc.on('click', btnCloseModal, e => {
			choosenFlight1 = []
			returnContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
		})
		_.$doc.on('click', overlayModal, e => {
			choosenFlight1 = []
			returnContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
		})

        _.showToolTip($('[data-target-content]'))

        // allContainer.on('click', '[data-book]', e => {
        //     let $this   = $(e.currentTarget)
        //     let ref     = $this.data('book')
        //     e.preventDefault()
        //     $flightModal.addClass('is-active')
        //     originContainer.find(`[data-ref="${ref}"] [data-selected]`).trigger('change')
        // })

        $flightModal.on('click', '.btn-close', e => {
            $('[data-selected]').prop('checked', false)
        })

        $flightModal.on('click', '.overlay', e => {
            $('[data-selected]').prop('checked', false)
        })
		
	})
	.catch(_.noop())
	}
}

const clearSelection = (originContainer, returnContainer) => {
	if(flightTransits > 1) {
		choosenFlight = []
		originContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
	} else {
		returnContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
	}
}

const filterItems = (data, $wrapper, $container, i) => {
    
	$wrapper.on('change', '.row-sort input', e => {
		let $this 	               = $(e.currentTarget)
    	let $form   	           = $this.closest('form')
    	let serial                 = $form.serialize()
    	let item 		           = _.decodeUri(serial)
    	let sorter  	           = item.sort.split('_')
    	let sortItem	           = sorter[0]
    	let sortOrder 	           = sorter[1]
    	let dataSorted             = data.sort(_.compareValues(sortItem, sortOrder))
        console.log(item.ArrivalTimeGroup)
    	let filterTransit          = item.FlightTransit.split('%2C')
        let filterAirline          = item.FlightAirline.split('%2C')
        let filterDepartureTime    = item.DepartureTimeGroup.split('%2C')
        let filterArrivalTime      = item.ArrivalTimeGroup.split('%2C')
        let filterFlight           = filterTransit.concat(filterAirline)
    	let isEmptyTransit         = filterTransit.includes("-1") || filterTransit[0] === ''
        let isEmptyAirline         = filterAirline.includes("-1") || filterAirline[0] === ''
        let isEmptyDeparture       = filterDepartureTime.includes("-1") || filterDepartureTime[0] === ''
        let isEmptyArrival          = filterArrivalTime.includes("-1") || filterArrivalTime[0] === ''
        let isEmptyFilter          = isEmptyTransit || isEmptyAirline || isEmptyDeparture || isEmptyArrival
    	let dataFiltered
        // console.log(filterDepartureTime)
        let allTransit    = () => {
            let collected = []
            dataSorted.map(t => {
                collected.push(t.FlightTransit)
            })

            return _.nub(collected)
        }

        if(!filterTransit[0].length) {
            filterTransit = allTransit()
        }

        let allAirplane     = () => {
            let collected = []
            dataSorted.map(t => {
                collected.push(t.FlightAirlineCode)
            })

            return _.nub(collected)
        }

        if(!filterAirline[0].length) {
            filterAirline = allAirplane()
        }

        let allDeparture     = () => {
            let collected = []
            dataSorted.map(t => {
                collected.push(t.DepartureTimeGroup)
            })

            return _.nub(collected)
        }

        if(!filterDepartureTime[0].length) {
            filterDepartureTime = allDeparture()
        }

        let allArrival     = () => {
            let collected = []
            dataSorted.map(t => {
                collected.push(t.ArivalTimeGroup)
            })

            return _.nub(collected)
        }

        if(!filterArrivalTime[0].length) {
            filterArrivalTime = allArrival()
        }

        dataFiltered = _.getFilter(dataSorted, {
            FlightAirlineCode: filterAirline, 
            FlightTransit : filterTransit,
            DepartureTimeGroup: filterDepartureTime,
            ArivalTimeGroup: filterArrivalTime
        })

        if(!dataFiltered.length) {
            $('#flightNotFound').removeClass('hidden')
            $container.empty()
        } else {
            $('#flightNotFound').addClass('hidden')
    	   renderResItems(dataFiltered, $container, i)
        }
        
    	// window.history.pushState('', '', '?'+serial)
	})
}

const filterFor = (comp, arr) => {
    for (var i = 0; i < arr.length; i++) {
        comp === arr[i]
    }
}

const renderFilterTransit = (data, container, idx) => {
	data.map((t, i) => {
		container.append(filterTransit(t, idx, i))
	})
}

const filterTransit = (data, idx, i) => {
	return `
<li>
    <label class="btn-label" for="transit_${data}${idx}">
        <input class="input-check" data-checker="relCheck${idx}" type="checkbox" id="transit_${data}${idx}" name="filter_transit_${idx}${data}" value="${data}">
        <span>${data == 0 ? `Direct` : `${data}x transit`}</span>
    </label>
</li>`
}

const renderFilterAirline = (data, container, i) => {
	// container.empty()
	data.map(f => {
		container.append(filterAirline(f, i))
	})
}

const filterAirline = (data, i) => {
	return `
<li>
    <label class="btn-label" for="airline_${data.Code}${i}" title="${data.Name}">
        <input class="input-check" data-checker="relCheckAirplane${i}" type="checkbox" id="airline_${data.Code}${i}" name="airline_${data.Code}${i}" data-val="${data.Code}" value="${data.Code}">
        <span class="text-ellipsis">${data.Name}</span>
        <img src="${data.Logo}" alt="${data.Code}" width="18" height="18">
    </label>
</li>`
}

const renderResItems = (itemsData, container, i) => {
    // console.log(itemsData)
	container.empty()
	itemsData.map((item) => {

		container.append(flightItem(item, i))
	})
} 

const flightItem = (data, i) => {
    // console.log(data)
    let dataItem    = data
    let dataFlight  = dataItem.dataFlight
    let isPromoted  = dataItem.isPromoted

    let list = `
        <div class="block border flight__item ${isPromoted ? `is-promoted`: ``}" data-departure="${dataItem.DepartureTimeGroup}">
            ${dataFlight.map((item, i) => `${groupItem(item, i)}`).join('')}
            <div class="pv-12 ph-16 flex v-ct fill-softtblue full-v">
                <div class="fg-1 text-right mr-half line--1">
                    <small class="in-block">Harga Total</small><strong class="in-block ml-small t--larger-l text-red">
                        IDR ${_.currencyIDformat(dataItem.TotalFare)}
                    </strong>
                    <div><a href="#f_P_${i}_${data.RefNumber}" data-panel-target="#f_P_${i}_${data.RefNumber}">
                        Detail Harga <span class="fa fa-caret-down"></span>
                    </a></div>
                </div>
                <label for="flight_${dataItem.id0}" class="btn btn--smaller btn-check-flight btn--ghost-check btn--round">
                    <input type="radio" id="flight_${dataItem.id0}" value="${dataItem.id0}" name="flight-${i}" data-selected="${dataItem.RefNumber}" data-container="#selectedFlight${i}" data-ref-target="ref-${dataItem.RefNumber}">
                    <span class="bg-label"></span>
                    <strong class="btn-text text-up">Pilih</strong>
                </label>
            </div>
            <div class="panel-content ph-16" id="f_P_${i}_${data.RefNumber}">
            <div class="bzg">
            <div class="bzg_c" data-col="m6, l4" data-offset="m6, l8"><table class=" table--zebra"><tbody>
                ${
                    dataFlight[0].Fares.map((df, i) => `${
                        priceDetail(df)
                    }`).join('')
                }
            </tbody></table></div>
            </div>
        </div>
        </div>`

    return list
}


const groupItem = (data, i) => {
	let f_Seq 	 = data.FlightSequences
	let lastSeq  = _.lastArr(f_Seq)
	let isTransit= f_Seq.length > 1
	let fares    = data.Fares[0]
    let airlineCode = $('[data-airline]').attr('data-airline')
    let dataAirLineCode = f_Seq[0].Airline.Code.toLowerCase()
    let Logos    = []
    let transits

    if(isTransit) {
        transits = f_Seq.length - 1
    }

    f_Seq.map(fs=> {
        Logos.push(fs.Airline)
    })
    Logos = _.nubBy(Logos, 'Logo')

    let templates = `
    <div class="pv-12 ph-16" id="item_${data.Id}" data-ref="ref-${data.RefNumber}">
        <small class="text-blue t-bold flex v-ct">${i == 0 ? `<span class="fa fa-1-5x fa-plane mr-small"></span> <span>Keberangkatan</span>` : `<span class="fa fa-1-5x fa-plane fa-rotate-90 mr-small"></span> <span>Kedatangan</span>`}</small>
        <hr class="mb-half">
        <div class="mb-small bzg bzg--small-gutter">
            <div class="bzg_c" data-col="x8, l10, w8" data-offset="w1">
                <div class="bzg bzg--small-gutter nowrap">
                    <div class="bzg_c text-center" data-col="m3, l4">
                        <div class="airlines-logo">
                        <img src="${f_Seq[0].MarketingCompanyLogo}" alt="" class="img-contain airways-logo">
                        </div>
                        <small class="text-ellipsis">${f_Seq[0].MarketingCompanyName}</small>
                    </div>
                    <div class="bzg_c line--small text-center" data-col="x4, m3, l2">
                        <div class="line--40 t-strong t--larger-l">${f_Seq[0].Departure.Time}</div>
                        <small>${f_Seq[0].Departure.Location}</small>
                    </div>
                    <div class="bzg_c line--small text-center" data-col="x4, m3, l2">
                        <div class="line--40 t-strong t--larger-l">${f_Seq[lastSeq].Arrival.Time}</div>
                        <small>${f_Seq[lastSeq].Arrival.Location}</small>
                    </div>
                    <div class="bzg_c line--small text-center" data-col="x4, m3, l2">
                        <div class="line--40 t-strong t--larger-l">${_.fDur(data.FlightDuration)}</div>
                        <small>Durasi</small>
                    </div>
                    <div class="bzg_c line--small text-center" data-col="x4, m3, l2">
                        <strong class="line--40 text-red"><small>${isTransit ? `<span class="badge-red">${transits}</span> Transit` : `Langsung`}</small></strong>
                    </div>
                </div>
            </div>
            <div class="bzg_c text-center" data-col="x4, l2">
                <div data-target-content="#tip-${data.Id}" data-tip-size="auto" class="line--40">
                <span class="his-koper"></span>
                ${fares.MealAllowance !== null ? `
                    <span class="fa fa-cutlery" aria-hidden="true"></span>
                `: ``}
                </div>
                <div class="tip-content" id="tip-${data.Id}">
                Bagasi ${fares.BagAllowance} ${fares.BagUnit}
                ${fares.MealAllowance !== null ? `
                    <br>Makanan di pesawat
                `: ``}
                </div>
            </div>
        </div>
        <nav class="flight-item-nav">
            <a href="#f_D_${i}_${data.RefNumber}" data-panel-target="#f_D_${i}_${data.RefNumber}">
                Detail Penerbangan
                <span class="fa fa-caret-down"></span>
            </a>
            <!-- <a href="#f_P_${i}_${data.RefNumber}" data-panel-target="#f_P_${i}_${data.RefNumber}">
                Detail Harga
                <span class="fa fa-caret-down"></span>
            </a> -->
        </nav>
        <div class="panel-content " id="f_D_${i}_${data.RefNumber}">
            ${
                data.FlightSequences.map((fs, i) => `${
                    flightDetail(fs, i, data.FlightSequences.length, data.Fares)
                }`).join('')
            }
        </div>
    </div>${i == (f_Seq.length - 1) ? '' : `<hr class="mb-0">`}`

    if(airlineCode !== undefined) {
        airlineCode.toLowerCase()
    }

	return `${dataAirLineCode === airlineCode || airlineCode === undefined || airlineCode === '' ? templates:``}`
    // return templates
}

const flightDetail 	= (data, i, seq, fares) => {
	let depart 		= data.Departure
	let arrival 	= data.Arrival
	let airline 	= data.Airline

	return `
    <div class="flight-detail flight-detail--start">
        <time class="time">
            <strong>${depart.Time}</strong><br>
            ${_.dateFormat(depart.Date, 'DD_MMM')}
        </time>
        <div class="content">
            ${depart.Name} (${depart.Location})
        </div>
    </div>
    <div class="flight-detail flight-detail--duration">
        <time class="time">${_.fDur(data.FlightDuration)}</time>
        <div class="content">
            <div class="mb-half">
                <img src="${data.MarketingCompanyLogo}" alt="" width="36"> ${data.MarketingCompany}-${airline.FlightNo}, ${flightClass(data.Eticket)}<br>
                Dioperasikan oleh: ${data.OperatingCompanyName}
            </div><hr class="mb">
            <div class="mb-2">
            <div class="mb-small nowrap">
                <span class="his-koper"></span>
                Bagasi ${fares[0].BagAllowance} ${fares[0].BagUnit}
            </div>
            ${fares[0].MealAllowance !== null ? `
            <div class="mb-small nowrap">
                <span class="fa fa-cutlery" aria-hidden="true"></span>
                Makanan
            </div>` : ``}
            </div>
            <div class="bzg">
                <div class="bzg_c mb-half" data-col="m6">
                    <span>Model</span><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <span>Denah Kursi</span><br>
                    <strong>-</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <span>Kursi</span><br>
                    <strong>${flightClass(data.Eticket)}</strong>
                </div>
                <div class="bzg_c mb-half" data-col="m6">
                    <span>Jarak Antar Kursi</span><br>
                    <strong>-</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="flight-detail flight-detail--end">
        <time class="time">
            <strong>${arrival.Time}</strong><br>
            ${_.dateFormat(arrival.Date, 'DD_MMM')}
        </time>
        <div class="content">
            ${arrival.Name} (${arrival.Location})
        </div>
    </div>
${i + 1 != seq ? `
<div class="mb-half block--inset-small fill-lightgrey">
    Transit for ${_.fDur(data.TransitDuration)} in ${arrival.Name} (${arrival.Location})
</div>` : ``}`
}

const priceDetail = (data) => {
    let ps      = pessangers[0]
    let adult   = ps[0]
    let child   = ps[1]
    let infant  = ps[2]
    let adultFare = data.AdultFare * adult
    let childFare = data.ChildFare * child
    let infantFare= data.InfantFare * infant

	return `
<tr><td>Adult Fare <small>tax included</small> (x${adult})</td>
<td class="text-right">IDR ${_.currencyIDformat(adultFare)}</td></tr>
${child > 0 ? `<tr><td>Child Fare <small>tax included</small> (x${child})</td>
<td class="text-right">IDR ${_.currencyIDformat(childFare)}</td></tr>` : ``}
${infant > 0 ? `<tr><td>Infant Fare <small>tax included</small> (x${infant})</td>
<td class="text-right">IDR ${_.currencyIDformat(infantFare)}</td></tr>` : ``}
${data.VAT !== undefined ? `<tr><td>VAT 1% </td><td class="text-right">IDR ${_.currencyIDformat(data.VAT)}</td></tr>` :``}
<tr><td><strong>Total Fare</strong></td><td class="text-right"><strong class="text-red">IDR ${_.currencyIDformat(data.TotalFare)}</strong></td></tr>`
}

const price_Detail2 = (data) => {
    return `
<tr><td>Total Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.TotalFare)}</td></tr>
<tr><td>Total Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.TotalTax)}</td></tr>
<tr><td>Adult Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.AdultFare)}</td></tr>
<tr><td>Adult Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.AdultTax)}</td></tr>
<tr><td>Child Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.ChildFare)}</td></tr>
<tr><td>Child Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.ChildTax)}</td></tr>
<tr><td>Infant Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.InfantFare)}</td></tr>
<tr><td>Infant Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.InfantTax)}</td></tr>
<tr><td>Airport Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.AirportTax)}</td></tr>
<tr><td>Insurance</td><td class="text-right">IDR ${_.currencyIDformat(data.Insurance)}</td></tr>
<tr><td>Iwjr</td><td class="text-right">IDR ${_.currencyIDformat(data.Iwjr)}</td></tr>
<tr><td>Others</td><td class="text-right">IDR ${_.currencyIDformat(data.Others)}</td></tr>
<tr><td>VAT</td><td class="text-right">IDR ${_.currencyIDformat(data.VAT)}</td></tr>`
}


const getItemId = (arr, id) => {
	function selectedId(ref) { return ref.RefNumber === id }
	return arr.find(selectedId)
}

const editSelectedFlight = (container, itemContainer) => {
	container.on('click', '.edit-flight-start', e => {
		let $this 			= $(e.currentTarget)
		let $selectedFlight = itemContainer.find('[data-selected]')
		let $thisSelected   = $this.closest('.selected-flight')

		container.removeClass('flight-selected is-changed')
		$selectedFlight.removeAttr('checked')
		$selectedFlight.prop('checked', false)
		$thisSelected.empty()
		choosenFlight0 = []

        setTimeout(() => {
            container.addClass('is-changed')
            _.goToTop()
        }, 250)

		e.preventDefault()
	})
}

const renderSelectedFlight = (data, wrapper, i) => {
	// let dataSelected = 
    // let ids = data.filter(ref => ref.RefNumber === '1')
    // console.log(ids)
	wrapper.on('change', '[data-selected]', e => {
		let $this 		= $(e.currentTarget)
		let $target 	= $($this.data('container'))
		let dataId 		= $this.attr('data-selected')
		// let dataSelected= getItemId(data, dataId)
        let dataSelected= data.filter(ref => ref.RefNumber === dataId)
		let $container 	= $this.closest('[data-flight]')
		let dataName    = $this.attr('name')
        let itemRef     = $this.data('ref-target')

        // console.log(data, dataId)    
        // $container.find(`.res--return [data-ref]`).removeClass('is-active')
        // $container.find(`.res--return [data-ref='${itemRef}']`).addClass('is-active')
        // console.log(dataSelected)

		// if(flightTransits > 1) {
            $container.removeClass('is-changed')
			$target.empty()
			// $target.append(selectedFlight(dataSelected, i, itemRef)) 

			// $container.addClass('flight-selected')
            setTimeout(() => {
        // console.log(flightTransits)
                $container.addClass('is-changed')
                // _.goToTop()
            }, 250)
		// }

        // console.log(dataSelected)

		// if(dataName === 'flight-0') {
			choosenFlight0 = []
			choosenFlight0.push(dataSelected)
		// } else { 
			choosenFlight1 = []
			choosenFlight1.push(dataSelected)
		// }

        // console.log(choosenFlight0[0][0].TotalFare)
		
		let price0 	  = choosenFlight0[0][0].TotalFare
		let flightId0 = choosenFlight0[0][0].id0
		let flightId1 = null
		let price1	  = 0
		let flightDataId = flightId0
		if(choosenFlight1.length > 0) {
			price1    = choosenFlight1[0][0].TotalFare
			flightId1 = choosenFlight1[0][0].id1

			flightDataId = [flightId0, flightId1]
		}

        // console.log(flightDataId)

		// let totalPrice= price0 + price1
        let totalPrice= price0
        // console.log(_.currencyIDformat(totalPrice))
		choosenFlight = choosenFlight0.concat(choosenFlight1)

		$('#flightsDetailConfirm').empty().append(flightsDetailConfirm(choosenFlight))
		$('#flightsDetailInfo').empty().append(flightInfoDetailConfirm(choosenFlight))
		$('#pricesDetailInfo').empty().append(priceInfoDetailConfirm(choosenFlight))

		// if(choosenFlight.length == flightTransits) {
			$('.total-flight-price span').html(_.currencyIDformat(totalPrice))
			
			$(btnBook).attr('data-flight', [flightDataId])
            $(inputFlightsId).val([flightDataId]).attr('data-flight', [flightDataId])
		// }

        $flightModal.addClass('is-active')
	})
}

const priceInfoDetailConfirm = data => {
    let itemData = data[0][0].dataFlight
	return `
<div class="bzg">
	<div class="bzg_c" data-col="m6"><table class=" table--zebra"><tbody>
		${
			itemData[0].Fares.map((df, i) => `${
				priceDetail(df)
			}`).join('')
		}
	</tbody></table></div></div>`
}

const priceInfoDetailConfirm_2 = data => {
    return `
<div class="bzg">
${data.map((d, i) => `
    <div class="bzg_c" data-col="m6"><h3 class="mb-0 text-blue">${i == 0 ? `Berangkat` : `Pulang`}</h3><table class=" table--zebra"><tbody>
        ${
            d.Fares.map((df, i) => `${
                priceDetail(df)
            }`).join('')
        }
    </tbody></table></div>`).join('')}</div>`
}

const flightInfoDetailConfirm = (data) => {
    let itemData = data[0][0].dataFlight
	return `
${itemData.map((d, t) => `
    ${t != 0 ? `<hr class="mb-half">`:``}
	<h3 class="mb-0 text-blue">${t == 0 ? `<span class="fa fa-plane mr-small"></span> <span>Keberangkatan</span>` : `<span class="fa fa-plane fa-rotate-90 mr-small"></span> <span>Kedatangan</span>`}</h3>
	${d.FlightSequences.map((fs, i) => `${
		flightDetail(fs, i, d.FlightSequences.length, d.Fares)
	}`).join('')}
`).join('')}`
}

const flightsDetailConfirm = (data) => {
    let itemData = data[0][0].dataFlight

	return `
${itemData.map((d, i) => `
${i == 1 ? `<hr class="mb-small">`:``}
<div class="block bzg line--small">
	<div class="bzg_c" data-col="m6, l3">
		${i == 0 ? `Keberangkatan` : `Kedatangan`}<br>
		<small class="text-grey">${_.dateFormat(d.FlightStart, 'DDDD_DD_MMMM_YYYY')}</small>
	</div>
	<div class="bzg_c mb-small" data-col="m6, l3">
		<div class="flex a-center">
			<img src="${d.FlightSequences[0].MarketingCompanyLogo}" alt="" width="36">
			<div class="ml-small fg-1">
				${d.FlightSequences[0].MarketingCompanyName}<br><span class="text-grey">${flightClass(d.FlightSequences[0].Eticket)}</span>
			</div>
		</div>
	</div>
	<div class="bzg_c" data-col="s4, l2">
		${d.FlightSequences[0].Departure.Time}<br><span class="text-grey">${cityName(d.FlightSequences[0].Departure.Name)} (${d.FlightSequences[0].Departure.Location})</span>
	</div>
	<div class="bzg_c" data-col="s4, l2">
		${d.FlightSequences[_.lastArr(d.FlightSequences)].Arrival.Time}<br><span class="text-grey">${cityName(d.FlightSequences[_.lastArr(d.FlightSequences)].Arrival.Name)} (${d.FlightSequences[_.lastArr(d.FlightSequences)].Arrival.Location})</span>
	</div>
	<div class="bzg_c" data-col="s4, l2">
		Total Durasi<br>
		<span class="text-grey">${_.fDur(d.FlightDuration)}, ${d.FlightSequences > 1 ? 'Transit' : 'Langsung'}</span>
	</div>
</div>
`).join('')}`
}

const selectedFlight = (data, i, ref) => {
    let selectedItem = `
    <div class="block">
        ${data[0].dataFlight.map((item, i) => `
            ${selectedFlightItem(item, i)}
        `).join('')}
        <div class="block-half border p-small">
            <table class="mb-0 line--small"><tbody>
                <tr>
                    <td>Harga pergi<br><em class="text--smaller">(Termasuk semua pajak dan biaya penerbangan)</em></td>
                    <td class="nowrap text-right"><strong class="text-red">IDR ${_.currencyIDformat(data[0].TotalFare)}</strong></td>
                </tr>
            </tbody></table>
        </div>
        <button class="btn btn--round btn--red" type="button" data-book="${ref}">Lanjutkan</button>
    </div>`

    return selectedItem
}

const selectedFlightItem = (data, i) => {
    // console.log(data)
    let flight   = data.dataFlight
	let f_Seq 	 = data.FlightSequences
	let firstSeq = f_Seq[0]
	let lastSeq  = f_Seq[_.lastArr(f_Seq)]
	let Departure= firstSeq.Departure
	let Arrival  = lastSeq.Arrival
	let Airline  = firstSeq.Airline
	let Class 	 = firstSeq.Eticket
	let isTransit= f_Seq.length > 1

	return `
<div class="block-half border">
    <div class="p-small fill-lightgrey">
        <span class="fa fa-plane text-blue ${i == 1 ? ` fa-rotate-180`:``}"></span>
        <strong class="ml-small text-up text-blue">Penerbangan ${i == 0 ? `Pergi`:`Pulang`}</strong>
    </div>
    <div class="p-small">
        <div class="t-bold mb-small">${Departure.Time} - ${cityName(Departure.Name)} (${Departure.Location})</div>
        <div class="direct-container">
            <div class="flex a-start">
                <img src="${Airline.Logo}" alt="" width="36">
                <div class="fg-1 ml-small">
                    <strong>${Airline.Name}</strong><br>
                    <small><span class="fa fa-calendar"></span> ${_.dateFormat(data.FlightStart, 'DDDD_DD_MMMM_YYYY')}</small>
                </div>
            </div>
        </div>
        <div class="t-bold mb-small">${Arrival.Time} - ${cityName(Arrival.Name)} (${Arrival.Location})</div>
        <small class="text-grey">
            <span class="fa fa-info-circle"></span>
            Waktu diatas adalah waktu setempat
        </small>
    </div>
</div>`
}

const flightClass = (code) => {
	if(code === 'A') return `First`
	else if (code === 'J' || code === 'C' || code === 'D' || code === 'I' || code === 'R') return `Business`
	else if (code === 'W' || code === 'E' || code === 'T') return `Premium Economy`
	else return `Economy`
}

const cityName = (name) => {
	let fullName = name.split('(')[1]
	let names 	= fullName.split(')')[0]

    // console.log(names)

	return names
}
