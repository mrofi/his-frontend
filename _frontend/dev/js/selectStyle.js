import * as _ from './utils'

export default {
	selectStyle() {
		_.existJquery($('.selectstyle')).then($select => {
			$select.selectric({
				forceRenderBelow: true
			})
		}).catch(e=>{})
	}
}