import * as _ from './utils'

export default {
	addPerson() {
		_.existJquery($('.add-person')).then($addPerson => {
			let adultVal 	= 0
			let infantVal  	= 0
			let total 		= 0
			let btnInfant  	= '[data-person="infant"]' 
			// let $datePicker = $addPerson.find('.pickadate')

			$addPerson.on('click', '[data-action]', e=> {
				let $this 		= $(e.currentTarget)
				let dataInput 	= $this.data('input')
				let $input 		= $(dataInput)
				let val 		= $input.val()
				let maxFields 	= Number($input.attr('max')) - 1
				let action 		= $this.data('action')
			    let $btnInfant 	= $this.parents('.add-person').siblings().find(btnInfant)

				let person 		= $this.data('person')
				let $container 	= $('#'+person+' > .items')
				let $fieldItem 	= $this.parents('.fields__item')

				let $fields = `<div class="bzg fields__item">
			        <div class="form__row bzg_c" data-col="m5, l3">
			            <label class="space-right t-strong" for="">Titel *</label><br>
			            <label for="title_mr_${person}_${val}" class="btn-label">
			                <input type="radio" id="title_mr_${person}_${val}" title="Mr" value="Mr" name="gender_${person}[${val}]" checked >
			                <span class="btn-label__text">Mr.</span>
			            </label>
			            <label for="title_mrs_${person}_${val}" class="btn-label">
			                <input type="radio" id="title_mrs_${person}_${val}" title="Mrs" value="Mrs" name="gender_${person}[${val}]">
			                <span class="btn-label__text">Mrs.</span>
			            </label>
			            <label for="title_ms_${person}_${val}" class="btn-label">
			                <input type="radio" id="title_ms_${person}_${val}" title="Ms" value="Ms" name="gender_${person}[${val}]">
			                <span class="btn-label__text">Ms.</span>
			            </label>
			        </div>
			        <div class="form__row bzg_c" data-col="m6, l5">
			            <label for="name_${person}_${val}" class="t-strong">Nama Lengkap *</label><br>
			            <input type="text" class="form-input form-input--block" id="name_${person}_${val}" name="name_${person}[${val}]" required placeholder="Nama Lengkap Sesuai Passport">
			        </div>
			        <div class="form_row bzg_c relative" data-col="m5, l3">
			        	<label for="birthdd_${person}_${val}" class="t-strong pikaLabel">Tanggal Lahir</label>
			        	<span>&nbsp;</span><br>
			        	<input type="text" class="form-input form-input--block pickadate" id="birthdd_${person}_${val}" name="birthdd_${person}[${val}]" required readonly>
			        </div>
			        <div class="form__row bzg_c text-right" data-col="m1">
			        	<label><span class="sr-only">Remove</span></label><br>
						<button class="btn btn--round btn--smaller btn--ghost-red btn-remove" data-person="${person}" data-action="remove" data-input="${dataInput}" type="button">
							<span class="fa fa-close"></span>
						</button>
			        </div>
			    </div>`

			    let allowed 	= total <= maxFields
			    let addButton 	= action === 'add'
			    let removeBtn 	= action === 'remove'
			    let personAdult = person === 'adult'
			    let personChild = person === 'child'
			    let personInfant = person === 'infant'
			    let isAdult 	= personAdult && allowed
			    let isChild 	= personChild && allowed
			    let isInfant 	= personInfant && allowed

			    if(isChild && addButton) {
			    	val++
			    	total++
			    	addFields()
			    } else if (isAdult && addButton) {
			    	val++
			    	total++
			    	adultVal = val
			    	addFields()
			    	$btnInfant.removeAttr('disabled')
			    } else if (isInfant && addButton && infantVal < adultVal) {
			    	val++
			    	total++
			    	infantVal = val
			    	addFields()
			    }

			    if (removeBtn && personChild) {
			    	removeField()
			    	// console.log('remover', person)
			    } else if(removeBtn && personInfant) {
			    	removeField()
			    	infantVal = val
			    	// console.log('remover', person)
			    } else if(removeBtn && personAdult && adultVal > infantVal) {
			    	removeField()
			    	adultVal = val
			    	// console.log('remover', person)
			    }

			    function addFields() {
				    $input.val(val)
				    $container.append($fields)
			    }
				
			    function removeField() {
			    	val--
			    	total--
			    	$input.val(val)
			    	$fieldItem.remove()
			    }
			    // console.log(infantVal, adultVal)
				// console.log(total, maxFields+1)
				
				
			})
		    	_.pickaDate($addPerson, '.pikaLabel')


			// $addPerson.on('click', '.btn-remove', e=> {
			// 	let $this 		= $(e.currentTarget)
			// 	let $fieldItem 	= $this.parents('.fields__item')
			// 	let dataInput 	= $this.data('input')
			// 	let $input 		= $(dataInput)
			// 	let val 		= $input.val()

			// 	$fieldItem.remove()
			// 	val--
			// 	total--
			// 	$input.val(val)
			// })

			function getSum(a, b) {
            	return a + b
            }
		}).catch(e=>{})
	}
}