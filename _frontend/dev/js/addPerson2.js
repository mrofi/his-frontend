import * as _ from './utils'

export default {
	addPerson() {
		_.existJquery($('.add-person')).then($addPerson => {
			let adult 	= 0
			let child 	= 0
			let infant  = 0
			let dataMax = []
			let max 	= 0
			let selected= 0

			let selectPerson = $addPerson.selectric()

			$addPerson.on('change', e=> {
				let $this 		= $(e.currentTarget)
				let val 		= $this.val()
				let template	= $('#person').html()
				let person 		= $this.data('person')
				let $container 	= $('#'+person+' > .items')
				let listed 		= $container.data('list')
				let $opts 		= $this.find('option')
				let $parent 	= $container.parent()

				max = ($opts.length)

				$container.empty()

				for (var i = 0; i < val; i++) {
					
					let $fields = `<div class="bzg">
				        <div class="form__row bzg_c" data-col="m5, l4">
				            <label class="space-right t-strong" for="">Titel *</label><br>
				            <label for="title_mr_${person}_${i}" class="btn-label">
				                <input type="radio" id="title_mr_${person}_${i}" title="Mr" value="Mr" name="gender_${person}[${i}]" checked >
				                <span class="btn-label__text">Mr.</span>
				            </label>
				            <label for="title_mrs_${person}_${i}" class="btn-label">
				                <input type="radio" id="title_mrs_${person}_${i}" title="Mrs" value="Mrs" name="gender_${person}[${i}]">
				                <span class="btn-label__text">Mrs.</span>
				            </label>
				            <label for="title_ms_${person}_${i}" class="btn-label">
				                <input type="radio" id="title_ms_${person}_${i}" title="Ms" value="Ms" name="gender_${person}[${i}]">
				                <span class="btn-label__text">Ms.</span>
				            </label>
				        </div>
				        <div class="form__row bzg_c" data-col="m7, l8">
				            <label for="" class="t-strong">Nama Lengkap *</label><br>
				            <input type="text" class="form-input form-input--block" id="" name="name_${person}[${i}]" required placeholder="Nama Lengkap Sesuai Passport">
				        </div>
				    </div>`
					
					if(val > 0) {
						$parent.show()
						$container.append($fields)
					}
				}

				if(person === 'adult') {
					adult = val
				} else if (person === 'child') {
					child = val
				} else if (person === 'infant') {
					infant = val
				}

				let total = [Number(adult), Number(child), Number(infant)]
				selected = total.reduce(getSum)
				let allowed = max - selected

				console.log(selected)

				// $this.parents('.add-person__item').siblings().find('option').prop('disabled', true)
				// $this.parents('.add-person__item').siblings().find('option').eq(allowed).prevAll().prop('disabled', false)
				// selectPerson.selectric('refresh')

				if (val == 0) {
					$parent.hide()
				}
			})

			function getSum(a, b) {
            	return a + b
            }
		}).catch(e=>{})
	}
}