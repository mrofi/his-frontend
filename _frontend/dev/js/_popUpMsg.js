import * as _ from './utils'

export default {
	_popMsg(message, type, timeout = 0) {
        let notifEl    = $('#notif_tmpl').html()
        $(notifEl).insertAfter('.sticky-footer-container')

        let $notif      = $('.notif')

        let notiftype   = 'notif--' + type

        $notif.find('.notif__text').html(message)
        $notif.find('.notif__box').addClass(notiftype)

        setTimeout(function() {
            $notif.addClass(_.classes.isActive);
        }, 500);

        $notif.on('click', '.btn-close', function(e) {
            e.preventDefault();
            removeNotif();
        })

        // if(timeout != 0) {
            setTimeout(function() {
                if($notif.hasClass(_.classes.isActive)) {
                    removeNotif();
                }
            }, timeout)
        // }

        $(document).keyup(function(e) {
            if(_.escKeyPress(e)) {
                if($notif.hasClass(_.classes.isActive)) {
                    removeNotif()
                }
            }
        })

        function removeNotif() {
            $notif.removeClass(_.classes.isActive);
            // $notif.find('.notif__box').removeClass(notiftype);
            setTimeout(function() {
                $notif.remove();
            }, 500);
        }
    }
}
