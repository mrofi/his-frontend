import * as _ from './utils'

export default {
	select2() {
		_.existJquery($('.select_2')).then($select2 => {
            let ajaxOpts = (ajaxUrl) => {
                return {
                    ajax: {
                        url: ajaxUrl,
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return  {
                                search: params.term
                            }
                        },
                        processResults: function (data) {

                            return {
                                results: data
                            }
                        },
                        cache: true
                    },
                    minimumInputLength: 2
                }
            }

            for (var i = 0; i < $select2.length; i++) {
                let $this       = $select2.eq(i)
                let selectData  = $this.attr('data-select')

                if(selectData !== undefined) {
                    $this.select2(ajaxOpts(selectData))
                } else {
                    $this.select2()
                }
            }
        }).catch(e=>{})
	}
}