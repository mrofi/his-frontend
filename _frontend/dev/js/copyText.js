import * as _ from './utils'

export default {
	copyText() {
		_.existJquery('[data-copy]')
		.then($btnCopy => {

			$btnCopy.on('click', e => {
            	e.preventDefault()
            	let $this 		= $(e.currentTarget)
            	let $copyTarget = $($this.data('copy'))

            	$copyTarget.select()

            	document.execCommand("copy")
			})

		}).catch(e=>{})
	}
}