import * as _ from './utils'

export default {
	colList() {
		_.existJquery($('.col-list')).then($colList => {
			for (var i = 0; i < $colList.length; i++) {
				function maxHeight() {
					let $col 		= $colList.eq(i)
					let colHeight 	= $colList.outerHeight()

					$col.css('max-height', colHeight)

					setTimeout(function(){
						$col.css('max-height', colHeight/1.5)
						$col.addClass(_.classes.isActive)	
					})
				}

				maxHeight()

				_.$win.resize(e=> {
					maxHeight()
				})
			}
		}).catch(e=>{})
	}
}