import * as _ from './utils'

export default {
	hisMap() {
		_.existJquery($('.map-container')).then( $mapArea => {
			function hismap() {
			let dataMap 		= $mapArea.data('map')
			let markerIcon 		= $mapArea.data('marker')
			let mapId 			= $mapArea.find('.map-area').attr('id')
			let $mapSearch  	= $('.map-search')
			let $inputSearch	= $('.map-input-search')
			let $mapNav 		= $('.map-nav')
			let $mapNavContainer= $('.map-nav-container')
			let $mapDetails 	= $('.location-details')
			let $searchFilter	= $('.map-search-filter')
			let mapNavTmpl		= $('#mapNavTmpl').html()
			let searhMapTmpl	= $('#searchList').html()
			let btnAction 		= '.btn-action'
			let btnReset 		= '.btn-reset'
			let dataLat 		= dataMap.lat
			let title 			= $mapArea.data('title')
				// console.log('pinIcon')
			// let pinIcon 		= {
   //              url: markerIcon,
   //              scaledSize: new google.maps.Size(30, 42)
   //          }
			// let map


			if($mapNav.length) {
					// _.log(mapNavTmpl)
				mapNavTmpl		= Handlebars.compile(mapNavTmpl)
				searhMapTmpl	= Handlebars.compile(searhMapTmpl)
			}

				// console.log(dataMap)
			if(dataLat !== undefined) {
				let map = new google.maps.Map(document.getElementById(mapId), {
					mapTypeId: 'roadmap',
			        zoom: 12,
			        center: dataMap
				})

				let marker = new google.maps.Marker({
					position: dataMap,
					map: map,
					icon: {
		                url: markerIcon,
		                scaledSize: new google.maps.Size(30, 42)
		            }
				})

				let infowindow = new google.maps.InfoWindow({
					content: `<h4 class="no-space">${title}</h4>`,
					maxWidth: 200
				})

				infowindow.open(map, marker)

			}else {
        		mapInit()
        	}
				
            function mapInit () {
				_.getJSON(dataMap).then(data => {
					let mapOptions = {
				        mapTypeId: 'roadmap',
				        zoom: 12,
				        center: {
	                        lat: data[0].locations[0].lat,
	                        lng: data[0].locations[0].lng
	                    }
				    }
				    let allLocation = []
				    let allArea 	= []
				    let map
					let bounds = new google.maps.LatLngBounds()
					map = new google.maps.Map(document.getElementById(mapId), mapOptions)

					let infoWindow = new google.maps.InfoWindow()
					

	                initMarkers()
	             //    _.log(allLocation)

	                if (navigator.geolocation) {
          				navigator.geolocation.getCurrentPosition(function(position) {
            				var pos = {
              					lat: position.coords.latitude,
              					lng: position.coords.longitude
            				}
            				let marker = new google.maps.Marker({
		                        map: map,
		                        position: pos
		                    })
				            map.setCenter(pos)
				            // console.log(position)
			          	})
			        }

		        	for (var i = 0; i < data.length; i++) {
		        		let areas = data[i].area

		        		allArea.push(areas)
		        	}

		        	$searchFilter.on('change', e => {
		        		bounds = new google.maps.LatLngBounds(null)
		        		let $this = $(e.currentTarget)
		        		let val = $this.val()

		        		let result = data.filter( function (areas) {
				            return areas.area === val
				        })
				        if(val === 'all') {
				        	$mapNavContainer.empty().append(mapNavTmpl(data))
				        	initMarkers()
				        } else {
	        				$mapNavContainer.empty().append(mapNavTmpl(result))
	        				let resLocation = result[0].locations
		        			for (var i = 0; i < resLocation.length; i++) {
		        				let resMarker = resLocation[i]

		                		mapBounds(resMarker)
		        			}
	        			}
		        	})

	                if($mapNav.length) {
		                $mapNavContainer.append(mapNavTmpl(data))
		                $searchFilter.append(searhMapTmpl(data))
		                $searchFilter.selectric()
		            }

		            $mapNav.on('click', '.btn-trigger', e=> {
		        		e.preventDefault()
		        		let $this 		= $(e.currentTarget)
		        		let lat 		= $this.data('lat')
		        		let lng 		= $this.data('lng')
		        		let panToLoc	= new google.maps.LatLng(lat, lng)

		        		map.panTo(panToLoc)
                        map.setZoom(17)
		        	})

		        	function initMarkers() {
		        		for (let i = 0; i < data.length; i++) {
		                	let locations = data[i].locations
		                	for (let j = 0; j < locations.length; j++) {
		                		allLocation.push(locations[j])
		                		let thisMarker = locations[j]
			                 	mapBounds(thisMarker)
		                	}
		                }
		        	}

		        	function mapBounds(markers) {
	                	let myLatlng = new google.maps.LatLng(markers.lat, markers.lng)
	                	let pins = data[0].locations.length
                		bounds.extend(myLatlng);

                		let marker = new google.maps.Marker({
	                        map: map,
	                        position: {
	                            lat: markers.lat,
	                            lng: markers.lng
	                        },
	                        icon: {
				                url: markerIcon,
				                scaledSize: new google.maps.Size(30, 42)
				            }
	                    })

	                    let infoContent = `<div class="hismap-info">
	                    <h4 class="no-space txt-red txt-up">${markers.location}</h4>
	                    ${markers.address}<br>
	                    <strong><a href="tel:${markers.telephone[0]}">${markers.telephone[0]}</a></strong>
	                    </div>`

	                    if(pins > 1) {
	                		map.fitBounds(bounds)
	                		map.panToBounds(bounds)
                		}

            			google.maps.event.addListener(marker, "click", function (e) {
	                        map.panTo(myLatlng)
	                        infoWindow.setContent(infoContent)
                            infoWindow.open(map, marker)
	                    })
	                }
				})
			}
			}

			// hismap()
			window.onload = hismap()
		}).catch(e=>{})
	}
}