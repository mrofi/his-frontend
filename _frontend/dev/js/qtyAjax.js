import * as _ from './utils'

export default {
	qtyAjax() {
		_.existJquery($('[data-items]')).then($qtyAjaxUpdater => {
			let isLoading = false
			$qtyAjaxUpdater.on('change', e => {
				let $this 			= $(e.currentTarget)
				let thisVal 		= $this.val()
				let dataUrl 		= $this.attr('data-items')
				let dataPath 		= $this.attr('data-path')
				let valUrl  		= dataPath+thisVal.split('/').join('')+'-'+dataUrl
				let $itemContainer 	= $('.item-container')
				let $sumContainer 	= $('.sum-container')
				let itemField 		= $this.data('tmplist')
				let itemSum 		= $this.data('tmplsum')
				let templateItem 	= $(itemField).html()
				let templateSum 	= $(itemSum).html()

				HandlebarsIntl.registerWith(Handlebars)
				// console.log(templateSum)
				templateItem 		= Handlebars.compile(templateItem)
				templateSum 		= Handlebars.compile(templateSum)
				if(!isLoading && valUrl) {
					isLoading = true
					_.getJSON(valUrl).then( data => {
						$('.total-input--all').html(0)
	                    $itemContainer.empty().append(templateItem(data))
	                    $sumContainer.empty().append(templateSum(data))
	                    // setTimeout(function() {
	                    	// Site.qtyInput()
	                    	$('.n-total').html(0)
	                    // }, 500)
	                }).catch(e => {
	                	isLoading = false
	                })
                }
			})
		}).catch(e=>{})
	}
}
