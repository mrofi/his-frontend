import * as _ from './utils'

export default {
	qrCode() {
		_.existJquery($('.qr-code')).then($qrcode => {

            function renderQR(elID, content) {
                new QRCode(document.getElementById(elID), {
                    text: content,
                    colorDark : "#000000",
                    colorLight : "#ffffff"
                })
            }

            for (var i = 0; i < $qrcode.length; i++) {
                let $this       = $qrcode.eq(i)
                let qrID        = $this.attr('id')
                let qrContent   = $this.parent().attr('href')

                // console.log(qrID, qrContent)

                renderQR(qrID, qrContent)
            }
		}).catch(e=>{})
	}
}

