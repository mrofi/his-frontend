import * as _ from './utils'

export default {
	dupliFormVal() {
		_.existJquery($('.copy-val')).then($copyVal => {
            let $destFields     = $('[data-origin]')

            $copyVal.on('change', '.btn-copy > input', e=> {
                let $this   = $(e.currentTarget)

                if($this.is(':checked')) {
                    let msg = $this.data('msg')
                    for (var i = 0; i < $destFields.length; i++) {
                        let $thisFields = $destFields.eq(i)
                        let origin      = '#'+$thisFields.data('origin')
                        let $origin     = $(origin)
                        let $radio      = $(origin+'[type="radio"]')
                        let originVal   = $origin.val()
                        
                        if(originVal === '') {
                            Site._popMsg(msg, 'danger', 10000)
                            $this.prop('checked', false)
                        } else {
                            $thisFields.val(originVal)
                            $thisFields.addClass('copied')
                            setTimeout(function() {
                                $thisFields.removeClass('copied')
                            }, 500)
                        }

                        if($radio.is(':checked')) {
                            let target = $radio.data('dest')
                            $('#'+target).prop('checked', true)
                        }
                    }
                }
            })
    	}).catch(e=>{})
	}
}