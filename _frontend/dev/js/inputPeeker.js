import * as _ from './utils'

export default {

	peeker() {
		_.existJquery($('.input-peeker')).then($peeker => {
			let isPeek = 'is-peek'
			let $inputPeek = $('.input-peek')

			$peeker.on('click', '.btn-peek', e => {
				let $this = $(e.currentTarget)
				let $input = $($this.data('input'))

				if($this.hasClass( isPeek )) {
					$input.attr('type', 'password')
					$this.removeClass( isPeek )
				} else {
					$this.addClass( isPeek )
					$input.attr('type', 'text')
				}
			})

			// $inputPeek.focusout(e => {
			// 	let $this = $(e.currentTarget)
			// 	$this.attr('type', 'password')
			// })
		})
		.catch(_.noop)
	}
}