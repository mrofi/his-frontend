import * as _ from './utils'

export default {
	slideOnMobile() {
		_.existJquery($('.slide-on-mobile')).then($slider => {
			let resizeTime
			let isMedium = matchMedia('screen and (max-width: 72em)').matches

			initSlider()

			// _.$win.on('resize', e=>{
			// 	clearTimeout(resizeTime)

			// 	resizeTime = setTimeout(()=>{
			// 		var isResizeMedium = _.$win.width() < 1152

			// 		initSlider(isResizeMedium)
			// 	}, 250)
			// })

			function initSlider() {

				for (var i = 0; i < $slider.length; i++) {
					let $thisSlider = $slider.eq(i)
					let smallSlides = $thisSlider.data('slide-small')
					let mediumSlides = $thisSlider.data('slide-medium')
                    let $item       = $thisSlider.find('.slides__item')
                    let isCenter    = $thisSlider.data('center')

                    smallSlides = Number(smallSlides)

					// if(isMatch) {
     //                    console.log(isMatch)
					// 	buildSlider($thisSlider, smallSlides, mediumSlides, $item, isCenter)
					// } else {
					// 	$thisSlider.slick('unslick')
					// }
                    buildSlider($thisSlider, smallSlides, mediumSlides, $item, isCenter)
				}
			}

			function buildSlider($el, small, medium, $item, center = true) {
                let mediumSmall = small + 1
                
				$el.not('.slick-initialized').slick({
					slidesToShow: small,
	                // arrows: false,
	                // dots: true,
	                prevArrow: _.slickPrev,
	                nextArrow:  _.slickNext,
	                infinite: false,
                    // centerMode: true,
                    mobileFirst: true,
                    focusOnSelect: true,
                    centerMode: center,
                    centerPadding: '30px',
                    responsive: [
                        {
                            breakpoint: 1152,
                            settings: 'unslick'
                        },
                        {
                            breakpoint: 760,
                            settings: {
                                slidesToShow: medium,
                                centerMode: false
                            }
                        },
                        {
                            breakpoint: 540,
                            settings: {
                                slidesToShow: mediumSmall,
                                centerMode: false
							}
						}
					]
				})

                // $el.on('swipe', (slick, currentSlide, nextSlide)=> {
                //     let current    = $el.slick('slickCurrentSlide')
                //     let $current    = $el.find('.slick-current')

                //     console.log(current)

                //     $item.removeClass('prev-slide')
                //     // $current.prev().addClass('prev-slide')
                //     $item.eq(current - 1).addClass('prev-slide')
                // })

                $el.on('beforeChange', (event, slick, currentSlide, nextSlide)=> {
                    // let current    = $el.slick('slickCurrentSlide') - 1
                    let $current   = $el.find('.slick-current')
                    let nextItem        = nextSlide - 1

                    $item.removeClass('prev-slide')
                    // $current.prev().addClass('prev-slide')
                    $item.eq(nextItem).addClass('prev-slide')
                })

                // $el.on('init', (event, slick, currentSlide)=> {
                //     // let current    = $slider.slick('slickCurrentSlide')
                //     let $current    = $slides.find('.slick-current')

                //     $item.removeClass('prev-slide')
                //     $current.prev().addClass('prev-slide')
                // })
			}
		}).catch(e=>{})
	}
}
