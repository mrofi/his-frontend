import * as _ from './utils'

export default {
	slideVideo() {
		_.existJquery($('.slide-video')).then($slider => {
			$slider.slick({
                waitForAnimate: false,
                arrows: false,
                dots: true
			})

			let $video      = $slider.find('.video-iframe')

            $slider.on('afterChange', function(event, slick, currentSlide, nextSlide){

                for (var i = $video.length - 1; i >= 0; i--) {

                    $video[i].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
                };
            });
		}).catch(e=>{})
	}
}