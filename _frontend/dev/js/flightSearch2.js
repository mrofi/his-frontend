import * as _ from './utils'

export default {
	flightSearch() {
		Promise.all([
            _.existJquery($('[data-flight]'))
        ]).then(res => {
            let $container  	 = res[0]
            let flightData  	 = $container.attr('data-flight')
            let $originContainer = $('#originRes')
            let $returnContainer = $('#returnRes')
            let $loader          = $($container.data('loader'))

            $container.addClass('is-changed')
            renderFlightData(flightData, $originContainer, $returnContainer, $container, $loader)

        }).catch(e => {})
	}
}

let choosenFlight
let choosenFlight0 	= []
let choosenFlight1 	= []
let flightTransits 	= 0
let $flightModal 	= $('#flightsChoosenConfirm')
let btnBook 		= '#flightsChoosenConfirm .btn-book'
let inputSearchId   = '#flightsChoosenConfirm input[data-search]'
let inputFlightsId  = '#flightsChoosenConfirm input[data-flight]'
let inputPassenger  = '#flightsChoosenConfirm input[data-ps]'
let btnCloseModal 	= '#flightsChoosenConfirm .btn-close'
let overlayModal 	= '#flightsChoosenConfirm .overlay'
let pessangers      = []

const renderFlightData = (data, originContainer, returnContainer, allContainer, loader) => {
	if(data) {
	_.getJSON(data).then( data => {
        loader.hide()
        let resMsg          = data.Messages[0].Type.toLowerCase()

        if(resMsg === 'warning') {
            $('#flightNotFound').removeClass('hidden')
            return
        }
		let flightIndexes 	= data.FlightIndexes
		let flightsOrigin 	= flightIndexes[0].FlightDetails
		let flightsReturn
		if (flightIndexes.length > 1) flightsReturn 	= flightIndexes[1].FlightDetails
		// console.log(flightsReturn)
		let $origin 		= originContainer.find('.res-container')
		let $return 		= returnContainer.find('.res-container')

		let $oriTransit 	= originContainer.find('.transit-filter')
		let $oriAirline 	= originContainer.find('.airline-filter')

		let airPlaneOrigin  = []
		let transitOrigin   = []

        // store data timestamp
        sessionStorage.setItem('flightSearch', new Date())
        // store data pessangers
        pessangers.push(data.ResultPassengers)
        // get transit count
		flightTransits      = flightIndexes.length

		flightsOrigin.map(oriDetail => {
			transitOrigin.push(oriDetail.FlightSequences.length - 1)

			oriDetail.FlightSequences.map(oriPlane => {
				airPlaneOrigin.push(oriPlane.Airline)
			})
		})

		transitOrigin 		= _.nub(transitOrigin)
		transitOrigin 		= transitOrigin.sort()

		airPlaneOrigin 		= _.nubBy(airPlaneOrigin, 'Code')
		airPlaneOrigin 		= airPlaneOrigin.sort(_.compareValues('Code'))
		flightsOrigin 		= flightsOrigin.sort(_.compareValues('TotalFare'))

		renderResItems(flightsOrigin, $origin, '0')

		renderFilterAirline(airPlaneOrigin, $oriAirline, '0')
		renderFilterTransit(transitOrigin, $oriTransit, '0')
		
		if(flightsReturn !== undefined) {
			let $retTransit 	= returnContainer.find('.transit-filter')
			let $retAirline 	= returnContainer.find('.airline-filter')
			let airPlaneReturn 	= []
			let transitReturn 	= []

			flightsReturn.map(returnDetail => {
				transitReturn.push(returnDetail.FlightSequences.length - 1)
				returnDetail.FlightSequences.map(returnPlane => {
					airPlaneReturn.push(returnPlane.Airline)
				})
			})

			transitReturn 		= _.nub(transitReturn)
			transitReturn 		= transitReturn.sort()

			airPlaneReturn 		= _.nubBy(airPlaneReturn, 'Code')
			airPlaneReturn 		= airPlaneReturn.sort(_.compareValues('Code'))
			flightsReturn 		= flightsReturn.sort(_.compareValues('TotalFare'))

			renderResItems(flightsReturn, $return, '1')
			renderFilterAirline(airPlaneReturn, $retAirline, '1')
			renderFilterTransit(transitReturn, $retTransit, '1')
			renderSelectedFlight(flightsReturn, returnContainer, 1)
			filterItems(flightsReturn, returnContainer, $return, 1)
		}

		// filterItem(flightsOrigin, originContainer, $origin, '0')
		// sortItem(flightsOrigin, originContainer, $origin)

		renderSelectedFlight(flightsOrigin, originContainer, 0)

		editSelectedFlight(allContainer, originContainer)

		filterItems(flightsOrigin, originContainer, $origin, 0)

		// $('#flightsChoosenConfirm .btn-book').attr('data-search', data.ResultID)
		$(btnBook).attr({
			'data-search' : data.ResultID,
			'data-ps': data.ResultPassengers
		})

        $(inputSearchId).val(data.ResultID).attr('data-search', data.ResultID)
        $(inputPassenger).val(data.ResultPassengers).attr('data-ps', data.ResultPassengers)

		_.$doc.on('click', btnCloseModal, e => {
			choosenFlight1 = []
			returnContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
		})
		_.$doc.on('click', overlayModal, e => {
			choosenFlight1 = []
			returnContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
		})

        _.showToolTip($('[data-target-content]'))

        allContainer.on('click', '[data-book]', e => {
            let $this   = $(e.currentTarget)
            let ref     = $this.data('book')
            e.preventDefault()

            returnContainer.find(`[data-ref="${ref}"] [data-selected]`).trigger('change')
        })
		
	})
	.catch(_.noop())
	}
}

const clearSelection = (originContainer, returnContainer) => {
	if(flightTransits > 1) {
		choosenFlight = []
		originContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
	} else {
		returnContainer.find('[data-selected]').removeAttr('checked').prop('checked', false)
	}
}

const filterItems = (data, $wrapper, $container, i) => {

	$wrapper.on('change', '.row-sort input', e => {
		let $this 		  = $(e.currentTarget)
    	let $form   	  = $this.closest('form')
    	let serial 		  = $form.serialize()
    	let item 		  = _.decodeUri(serial)
    	let sorter  	  = item.sort.split('_')
    	let sortItem	  = sorter[0]
    	let sortOrder 	  = sorter[1]
    	let dataSorted    = data.sort(_.compareValues(sortItem, sortOrder))
    	let filterTransit = item.FlightTransit.split('%2C')
        let filterAirline = item.FlightAirline.split('%2C')
        let filterFlight  = filterTransit.concat(filterAirline)
    	let isEmptyTransit= filterTransit.includes("-1") || filterTransit[0] === ''
        let isEmptyAirline= filterAirline.includes("-1") || filterAirline[0] === ''
        let isEmptyFilter = isEmptyTransit || isEmptyAirline
    	let dataFiltered

        // console.log(filterAirline)
        let allTransit    = () => {
            let collected = []
            dataSorted.map(t => {
                collected.push(t.FlightTransit[0])
            })

            return _.nub(collected)
        }

        let allAirplane     = () => {
            let collected = []
            dataSorted.map(t => {
                collected.push(t.FlightAirlineCode)
            })

            return _.nub(collected)
        }


        if(!filterTransit[0].length) {
            filterTransit = allTransit()
        }

        if(!filterAirline[0].length) {
            filterAirline = allAirplane()
        }
        // console.log(filterAirline)
        // filterTransit = _.sortAsc(filterTransit)

    	// if(isEmptyFilter) {
     //        dataFiltered = dataSorted
     //        console.log('ASU')
        // } else {
            dataFiltered = _.getFilter(dataSorted, {FlightAirlineCode: filterAirline, FlightTransit : filterTransit})
        // }
        

    	renderResItems(dataFiltered, $container, i)
    	// window.history.pushState('', '', '?'+serial)
	})
}

const filterFor = (comp, arr) => {
    for (var i = 0; i < arr.length; i++) {
        comp === arr[i]
    }
}

const renderFilterTransit = (data, container, idx) => {
	data.map((t, i) => {
		container.append(filterTransit(t, idx, i))
	})
}

const filterTransit = (data, idx, i) => {
	return `
<li>
    <label class="btn-label" for="transit_${data}${idx}">
        <input class="input-check" data-checker="relCheck${idx}" type="checkbox" id="transit_${data}${idx}" name="filter_transit_${idx}${data}" value="${data}">
        <span>${data == 0 ? `Direct` : `${data}x transit`}</span>
    </label>
</li>`
}

const renderFilterAirline = (data, container, i) => {
	// container.empty()
	data.map(f => {
		container.append(filterAirline(f, i))
	})
}

const filterAirline = (data, i) => {
	return `
<li>
    <label class="btn-label" for="airline_${data.Code}${i}" title="${data.Name}">
        <input class="input-check" data-checker="relCheckAirplane${i}" type="checkbox" id="airline_${data.Code}${i}" name="airline_${data.Code}${i}" data-val="${data.Code}" value="${data.Code}">
        <span class="text-ellipsis">${data.Name}</span>
        <img src="${data.Logo}" alt="${data.Code}" width="18" height="18">
    </label>
</li>`
}

const renderResItems = (itemsData, container, i) => {
	container.empty()
	itemsData.map((item) => {
		container.append(flightItem(item, i))
	})
} 


const flightItem = (data, i) => {
	let f_Seq 	 = data.FlightSequences
	let lastSeq  = _.lastArr(f_Seq)
	let isTransit= f_Seq.length > 1
	let fares    = data.Fares[0]
    let airlineCode = $('[data-airline]').attr('data-airline')
    let dataAirLineCode = f_Seq[0].Airline.Code.toLowerCase()
    let Logos    = []
    f_Seq.map(fs=> {
        Logos.push(fs.Airline)
    })
    Logos = _.nubBy(Logos, 'Logo')

    let templates = `
<div class="block--half pv-12 ph-16 border flight__item" id="item_${data.Id}" data-ref="ref-${data.RefNumber}">
    <div class="mb-small bzg bzg--small-gutter">
        <div class="bzg_c" data-col="s5, l7">
            <div class="bzg bzg--small-gutter nowrap">
                <div class="bzg_c text-center" data-col="m3, l6">
                    <div class="airlines-logo">
                    ${Logos.map(f => `<img src="${f.Logo}" alt="" class="img-contain airways-logo airways-logo--${f.Code}">`).join('')}
                    </div>
                    <small class="text-ellipsis">${f_Seq[0].Airline.Name}</small>
                </div>
                <div class="bzg_c line--small text-center" data-col="s4, m3, l2">
                    <div class="line--40 t-strong t--larger-l">${f_Seq[0].Departure.Time}</div>
                    <small>${f_Seq[0].Departure.Location}</small>
                </div>
                <div class="bzg_c line--small text-center" data-col="s4, m3, l2">
                    <div class="line--40 t-strong t--larger-l">${f_Seq[lastSeq].Arrival.Time}</div>
                    <small>${f_Seq[lastSeq].Arrival.Location}</small>
                </div>
                <div class="bzg_c line--small text-center" data-col="s4, m3, l2">
                    <div class="line--40 t-strong t--larger-l">${_.fDur(data.FlightDuration)}</div>
                    <small>${isTransit ? 'Transit' : 'Langsung'}</small>
                </div>
            </div>
        </div>
        <div class="bzg_c text-center" data-col="s3, l2">
            <div data-target-content="#tip-${data.Id}" data-tip-size="auto">
            <span class="his-koper"></span>
            ${fares.MealAllowance !== null ? `
                <span class="fa fa-cutlery" aria-hidden="true" title="Makanan ${fares.MealAllowance} ${fares.MealUnit}"></span>
            `: ``}
            </div>
            <div class="tip-content" id="tip-${data.Id}">Bagasi ${fares.BagAllowance} ${fares.BagUnit}</div>
        </div>
        <div class="bzg_c text-center" data-col="s4, l3">
            <div class="mb-small">
                <div class="line--40 t-strong t--larger-l text-red">
                    IDR ${_.currencyIDformat(fares.AdultFare)}
                </div>
                <small>Harga per Pax</small>
            </div>
            <label for="flight_${data.Id}" class="btn btn--smaller btn-check-flight btn--ghost-check btn--round ${i > 0 ? `invisible`: ``}">
                <input type="radio" id="flight_${data.Id}" name="flight-${i}" data-selected="${data.Id}" data-container="#selectedFlight${i}" data-ref-target="ref-${data.RefNumber}">
                <span class="bg-label"></span>
                <strong class="btn-text text-up">Pilih</strong>
            </label>
        </div>
    </div>
    <nav class="flight-item-nav">
        <a href="#f_D_${i}_${data.RefNumber}" data-panel-target="#f_D_${i}_${data.RefNumber}">
            Detail Penerbangan
            <span class="fa fa-caret-down"></span>
        </a>
        <a href="#f_P_${i}_${data.RefNumber}" data-panel-target="#f_P_${i}_${data.RefNumber}">
            Detail Harga
            <span class="fa fa-caret-down"></span>
        </a>
    </nav>
    <div class="panel-content t--smaller" id="f_D_${i}_${data.RefNumber}">
        ${
            data.FlightSequences.map((fs, i) => `${
                flightDetail(fs, i, data.FlightSequences.length, data.Fares)
            }`).join('')
        }
    </div>
    <div class="panel-content t--smaller" id="f_P_${i}_${data.RefNumber}">
        <div class="bzg">
        <div class="bzg_c" data-col="m6"><table class=" table--zebra"><tbody>
            ${
                data.Fares.map((df, i) => `${
                    priceDetail(df)
                }`).join('')
            }
        </tbody></table></div>
        </div>
    </div>
</div>`
    if(airlineCode !== undefined) {
        airlineCode.toLowerCase()
    }
	return `${dataAirLineCode === airlineCode || airlineCode === undefined || airlineCode === '' ? templates:``}`
    // return templates
}

const flightDetail 	= (data, i, seq, fares) => {
	let depart 		= data.Departure
	let arrival 	= data.Arrival
	let airline 	= data.Airline

	return `
<div class="bzg bzg--small-gutter bzg--v-center">
    <div class="bzg_c" data-col="s8, l9">
        <div class="flight-detail flight-detail--start">
            <time class="time">
                <strong>${depart.Time}</strong><br>
                ${_.dateFormat(depart.Date, 'DD_MMM')}
            </time>
            <div class="content">
                ${depart.Name} (${depart.Location})
            </div>
        </div>
        <div class="flight-detail flight-detail--duration">
            <time class="time">${_.fDur(data.FlightDuration)}</time>
            <div class="content">
                <img src="${airline.Logo}" alt="" width="36"> ${airline.FlightNo}, ${flightClass(data.Eticket)}<br>
                Dioperasikan oleh: ${data.OperatingCompanyName}
            </div>
        </div>
        <div class="flight-detail flight-detail--end">
            <time class="time">
                <strong>${arrival.Time}</strong><br>
                ${_.dateFormat(arrival.Date, 'DD_MMM')}
            </time>
            <div class="content">
                ${arrival.Name} (${arrival.Location})
            </div>
        </div>
    </div>
    <div class="bzg_c" data-col="s4, l3">
        <div class="mb-small nowrap">
            <span class="his-koper"></span>
            Bagasi ${fares[0].BagAllowance} ${fares[0].BagUnit}
        </div>
        ${fares[0].MealAllowance !== null ? `
        <div class="mb-small nowrap">
            <span class="fa fa-cutlery" aria-hidden="true"></span>
            Makanan
        </div>` : ``}
    </div>
</div>
${i + 1 != seq ? `
<div class="mb-half block--inset-small fill-lightgrey">
    Transit for ${_.fDur(data.TransitDuration)} in ${arrival.Name} (${arrival.Location})
</div>` : ``}`
}

const priceDetail = (data) => {
    let ps      = pessangers[0]
    let adult   = ps[0]
    let child   = ps[1]
    let infant  = ps[2]
    let adultFare = data.AdultFare * adult
    let childFare = data.ChildFare * child
    let infantFare= data.InfantFare * infant

	return `
<tr><td>Adult Fare <small>tax included</small> (x${adult})</td>
<td class="text-right">IDR ${_.currencyIDformat(adultFare)}</td></tr>
${child > 0 ? `<tr><td>Child Fare <small>tax included</small> (x${child})</td>
<td class="text-right">IDR ${_.currencyIDformat(childFare)}</td></tr>` : ``}
${infant > 0 ? `<tr><td>Infant Fare <small>tax included</small> (x${infant})</td>
<td class="text-right">IDR ${_.currencyIDformat(infantFare)}</td></tr>` : ``}
<tr><td>VAT 1%</td><td class="text-right">IDR ${_.currencyIDformat(data.VAT)}</td></tr>
<tr><td><strong>Total Fare</strong></td><td class="text-right"><strong class="text-red">IDR ${_.currencyIDformat(data.TotalFare)}</strong></td></tr>`
}

const price_Detail2 = (data) => {
    return `
<tr><td>Total Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.TotalFare)}</td></tr>
<tr><td>Total Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.TotalTax)}</td></tr>
<tr><td>Adult Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.AdultFare)}</td></tr>
<tr><td>Adult Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.AdultTax)}</td></tr>
<tr><td>Child Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.ChildFare)}</td></tr>
<tr><td>Child Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.ChildTax)}</td></tr>
<tr><td>Infant Fare</td><td class="text-right">IDR ${_.currencyIDformat(data.InfantFare)}</td></tr>
<tr><td>Infant Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.InfantTax)}</td></tr>
<tr><td>Airport Tax</td><td class="text-right">IDR ${_.currencyIDformat(data.AirportTax)}</td></tr>
<tr><td>Insurance</td><td class="text-right">IDR ${_.currencyIDformat(data.Insurance)}</td></tr>
<tr><td>Iwjr</td><td class="text-right">IDR ${_.currencyIDformat(data.Iwjr)}</td></tr>
<tr><td>Others</td><td class="text-right">IDR ${_.currencyIDformat(data.Others)}</td></tr>
<tr><td>VAT</td><td class="text-right">IDR ${_.currencyIDformat(data.VAT)}</td></tr>`
}


const getItemId = (arr, id) => {
	function selectedId(ids) { return ids.Id === id }
	return arr.find(selectedId)
}

const editSelectedFlight = (container, itemContainer) => {
	container.on('click', '.edit-flight-start', e => {
		let $this 			= $(e.currentTarget)
		let $selectedFlight = itemContainer.find('[data-selected]')
		let $thisSelected   = $this.closest('.selected-flight')

		container.removeClass('flight-selected is-changed')
		$selectedFlight.removeAttr('checked')
		$selectedFlight.prop('checked', false)
		$thisSelected.empty()
		choosenFlight0 = []

        setTimeout(() => {
            container.addClass('is-changed')
            _.goToTop()
        }, 250)

		e.preventDefault()
	})
}

const renderSelectedFlight = (data, wrapper, i) => {
	// let dataSelected = 
	wrapper.on('change', '[data-selected]', e => {
		let $this 		= $(e.currentTarget)
		let $target 	= $($this.data('container'))
		let dataId 		= $this.data('selected')
		let dataSelected= getItemId(data, dataId)
		let $container 	= $this.closest('[data-flight]')
		let dataName    = $this.attr('name')
        let itemRef     = $this.data('ref-target')

		// console.log(flightTransits)
        $container.find(`.res--return [data-ref]`).removeClass('is-active')
        $container.find(`.res--return [data-ref='${itemRef}']`).addClass('is-active')
		if(flightTransits > 1) {
        $container.removeClass('is-changed')
			$target.empty()
			$target.append(selectedFlight(dataSelected, i, itemRef)) 

			$container.addClass('flight-selected')
            setTimeout(() => {
        // console.log(flightTransits)
                $container.addClass('is-changed')
                _.goToTop()
            }, 250)
		}

		if(dataName === 'flight-0') {
			choosenFlight0 = []
			choosenFlight0.push(dataSelected)
		} else { 
			choosenFlight1 = []
			choosenFlight1.push(dataSelected)
		}
		
		let price0 	  = choosenFlight0[0].TotalFare
		let flightId0 = choosenFlight0[0].Id
		let flightId1 = null
		let price1	  = 0
		let flightDataId = flightId0
		if(choosenFlight1.length > 0) {
			price1    = choosenFlight1[0].TotalFare
			flightId1 = choosenFlight1[0].Id

			flightDataId = [flightId0, flightId1]
		}

		// let totalPrice= price0 + price1
        let totalPrice= price0
		choosenFlight = choosenFlight0.concat(choosenFlight1)

		$('#flightsDetailConfirm').empty().append(flightsDetailConfirm(choosenFlight))
		$('#flightsDetailInfo').empty().append(flightInfoDetailConfirm(choosenFlight))
		$('#pricesDetailInfo').empty().append(priceInfoDetailConfirm(choosenFlight))

		if(choosenFlight.length == flightTransits) {
			$('.total-flight-price span').html(_.currencyIDformat(totalPrice))
			$flightModal.addClass('is-active')
			$(btnBook).attr('data-flight', [flightDataId])
            $(inputFlightsId).val([flightDataId]).attr('data-flight', [flightDataId])
		}
	})
}

const priceInfoDetailConfirm = data => {
	return `
<div class="bzg">
	<div class="bzg_c" data-col="m6"><table class=" table--zebra"><tbody>
		${
			data[0].Fares.map((df, i) => `${
				priceDetail(df)
			}`).join('')
		}
	</tbody></table></div></div>`
}

const priceInfoDetailConfirm_2 = data => {
    return `
<div class="bzg">
${data.map((d, i) => `
    <div class="bzg_c" data-col="m6"><h3 class="mb-0 text-blue">${i == 0 ? `Berangkat` : `Pulang`}</h3><table class=" table--zebra"><tbody>
        ${
            d.Fares.map((df, i) => `${
                priceDetail(df)
            }`).join('')
        }
    </tbody></table></div>`).join('')}</div>`
}

const flightInfoDetailConfirm = (data) => {
	return `
${data.map((d, t) => `
	<h3 class="mb-0 text-blue">${t == 0 ? `Berangkat` : `Pulang`}</h3>
	${d.FlightSequences.map((fs, i) => `${
		flightDetail(fs, i, d.FlightSequences.length, d.Fares)
	}`).join('')}
`).join('')}`
}

const flightsDetailConfirm = (data) => {
	return `
${data.map((d, i) => `
${i == 1 ? `<hr class="mb-small">`:``}
<div class="block bzg line--small">
	<div class="bzg_c" data-col="m6, l3">
		${i == 0 ? `Pergi` : `Pulang`}<br>
		<small class="text-grey">${_.dateFormat(d.FlightStart, 'DDDD_DD_MMMM_YYYY')}</small>
	</div>
	<div class="bzg_c mb-small" data-col="m6, l3">
		<div class="flex a-center">
			<img src="${d.FlightSequences[0].Airline.Logo}" alt="" width="36">
			<div class="ml-small fg-1">
				${d.FlightSequences[0].Airline.Name}<br><span class="text-grey">${flightClass(d.FlightSequences[0].Eticket)}</span>
			</div>
		</div>
	</div>
	<div class="bzg_c" data-col="s4, l2">
		${d.FlightSequences[0].Departure.Time}<br><span class="text-grey">${cityName(d.FlightSequences[0].Departure.Name)} (${d.FlightSequences[0].Departure.Location})</span>
	</div>
	<div class="bzg_c" data-col="s4, l2">
		${d.FlightSequences[_.lastArr(d.FlightSequences)].Arrival.Time}<br><span class="text-grey">${cityName(d.FlightSequences[_.lastArr(d.FlightSequences)].Arrival.Name)} (${d.FlightSequences[_.lastArr(d.FlightSequences)].Arrival.Location})</span>
	</div>
	<div class="bzg_c" data-col="s4, l2">
		Total Durasi<br>
		<span class="text-grey">${_.fDur(d.FlightDuration)}, ${d.FlightSequences > 1 ? 'Transit' : 'Langsung'}</span>
	</div>
</div>
`).join('')}`
}

const selectedFlight = (data, i, ref) => {
	let f_Seq 	 = data.FlightSequences
	let firstSeq = f_Seq[0]
	let lastSeq  = f_Seq[_.lastArr(f_Seq)]
	let Departure= firstSeq.Departure
	let Arrival  = lastSeq.Arrival
	let Airline  = firstSeq.Airline
	let Class 	 = firstSeq.Eticket
	let isTransit= f_Seq.length > 1

	return `
<div class="block">
<div class="block-half border">
    <div class="p-small fill-lightgrey">
        <span class="fa fa-plane text-blue ${i == 1 ? ` fa-rotate-180`:``}"></span>
        <strong class="ml-small text-up text-blue">Penerbangan ${i == 0 ? `Pergi`:`Pulang`}</strong>
    </div>
    <div class="p-small">
        <div class="t-bold mb-small">${Departure.Time} - ${cityName(Departure.Name)} (${Departure.Location})</div>
        <div class="direct-container">
            <div class="flex a-start">
                <img src="${Airline.Logo}" alt="" width="36">
                <div class="fg-1 ml-small">
                    <strong>${Airline.Name}</strong><br>
                    <small><span class="fa fa-calendar"></span> ${_.dateFormat(data.FlightStart, 'DDDD_DD_MMMM_YYYY')}</small>
                </div>
            </div>
        </div>
        <div class="t-bold mb-small">${Arrival.Time} - ${cityName(Arrival.Name)} (${Arrival.Location})</div>
        <small class="text-grey">
            <span class="fa fa-info-circle"></span>
            Waktu diatas adalah waktu setempat
        </small>
    </div>
</div>
<div class="block-half border p-small">
	<table class="mb-0 line--small"><tbody>
		<tr>
			<td>Harga pergi<br><em class="text--smaller">(Termasuk semua pajak dan biaya penerbangan)</em></td>
			<td class="nowrap text-right"><strong class="text-red">IDR ${_.currencyIDformat(data.Fares[0].AdultFare)}</strong></td>
		</tr>
	</tbody></table>
	${i == 0 ? `<div class="p-8 text-center fill-lightgrey">
	    <a href="#" class="edit-flight-start">Ganti Penerbangan Pergi</a>
	</div>` : ``} 
</div>
<button class="btn btn--round btn--red" type="button" data-book="${ref}">Lanjutkan</button>
</div>
`
}

const selectedFlight2 = (data, i) => {
	let f_Seq 	 = data.FlightSequences
	let firstSeq = f_Seq[0]
	let lastSeq  = f_Seq[_.lastArr(f_Seq)]
	let Departure= firstSeq.Departure
	let Arrival  = lastSeq.Arrival
	let Airline  = firstSeq.Airline
	let Class 	 = firstSeq.Eticket
	let isTransit= f_Seq.length > 1

	return `
<div class="block text-center t-bold">
    <div class="mb-0 h2 text-red">(${Departure.Location}) <span class="fa fa-exchange"></span> (${Arrival.Location})</div>
    ${_.dateFormat(data.FlightStart, 'DDDD_DD_MMMM_YYYY')}
    <div class="mb-0 h2 text-red">IDR ${_.currencyIDformat(data.TotalFare)}</div>
</div>
<div class="border">
<div class="p-8">
    <div class="flex a-center line--small mb-half">
        <img src="${Airline.Logo}" alt="" width="36">
        <div class="fg-1 ml-half">
            ${Airline.Name}<br><span class="text-grey">${flightClass(Class)}</span>
        </div>
    </div>
    <table class="mb-0 t--smaller">
    <tbody>
        <tr class="h1">
            <td>${Departure.Time}</td><td>${Arrival.Time}</td>
        </tr>
        <tr class="text-grey">
            <td>${cityName(Departure.Name)} (${Departure.Location})</td>
            <td class="pl-small">${cityName(Arrival.Name)} (${Arrival.Location})</td>
        </tr>
        <tr>
            <td>
                <strong>Total Durasi</strong><br>
                ${_.fDur(data.FlightDuration)}, ${isTransit ? 'Transit' : 'Langsung'}
            </td>
            <td class="pl-small t--larger"><a href="#">Detail</a></td>
        </tr>
        <tr>
            <td><strong>Harga Pergi</strong></td><td></td>
        </tr>
        <tr>
            <td class="line--small">
                <em>(Termasuk semua pajak dan biaya penerbangan)</em>
            </td>
            <td class="pl-small">
                <strong class="text-red nowrap">IDR ${_.currencyIDformat(data.TotalFare)}</strong>
            </td class="pl-small">
        </tr>
    </tbody>
    </table>
</div>
${i == 0 ? `<div class="p-8 text-center fill-lightgrey">
    <a href="#" class="edit-flight-start">Ganti Penerbangan Pergi</a>
</div>` : ``} 
</div>`
}

const flightClass = (code) => {
	if(code === 'A') return `First`
	else if (code === 'J' || code === 'C' || code === 'D' || code === 'I' || code === 'R') return `Business`
	else if (code === 'W' || code === 'E' || code === 'T') return `Premium Economy`
	else return `Economy`
}

const cityName = (name) => {
	let fullName = name.split('(')[1]
	let names 	= fullName.split(')')[0]

    // console.log(names)

	return names
}
