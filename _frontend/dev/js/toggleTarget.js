import * as _ from './utils'

export default {
	toggleTarget() {
		_.existJquery($('.toggle-trigger')).then($toggler => {
			$toggler.on('click', e=> {
				let $this 	= $(e.currentTarget)
				let $target = $($this.data('target'))
				e.preventDefault()
				$this.toggleClass(_.classes.isActive)
				$target.toggleClass(_.classes.isActive)
			})
		}).catch(e=>{})

		
		let $doc 	= $(document)
		let btn 	= '[data-panel-target]'

		$doc.on('click', btn, e=> {
			e.preventDefault()
			let $this 	= $(e.currentTarget)
			let $target = $($this.data('panel-target'))
			let $btns 	= $(btn)
			let $panels = $('.panel-content')

			if($this.hasClass(_.classes.isActive)) {
				$this.removeClass(_.classes.isActive)
				$target.removeClass(_.classes.isActive)
			} else {
				$btns.removeClass(_.classes.isActive)
				$panels.removeClass(_.classes.isActive)
				$this.addClass(_.classes.isActive)
				$target.addClass(_.classes.isActive)

			}
		})
	}
}