import * as _ from './utils'

export default {
	tabNav() {
		_.existJquery($('.tab-nav')).then($tabNav => {
			$tabNav.on('click', 'a', e=> {
				let $this 	= $(e.currentTarget)
				let target 	= $this.attr('href')
				let $target = $(target)
				let $allPanel= $target.siblings()
				let $allNav = $this.parent().siblings()
				$allNav.removeClass(_.classes.isActive)
				$allPanel.removeClass(_.classes.isActive)
				// console.log(target)

				$this.parent().addClass(_.classes.isActive)
				$target.addClass(_.classes.isActive)

				e.preventDefault()
			})
		}).catch(e=>{})
	}
}