import * as _ from './utils'

export default {
	imgPopup() {
		_.existJquery($('.imgPopup')).then($imgPopup => {
			let imgPopup = '.imgPopup'

			baguetteBox.run(imgPopup, {
				captions: function(element) {
			        return element.getElementsByTagName('img')[0].alt
			    },
			    onChange: function(currentIndex, imagesElements) {
			    	let curImg  = imagesElements[currentIndex]
			    	let imgSrc 	= curImg.getElementsByTagName('img')[0].src
			    	let $figZoom= $(imagesElements[currentIndex]).find('.img-container')
			    	
			    	$figZoom.zoom({
			    		url: imgSrc
			    	})
			    }
			})
		}).catch(e=>{})
	}
}