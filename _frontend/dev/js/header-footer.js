import * as _ from './utils'
import activeStateMobile from './activeStateMobile'
import WPViewportFix from './windowsPhoneViewportFix'
import objectFitPolyfill from './objectFitPolyfill'
import headroom from './headroom'
import navToggle from './navToggle'
import toggleTarget from './toggleTarget'
import subMainNav from './subMainNav'
import btnToTop from './btnToTop'

const App = {
    ...activeStateMobile,
    ...WPViewportFix,
    ...objectFitPolyfill,
    ...headroom,
    ...navToggle,
    ...toggleTarget,
    ...subMainNav,
    ...btnToTop
}

for (let fn in App) {
    if(fn[0] !== '_')
        App[fn]()
}

window.Site = App

export default App
