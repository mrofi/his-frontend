import * as _ from './utils'

export default {
	slide5() {
		_.existJquery($('.slide-5')).then($slider => {
			$slider.slick({
				slidesToShow: 5,
                waitForAnimate: false,
                prevArrow: _.slickPrev,
                nextArrow:  _.slickNext,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 3
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
					}
				]
			})
		}).catch(e=>{})
	}
}