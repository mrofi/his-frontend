import * as _ from './utils'

export default {
	rating() {
		_.existJquery($('.add-rating')).then($addRating => {
			let $btnStar 	= $('.btn-star')
			let radioCheck 	= '.btn-radio'

			$btnStar.hover(
                function(){ 
                 $(this).addClass('hover');
                 $(this).prevAll().addClass('hover');
             },
             function(){ 
                 $(this).removeClass('hover');
                 $(this).prevAll().removeClass('hover');
             }
             )

            $addRating.on('change', radioCheck, e => {
            	let $this 	= $(e.currentTarget)
                if($this.is(':checked')) {
                    $this.parent().siblings().removeClass('checked')
                    $this.parent().nextAll().removeClass('checked')
                    $this.parent().prevAll().addClass('checked')
                    $this.parent().addClass('checked')
                } else {
                    $this.parent().removeClass('checked')
                }
            })
        }).catch(e=>{})

        _.existJquery($('.rating')).then($rating => {
            for (var i = 0; i < $rating.length; i++) {
                let $this  = $rating.eq(i)
                let val     = Number($this.data('rating'))
                
                if(val > 0 && val < 1) {
                    $this.addClass('rate-point-1')
                } else if (val > 1 && val < 2) {
                    $this.addClass('rate-point-2')
                } else if (val > 2 && val < 3) {
                    $this.addClass('rate-point-3')
                } else if (val > 3 && val < 4) {
                    $this.addClass('rate-point-4')
                } else if (val > 4 && val < 5) {
                    $this.addClass('rate-point-5')
                } else if (val === 1) {
                    $this.addClass('rate-1')
                } else if (val === 2) {
                    $this.addClass('rate-2')
                } else if (val === 3) {
                    $this.addClass('rate-3')
                } else if (val === 4) {
                    $this.addClass('rate-4')
                } else if (val === 5) {
                    $this.addClass('rate-5')
                }
            }
        }).catch(e=>{})
	}
}