import * as _ from './utils'

export default {
	selectFilter() {
		_.existJquery($('.select-filter')).then($selectFilter => {
			

			for (var i = 0; i < $selectFilter.length; i++) {
				let $this = $selectFilter.eq(i)

				let $target 	= $($this.data('target'))
				let options 	= $this.next('template').html()
				let listTemplate= $($this.data('tmpl')).html()
				let dataUrl 	= $this.data('item')
				
				options 		= Handlebars.compile(options)
				listTemplate 	= Handlebars.compile(listTemplate)

				_.getJSON(dataUrl).then(data => {
					let allLocation = []
					let allArea = []

					$target.empty().append(listTemplate(data))
					$selectFilter.append(options(data))
					$selectFilter.selectric()
					_.lazyLoad($('[data-src]'))
					
					for (var i = 0; i < data.length; i++) {
		        		let areas = data[i].area

		        		allArea.push(areas)
		        	}

		        	for (var i = 0; i < data.length; i++) {
		        		let selected = data[i].selected
		        		let dataSelected = []

		        		if(selected) {
		        			dataSelected.push(data[i])
		        			let selectedArea 	= dataSelected[0].area
		        			// console.log(selectedArea)
		        			$target.empty().append(listTemplate(dataSelected))
		        			$(`option[value="${selectedArea}"]`).prop('selected', true)
		        			$selectFilter.selectric()
		        			_.lazyLoad($('[data-src]'))
		        		}
		        	}

		        	$selectFilter.on('change', e=> {
						let $this 		= $(e.currentTarget)
						let val 	= $this.val()

						let result = data.filter( function (areas) {
				            return areas.area === val
				        })

						if(val === 'All') {
				        	$target.empty().append(listTemplate(data))
				        } else {
	        				$target.empty().append(listTemplate(result))
	        			}

	        			_.lazyLoad($('[data-src]'))
					})

				})
			}
		}).catch(e=>{})
	}
}