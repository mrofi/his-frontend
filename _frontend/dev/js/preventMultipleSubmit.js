import * as _ from './utils'

export default {
	preventMultipleSubmit() {
		_.existJquery($('.btn-submit')).then($btn_Submit => {
			const $form 	= $('form')

			$form.on('submit', e => {
				let $this = $(e.currentTarget)
				let $btnSubmit = $this.find('.btn-submit')
				$btnSubmit.addClass('on-submit')
			})

            // $btn_Submit.on('click', e=> {
            //     e.preventDefault()
            //     let $this = $(e.currentTarget)
            //     $this.addClass('on-submit')
            //     $this.attr('disabled')

            //     setTimeout(()=>{
            //         $this.removeAttr('disabled')
            //     }, 3000)
            // })


            $btn_Submit.on('click', e=>{
                btnSubmit(e)
            })
            $('[type="submit"]').on('click', e=>{
                btnSubmit(e)
            })

            function btnSubmit(e) {
                let $this = $(e.currentTarget)
                $this.addClass('on-submit')
                $this.attr('disabled')
                // e.preventDefault()

                setTimeout(()=>{
                    $this.removeAttr('disabled').removeClass('on-submit')
                    $this.blur()
                }, 3000)
            }
		}).catch(e=>{})
	}
}
