import * as _ from './utils'

export default {
	slide3max() {
		_.existJquery($('.slide-3-max')).then($slider => {
			$slider.slick({
				slidesToShow: 3,
                arrows: false,
                dots: true,
                infinite: true,
				responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3
                        }
                    },
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
					}
				]
			})
		}).catch(e=>{})
	}
}
