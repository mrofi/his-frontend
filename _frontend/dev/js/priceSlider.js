import * as _ from './utils'

export default {
	priceSlider() {
		_.exist('.nouislider-container').then(inputslider => {
			let priceSlider = document.getElementById('priceSlider')
			let dataMin 	= priceSlider.dataset.min
			let dataMax 	= priceSlider.dataset.max
            let minStart    = priceSlider.dataset.start

            minStart        = JSON.parse(minStart)

			dataMin = Number(dataMin)
			dataMax = Number(dataMax)

			noUiSlider.create(priceSlider, {
				start: minStart,
				connect: true,
				step: 100000,
				range: {
					'min': [dataMin],
					'max': [dataMax]
				}
			});

			let nodes = [
				document.getElementById('lowerPrice'), // 0
				document.getElementById('upperPrice')  // 1
			]

			let inputs = [
				document.getElementById('lowerPriceInput'), // 0
				document.getElementById('upperPriceInput')  // 1
			]

			// Display the slider value and how far the handle moved
			// from the left edge of the slider.
			priceSlider.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {
				let handleVal = Number(values[handle]).toLocaleString()
				nodes[handle].innerHTML = handleVal
				inputs[handle].value = values[handle]
			});
		}).catch(e=>{})
	}
}
