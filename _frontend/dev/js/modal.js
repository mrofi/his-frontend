import * as _ from './utils'

export default {
	modal() {
		_.existJquery($('.modal')).then($modal => {
			let btnClose = '.modal .btn-close'
			let overlay  = '.modal .overlay'
			let $modalTrigger = $('.btn-modal')
			_.$doc.on('click', btnClose, e => {
				// let $this   = $(e.currentTarget)

				$modal.removeClass('is-active')
			})

			_.$doc.on('click', overlay, e => {
				$modal.removeClass('is-active')
			})
			
			_.$doc.on('click', '.btn-refresh', e => {
				window.location.reload()
			})

			$modalTrigger.on('click', e => {
				let $this = $(e.currentTarget)
				let $modalTarget = $($this.data('modal'))
				e.preventDefault()

				$modalTarget.addClass(_.classes.isActive)
			})


			let dataOri = [
				
			] 

			let data = []

			dataOri.map(o => {
				let names = {}
				names[o.name] = o.max
				
				data.push(names)
			})
			console.log()
		}).catch(e=>{})
	}
}