import * as _ from './utils'

export default {
	bazeForm() {
		_.existJquery($('.baze-form')).then($form => {
            for (var i = 0; i < $form.length; i++) {
                let $thisForm = $form.eq(i)
                let msgEmpty  = $thisForm.data('empty')
                let msgEmail  = $thisForm.data('email')
                let msgNumber = $thisForm.data('number')
                let msgMin    = $thisForm.data('numbermin')
                let msgMax    = $thisForm.data('numbermax')
                let msgPsw    = $thisForm.data('password')
                
    	        $thisForm.bazeValidate({
                    classInvalid    : 'form-input--error',
                    classValid      : 'form-input--success',
                    classMsg        : 'msg-error',
                    msgEmpty        : msgEmpty,
                    msgEmail        : msgEmail,
                    msgNumber       : msgNumber,
                    msgExceedMin    : msgMin + '%s.',
                    msgExceedMax    : msgMax + '%s.',
                    onValidating    : function() {
                        $thisForm.find('.input-peek').attr('type', 'password');
                        $thisForm.find('.btn-peek').removeClass('is-peek');
                    },
                    onValidated     : null
                })
            }

            $form.on('change', '.submit-activator', e=> {
                let $this = $(e.currentTarget)
                let $btnSubmit = $this.closest('.form').find('.btn-submit')

                if($this.is(':checked')) {
                    $btnSubmit.removeAttr('disabled')
                } else {
                    $btnSubmit.attr('disabled', 'disabled')
                }
            })

            radio()

            function radio() {
                let $radio      = $('input[type="radio"][required]')

                $radio.each(function() {
                    let $this       = $(this)
                    let radioname   = $this.attr('name')
                    let $radioName  = $(`input[name="${radioname}"]`)
                    let isChecked   = $radioName.is(':checked')

                    if(!isChecked) {
                        for (var i = 0; i < $radioName.length; i++) {
                            let $this = $radioName.eq(i)
                            $radioName.eq(0).prop('checked', true)
                        }
                    }
                })
            }

    	}).catch(e=>{})
	}
}