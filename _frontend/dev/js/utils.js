export function noop() {}

export function log(n) {
    console.log(n)
    return n
}

export function pipe(...fns) {
    return function (input) {
        return fns.reduce((output, fn) => fn(output), input)
    }
}

export function map(cb) {
    return function (arr) {
        return arr.map(cb)
    }
}

export function filter(cb) {
    return function (arr) {
        return arr.filter(cb)
    }
}

export function reduce(cb, initial) {
    return function (arr) {
        return arr.reduce(cb, initial)
    }
}

export function each(cb) {
    return function (arr) {
        arr.forEach(cb)
        return arr
    }
}

export function get(prop) {
    return function (obj) {
        return obj[prop]
    }
}

export function getScript(url) {
    return new Promise((resolve, reject) => {
        $.getScript(url).done(resolve).fail(reject)
    })
}

export function set(prop, value) {
    return function (obj) {
        obj[prop] = value
        return obj
    }
}

export function clone(obj) {
    return {...obj}
}

export function copyArray(arr) {
    return [...arr]
}

export function length(item) {
    return item.length
}

export function toNumber(n) {
    return Number(n)
}

export function equal(comparison) {
    return function (value) {
        return value === comparison
    }
}

export function notEqual(comparison) {
    return function (value) {
        return !equal(comparison)(value)
    }
}

export function gt(a) {
    return function (b) {
        return b > a
    }
}

export function gte(a) {
    return function (b) {
        return b >= a
    }
}

export function lt(a) {
    return function (b) {
        return b < a
    }
}

export function lte(a) {
    return function (b) {
        return b <= a
    }
}

export function query(context) {
    return function (selector) {
        return context.querySelector(selector)
    }
}

export function queryAll(context) {
    return function (selector) {
        return [...context.querySelectorAll(selector)]
    }
}

export function addEvent(type, cb, capture = false) {
    return function (element) {
        element.addEventListener(type, cb, capture)
        return element
    }
}

export function addClass(...classes) {
    return function (element) {
        element.classList.add(...classes)
        return element
    }
}

export function removeClass(...classes) {
    return function (element) {
        element.classList.remove(...classes)
        return element
    }
}

export function toggleClass(...classes) {
    return function (element) {
        element.classList.toggle(...classes)
        return element
    }
}

// export function getJSON(url) {
//     return new Promise((resolve, reject) => {
//         fetch(url).then(res => {
//             if ( res.ok )
//                 resolve(res.json())
//             reject('Network response not ok')
//         }).catch(reject)
//     })
// }

// export function getJSON(url) {
//     return new Promise((resolve, reject) => {
//         fetch(url, {credentials: 'include'}).then(res => {
//             if ( res.ok )
//                 resolve(res.json())
//             reject('Network response not ok')
//         }).catch(reject)
//     })
// }

// getJSON jquery
export function getJSON(url) {
    return new Promise((resolve, reject) => {
        $.getJSON(url).done(resolve).fail(reject)
    })
}

export function exist(selector) {
    return new Promise((resolve, reject) => {
        const elems = queryAll(document)(selector)

        if ( elems.length )
            resolve(elems)
        reject(`no element found for ${selector}`)
    })
}

export function existJquery(selector) {
    return new Promise((resolve, reject) => {
        let $elem = $(selector)
        if ( $elem.length )
            resolve($elem)
        reject(`no element found for ${selector}`)
    })
}

export function escKeyPress(e) {
    return e.keyCode == 27;
}

export function lazyLoad(images) {
    let $img = $(images)

    $img.unveil(100, function() {
        let $this   = $(this)
        $this.on('load', function() {
            $this.addClass('loaded')
            objectFitImages()
        })
    })

    // $img.trigger('unveil')

    $(window).off("unveil")
    $(window).trigger("lookup")
}

export function _createSlider($element, opts = {}) {
    return new Promise((resolve, reject) => {
        const defaults = {
            slidesToShow: 4,
            // variableWidth: true,
            accessibility: false,
            prevArrow: '<button type="button" class="slick-prev" type="button"><span class="fa fa-2x fa-angle-left"></span></button>',
            nextArrow: '<button type="button" class="slick-next" type="button"><span class="fa fa-2x fa-angle-right"></span></button>'
        }

        $element.on('init', () => {
            resolve($element)
        })

        $element.slick({
            ...defaults,
            ...opts
        })
    })
}

export function showLoader($container, show = true) {
    const $loader = `<div class="box-loader">
        <div class="loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>`
    const $boxLoader = $('.box-loader')

    if(show) {
        $container.append($loader)
    } else {
        $boxLoader.remove()
    }
}

export function pickaDate($input, label) {
    let pickerDate = ''

    $input.on('click', label, e => {
        // console.log('click')
        let $this = $(e.currentTarget)
        let $inputID = $('#'+$this.attr('for'))
        pickerDate = new Pikaday({
            field: $inputID[0],
            firstDay: 0,
            minDate: new Date(1929, 12, 31),
            maxDate: new Date(2020, 12, 31),
            format: 'YYYY/MM/DD',
            keyboardInput: false,
            blurFieldOnSelect: false,
            onSelect: function(e) {
                this.destroy()
            }
        })
    })
}

export function currencyIDformat(arr, separator = '.') {
    let curr        = Number(arr).toLocaleString()
    let main        = curr.split('.')[0]
    let dec         = curr.split('.')[1]
    let mainAmount  = main.split(',').join(separator)

    if(dec===undefined) {
        dec = ''
    } else {
        dec = ','+dec
    }

    let amounts = mainAmount+dec

    return amounts
}

export function getTotalSum(a, b) {
    return a + b
}

export function reduceSum(arr) {
    return arr.reduce(getTotalSum)
}

export function countNights (firstDay, lastDay) {
    firstDay = new Date(firstDay)
    lastDay = new Date(lastDay)
    let oneDay = 24*60*60*1000
    let nights = 0
    if(firstDay !== undefined && lastDay !== undefined) {
        nights = Math.round(Math.abs((firstDay.getTime() - lastDay.getTime())/(oneDay)))

        // $nDays.text(nights)
    }

    return nights
}

export function compare(a, b) {
  if (a > b) {
    return 1;
  } else if (a < b) {
    return -1;
  } else {
    return 0;
  }
}

export function renderTime(time) {
    return moment(time).format('h:mm')
}

export function lastArr(arr) {
    return arr.length - 1
}

export function fDur(time) {
    let dur = time.split(',')
    let hour = dur[0]
    let minute = dur[1]

    return `${hour}j ${minute}m`
}

export function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

export function nubBy(arr, comp) {
    const unique = arr
       .map(e => e[comp])

     // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e]).map(e => arr[e]);

    return unique;
}

export function nub(arr) {
    let unique_array = arr.filter((elem, index, self) => {
        return index == self.indexOf(elem);
    })
    return unique_array
}

export function toList(as, t) {
    return t === 'string' ? as.join('') : as
}

export function sortAsc(as) {
    var bs = Array.from(as.slice(0));

    return toList(bs.sort(compare), _typeof(as))
}

export function sortDsc(as) {
    return sortAsc(as).reverse()
}

export function compareValues(key, order='asc') {
  return function(a, b) {
    if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string') ?
      a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string') ?
      b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order == 'desc') ? (comparison * -1) : comparison
    );
  };
}

export function filterMulti(data, filterArr, comp) {
    let filterd = data.filter(el => {
        let cats = el[comp]

        return cats.filter(cat => {
            return filterArr.indexOf(cat) > -1
        }).length
        // }).length == filterArr.length

    })
    // console.log(filterd)

    return filterd
}

export function getFilter(arr, criteria) {
    // console.log(arr)
    return arr.filter(o => Object.keys(criteria).every(k => criteria[k].some(f => o[k] === f)))
}

export function decodeUri(uri) {
    return JSON.parse('{"' + decodeURI(uri).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
}

export const MMM = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agt", "Sep", "Okt", "Nov", "Des"]
export const MMMM = [
    "Januari", 
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"]

export const DDDD = [
    "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"
]

export function dateFormat(date, format="YYYY-MM-DD") {
    let nDate   = new Date(date)
    let Year    = nDate.getFullYear()
    let Month   = nDate.getMonth()
    let Day     = nDate.getDate()
    let DayIndex= nDate.getDay()

    if (format === 'DD_MMM') {
        return `${Day} ${MMM[Month]}`
    } else if (format === 'DDDD_DD_MMMM_YYYY') {
        return `${DDDD[DayIndex]}, ${Day} ${MMMM[Month]} ${Year}`
    } else {
        return `${Year}-${Month}-${Day}`
    }
}

export function getObjectPos(obj) {
    let offset = obj.offset().top
    let windowScroll = $(window).scrollTop()

    return offset - windowScroll
}

export function toolTip(el, show = false) {
    let $targetContent  = $(el.data('target-content'))
    let $tip            = $('.tool-tip')
    let windowHeight    = $(window).height()
    let windowWidth     = $(window).width()
    let tipHeight       = $targetContent.outerHeight()
    let tipWidth        = $targetContent.outerWidth()
    let elHeight        = el.outerHeight()
    let elWidth         = el.outerWidth()
    let elOffset        = getObjectPos(el)
    let elLeft          = el.offset().left
    let elTop           = el.offset().top
    let tipSize         = el.data('tip-size')
    let elRight         = windowWidth - (elWidth + elLeft)
    let tipContent      = $targetContent.html()

    $tip.empty().removeClass('tool-tip--auto')

    let tipSizeClass    = tipSize !== undefined ? `tool-tip--${tipSize}` : ``

    if(show === true) {
        $tip.append(tipContent)
        $tip.addClass(`${classes.isActive} ${tipSizeClass}`)
    } else { 
        $tip.removeClass(classes.isActive)
    }

    // console.log(elOffset, elTop)

    $tip.removeClass('is-top is-bottom is-left is-right is-center')
    el.removeClass('is-top is-bottom')

    if (elOffset >= windowHeight - tipHeight - elHeight) {
        $tip.css('top', elTop).addClass('is-bottom')
        el.addClass('is-bottom')
    } else {
        $tip.css('top', elTop + elHeight).addClass('is-top')
        el.addClass('is-top')
    }

    if(elLeft < tipWidth) {
        $tip.css('left', elLeft).addClass('is-left')
    } else if (elRight < tipWidth) {
        $tip.css('left', elWidth + elLeft).addClass('is-right')
    } else {
        $tip.css('left', elWidth/2 + elLeft).addClass('is-center')
    }
}

export function showToolTip(trigger) {
    trigger.hover(
    (e) => {
        toolTip($(e.currentTarget), true)
    }, (e) => {
        toolTip($(e.currentTarget), false)
    })
}

export function getMinuteDuration(start, end) {
    let diff = (end - start) / 1000
    diff /= 60

    return Math.abs(Math.round(diff))
}

export function goToTop () {
    $('html, body').animate({
        scrollTop: 48
    }, 500);
}

export function concated(data, item) {
    let con = []
    data.map(d => {
        d[item].map(i => {
            con.push(i)
        })
    })

    return con
}

export function groupArr (data, item) {
    data.reduce((r, a) => {
        r[a[item]] = [...r[a[item]] || [], a];
        return r;
    }, {});
}

export function timeGroup (time) {
    let groupTime
    time = Number(time.split(':').join(''))
    
    if (time > 0 && time <= 600) {
        groupTime = '00.00-06.00'
    } else if (time > 600 && time <= 1200) {
        groupTime = '06.00-12.00'
    } else if (time > 1200 && time <= 1800) {
        groupTime = '12.00-18.00'
    } else if (time > 1800 && time <= 2400) {
        groupTime = '18.00-00.00'
    }

    return groupTime
}

export function toBytes (mega) {
    return mega * 1048576 
}
 
export let slickPrev = '<button type="button" class="slick-prev" type="button"><span class="fa fa-2x fa-angle-left"></span></button>'
export let slickNext = '<button type="button" class="slick-next" type="button"><span class="fa fa-2x fa-angle-right"></span></button>'
export let pikadayOpts = ''

export let classes = {
    isLoading: 'is-Loading',
    isActive: 'is-active',
    isClose: 'is-close',
    noScroll: 'no-scroll'
};

export let $win = $(window);
export let $body = $('body');
export let $doc = $(document);
