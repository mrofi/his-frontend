import * as _ from './utils'

export default {
	headroom() {
		_.existJquery($('.site-header')).then($header => {
			let $main   = $('.sticky-footer-container')
			let headerFixed = 'header-fixed'
			
			_.$win.scroll(e=> {
                let isTop   = $main.offset().top
                let readyTop= isTop + 138

                if(_.$win.scrollTop() > readyTop) {
                    $header.addClass(_.classes.isActive)
                    _.$body.addClass(headerFixed)
                } else if(_.$win.scrollTop() === isTop) {
                    $header.removeClass(_.classes.isActive)
                    _.$body.removeClass(headerFixed)
                }
            })
		}).catch(e=>{})
	}
}