import * as _ from './utils'

export default {
	qtyInput() {
		_.existJquery($('.pick-guest')).then($pickGuest => {
			let qty_input = '.qty-input'
			let total 	  	= 1

			let $inptProducts= $('.input-product')
			let totalProduct = 0
            let products = []
            

            // console.log(totalProduct)

            for (var i = 0; i < $inptProducts.length; i++) {
            	let $thisProduct = $inptProducts.eq(i)
            	let minProduct = Number($thisProduct.attr('min'))

            	products.push(minProduct)
            }
            //

            function getSum(a, b) {
            	return a + b
            }

			for (var i = 0; i < $pickGuest.length; i++) {
				let $thisPick 		= $pickGuest.eq(i)
				let thisPickID  	= $thisPick.attr('id')
				console.log(thisPickID)
				// let totalID 		= thisPickID
				let totalID 		= 0
				let $sticky 		= $thisPick.closest('.sticky')
				let $bookSum 		= $($thisPick.data('sum'))
				let $inputAdult 	= $thisPick.find('.input-adult')
				let $inputDef 		= $thisPick.find('.input-default')
				let $inputMin 		= $thisPick.find('[min]')
				let $inputInfant	= $thisPick.find('.input-infant')
				let adult 			= thisPickID + $inputAdult.val()
				let defaultPrice	= $inputDef.data('price')
				let totalPrice 		= 0
				let totalTarget 	= $thisPick.data('total')
				let $totalTarget	= $(totalTarget)
				let $totalVat 		= $($thisPick.data('totalvat'))
				let $toalSur 		= $($thisPick.data('totalsurcharge'))
				let vatsTotal 		= 0
				let sursTotal 		= 0
				let value 			= 0
				let subTotalItem	= 0
                let inclusions 		= 0
                let includeVal		= 0
                let dataMin			= []
                
				adult 				= 1

				if(!$inputMin.length) {
					totalID = $thisPick.data('min')
				} else {
					let minInput 		= $inputMin.attr('min')
					totalID 			= Math.max(Number(minInput))
				}


                console.log(totalID)

				// totalProduct 	= 1
				let bookTotal 	= []
				let $min 		= $('[min="1"]')
            	if($min.length) {
            		let minPrice 	= $min.data('price')
                	totalPrice = minPrice
                }


                let $productInfo = $thisPick.find('.additional-info')
             	if($productInfo.length) {
	             	console.log(products)

	            	totalProduct = products.reduce(getSum)
	            	$productInfo.text(totalProduct)
            	}
                
	                // console.log('adult '+adult)

				$('#'+thisPickID).on('click', '[data-action]', e=> {
					let $this 			= $(e.currentTarget)
					let $input 			= $this.siblings('.form-input')
					let $inputProduct 	= $this.siblings('.input-product')
					let inputSingle 	= $input.data('single')
					let min 			= $input.attr('min')
	                let max 			= $input.attr('max') - 1
	                let action 			= $this.attr('data-action')
	                let type 			= $this.attr('data-type')
	                let age 			= $this.attr('data-age')
	                value 				= $input.val()
	                let steps 			= $input.data('step')
	                // let valueProduct 	= $inputProduct.val()
	                let totalPsg 		= $input.data('psg')
	                let $thisVal    	= $this.parents(qty_input).find('.total-input')
	                let $btnMin 		= $this.siblings('[data-action="minus"]')
	                let $btnPlus 		= $this.siblings('[data-action="plus"]')

	                let maxTotal 		= (totalID + 1) <= (max + 1)
	                let isPlus 			= action === 'plus' && (value + 1) && value <= max
	                let isMin 			= action === 'minus'
	                // let isMinus 		= action === 'minus' && (value - 1) >= min && (totalID - 1)
	                let isMinus 		= action === 'minus' && value >= min && totalID
	                let newAdult 		= $inputAdult.val()
	                let newInfant   	= Number($inputInfant.val()) + 1
	                let isInfant 		= age === 'infant' && value <= (newAdult - 1)
	                let isAdult 		= age === 'adult' && value >= newInfant
	                let inputTarget 	= $input.attr('data-target')
	                let inputPrice 		= $input.data('price')
	                let $inputTarget	= $(inputTarget)
	                let $itemTarget 	= $inputTarget.parents('tr')
	                let $totalItem 		= $itemTarget.find('.item-total')

	                let isPlusNoInfant	= isPlus && maxTotal && age !== 'infant'
	                let isPlusInfant 	= action === 'plus' && isInfant && maxTotal
	                let isMinAdult 		= isMinus && isAdult
	                let isMinAdultNoInfant= isMinus && age === 'adult' && age !== 'infant'
	                let isMinElse 		= isMinus && age !== 'adult' && age !== 'infant'

	                
	                // console.log(max, totalID)
	                // if(action === 'plus' &&)

	                if(steps === undefined) {
	                	steps = 1
	                }

	                function valMin() {
	                	value--
	                    totalID--
	                    // console.log(steps, max + 1)
	                    let multi = totalID-=steps - 1
	                }

	                function valPlus() {
		                	console.log(steps)
	                	if(steps <= (max - totalID)) {
		                	value++
		                	totalID++

	                		let multi = totalID+=steps - 1
	                	}
	                }

	                $sticky.addClass('is-update')

	                let sticky = new Sticky('.is-update')

	                if (isPlusNoInfant && type === "person") {
	                    valPlus()
	                    $btnMin.removeAttr('disabled')
	                } else if (isPlusInfant && type === "person") {
	                	valPlus()
	                    $btnMin.removeAttr('disabled')
	                } else if (isMinus && age === 'infant' && type === "person" && totalID > totalProduct) {
	                    valMin()
	                } else if (isMinAdult && type === "person" && totalID > totalProduct) {
	                    valMin()
	                } else if (isMinElse && type === "person" && totalID > totalProduct) {
	                	valMin()
	                } else if (isMinAdultNoInfant && type === "person" && totalID > totalProduct) {
	                    valMin()
	                }

	                // console.log(totalID+=steps - 1)

	                $bookSum.show()
	                // console.log(value)
	                if(value >=1) {
		                $itemTarget.show()
		            } else {
		            	$itemTarget.hide()
		            }

	                let isMinAdultProduct 		= isMin && isAdult && totalProduct >= 1
	                let isMinAdultNoInfantProduct= isMin && age === 'adult' && age !== 'infant' &&  totalProduct >= 1
	                let isMinElseProduct 		= isMin && age !== 'adult' && age !== 'infant' && totalProduct >= 1

	                if (isPlus && age !== 'infant' && type === "product") {
	                    value++
	                    totalProduct++
	                    $btnMin.removeAttr('disabled')
	                } else if (action === 'plus' && isInfant && type === "product") {
	                	value++
	                	totalProduct++
	                    $btnMin.removeAttr('disabled')
	                } else if (isMin && age === 'infant' && type === "product") {
	                    value--
	                    totalProduct--
	                } else if (isMinAdultProduct && type === "product") {
	                    value--
	                    totalProduct--
	                } else if (isMinElseProduct && type === "product") {
	                	value--
	                	totalProduct--
	                } else if (isMinAdultNoInfantProduct && type === "product") {
	                    value--
	                    totalProduct--
	                }
	                
	                let inputTargetVal = value * inputPrice

	                $input.val(value)
	                $thisVal.text(value)
	                
	                $totalItem.html(value)
	                $inputTarget.html(inputTargetVal.toLocaleString())

	                // console.log(steps, newVal)

	                if(type !== "product") {
	                	$('#'+totalPsg).text(totalID)
	                	// console.log(type)
	                } else {
	                	$('#'+totalPsg).text(totalProduct)
	                	// console.log(type)
	                }

	                

	                // Check inclusion()
	                let $checkInclude 	= $('.check-inclusion')
                	let $targetInclude 	= $($checkInclude.data('includetarget'))

                	if ($checkInclude.hasClass(_.classes.isActive)) {
                		includeVal 		= $checkInclude.data('include')
                		inclusions 		= includeVal*totalID
                		$targetInclude.html(inclusions.toLocaleString())
                		console.log(inclusions, includeVal)
                	} else {
                		includeVal = 0
                	}

	                // _.log(totalProduct)
	                for (var i = 0; i < $input.length; i++) {
	                	let $this 		= $input.eq(i)
	                	let item  		= Number($this.data('price'))
	                	let vat 		= Number($this.data('vat'))
	                	let surcharge	= Number($this.data('surcharge'))

	                	if(vat === undefined) {
	                		vat = 0
	                	}

	                	if (surcharge === undefined) {
	                		surcharge = 0
	                	}

	                	// console.log(vat, surcharge)

	                	subTotalItem	= item + vat + surcharge
	                	let minSubTotal = item + vat + surcharge

	                	// console.log(inclusions)

		                if (isPlusNoInfant || isPlusInfant) {
		                    getPlus(subTotalItem, vat, surcharge, inclusions)
		                    // console.log(subTotalItem, totalPrice, includeVal)
		                // } else {
		                    // getMinus(subTotalItem, vat, surcharge)
		                } else if (isMinus && age === 'infant' || isMinAdult || isMinElse || isMinAdultNoInfant) {

		                    getMinus(subTotalItem, vat, surcharge, inclusions)
		                    // console.log('min'+subTotalItem, includeVal)
		                }
		                // } else if (isMinAdult) {
		                //     getMinus(subTotalItem, vat, surcharge)
		                // } else if (isMinElse) {
		                // 	getMinus(subTotalItem, vat, surcharge)
		                // } else if (isMinAdultNoInfant) {
		                //     getMinus(subTotalItem, vat, surcharge)
		                // }
	                }

	                function getPlus(items, vats, surs, include = 0) {
	                	let plusVat 	= vatsTotal+=vats
	                	let plusSurs	= sursTotal+=surs
	                	let plusTotal 	= totalPrice+=items
	                	let plusAll 	= plusTotal + include

	                	$totalVat.html(plusVat.toLocaleString())
	                	$toalSur.html(plusSurs.toLocaleString())
						$totalTarget.html(plusAll.toLocaleString())
	                }


	                function getMinus(items,  vats, surs, include = 0) {
	                	let minusVat 	= vatsTotal-=vats
	                	let minusSurs	= sursTotal-=surs
	                	let minusTotal 	= totalPrice-=items
	                	let minusAll 	= minusTotal + include
	                	$totalVat.html(minusVat.toLocaleString())
	                	$toalSur.html(minusSurs.toLocaleString())
						$totalTarget.html(minusAll.toLocaleString())
	                }


                	$checkInclude.on('change', e=> {
						let $this 		 	= $(e.currentTarget)
						let includedItem 	= $this.data('include')
						inclusions		 	= includedItem * totalID
						let $target 	 	= $($this.data('includetarget'))
						let withInclude 	= totalPrice + inclusions
						let noInclude 		= withInclude - inclusions
						

						// includeVal 			= inclusions

						if($this.is(':checked')) {
							$this.addClass(_.classes.isActive)
							$target.html(inclusions.toLocaleString())
							$totalTarget.html(withInclude.toLocaleString())
							$thisPick.attr('data-inclusions', inclusions)
							// console.log(totalPrice, includeVal)
						} else {
							$this.removeClass(_.classes.isActive)
							$totalTarget.html(totalPrice.toLocaleString())
							$target.html('0')
							inclusions = 0
							// console.log(totalPrice, includeVal)
						}
					})
	                // }


	                
	                e.preventDefault()
				})
				// btn action

				let $storeVal 	= $('.store-val')

				$storeVal.on('change', e => {
					let $this 	= $(e.currentTarget)
					let info 	= $this.data('val')
					let target  = $this.data('target')
					let $target = $("#"+target)

					$target.text(', '+info)
				})
			}
			
		}).catch(e=>{})
	}
}
