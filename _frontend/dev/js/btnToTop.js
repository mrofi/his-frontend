import * as _ from './utils'

export default {
	btnToTop() {
		_.existJquery($('.btn-to-top')).then($btnToTop => {
			let showArea    = $(window).height() / 2;
            let mywindow    = $(window);

            mywindow.scroll(function () {
                if (mywindow.scrollTop() > showArea) {
                    $btnToTop.addClass('visible');
                }
                else {
                    $btnToTop.removeClass('visible');
                }
            });

            $btnToTop.on('click', function() {
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            });
		}).catch(e=>{})
	}
}
            