import * as _ from './utils'

export default {
	flightNavFilter() {
		_.existJquery($('.row-sort')).then($rowSort => {
			let $win = $(window)
			let elTop = $rowSort.offset().top

			$win.scroll(() => {
				if($win.scrollTop() > elTop) {
					$rowSort.parent().addClass('row-is-fixed')
				} else {
					$rowSort.parent().removeClass('row-is-fixed')
				}
			})
		}).catch(e=>{})
	}
}