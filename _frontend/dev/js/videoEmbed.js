import * as _ from './utils'

export default {
	videoEmbed() {
		_.existJquery($('.video-iframe')).then($videoEmbed => {
            function videoInit() {
    			for (var i = 0; i < $videoEmbed.length; i++) {
                    let $videos     = $videoEmbed.eq(i)
                    let videoSrc    = $videos.attr('data-src');

                    $videos.attr('src', videoSrc)
                }
            }

            window.onload = videoInit()
		}).catch(e=>{})
	}
}
            