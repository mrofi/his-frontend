import * as _ from './utils'

export default {
	onePageNav() {
		_.existJquery($('.one-page-nav')).then($onePageNav => {
			for (var i = 0; i < $onePageNav.length; i++) {
				let $this 	= $onePageNav.eq(i)
				let $parents = $this.parents('.one-page-nav-container')
				let $parent = $this.parent('.inpage-nav')
				let nav 	= $this.attr('id')
				let $nav 	= $('#'+nav)
				let $navBtn = $nav.find('.nav__item')
                let $stickyTrigger = $('.sticky-trigger')
				let parent

				parent 	= $parents.width()

				_.$win.resize(function(e) {
					parent = $parents.width()
					// console.log(parent)
				})

                let $navList    = $nav.find('ul')

				$nav.onePageNav({
					currentClass: 'current',
					filter: ':not(.external)',
					scrollChange: function() {
						let currentPos  = $nav.find('.current')
						if($('.flexbox').length) {
							$(currentPos).parent().animate({scrollLeft: $(currentPos).offset().left - 20}, 500)
						}
					}
				});

				_.$win.scroll(e=> {
	                let isTop   = $parents.offset().top
	                let readyTop= isTop + 50

	                if(_.$win.scrollTop() > readyTop) {
	                    $parent.addClass(_.classes.isActive)
	                    $parent.css('width', parent)
	                    $parent.parent().css('padding-top', '54px')
                        // console.log(isTop, _.$win.scrollTop())
	                } else {
	                    $parent.removeClass(_.classes.isActive)
	                    $parent.css('width', 'auto')
	                    $parent.parent().css('padding-top', '0')
	                }

	                if($stickyTrigger.length && !$($stickyTrigger).hasClass('is-fixed') && _.$win.width() < 800) {
	                	$parent.removeClass(_.classes.isActive)
	                    $parent.css('width', 'auto')
	                    $parent.parent().css('padding-top', '0')
	                }
	            })
			}
		}).catch(e=>{})
	}
}
