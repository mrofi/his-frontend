import * as _ from './utils'

export default {
	getVoucher() {
		_.existJquery($('[data-voucher]')).then($voucher => {
            let voucher     = $voucher.data('voucher')
            let $dataForm   = $($voucher.data('form'))

            let $tableTotal = $($voucher.data('total'))
            let $totalRow   = $tableTotal.find('tbody')
            let $total      = $tableTotal.find('[data-total]')
            let btnChange   = '.btn-change-voucher'

            const setMsg    = (msg) => {
                return `<span class="msg-text text-smaller">${msg}</span>`
            }

            const postVoucher = (data_form, name, $trigger) => {
                $trigger.attr('disabled', 'disabled')
                if(voucher) {
                    $.ajax({
                        type: 'POST',
                        url: voucher,
                        dataType: 'json',
                        data: data_form,
                    })
                    .done(function(data) {
                        $trigger.removeAttr('disabled')
                        // console.log(data)
                        let voucherVal  = Number(data.value)
                        let voucherName = data.title
                        let total       = $total.data('total')
                        let isSuccess   = data.success
                        let $btnChange  = $voucher.find(btnChange)
                        let $msgContainer= $('.msg-container')
                        total           = Number(total)
                        isSuccess       = data.success

                        if(isSuccess) {
                            total-=voucherVal
                            $totalRow.append(voucherHtml(voucherName, _.currencyIDformat(voucherVal, ',')))
                            $total.html(`IDR ${_.currencyIDformat(total, ',')}`)
                            $voucher.attr('data-valid', 'is-valid')

                            Site.stickyElements()
                        } else {
                            $voucher.attr('data-valid', 'is-invalid')
                        }

                        $voucher.find('.form-voucher').hide()
                        $btnChange.removeClass('hidden')
                        // msg.insertBefore($btnChange)
                        // console.log(msg)
                        $msgContainer.append(setMsg(data.message))

                    })
                }
            }


            const voucherHtml = (name, value) => {
                return `<tr class="item-voucher"><td>${name}</td><td class="text-right"><strong> - IDR ${value}</strong></td></tr>`
            }

            $voucher.on('keyup', '.form-input', e=> {
                let $this       = $(e.currentTarget)
                let val         = $this.val().length
                let $btn        = $this.siblings('.btn-apply')

                if(val) $btn.removeAttr('disabled')
                else $btn.attr('disabled', 'disabled')
            })

            $voucher.on('click', '.btn-apply', e=> {
                let $this       = $(e.currentTarget)
                let $input      = $this.siblings('input')
                let dataForm    = $dataForm.serialize()
                let inputVal    = $input.val()
                let inputName   = $input.data('name')
                let $msg        = $('[data-voucher] .msg-text')
                let $itemVoucher= $('.item-voucher')
                let $form       = $this.closest('form')
                let $requiredInput = $form.find('[required]')
                let emailGuest  = $form.find('[data-email-guest]').val()
                let flashMsg    = $form.data('flash-msg')
                let inputs      = []
                e.preventDefault()

                for (var i = 0; i < $requiredInput.length; i++) {
                    let $this = $requiredInput.eq(i)
                    let thisVal = $this.val().length

                    inputs.push(thisVal)
                }

                if (inputs.includes(0)) {
                    $('html, body').animate({
                        scrollTop: 200
                    }, 250)
                    Site._popMsg(flashMsg, 'danger')
                    return
                }

                $msg.remove()
                $itemVoucher.remove()
                $voucher.removeAttr('data-valid')

                dataForm+=`&${inputName}=${inputVal}&email=${emailGuest}`

                postVoucher(dataForm, inputName, $this)
            })

            $voucher.on('click', btnChange, e=> {
                let $this   = $(e.currentTarget),
                    $target = $($this.data('target'))

                $this.addClass('hidden')
                $this.siblings('.msg-container').empty()
                $this.siblings('.form-voucher').show()
                $target.val('').focus()
            })

		}).catch(e=>{})
	}
}
