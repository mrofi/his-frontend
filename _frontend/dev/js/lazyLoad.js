import * as _ from './utils'

export default {
	lazyLoad() {
		_.exist('.item-heavy').then(items => {
			_.lazyLoad(items)
		}).catch(e => {})
	}
}