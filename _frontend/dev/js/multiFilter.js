import * as _ from './utils'

export default {
	multiFilter() {
		_.existJquery($('.multi-filter')).then($filter => {

			for (var i = 0; i < $filter.length; i++) {
				let $this 	= $filter.eq(i)
				let storeID = $this.data('input')
				let $store  = $($this.data('input'))
				storeID 	= []

				$this.on('change', 'input', e => {
					let $this 			= $(e.currentTarget)
					let val 			= $this.val()
					let $checkAll		= $(`[data-checker="${$this.data('check')}"]`)
					let $allChecker 	= $(`[data-check="${$this.data('checker')}"]`)
					let checked 		= ["checked", "checked"]


					if($this.is(':checked')) {
						storeID.push(val)

						if(val === '-1') $checkAll.prop('checked', true).attr('checked', 'checked').trigger('change')
						else $allChecker.prop('checked', false).removeAttr('checked')

					} else {
						let removed = storeID.filter(store => store !== val && store !== '-1')

						if(val === '-1') {
							$checkAll.prop('checked', false).removeAttr('checked')
							storeID = []
						} else {
							$allChecker.prop('checked', false).removeAttr('checked')
						}

						storeID = removed
					}

					$store.val(storeID)

				})

				// console.log(storeID.filter(store => store !== '0'))

			}
		}).catch(e=>{})
	}
}