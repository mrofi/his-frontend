import * as _ from './utils'

export default {
	_showLoader(selector, visible, error_message) {
		let $selector = $(selector)
        let $msg 	= `<p class="load-msg t-strong">
        	<span class="fa fa-warning"></span>
        	<span>${error_message}</span>
        	<span class="btn-close fa fa-close"></span>
        </p>`
		let $loader = `<div class="loader-container"><div class="loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div></div>`
    	let $isLoader = $selector.children('.loader-container')
        let $msgs = $selector.children('.load-msg')

        if(visible) {
        	$selector.addClass(_.classes.isLoading)
	        $selector.append($loader)
	        $msgs.remove()
	    } else if(!visible && error_message) {
	    	$isLoader.remove()
	    	$selector.removeClass(_.classes.isLoading)
	    	$selector.append($msg)
	    } else {
	    	$selector.removeClass(_.classes.isLoading)
	    	$isLoader.remove()
	    	$msgs.remove()
	    }

	    $selector.on('click', '.btn-close', e=> {
	    	let $this =$(e.currentTarget)

	    	$this.parent().remove()
	    })
	}
}