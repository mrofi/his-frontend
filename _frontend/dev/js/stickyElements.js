import * as _ from './utils'

export default {
	stickyElements() {
		let stickyElement = '.sticky'
		_.existJquery($(stickyElement)).then($stickyElement => {
			let resizeTime
			function goSticky() {
				let stickyHeight = Number($stickyElement.height().toFixed())
				let winHeight 	 = _.$win.height()
				let sticky 		 = new Sticky(stickyElement)

				// console.log(winHeight, stickyHeight)
				if(winHeight > stickyHeight) {
					sticky
				} else {
					sticky.destroy()
					$stickyElement.removeClass('is-sticky')
				}
			}

			window.onload = goSticky

			_.$win.on('resize', e=>{
				clearTimeout(resizeTime)

				resizeTime = setTimeout(()=>{
					goSticky()
				}, 250)
			})
		}).catch(e=>{})

		_.existJquery($('.sticky-trigger'))
		.then($stickyTrigger => {
			$stickyTrigger.closest('body').addClass('sticky-length')
		}).catch(e=>{})
	}
}