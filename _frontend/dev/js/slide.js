import * as _ from './utils'

export default {
	slide() {
        _.existJquery($('.slide-basic')).then($basic => {
            $basic.slick({
                prevArrow: _.slickPrev,
                nextArrow:  _.slickNext,
                dots: true
            })
        }).catch(e=>{})

		_.existJquery($('.slide-default')).then($slider => {
			$slider.slick({
                waitForAnimate: false,
                prevArrow: _.slickPrev,
                nextArrow:  _.slickNext,
                autoplay: true,
                autoplaySpeed: 5000
			})

			let $video      = $slider.find('.video-iframe')

            $slider.on('afterChange', function(event, slick, currentSlide, nextSlide){

                for (var i = $video.length - 1; i >= 0; i--) {
                    $video[i].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
                };
            });
		}).catch(e=>{})
	}
}
