import * as _ from './utils'

export default {
	slide2() {
		_.existJquery($('.slide-2')).then($slider => {
			$slider.slick({
				slidesToShow: 2,
                waitForAnimate: false,
                prevArrow: _.slickPrev,
                nextArrow:  _.slickNext,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
					}
				]
			})
		}).catch(e=>{})
	}
}