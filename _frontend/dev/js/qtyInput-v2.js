import * as _ from './utils'

export default {
	qtyInput() {
		_.existJquery($('.pick-guest')).then($pickGuest => {
			let buttonAction 	= '[data-action]'
			// let $qtyAjaxUpdater = $('[data-items]')
			let qtyItem			= '.qty-input'
			let $countReseter   = $('.count-reseter')
			let $itemContainer 	= $('.item-container')
			let $sumContainer 	= $('.sum-container')
			let $bookForm 		= $('.book-form')
			let params 			= {}
			let $items 			= $('[data-items]')
			let itemsInput 		= []
			let itemFilled 		= $items.length
			let countFilled		= 0
			let changed 		= false
			let itemUrl

			// console.log(itemFilled)

			for (var i = 0; i < $items.length; i++) {
				let $this 	= $items.eq(i)
				let name  	= $this.data('name')
				let itemName= $this.data('items')
				let itemVal = $this.val()
				let count 	= i

				if(itemName === '') {
					itemName = moment(new Date()).format('YYYY/MM/DD')
				}

				$this.on('change', e=>{
					let change  = 1
					changed 	= $this.data('changed')
					if (!changed) {
						itemFilled -= change
					}

					$this.attr('data-changed', true)
				})

				getItemUrl(itemName, name)
			}

			function getItemUrl(item, name, dataPath = '') {
				if(name === 'date') {
					item=item.split('/').join('')
				}
				params[name] = item
				let newParam = $.param(params).split('=').join('_')
				itemUrl 	= newParam.split('&').join('/')+'.json'
				itemUrl 	= dataPath+itemUrl
				// console.log(itemUrl)
			}

			function getSum(a, b) {
            	return a + b
            }

			for (var i = 0; i < $pickGuest.length; i++) {
				let $thisPick 		= $pickGuest.eq(i)
				let thisPickID  	= $thisPick.attr('id')
				let checkInclude 	= $thisPick.attr('data-sum')
				let $checkInclude 	= $(checkInclude)
				let $includeChecker = $checkInclude.find('.check-inclusion')
            	let $targetInclude 	= $($includeChecker.data('includetarget'))
				// let totalTarget 	= $thisPick.data('total')
				// let $totalTarget	= $(totalTarget)
				let $totalInputID 	= $pickGuest.find('.total-input--person')
				let [totalID, vat, surcharge, vatsTotal, sursTotal, value, subTotalItem, inclusions, includeVal] = Array(9).fill(0)
				let totalAdult 		= 0
				let totalInfant		= 0
				let totalProduct 	= 0
				let totalPrice 	 	= 0
				let totalItem 		= 0
				let discount 		= 0
				let totalDiscount 	= 0
				let $dataPsg 		= $('[data-psg]')
				let $adultgroupTarget, adultgrpTargetVal, adultgrpTargetStep, adultgrpTargetTotal
                let $infantgroupTarget, infantgrpTargetVal, infantgrpTargetStep, infantgrpTargetTotal
                infantgrpTargetVal = 0

                $thisPick.find(buttonAction).attr('data-bed', 'noroom')
                // console.log($dataPsg.length)

                if($dataPsg.length) {
                    totalIDs()
                }

				inputAdult()

				// console.log(totalProduct)

				$thisPick.on('click', buttonAction, e => {
					let $this 			= $(e.currentTarget)
					let $input 			= $this.siblings('.form-input')
					let $btnMin 		= $this.siblings('[data-action="minus"]')
					let $qtyItems 		= $this.parents(qtyItem)
					let $holder 		= $this.parents('.pick-guest')
					let $thisVal    	= $qtyItems.find('.total-input')
					let inputTarget 	= $input.attr('data-target')

	                let $inputTarget	= $(inputTarget)
	                let $itemTarget 	= $inputTarget.parents('tr')
	                let $totalItem 		= $itemTarget.find('.item-total')
	                let $bookSum 		= $($holder.data('sum'))
	                let $totalVat 		= $($holder.data('totalvat'))
	                let $totalVatRow 	= $totalVat.parents('tr')
					let $totalSur 		= $($holder.data('totalsurcharge'))
					let $totalSurRow	= $totalSur.parents('tr')
					let $vatLength 		= $bookSum.find('.total-vat')
					let $surchargeLength= $bookSum.find('.total-surcharge')
					let $totalDiscount  = $($holder.data('totaldiscount'))
					let $totalDiscRow 	= $totalDiscount.parents('tr')
					let totalTarget 	= $holder.data('total')
					let $totalTarget	= $(totalTarget)
					let $totalInclude 	= $bookSum.find('.total-includes')

					let $includeChecker = $bookSum.find('.check-inclusion')
            		let $targetInclude 	= $($includeChecker.data('includetarget'))

	                value 				= Number($input.val())
					let min 			= Number($input.attr('min'))
	                let max 			= Number($input.attr('max'))
	                let price 			= $input.data('price')
	                let vat 			= $input.data('vat')
	                let surcharge 		= $input.data('surcharge')
	                let discount 		= $input.data('discount')

	                let action 			= $this.attr('data-action')
	                let type 			= $this.attr('data-type')
	                let age 			= $this.attr('data-age')
	                let steps 			= $input.data('step')
	                let step_item 		= $input.data('step-item')

	                let totalPsg 		= $input.data('psg')
	                let $totalID 		= $('#'+totalPsg)

	                let group 			= $this.attr('data-group')
	                let adultgroupTarget, infantgroupTarget
	                let bed 			= $this.attr('data-bed')
	                let nobed 			= bed === 'nobed'
	                let extrabed 		= bed === 'extrabed'
	                let noroom 			= bed === 'noroom'


	                if(age === 'infant') {
	                	adultgroupTarget 	= `adult${group}`
	                	$adultgroupTarget	= $(`[data-group-name="${adultgroupTarget}"]`)
	                	adultgrpTargetVal	= Number($adultgroupTarget.val())
	                	adultgrpTargetStep 	= Number($adultgroupTarget.data('step-item'))
	                	adultgrpTargetTotal = adultgrpTargetVal * adultgrpTargetStep
	                }

	                if(age === 'adult' && action === 'minus') {
	                	infantgroupTarget 	= `infant${group}`
	                	$infantgroupTarget	= $(`[data-group-name="${infantgroupTarget}"]`)
	                	let infantTargetVal  = []

	                	for (var i = 0; i < $infantgroupTarget.length; i++) {
	                		let $thisTarget = $infantgroupTarget.eq(i)

	                		let thisgrpTargetVal= Number($thisTarget.val())

	                		infantTargetVal.push(thisgrpTargetVal)
	                	}
	                	// console.log($infantgroupTarget.length)
	                	if(!$infantgroupTarget.length) {
		                	infantgrpTargetVal = 0
		                } else {
		                	infantgrpTargetVal = infantTargetVal.reduce(getSum)
		                }
	                }

	                // console.log(adultgroupTarget, infantgroupTarget)

	                // if
	                if(steps === undefined) {
	                	steps = 1
	                }

	                if(step_item === undefined) {
	                	step_item = 1
	                }

	                if(price === undefined) {
	                	price = 0
	                }

	                if(vat === undefined) {
                		vat = 0
                	}

                	if (surcharge === undefined) {
                		surcharge = 0
                	}

                	if (vatsTotal === undefined) {
                		vatsTotal = 0
                	}

                	if (sursTotal === undefined) {
                		sursTotal = 0
                	}

                	if (discount === undefined) {
                		discount = 0
                	}
	                // setOne(steps)
	                // setOne(step_item)
	                // setZero(price)
	                // setZero(vat)
	                // setZero(surcharge)
	                // setZero(vatsTotal)
	                // setZero(sursTotal)
	                // setZero(discount)

	                let allowedStep 	= (max - totalID) >= steps && (max - totalID) >= step_item
	                let person 			= type === 'person'
	                let product 		= type === 'product'
	                let productFree 	= type === 'product-free'
	                let btnPlus 		= action === 'plus'
	                let btnMin 			= action === 'minus'
	                let isnMax 			= totalID < max && allowedStep
	                let isnMin			= totalID > min && value > min

	                //if Age
	                let adult 			= age === 'adult'
	                let infant 			= age === 'infant'

	                // Adult - infant
	                let isPlusAdult 	= adult && btnPlus && isnMax
	                let isMinAdult 		= adult && btnMin && isnMin && value > totalInfant
	                let isPlusInfant 	= infant && btnPlus && isnMax && totalAdult > totalInfant && noroom
	                let isMinInfant 	= infant && btnMin && isnMin
	                let isnRelatedAge 	= !adult && !infant

	                let infatPlusBed		= infant && btnPlus && isnMax && adultgrpTargetVal > value
	                let isPlusInfantNobed 	=  infatPlusBed && nobed
	                let isPlusInfantExtrabed= infatPlusBed && extrabed

	                // let isMinAdultBed 	= adult && btnMin && isnMin && value > infantgrpTargetVal
	                let isMinAdultBed 	= isMinAdult && value > infantgrpTargetVal

	                //Product Role
	                let isMinAdultProduct 			= btnMin && adult && value > totalInfant && totalProduct >= 1
	                let isMinAdultNoInfantProduct	= btnMin && adult && !infant && totalProduct >= 1
	                let isMinElseProduct 			= btnMin && !adult && !infant && totalProduct >= 1
	                let personMax 					= totalProduct < totalID

	                function valPlus() {
	                	value = value+=steps
	                	$btnMin.removeAttr('disabled')
	                }

	                function valMin() {
	                	value = value-steps
	                }

	                // TYPE PERSON
	                if(person) {
		                if (isPlusAdult) {
		                	valPlus()
		                	totalID = totalID+=step_item
		                	totalAdult = totalAdult+=step_item
		                	totalItem++
		                	// console.log('plus adult')
		                } else if (isPlusInfantExtrabed || isPlusInfantNobed) {
		                	valPlus()
		                	totalID = totalID+=step_item
		                	totalInfant++
		                	totalItem++
		                	infantgrpTargetVal++
		                	// console.log(value, totalAdult/2, (totalAdult/2) > value, 'plus infant bed')
		                } else if (isPlusInfant) {
		                	valPlus()
		                	totalID = totalID+=step_item
		                	totalInfant++
		                	totalItem++
		                	// console.log('plus infant')
		                } else if (isMinAdultBed) {
		                	valMin()
		                	totalID = totalID-step_item
		                	totalAdult = totalAdult-=step_item
		                	totalItem--
		                	// console.log('min adult bed', step_item, value, infantgrpTargetVal)
		                } else if (isMinAdult) {
		                	valMin()
		                	totalID = totalID-step_item
		                	totalAdult = totalAdult-=step_item
		                	totalItem--
		                	// console.log('min adult')
		                } else if (isMinInfant) {
		                	valMin()
		                	totalID = totalID-step_item
		                	totalInfant--
		                	totalItem--
		                	// console.log('min infant')
	                	} else if (btnPlus && isnMax && isnRelatedAge) {
	                		valPlus()
	                		totalID = totalID+=step_item
	                		totalItem++
		                	// console.log('plus general')
		                } else if (btnMin && isnMin && isnRelatedAge) {
		                	valMin()
		                	totalID = totalID-step_item
		                	totalItem--
		                	// console.log('min general')
		                }
		            }
		            // console.log(value, infantgrpTargetVal, value > infantgrpTargetVal)
		            // console.log(totalProduct, totalID)
		            if (product) {
		            	if(btnPlus && personMax && !infant) {
		            		valPlus()
		            		totalProduct++
		            		// console.log('btnPlus && personMax && !infant')
		            	} else if (isPlusInfant) {
		            		valPlus()
		            		totalProduct++
		            		// console.log('isPlusInfant')
		            	} else if (isMinInfant && totalProduct > min) {
		            		valMin()
	                    	totalProduct--
	                    	// console.log('isMinInfant && totalProduct > min')
		            	} else if (isMinElseProduct && totalProduct > min) {
		            		valMin()
	                    	totalProduct--
	                    	// console.log('isMinElseProduct && totalProduct > min')
		            	} else if (isMinAdultNoInfantProduct && totalProduct > min) {
		            		valMin()
	                    	totalProduct--
	                    	// console.log('isMinAdultNoInfantProduct && totalProduct > min')
		            	}
		            }
		            // console.log(totalPrice)
	                // console.log(includeVal, totalID)

		            if (productFree && btnPlus && totalProduct < max && totalID > 0) {
		            	valPlus()
		            	totalProduct++
		            } else if (productFree && btnMin && totalProduct > min) {
		            	valMin()
		            	totalProduct--
		            }

		            if ($includeChecker.hasClass(_.classes.isActive)) {
                		includeVal 		= $includeChecker.data('include')
                		inclusions 		= includeVal*totalID
                		$targetInclude.html(_.currencyIDformat(inclusions))
                		$totalInclude.html(totalID)
                		// console.log(inclusions, includeVal, totalID)
                	} else {
                		inclusions = 0
                		// console.log(inclusions)
                	}
                	// console.log(vat, surcharge)
                	let inputTargetVal 	= value * price
                	price 				= price*steps
                	vat 				= vat*steps
                	surcharge 			= surcharge*step_item
                	discount 			= discount*steps
                	// totalPrice 			= totalPrice

                	if (product && personMax && btnPlus && !infant) {
	                	getPlus()
	                } else if (!product && btnPlus && isnMax && !infant || isPlusInfant || isPlusInfantExtrabed || isPlusInfantNobed) {
	                	getPlus()
	                } else if (isMinInfant) {
	                    getMinus()
	                    // console.log(totalPrice, 'min isMinInfant')
	                } else if (isMinAdult) {
	                    getMinus()
	                    // console.log('min isMinAdult')
	                } else if (btnMin && isnMin && !adult && !infant) {
	                    getMinus()
	                    // console.log('min btnMin && isnMin && !adult && !infant')
	                } else if (btnMin && isnMin && adult && !infant && totalAdult > totalInfant && noroom) {
	                    getMinus()
	                    // console.log('min btnMin && isnMin && adult && !infant && noroom')
	                } else if (isMinAdultBed && !infant) {
	                    getMinus()
	                    // console.log('min btnMin && isnMin && adult && !infant')
	                }
	                // console.log(totalAdult, totalInfant, totalAdult > totalInfant)

	                function getPlus() {
	                	totalDiscount   = totalDiscount+=discount
		                vatsTotal 		= vatsTotal+=vat
		                sursTotal 		= sursTotal+=surcharge
		                totalPrice 		= totalPrice+=price
		                totalPrice 		= totalPrice-=discount
		                // totalPrice 		= totalPrice+inclusions
		                // console.log(vatsTotal, vat)
	                }

	                function getMinus() {
	                	totalDiscount   = totalDiscount-=discount
		                vatsTotal 		= vatsTotal-=vat
		                sursTotal 		= sursTotal-=surcharge
		                totalPrice 		= totalPrice-=price
		                totalPrice 		= totalPrice+=discount
	                }

	                let grandTotal  = totalPrice+vatsTotal+sursTotal+inclusions

	                $totalVat.html(_.currencyIDformat(vatsTotal))
                	$totalSur.html(_.currencyIDformat(sursTotal))
	                $totalTarget.html(_.currencyIDformat(grandTotal))

	                $input.val(value)
	                $thisVal.html(value)
	                $totalID.html(totalID)
	                $vatLength.html(totalItem)
	                $surchargeLength.html(totalID)
	                $totalItem.html(value)
	                $inputTarget.html(_.currencyIDformat(inputTargetVal))
	                $totalDiscount.html(_.currencyIDformat(totalDiscount))

	                if(!product) {
	                	$totalID.text(totalID)
	                	// console.log(type)
	                } else {
	                	$totalID.text(totalProduct)
	                	// console.log(type)
	                }

	                $bookSum.show()
	                // console.log(value)
	                if(value >=1) {
		                $itemTarget.show()
		            } else {
		            	$itemTarget.hide()
		            }

		            showRow($totalVatRow, vatsTotal)
		            showRow($totalSurRow, sursTotal)
		            showRow($totalDiscRow, totalDiscount)

		            _.$win.trigger('resize')
				})

				// console.log(totalPrice)

				$checkInclude.on('change', '.check-inclusion', e=> {
					let $this 		 	= $(e.currentTarget)
					let includedItem 	= $this.data('include')
					let $totalTarget 	= $this.parents('.book-sum').find('.grand-total')
					let $totalInclude 	= $this.parents('.book-sum').find('.total-includes')

					if(inclusions === undefined) {
						inclusions = 0
					}

					inclusions		 	= includedItem * totalID
					let $target 	 	= $($this.data('includetarget'))
					let withInclude 	= totalPrice + vatsTotal + sursTotal + inclusions
					let noInclude 		= withInclude - inclusions
					includeVal 			= includedItem
					// console.log(includedItem, totalID, inclusions, withInclude)
					if($this.is(':checked')) {
						$this.addClass(_.classes.isActive)
						$target.html(_.currencyIDformat(inclusions))
						$totalTarget.html(_.currencyIDformat(withInclude))
						$thisPick.attr('data-inclusions', inclusions)
						$totalInclude.html(totalID)
						// console.log(totalPrice, includeVal)
					} else {
						$this.removeClass(_.classes.isActive)
						$totalTarget.html(_.currencyIDformat(noInclude))
						$target.html('0')
						inclusions = 0
						$totalInclude.html(0)
						// console.log(totalPrice, includeVal)
					}
				})

				let isLoading = false

				Handlebars.registerHelper('toLocaleStr', (val)=>{
	                val = Handlebars.escapeExpression(val)

	                let toNumber = _.currencyIDformat(val)

	                return new Handlebars.SafeString(
	                    // Number(val).toLocaleString().split(',').join('.')
	                    toNumber
	                )
	            })

				$bookForm.on('change', '[data-items]', e=> {
                    countFilled++
                    let $this           = $(e.currentTarget)
                    let dataPath        = $this.data('path')
                    let name            = $this.data('name')
                    let itemName        = $this.val()
                    let $formContainer  = $bookForm.parent()
                    let getPersonEnabled= countFilled >= itemFilled
                    // console.log(countFilled >= itemFilled, countFilled, itemFilled)
                    getItemUrl(itemName, name, dataPath)

                    let itemField       = $this.data('tmplist')
                    let itemSum         = $this.data('tmplsum')
                    let templateItem    = $(itemField).html()
                    let templateSum     = $(itemSum).html()
                    let minID           = []

                    HandlebarsIntl.registerWith(Handlebars)
                    templateItem        = Handlebars.compile(templateItem)
                    templateSum         = Handlebars.compile(templateSum)
                    resetCount()
                    // console.log(!isLoading, itemUrl, itemFilled == 0)
                    
                    if(!isLoading && itemUrl && itemFilled == 0) {
                        isLoading = true
                        _.showLoader($formContainer)
						_.getJSON(itemUrl).then( data => {
							// console.log(data)
							isLoading = false
							_.showLoader($formContainer, false)
		                    setTimeout(function() {
		                    	totalIDs()
								inputAdult()
		                    }, 1000)

							$('.total-input--all').html(0)
                            $itemContainer.parent().siblings('.btn-toggle').removeClass('is-disabled')
		                    $itemContainer.empty().append(templateItem(data))
		                    $sumContainer.empty().append(templateSum(data))

		                    let res = data
		                    for (var i = res.length - 1; i >= 0; i--) {
		                    	let eachRes = res[i]
		                    	let eachMin = eachRes.input.min

		                    	minID.push(eachMin)
		                    }

		                    totalID = minID.reduce(getSum)
		                    $totalInputID.html(totalID)
		                    // console.log(totalID > 0)
		                    $sumContainer.parent().hide()
		                }).catch(e => {
		                	isLoading = false
		                	_.showLoader($formContainer, false)
		                })
	                }
				})

				let $storeVal 	= $('.store-val')

				$storeVal.on('change', e => {
					let $this 	= $(e.currentTarget)
					let info 	= $this.data('val')
					let target  = $this.data('target')
					let $target = $("#"+target)

					$target.text(', '+info)
					// console.log('changed')
				})

				$countReseter.on('change', e=> {
					resetCount()
					$('.total-input--all').html(0)
                    $itemContainer.empty()
                    $sumContainer.empty()
				})

				function resetCount() {
					[totalID, vat, surcharge, vatsTotal, sursTotal, value, subTotalItem, inclusions, includeVal] = Array(9).fill(0)
					totalAdult 		= 0
					totalInfant		= 0
					totalProduct 	= 0
					totalPrice 	 	= 0
					totalItem 		= 0
					discount 		= 0
					totalDiscount 	= 0
					$totalInputID.html(totalID)
				}

				function totalIDs() {
                    let $inputs         = $thisPick.find('input[data-psg]:not(.input-product)')
                    let $products       = $thisPick.find('.input-product')
                    let getTotalID      = []
                    let getTotalPrice   = []
                    let getVatID        = []
                    let getSurchargeID  = []
                    let getProductMin   = []

                    for (var i = 0; i < $inputs.length; i++) {
                        let $eachInput  = $inputs.eq(i)
                        let eachVal     = Number($eachInput.val())
                        let eachPrice   = $eachInput.data('price')
                        if(eachPrice === undefined) {
                            eachPrice = 0
                        }
                        let eachMin     = Number($eachInput.attr('min'))
                        let minPrice    = eachPrice * eachMin
                        let eachVat     = $eachInput.data('vat')
                        if(eachVat === undefined) {
                            eachVat = 0
                        }
                        let minVat      = eachVat * eachMin
                        let eachSurcharge=$eachInput.data('surcharge')
                        if(eachSurcharge === undefined) {
                            eachSurcharge = 0
                        }
                        let minSurcharge= eachSurcharge * eachMin

                        getTotalID.push(eachVal)
                        getTotalPrice.push(minPrice)
                        getVatID.push(minVat)
                        getSurchargeID.push(minSurcharge)
                        // console.log(eachVat)
                    }

                    // console.log(getProductMin)

                    totalID             = getTotalID.reduce(getSum)
                    totalPrice          = getTotalPrice.reduce(getSum)
                    vatsTotal           = getVatID.reduce(getSum)
                    sursTotal           = getSurchargeID.reduce(getSum)
                    totalItem = totalID
                    $totalInputID.html(totalID)

					if($products.length) {
						for (var i = 0; i < $products.length; i++) {
							let $thisProduct = $products.eq(i)
							let productMin   = Number($thisProduct.attr('min'))

							getProductMin.push(productMin)
						}
						totalProduct 		= getProductMin.reduce(getSum)
					}
				}

				function inputAdult() {
					let inputAdult 		= $thisPick.find('.input-adult').val()
					if(inputAdult > 0) {
						totalAdult = Number(inputAdult)
					}
				}
			}

			function setZero(value) {
				if(value === undefined) {
					value = 0
				}
			}

			function setOne(value) {
				if (value === undefined) {value = 1}
			}

			function showRow($row, values) {
				if(values > 0) {
					$row.show()
				} else if(values == 0) {
					$row.hide()
				}
			}

		}).catch(e=>{})
	}
}
