import * as _ from './utils'

export default {
	flightSearch() {
		Promise.all([
            _.existJquery($('[data-flight]'))
        ]).then(res => {
            let [ $container ]  = res
            let $result         = $('.flight-result__item')
            let $flightSum      = $('.flight-summary')
            let $targetContainer= $('.flight-summary-container')
            HandlebarsIntl.registerWith(Handlebars)
            // _.log($result.length)
            for (var i = 0; i < $container.length; i++) {
                let $thisContainer  = $container.eq(i)
                let $dataContainer  = $thisContainer.find('.data-container')
                let uri             = $thisContainer.attr('data-flight')
                let tmpl            = $thisContainer.attr('data-tmpl')
                let tripType        = $thisContainer.data('trip')
                let template        = $(tmpl).html()
                let isLoading       = false
                let isEnd           = false
                let isHidden        = false
                let $loader         = $('.loader')
                let $isEnded        = $('.is-ended')
                let gallerySlide    = '.slideGallery'
                template            = Handlebars.compile(template)
                let bottomPos       = window.innerHeight > (document.body.offsetHeight - 50)
                let screen          = window.innerHeight

                if(!uri) {
                    $thisContainer.remove()
                    $('.flight-summary--return').remove()
                    $thisContainer.on('change', '.btn-check-flight input', e=> {
                        let $this           = $(e.currentTarget)

                        if($this.is(':checked')) {
                        $('.btn-book-flight').removeAttr('disabled')
                        // console.log('checked')
                        }
                    })
                }

                if(!isLoading && bottomPos && uri) {
                    isLoading = true
                    $loader.show()
                    window.setTimeout(() => {
                        _.getJSON(uri).then( data => {
                            let currentData = data
                            updateView(currentData)
                            isLoading = false
                            // $thisContainer.attr('data-flight', data.next)
                            $loader.hide()
                            detailFlight()
                            chooseFlight()

                            sessionStorage.setItem(`${tripType}-flights`, JSON.stringify(currentData))

                            let nameID = currentData[0].origin[0].city
                            let summary= '<div class="bzg_c" data-col="m6" id="summary_'+nameID+'"></div>'
                            let $btnChecker = $thisContainer.find('[name]')
                            $btnChecker.attr('data-trip', tripType)
                            $targetContainer.append(summary)

                            $thisContainer.on('click', '.sort--depart', e=> {
                                let $this   = $(e.currentTarget)

                                if ($this.hasClass('is-asc')) {
                                    isDESC($this)
                                    currentData.sort( function (prev, next) {
                                        if (prev.depart < next.depart) return 1
                                        if (prev.depart > next.depart) return -1
                                        if (prev.depart === next.depart) return 0
                                    })
                                } else {
                                    isASC($this)
                                    currentData.sort( function (prev, next) {
                                        if (prev.depart > next.depart) return 1
                                        if (prev.depart < next.depart) return -1
                                        if (prev.depart === next.depart) return 0
                                    })
                                }

                                updateView(currentData)
                                let $btnChecker = $thisContainer.find('[name]')
                                $btnChecker.attr('data-trip', tripType)
                            })

                            $thisContainer.on('click', '.sort--price', e=> {
                                let $this   = $(e.currentTarget)

                                if ($this.hasClass('is-asc')) {
                                    isDESC($this)
                                    currentData.sort( function (prev, next) {
                                        if (prev.price < next.price) return 1
                                        if (prev.price > next.price) return -1
                                        if (prev.price === next.price) return 0
                                    })
                                } else {
                                    isASC($this)
                                    currentData.sort( function (prev, next) {
                                        if (prev.price > next.price) return 1
                                        if (prev.price < next.price) return -1
                                        if (prev.price === next.price) return 0
                                    })
                                }

                                updateView(currentData)
                                let $btnChecker = $thisContainer.find('[name]')
                                $btnChecker.attr('data-trip', tripType)
                            })

                            $thisContainer.on('click', '.sort--duration', e=> {
                                let $this   = $(e.currentTarget)

                                if ($this.hasClass('is-asc')) {
                                    isDESC($this)
                                    currentData.sort( function (prev, next) {
                                        if (prev.duration_total < next.duration_total) return 1
                                        if (prev.duration_total > next.duration_total) return -1
                                        if (prev.duration_total === next.duration_total) return 0
                                    })
                                } else {
                                    isASC($this)
                                    currentData.sort( function (prev, next) {
                                        if (prev.duration_total > next.duration_total) return 1
                                        if (prev.duration_total < next.duration_total) return -1
                                        if (prev.duration_total === next.duration_total) return 0
                                    })
                                }

                                updateView(currentData)
                                let $btnChecker = $thisContainer.find('[name]')
                                $btnChecker.attr('data-trip', tripType)
                            })
                        }).catch(e => {
                            isLoading = false
                            $loader.hide()
                        })
                    }, 500)
                }

                function updateView(data) {
                    $dataContainer.empty();
                    $dataContainer.append(template(data))
                    chooseFlight()
                }

                function isASC($el) {
                    $el.removeClass('is-desc')
                    $el.addClass('is-asc')
                    $el.parent().siblings().find('.btn-sort').removeClass('is-asc').removeClass('is-desc')
                }

                function isDESC($el) {
                    $el.removeClass('is-asc')
                    $el.addClass('is-desc')
                    $el.parent().siblings().find('.btn-sort').removeClass('is-asc').removeClass('is-desc')
                }

                function detailFlight() {
                    $thisContainer.on('click', '.btn-toggle', e => {
                        let $this           = $(e.currentTarget)
                        let $parentToggle   = $this.parents('[data-toggle]')

                        $parentToggle.siblings().removeClass(_.classes.isActive)
                        $parentToggle.toggleClass(_.classes.isActive);
                    })
                }

                function chooseFlight() {
                    
                    function totalPrice() {
                        let total           = 0
                        let $inputChecked   = $('.btn-check-flight input:checked')
                        let $totalPrice     = $('.total-flight')

                        for (var i = 0; i < $inputChecked.length; i++) {
                            let $this = $inputChecked.eq(i)
                            let price = $this.data('price')
                            total+=price
                            $totalPrice.html(total.toLocaleString())
                        }
                    }

                    $thisContainer.on('change', '.btn-check-flight input', e=> {
                        totalPrice()
                        
                        let $this           = $(e.currentTarget)
                        let targetHtml      = $($this.data('target')).html()
                        let tripType        = $this.data('trip')
                        let $targetSummary  = $('#summary_'+tripType)
                        let $el             = '<div class="bzg bzg--small-gutter">'+targetHtml+'</div>'
                        let $departCheck   = $('[data-trip="depart"]')
                        let $returnCheck   = $('[data-trip="return"]')
                        
                        $targetSummary.empty().append($el)
                        sessionStorage.removeItem(`${tripType}-flights`)
                        setTimeout(function() {
                            $flightSum.addClass(_.classes.isActive)
                        }, 500)

                        // console.log($returnResult.length)

                        if(!$returnCheck.length) {
                            if($departCheck.is(':checked')) {
                                $('.btn-book-flight').removeAttr('disabled')
                            }
                        } else {
                            if($departCheck.is(':checked') && $returnCheck.is(':checked')) {
                                $('.btn-book-flight').removeAttr('disabled')
                            }
                        }
                    })
                }
            }
            $flightSum.on('click', '.btn-toggle', e=> {
                let $this   = $(e.currentTarget)
                // console.log('mini')
                $flightSum.toggleClass('is-minimize');
            })
        }).catch(e => {})
	}
}