import * as _ from './utils'

export default {
	fillUserData() {
		_.existJquery($('.form-input[data-passengers]')).then($userSelect => {
            const writeUserData = (data, num, optsVal, blank = 0) => {
                let dataUsr         = data.filter(usr => usr.passenger_name === optsVal)[0]
                let $psFirstName    = $(`[name="passenger_first_name[${num}]"]`)
                let $psLastName     = $(`[name="passenger_last_name[${num}]"]`)
                let $psDate         = $(`[name="passenger_birthdate_day[${num}]"]`)
                let $psMonth        = $(`[name="passenger_birthdate_month[${num}]"]`)
                let $psYear         = $(`[name="passenger_birthdate_year[${num}]"]`)
                let $psPasportDate  = $(`[name="passenger_passport_expired_day[${num}]"]`)
                let $psPasportMonth = $(`[name="passenger_passport_expired_month[${num}]"]`)
                let $psPasportYear  = $(`[name="passenger_passport_expired_year[${num}]"]`)
                let $psNationality  = $(`[name="passenger_nationality[${num}]"]`)
                let $psPasportIssuer= $(`[name="passenger_passport_issued_country[${num}]"]`)
                let $psPasportNumber= $(`[name="passenger_passport_number[${num}]"]`)

                if(dataUsr === undefined) {
                    $(`[name='passenger_title[${num}]']`).prop('checked', false)
                    $psFirstName.val('')
                    $psLastName.val('')
                    $psDate.val('').selectric('refresh')
                    $psMonth.val('').selectric('refresh')
                    $psYear.val('').selectric('refresh')
                    $psPasportDate.val('').selectric('refresh')
                    $psPasportMonth.val('').selectric('refresh')
                    $psPasportYear.val('').selectric('refresh')
                    $(`[name="passenger_gender[${num}]"]`).prop('checked', false)
                    $psNationality.val('').trigger('change')
                    $psPasportIssuer.val('').trigger('change')
                    $psPasportNumber.val('')
                    return
                }
                
                // Set user title
                let psTitle     = dataUsr.passenger_title
                let $psTitle    = $(`[name='passenger_title[${num}]'][value='${psTitle}']`)
                $psTitle.prop('checked', true)

                // Set user first name
                let psFirstName = dataUsr.passenger_first_name
                $psFirstName.val(psFirstName)

                // Set user last name
                let psLastName = dataUsr.passenger_last_name
                $psLastName.val(psLastName)

                // Set user birthdate
                let psBirthDate = dataUsr.passenger_birthdate.split('-')
                $psDate.val(psBirthDate[2])
                $psDate.selectric('refresh')
                $psMonth.val(psBirthDate[1])
                $psMonth.selectric('refresh')
                $psYear.val(psBirthDate[0])
                $psYear.selectric('refresh')

                let psPasportDate = dataUsr.passenger_passport_expired.split('-')
                $psPasportDate.val(psPasportDate[2])
                $psPasportDate.selectric('refresh')
                $psPasportMonth.val(psPasportDate[1])
                $psPasportMonth.selectric('refresh')
                $psPasportYear.val(psPasportDate[0])
                $psPasportYear.selectric('refresh')

                // Set Gender
                let psGender    = dataUsr.passenger_gender
                let $psGender   = $(`[name="passenger_gender[${num}]"][value="${psGender}"]`)
                $psGender.prop('checked', true)

                // Set Nationality 
                let psNationality = dataUsr.passenger_nationality
                $psNationality.val(psNationality).trigger('change')

                // Set passenger_passport_issued_country
                let psPasportIssuer = dataUsr.passenger_passport_issued_country
                $psPasportIssuer.val(psPasportIssuer).trigger('change')

                // Set Pasport Number
                let psPasportNumber     = dataUsr.passenger_passport_number
                $psPasportNumber.val(psPasportNumber)
            }


            $userSelect.on('change', e => {
                let $this       = $(e.currentTarget)
                let usrData     = $this.data('passengers') 
                let usrDataType = typeof usrData
                let nameNumber  = $this.data('number')
                let optsVal     = $this.val()
                let blank       = optsVal.length == 0

                if(usrDataType === 'string') {
                    _.getJSON(usrData).then(data => {
                        writeUserData(data, nameNumber, optsVal, blank)
                    })
                } else {
                    writeUserData(usrData, nameNumber, optsVal, blank)
                }
                
            })
        }).catch(e=>{})
	}
}