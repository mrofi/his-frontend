import * as _ from './utils'

export default {
	infiniteScroll() {
		Promise.all([
            _.existJquery($('[data-list]'))
        ]).then(res => {
            let [ $container ]  = res
            HandlebarsIntl.registerWith(Handlebars)

            Handlebars.registerHelper('eq', function () {
                const args = Array.prototype.slice.call(arguments, 0, -1);
                return args.every(function (expression) {
                    return args[0] === expression;
                });
            });
            // _.log($container)
            for (var i = 0; i < $container.length; i++) {
                let $thisContainer  = $container.eq(i)
                let uri             = $thisContainer.attr('data-list')
                let tmpl            = $thisContainer.attr('data-tmpl')
                let dataCategory    = $thisContainer.data('category')
                let chachedItem     = sessionStorage.getItem(`${dataCategory}-itemList`)
                let noStore         = $thisContainer.data('no-store')
                let page            = Number(sessionStorage.getItem(`${dataCategory}-page`)) || 1
                let template        = $(tmpl).html()
                let listData        = []
                let isLoading       = false
                let isEnd           = false
                let isHidden        = false
                let nextUrl         
                let $loader         = $('.loader')
                let $isEnded        = $('.is-ended')
                let $isBlank        = $('.is-blank')
                let gallerySlide    = '.slideGallery'
                template            = Handlebars.compile(template)
                let main            = $('.site-main')
                let bottomPos       = window.innerHeight > (main.outerHeight() - 50)
                let screen          = window.innerHeight

                // console.log(!noStore)
                if(chachedItem && !noStore) {
                    let dataItem = JSON.parse(chachedItem)
                    nextUrl = sessionStorage.getItem(`${dataCategory}-nextUrl`)
                    compileTemplate(dataItem, nextUrl)
                    // console.log(dataItem[0].data)
                    if(!nextUrl) {
                        isLoading = false
                        $loader.hide()
                        $isEnded.show()
                        $thisContainer.removeAttr('data-list')
                    }
                } else {
                    $loader.show()
                    window.setTimeout(() => {
                        _.getJSON(uri).then( data => {
                            listData.push(data)
                            let itemData    = listData[0].data
                            // console.log(itemData)
                            if(itemData === null) {
                                $loader.hide()
                                $isBlank.show()
                            } else {
                                compileTemplate(listData, data.next)
                            } 
                            if(!noStore) {
                                setSessionStorage(listData, data.next)
                            }
                        })
                    }, 500)
                }

                window.onscroll = ev => {
                    let bottomScroll= window.innerHeight + window.scrollY >= (main.outerHeight() - 50)
                    let nextUrl     = $thisContainer.attr('data-list')

                    if(!nextUrl) {
                        isLoading = false
                        $loader.hide()
                        $isEnded.show()
                        $thisContainer.removeAttr('data-list')
                    }
                    
                    if (!isLoading && bottomScroll && nextUrl && nextUrl != null) {
                        isLoading = true
                        $loader.show()
                        page++
                        window.setTimeout(() => {
                            _.getJSON(nextUrl).then( data => {
                                let appendedData = []
                                listData.push(data)
                                appendedData.push(data)
                                compileTemplate(appendedData, data.next)
                                if(!noStore) {
                                setSessionStorage(listData, data.next)
                                sessionStorage.setItem(`${dataCategory}-page`, page)
                                }
                            }).catch(e => {
                                isLoading = false
                                $loader.hide()
                                $isEnded.show()
                                $thisContainer.removeAttr('data-list')
                            })
                        }, 500)
                    }
                }

                function compileTemplate(data, next) {
                    $thisContainer.append(template(data))
                    $thisContainer.attr('data-list', next)
                    isLoading = false
                    $loader.hide()
                    // Site.navToggle()
                    _._createSlider($(gallerySlide))
                    _.lazyLoad('.item-heavy')
                    Site.tabNav()
                }

                function setSessionStorage(data, next) {
                    sessionStorage.setItem(`${dataCategory}-itemList`, JSON.stringify(data))
                    sessionStorage.setItem(`${dataCategory}-nextUrl`, next)
                }

                function noNextUrl() {
                    isLoading = false
                    $isEnded.show()
                    $thisContainer.removeAttr('data-list')
                }
            }


            function slider($element) {
            	$element.slick({
            		slidesToShow: 4,
            		
		            variableWidth: true,
		            accessibility: false,
		            prevArrow: _.slickPrev,
                	nextArrow:  _.slickNext
            	})
            }
        }).catch(e => {})
	}
}