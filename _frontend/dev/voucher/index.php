<?php

session_start();

class Voucher {
    public function getVoucher() {

        session_unset();
        $res = rand(0, 1);
        $data = [
            [
                'success' => false,
                'title' => 'Voucher 20%',
                'message' => 'Kode Voucher sudah terpakai',
                'value' => 200000
            ],
            [
                'success' => true,
                'title' => 'Voucher 20%',
                'message' => 'Kode voucher berhasil digunakan',
                'value' => 200000
            ]
        ];

        return json_encode($data[$res]);
    }
}

$voucher = new Voucher();
print $voucher->getVoucher();
?>
