<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Aktivitas</a></li>
                <li><a href="#">Optional Tour</a></li>
                <li><a href="#">Visa</a></li>
            </ul>
        </div>
    </div>
    
    <section class="fill-softtblue block--inset-small pt pb shadowed">
        <div class="container container--small">
            <form action="">
                <div class="bzg">
                    <div class="bzg_c mb-half" data-col="m5, l4" data-offset="l1">
                        <label for="" class="sr-only">Filter by:</label>
                        <select name="" id="" class="form-input form-input--block selectstyle">
                            <option value="">Filter</option>
                        </select>
                    </div>
                    <div class="bzg_c" data-col="m7, l6">
                        <input type="search" class="form-input form-input--block" 
                        placeholder="Search promo">
                    </div>
                </div>
            </form>
        </div>
    </section>

    <section class="section-block--inset fill-softgrey">
        <div class="container container--smaller">
            <div class="block inset-on-m">
                <div class="bzg">
                    <?php for ($i=1; $i <= 9; $i++) { ?>
                    <div class="block bzg_c" data-col="m6, l4">
                        <div class="card--solid">
                            <figure class="fill-lightgrey">
                                <a href="#" class="responsive-media media--3-2">
                                    <img data-src="//via.placeholder.com/450x300?text=Promo_<?= $i ?>" alt="" class="lazyload">
                                </a>
                            </figure>
                            <div class="item-text">
                                <h4 class="text-normal mb-half">
                                    <a href="#" class="ellipsis-2 t-black" 
                                    title="Promo <?= $i ?> DBS Partnership Program Akhir Tahun">
                                        Promo <?= $i ?> DBS Partnership Program Akhir Tahun
                                    </a>
                                </h4>
                                <div class="mb-half line--small text-grey">
                                    <div class="mb-half">2 Juli 2019 - 30 Juni 2020</div>
                                    <div>
                                        <strong>Program cicilan</strong><br>
                                        3 bulan minimum transaksi 500 Rb
                                    </div>
                                </div>
                                <div class="code-badge--red">
                                    <div class="badge__content">
                                        <small>Kode Promo</small>
                                        <strong>PROMOCODE<?= $i ?></strong>
                                        <input type="hidden" value="PROMOCODE<?= $i ?>" id="PROMOCODE<?= $i ?>">
                                    </div>
                                    <button class="badge__btn" type="button" 
                                    data-copy="#PROMOCODE<?= $i ?>" aria-label="Copy">
                                        <span class="fa fa-copy"></span>
                                    </button>
                                </div>
                            </div>
                            <div class="card-action">
                                <a href="#" class="btn btn--red btn--block btn--round">
                                    DETAIL PROMO
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <ol class="pagination text-right">
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-left"></span>
                    </a>
                </li>
                <li>
                    <a href="#" class="active">
                        1
                    </a>
                </li>
                <li>
                    <a href="#">
                        2
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-right"></span>
                    </a>
                </li>
            </ol>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/underFooter.php'; ?>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/headroom.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/object-fit-images.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.selectric.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/lazysizes.min.js"></script>

<?php include '_partials/mainScripts.php'; ?>
