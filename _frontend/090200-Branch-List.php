<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Mice &amp; Corp</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="sr-only">Branch Office</h1>
        <section class="block">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
            </figure>
            <div class="block--inset">
                <h3 class="block--small">Pilih lokasi H.I.S. Travel Indonesia terdekat</h3>
                <select name="" id="" class="form-input form-input--block select-filter" data-item="dev/network.json" data-target="#branchList" data-tmpl="#branchTmpl">
                    <option value="All">All</option>
                </select>
                <template>
                    {{#each this}}
                    <option value="{{area}}">{{area}}</option>
                    {{/each}}
                </template>
            </div>
        </section>

        <template id="branchTmpl">
            {{#each this}}
            <div class="block">
                <hr class="block--half">
                <h3 class="text-up">{{area}}</h3>
                <div class="inset-on-m">
                <div class="bzg">
                    {{#each locations}}
                    <div class="block bzg_c" data-col="m4, l3">
                        <figure>
                            <a href="{{details}}" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="{{image}}" alt="">
                            </a>
                            <figcaption class="text-center">
                                <h3 class="text-up block--small line--small">
                                    <a href="{{details}}" class="link-black text--smaller">{{location}}</a>
                                </h3>
                                <div class="text-center mb-half">
                                    <a class="btn btn--round btn--smaller btn--red" 
                                    href="https://wa.me/{{whatsapp}}" target="_blank">
                                        Chat Now
                                    </a>
                                </div>
                                {{#each telephone}}
                                <a href="tel:{{this}}">{{this}}</a>
                                {{/each}}<br>
                                <div class="text--smaller line--small">{{office-hour}}</div>
                            </figcaption>
                        </figure>
                    </div>
                    {{/each}}
                </div>
                </div>
            </div>
            {{/each}}
        </template>
        <section class="section-block" id="branchList">
            
        </section>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
