<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Tour</a></li>
                <li><a href="#">Group Tour</a></li>
                <li><a href="#">Jepang</a></li>
                <li><a href="#">6D4N Mono Sapporo &amp; Tokyo</a></li>
            </ul>
        </div>
    </div>


    <div class="container container--smaller">
        <h1 class="block--small">6D4N Mono Sapporo &amp; Tokyo</h1>
        <div class="block bzg">
            <div class="bzg_c" data-col="l8">
                <section class="section-block--smaller">
                    <div class="responsive-media media--3-2">
                        <div class="demihero-slider slide-default">
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-1.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-2.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- demihero-slider -->
                    </div>
                </section>
                <section class="section--block one-page-nav-container">
                    <nav class="inpage-nav block fill--overlap fill-lightgrey ">
                        <ul class="list-nostyle navs--inline text-up one-page-nav" id="jrPassNav">
                            <li class="inpage-nav__item nav__item current">
                                <a href="#tour_point">Tour Point</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_itinerary">Itinerary</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_description">Deskripsi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_location">Lokasi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_summary">Rincian Tour</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#" class="external">Unduh Flyer</a>
                            </li>
                        </ul>
                        <div class="book-trigger flex a-center">
                            <div class="fg-1">
                                mulai dari<br>
                                <strong class="t--larger nowrap">
                                    IDR 26.504.500
                                </strong>
                            </div>
                            <div class="fg-1 text--smaller">
                                <a href="#bookNow" class="btn btn--round btn--block btn--red btn-sticky-trigger">
                                    <strong class="text-up">Pesan Sekarang</strong>
                                </a>
                            </div>
                        </div>
                    </nav>
                    <article>
                        <div class="block">
                            <?php include '_partials/optional-tour/tour_point.php'; ?>
                            <?php include '_partials/group-tour/tour_itinerary.php'; ?>
                            <?php include '_partials/optional-tour/tour_description.php'; ?>
                            <?php include '_partials/optional-tour/tour_location.php'; ?>
                            <?php include '_partials/group-tour/tour_summary.php'; ?>
                        </div>
                        <hr>
                    </article>
                </section>
            </div>
            <div class="bzg_c" data-col="l4" data-sticky-container>
                <div class="sticky-trigger fill-yellow is-fixed">
                    <div class="container container--smaller">
                        <div class="flex a-center">
                            <div class="fg-1 mr-small">
                                <strong class="in-block">mulai dari</strong><br>
                                <strong class="t--larger nowrap">
                                    IDR 26.504.500
                                </strong>
                            </div>
                            <div class="fg-1 text--smaller">
                                <a href="#bookNow" class="btn btn--round btn--block btn--red btn-sticky-trigger">
                                    <strong class="text-up">Pesan Sekarang</strong>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cards sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="120">
                    <div id="bookNow" class="sticky-target"></div>
                    <div class="card__item">
                        <div class="card-head cf block--inset fill-yellow">
                            <strong class="in-block">mulai dari</strong>
                            <strong class="t--large pull-right">
                                IDR 26.504.500
                            </strong>
                        </div>
                        <div class="block--inset card-content">
                            <form action="" class="form form--line">
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-pesawat"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            All Nipon Airlines<br>
                                            <img src="//placehold.it/60x25" alt="">
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-alarm"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            3 Hari 2 Malam
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-travel-bag"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            Optional Tour
                                        </strong>
                                    </div>
                                </div>
                                <!-- <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label>
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="">Minggu, 28 September 2018, 10 pax tersisa</option>
                                            <option value="">Durasi 2</option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label>
                                        <input type="text" class="form-input form-input--block pickaDay"
                                        data-items="group-tour.json" data-max-date="2018-12-20"
                                        id="departure_start" placeholder="Tanggal Berangkat">
                                    </div>
                                </div>
                                <div class="">
                                    <div class="input-iconic--left block--half">
                                        <label for="" class="label-icon">
                                            <span class="his-user-group"></span>
                                        </label>
                                        <div class="pick-guest fg-1" id="pickJRpassPsg" data-toggle
                                        data-min="1" data-totalvat="#totalVat_753" data-totalsurcharge="#totalSur_753"
                                        data-inclusions="0" data-total="#product_753"
                                        data-sum="#bookSummary">
                                            <button class="form-input form-input--block btn-toggle" type="button">
                                                <strong class="text-ellipsis">
                                                    Jumlah tamu: <span class="total-input--all" id="total_psgjr">0</span>
                                                </strong>
                                            </button>
                                            <div class="toggle-panel t-black fill-white">
                                                <div class="block--inset-small">
                                                    <fieldset class="over-y-auto">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Dewasa- TWN / TRP 2</strong><br>
                                                                IDR 50,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red"
                                                                data-action="minus" type="button" data-age="single" data-type="person">
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <!-- button -->
                                                                <input type="number"
                                                                class="form-input form-input--redline text-center input-person"
                                                                value="0" min="2" max="10"
                                                                data-step="1" data-psg="total_psgjr" data-price="50000000"
                                                                data-vat="500000" data-surcharge="100000"
                                                                data-target="#dewasa_1">
                                                                <!-- input -->
                                                                <button class="btn btn--plain btn--plain-red"
                                                                data-action="plus" type="button" data-age="single" data-type="person">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                                <!-- button -->
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Single- TWN/TRP 2</strong><br>
                                                                IDR 5,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="single" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person"
                                                                value="0" min="0" max="10" data-psg="total_psgjr"
                                                                data-step="1" data-price="5000000"
                                                                data-vat="500000" data-surcharge="100000"
                                                                data-target="#anak_1">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="single">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Anak- Twin Sharing 1</strong><br>
                                                                IDR 30,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person"
                                                                value="0" min="0" max="10" data-psg="total_psgjr"
                                                                data-step="1" data-price="30000000"
                                                                data-vat="300000" data-surcharge="100000"
                                                                data-target="#anak_2">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Anak- TWN/TRP 3</strong><br>
                                                                IDR 20,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person"
                                                                value="0" min="0" max="10" data-psg="total_psgjr"
                                                                data-step="1" data-price="20000000"
                                                                data-vat="300000" data-surcharge="100000"
                                                                data-target="#anak_3">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Anak- TWN/TRP 1</strong><br>
                                                                IDR 10,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person"
                                                                value="0" min="0" max="10" data-psg="total_psgjr"
                                                                data-step="1" data-price="10000000"
                                                                data-vat="100" data-surcharge="300"
                                                                data-target="#anak_4">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">

                                                    </fieldset>
                                                    <div class="text-right">
                                                        <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                                            Pilih
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- pick-guest -->
                                    </div>
                                </div>

                                <div id="bookSummary" class="book-sum">
                                    <table class="table-total">
                                        <tbody>
                                            <tr>
                                                <td>Dewasa- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="dewasa_1">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Single- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_1">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- Twin Sharing <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_2">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_3">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_4">0</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Vat(1%)</td>
                                                <td class="text-right">Rp. <span id="totalVat_753">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Surcharge</td>
                                                <td class="text-right">Rp. <span id="totalSur_753">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="inclusion_1">
                                                        <input type="checkbox" class="check-inclusion" id="inclusion_1"
                                                        data-include="10" data-includetarget="#total_includes_1">
                                                        Include Something <small>(<span class="total-includes"></span>)</small>
                                                    </label>
                                                </td>
                                                <td class="text-right">Rp. <span id="total_includes_1">0</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><hr class="no-space"></td>
                                            </tr>
                                            <tr>
                                                <td><strong>TOTAL</strong></td>
                                                <td class="text-right text-red t--larger t-strong">
                                                    Rp. <span id="product_753">0</span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <hr class="mb-half">
                                <div class="mb-half">
                                    <a href="#">Syarat dan Ketentuan</a>
                                </div>
                                <button class="btn btn--round btn--block btn--red" type="submit">
                                    <b class="text-up">Pesan Sekarang</b>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Produk Terkait
                </h2>
            </div>
            <div class="block content-section">
                <div class="bzg cards slide-on-mobile coverSlide" data-slide-small="1" data-slide-medium="3" data-slick='{"arrows": false, "dots": true}' data-center="true">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <a href="#" class="link-shadow" aria-label="KOREA JEJU PLUS HAN"></a>
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/400x400" alt="" class="item-heavy lazyload">
                                <div class="ribbon ribbon--gold">
                                    <span class="ribbon__text">Recommended</span>
                                </div>
                            </figure>
                            <div class="item-text item-text--extra">
                                <a href="#">
                                    <span class="in-block text-up">
                                        <span class="fa fa-globe"></span>
                                        Jepang
                                    </span>
                                    <strong class="text-up ellipsis-2 title">
                                        KOREA JEJU PLUS HAN
                                    </strong>
                                    <div class="cf">
                                        <strong class="t-switch-red-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                    </div>
                                </a>
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--round btn--ghost-red">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
