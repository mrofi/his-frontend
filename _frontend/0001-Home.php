<?php 
    $notLogin = 1
?>
<?php include '_partials/head.php'; ?>
<style type="text/css">
    .slider-hero .slick-dots {
        z-index: 6;
    }
</style>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <section class="section-slide-hero">
    	<div class="container">
    		<div class="slider-hero">
    			<div class="slider__item">
    				<a href="#">
    					<!-- <img class="img-dual lazyload" src="//via.placeholder.com/425"
                        data-small="//via.placeholder.com/425"
                        data-medium="assets/img/slidebanner-1.jpg" alt=""> -->

                        <picture>
                            <source data-srcset="assets/img/slidebanner-1.jpg"
                            media="(min-width: 768px)">
                            <img class="lazyload" data-src="//via.placeholder.com/425" alt="">
                        </picture>
    				</a>
    			</div>
                <div class="slider__item">
                    <a href="#">
                        <!-- <img class="img-dual lazyload" src="//via.placeholder.com/425"
                        data-small="//via.placeholder.com/425"
                        data-medium="assets/img/slidebanner-1.jpg" alt=""> -->

                        <picture>
                            <source data-srcset="assets/img/slidebanner-1.jpg"
                            media="(min-width: 768px)">
                            <img class="lazyload" data-src="//via.placeholder.com/425" alt="">
                        </picture>
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <!-- <img class="img-dual lazyload" src="//via.placeholder.com/425"
                        data-small="//via.placeholder.com/425"
                        data-medium="assets/img/slidebanner-1.jpg" alt=""> -->

                        <picture>
                            <source data-srcset="assets/img/slidebanner-1.jpg"
                            media="(min-width: 768px)">
                            <img class="lazyload" data-src="//via.placeholder.com/425" alt="">
                        </picture>
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <!-- <img class="img-dual lazyload" src="//via.placeholder.com/425"
                        data-small="//via.placeholder.com/425"
                        data-medium="assets/img/slidebanner-1.jpg" alt=""> -->

                        <picture>
                            <source data-srcset="assets/img/slidebanner-1.jpg"
                            media="(min-width: 768px)">
                            <img class="lazyload" data-src="//via.placeholder.com/425" alt="">
                        </picture>
                    </a>
                </div>
    		</div>
    	</div>
        <div class="slide-hero-arrows"></div>
    </section>
    <!-- section-slide-hero -->

    <?php include '_partials/_home/section-booking.php'; ?>

    <section class="section-block promo-text fill-lightgrey">
        <div class="container">
            <div class="promo-text__slide">
                <div class="promo__item">
                    <a href="#" class="flex">
                        <figure class="space-right size-32">
                            <img class="lazyload" data-src="assets/img/compas.png" alt="" width="32">
                        </figure>
                        <span class="fg-1">
                            <small>08 February 2017</small><br>
                            AIR ASIA Travel Fair 2017, 9-12 February 2017
                            at Kota Kasablanka, Jakarta
                        </span>
                    </a>
                </div>
                <!-- item -->
                <div class="promo__item">
                    <a href="#" class="flex">
                        <figure class="space-right size-32">
                            <img class="lazyload" data-src="assets/img/koper.png" alt="" width="32">
                        </figure>
                        <span class="fg-1">
                            6D/4N JEJU + HONG KONG<br>
                            (Group Tour) start from IDR 8.9jtan
                        </span>
                    </a>
                </div>
                <!-- item -->
                <div class="promo__item">
                    <a href="#" class="flex">
                        <figure class="space-right size-32">
                            <img class="lazyload" data-src="assets/img/koper.png" alt="" width="32">
                        </figure>
                        <span class="fg-1">
                            Japan Theme Park Ticket<br>
                            start from IDR 396.700~ (Rate : JPY 119)
                        </span>
                    </a>
                </div>
                <!-- item -->
                <div class="promo__item">
                    <a href="#" class="flex">
                        <figure class="space-right size-32">
                            <img class="lazyload" data-src="assets/img/compas.png" alt="" width="32">
                        </figure>
                        <span class="fg-1">
                            <small>08 February 2017</small><br>
                            AIR ASIA Travel Fair 2017, 9-12 February 2017
                            at Kota Kasablanka, Jakarta
                        </span>
                    </a>
                </div>
                <!-- item -->
            </div>
        </div>
    </section>

    <section class="section-block">
        <div class="container">
            <div class="bzg">
                <div class="block bzg_c" data-col="m6">
                    <h2 class="block--small text-up in-block">
                        Promo Bank
                    </h2>
                    <div class="slide-basic text-center">
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr1"></div>
                            </a>
                        </div>
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr2"></div>
                            </a>
                        </div>
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr3"></div>
                            </a>
                        </div>
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr4"></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m6">
                    <h2 class="block--small text-up in-block">
                        Event
                    </h2>
                    <div class="slide-basic text-center">
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr21"></div>
                            </a>
                        </div>
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr22"></div>
                            </a>
                        </div>
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr23"></div>
                            </a>
                        </div>
                        <div class="block--half slide__item">
                            <a href="0001-Home.php" class="qr-slide-wrapper">
                                <div class="qr-banner">
                                    <img data-src="assets/img/adds-medium.jpg" alt="" class="lazyload img-full">
                                </div>
                                <div class="qr-code" id="qr24"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </section>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="is-mobile">
                <div class="block bzg bzg--small-gutter">
                    <div class="bzg_c" data-col="x7">
                        <a href="#" class="is-block">
                            <img data-src="https://via.placeholder.com/600x400" alt="" class="lazyload">
                        </a>
                    </div>
                    <div class="bzg_c" data-col="x5">
                        <strong class="text-red no-space">SPECIAL PROMOTION</strong>
                        <h4 class="mb-small line--small">
                            <a href="#" class="link-black">
                                2 JR Pass Ordinary 7 Days - Last Minute Deals
                            </a>
                        </h4>
                        <p class="line--small mb-half text--smaller">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati dolor, ut voluptatum consectetur sint.
                        </p>
                        <h3 class="text-red mb-half">
                            IDR. 27.356.000 ~
                        </h3>
                        <a href="#" class="btn btn--round btn--smaller btn--ghost-red">SELENGKAPNYA</a>
                    </div>
                </div>
            </div>
            <div class="is-desktop">
                <h2>SPECIAL PROMOTION</h2>
                <a href="#" class="is-block text-center">
                    <img data-src="https://via.placeholder.com/950x243?text=Banner_Desktop" class="img-full item-heavy lazyload" alt="">
                </a>
            </div>
            <hr>
        </div>
    </section>

    <?php include '_partials/_home/group-tour.php'; ?>
    <?php include '_partials/_home/individual-tour.php'; ?>

    <section class="section-block section-block--inset bg-section" style="background-image: url('')">
        <img data-src="assets/img/bg-why.jpg" alt="" class="bg-section-img lazyload">
        <div class="container container--smaller text-white content-section">
            <h2 class="text-script text-center">
                Mengapa H.I.S.?
            </h2>
            <div class="why-slide">
                <div class="why-slide__item">
                    <div class="flex">
                        <figure class="c-white size-64 space-right">
                            <img class="lazyload" data-src="assets/img/icon-koper.png" alt="">
                        </figure>
                        <strong>
                            H.I.S. merupakan salah satu travel agen terbaik di Jepang, yang melayani lebih dari 30 tahun.
                        </strong>
                    </div>
                </div>
                <div class="why-slide__item">
                    <div class="flex">
                        <figure class="c-white size-64 space-right">
                            <img class="lazyload" data-src="assets/img/icon-shake.png" alt="">
                        </figure>
                        <strong>
                            Kami bertanggung jawab mendukung kebutuhan pengalaman Anda mulai pada saat reservasi hingga kembali ke rumah! Silakan bertanya dan konsultasi, staff kami akan melayani Anda.
                        </strong>
                    </div>
                </div>
                <div class="why-slide__item">
                    <div class="flex">
                        <figure class="c-white size-64 space-right">
                            <img class="lazyload" data-src="assets/img/icon-koper.png" alt="">
                        </figure>
                        <strong>
                            H.I.S. merupakan salah satu travel agen terbaik di Jepang, yang melayani lebih dari 30 tahun.
                        </strong>
                    </div>
                </div>
                <div class="why-slide__item">
                    <div class="flex">
                        <figure class="c-white size-64 space-right">
                            <img class="lazyload" data-src="assets/img/icon-shake.png" alt="">
                        </figure>
                        <strong>
                            Kami bertanggung jawab mendukung kebutuhan pengalaman Anda mulai pada saat reservasi hingga kembali ke rumah! Silakan bertanya dan konsultasi, staff kami akan melayani Anda.
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include '_partials/_home/optional-tour.php'; ?>
    <?php include '_partials/_home/theme-park.php'; ?>
    <?php include '_partials/_home/travel-support.php'; ?>
    <?php include '_partials/_home/counter-stats.php'; ?>
    <?php include '_partials/_home/prefecture-japan.php'; ?>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Public Relation
                </h2>
            </div>
        </div>
        <div class="container container--smaller">
            <div class="block bzg slide-on-mobile" data-slide-small="1" data-slide-medium="2"  data-slick='{"arrows": false, "dots": true}'>
                <div class="block--half bzg_c" data-col="l4">
                    <a href="#">
                        <img class="lazyload" data-src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="l4">
                    <a href="#">
                        <img class="lazyload" data-src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="l4">
                    <a href="#">
                        <img class="lazyload" data-src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="l4">
                    <a href="#">
                        <img class="lazyload" data-src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="l4">
                    <a href="#">
                        <img class="lazyload" data-src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="l4">
                    <a href="#">
                        <img class="lazyload" data-src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
            </div>
            <!-- <hr> -->
        </div>
    </section>

    <!-- <?php //include '_partials/_home/optional-tour-slide.php'; ?> -->
    <?php include '_partials/_home/testimonial.php'; ?>
    <?php include '_partials/_home/partner-maskapai.php'; ?>
    <?php include '_partials/_home/payment.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/underFooter.php'; ?>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/waypoint.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.counterup.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/headroom.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/baze.validate.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/object-fit-images.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/slick.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/moment.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.jquery.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.selectric.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/select2.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/qrcode.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/lazysizes.min.js"></script>

<script>
    var cachePath = './';
</script>
<?php include '_partials/mainScripts.php'; ?>
