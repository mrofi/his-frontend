<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pemesanan</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <ol class="block list-nostyle text-center text-up steps">
            <li class="steps__item current">
                <span>Rincian Anda</span>
            </li>
            <li class="steps__item">
                <span>Selesai</span>
            </li>
        </ol>

        <div class="block--double bzg">
            <div class="bzg_c" data-col="m8">
                <h1 class="block--small">Rincian Anda</h1>
                <div class="block p-smaller fill-lightgrey flex v-ct">
                    <img src="assets/img/icon-user.png" alt="" width="44" class="mr">
                    <div class="fg-1">
                        Silakan <a href="#">log-in</a> untuk mempercepat pengisian atau lanjut sebagai tamu
                    </div>
                </div>
                <form action="" class="form baze-form"
                data-empty="Input tidak boleh kosong"
                data-email="Alamat email tidak benar"
                data-number="Input harus berupa angka"
                data-numbermin="Angka minimal "
                data-numbermax="Angka maksimal"
                data-flash-msg="Lengkapi dulu data pemesanannya!">
                    <fieldset class="block">
                        <h2 class="text-up">Data Pemesan</h2>
                        <div class="form__row">
                            <label class="space-right t-strong" for="">Titel *</label>
                            <label for="title_mr" class="btn-label">
                                <input type="radio" id="title_mr" name="gender" data-dest="title_mr1">
                                <span class="btn-label__text">Mr.</span>
                            </label>
                            <label for="title_mrs" class="btn-label">
                                <input type="radio" id="title_mrs" name="gender" checked data-dest="title_mrs1">
                                <span class="btn-label__text">Mrs.</span>
                            </label>
                            <label for="title_ms" class="btn-label">
                                <input type="radio" id="title_ms" name="gender" data-dest="title_ms1">
                                <span class="btn-label__text">Ms.</span>
                            </label>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="oriFirstName" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" id="oriFirstName" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="oriLastName" class="t-strong">Nama Belakang *</label><br>
                                <input type="text" class="form-input form-input--block" id="oriLastName" required>
                            </div>
                        </div>
                        <div class="block--small bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nomor Telepon *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="+61">+61</option>
                                            <option value="+62" selected>+62</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x8">
                                        <input type="number" class="form-input form-input--block" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Email</label><br>
                                <input type="email" class="form-input form-input--block" data-email-guest required>
                            </div>
                        </div>
                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <hr>

                    <fieldset class="block copy-val">
                        <h2 class="text-up">Tamu 1</h2>
                        <div class="block block--inset-small fill-yellow t-strong">
                            <span class="text-red">* Perhatian</span><br>
                            Pastikan data tamu sudah sesuai dengan identitas paspor yang masih berlaku
                        </div>
                        <div class="form__row">
                            <label for=""></label><br>
                            <select name="" id="" class="form-input form-input--block selectstyle" 
                            data-passengers="dev/userPassengers.json" data-number="0">
                                <option value="">Isi data baru</option>
                                <option value="Rofi Udin">Rofi Udin</option>
                                <option value="Rofi Yanti">Rofi Yanti</option>
                            </select>
                        </div>
                        <div class="form__row">
                            <label for="same_as" class="btn-label btn-copy">
                                <input type="checkbox" id="same_as" data-msg="Pastikan <b>Data Pemesan</b> sudah terisi Cuk!">
                                <span class="btn-label__text">Sama dengan pemesan</span>
                            </label>
                        </div>
                        <div class="form__row">
                            <label class="space-right t-strong" for="">Titel *</label>
                            <label for="title_mr1" class="btn-label">
                                <input type="radio" id="title_mr1" name="passenger_title[0]" value="mr" checked data-origin="title_mr">
                                <span class="btn-label__text">Mr.</span>
                            </label>
                            <label for="title_mrs1" class="btn-label">
                                <input type="radio" id="title_mrs1" name="passenger_title[0]" value="mrs" data-origin="title_mrs">
                                <span class="btn-label__text">Mrs.</span>
                            </label>
                            <label for="title_ms1" class="btn-label">
                                <input type="radio" id="title_ms1" name="passenger_title[0]" value="ms" checked data-origin="title_ms">
                                <span class="btn-label__text">Ms.</span>
                            </label>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="destFirstName" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" id="destFirstName" data-origin="oriFirstName" required name="passenger_first_name[0]">
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Belakang *</label><br>
                                <input type="text" class="form-input form-input--block" id="destLastName" data-origin="oriLastName" required name="passenger_last_name[0]">
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Tanggal Lahir *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="passenger_birthdate_day[0]" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="28">28</option>
                                            <option value="31">31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="passenger_birthdate_month[0]" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="08">08</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="passenger_birthdate_year[0]" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000">2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Jenis Kelamin *</label><br>
                                <label for="male2" class="btn-label">
                                    <input type="radio" id="male2" name="passenger_gender[0]" value="male" checked>
                                    <span class="btn-label__text">Laki-laki</span>
                                </label>
                                <label for="female2" class="btn-label">
                                    <input type="radio" id="female2" name="passenger_gender[0]" value="female">
                                    <span class="btn-label__text">Perempuan</span>
                                </label>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Kewarganegaraan</label><br>
                                <select name="passenger_nationality[0]" id="" class="form-input form-input--block select_2">
                                    <option value="">Kewarganegaraan</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Jepang" selected="selected">Jepang</option>
                                </select>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Negara yang Mengeluarkan</label><br>
                                <select name="passenger_passport_issued_country[0]" id="" class="form-input form-input--block select_2">
                                    <option value="">Negara yang Mengeluarkan</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Jepang">Jepang</option>
                                </select>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nomor Paspor *</label><br>
                                <input type="text" class="form-input form-input--block" required
                                name="passenger_passport_number[0]">
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Masa Berlaku Paspor *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select id="" class="form-input form-input--block selectstyle"
                                        name="passenger_passport_expired_day[0]">
                                            <option value="1">1</option>
                                            <option value="28">28</option>
                                            <option value="31">31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select id="" class="form-input form-input--block selectstyle"
                                        name="passenger_passport_expired_month[0]">
                                            <option value="01">01</option>
                                            <option value="08">08</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select id="" class="form-input form-input--block selectstyle"
                                        name="passenger_passport_expired_year[0]">
                                            <option value="1990">1990</option>
                                            <option value="2000" >2000</option>
                                            <option value="2021">2021</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <hr>
                    <fieldset class="block">
                        <h2 class="text-up">Tamu 2</h2>
                        <div class="form__row">
                            <label class="space-right t-strong" for="">Titel *</label>
                            <label for="title_mr13" class="btn-label">
                                <input type="radio" id="title_mr13" name="gender13" required>
                                <span class="btn-label__text">Mr.</span>
                            </label>
                            <label for="title_mrs13" class="btn-label">
                                <input type="radio" id="title_mrs13" name="gender13">
                                <span class="btn-label__text">Mrs.</span>
                            </label>
                            <label for="title_ms13" class="btn-label">
                                <input type="radio" id="title_ms13" name="gender13">
                                <span class="btn-label__text">Ms.</span>
                            </label>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Belakang *</label><br>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Tanggal Lahir *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1">1</option>
                                            <option value="31" selected>31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="12" selected>12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000" selected>2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Jenis Kelamin *</label><br>
                                <label for="male23" class="btn-label">
                                    <input type="radio" id="male23" name="gender123" checked>
                                    <span class="btn-label__text">Laki-laki</span>
                                </label>
                                <label for="female23" class="btn-label">
                                    <input type="radio" id="female23" name="gender123">
                                    <span class="btn-label__text">Perempuan</span>
                                </label>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Nama Depan *</label><br>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="t-strong">Masa Berlaku Paspor *</label><br>
                                <div class="bzg bzg--small-gutter">
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1">1</option>
                                            <option value="31" selected>31</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="01">01</option>
                                            <option value="12" selected>12</option>
                                        </select>
                                    </div>
                                    <div class="bzg_c" data-col="x4">
                                        <select name="" id="" class="form-input form-input--block selectstyle">
                                            <option value="1990">1990</option>
                                            <option value="2000" selected>2000</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <strong class="text-red">* Wajib diisi</strong>
                    </fieldset>
                    <fieldset>
                        <h2 class="text-up">Metode Pembayaran</h2>
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m4">
                                <div class="block--inset-smallest border full-v">
                                    <label for="pay-1" class="btn-label btn--block">
                                        <input type="radio" id="pay-1" name="pay-method" checked required>
                                        <div class="btn-label__tex">
                                            <!-- <img src="assets/img/atm-bersma.png" alt="" width="40"> -->
                                            <img src="assets/img/prima.png" alt="" width="40"><br>
                                            <!-- <img src="assets/img/alto.png" alt="" width="40"> -->
                                            Bank Transfer
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m4">
                                <div class="block--inset-smallest border full-v">
                                    <label for="pay-2" class="btn-label btn--block">
                                        <input type="radio" id="pay-2" name="pay-method">
                                        <div class="btn-label__tex">
                                            <img src="assets/img/visa.jpg" alt="" width="40"><br>
                                            <!-- <img src="assets/img/master-card.png" alt=""  width="40"> -->
                                            Visa/Master Card<br>
                                            <a href="0001-Home.php">info</a>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m4">
                                <div class="block--inset-smallest border full-v">
                                    <label for="pay-3" class="btn-label btn--block">
                                        <input type="radio" id="pay-3" name="pay-method" >
                                        <div class="btn-label__tex">
                                            <img src="assets/img/bca.png" alt="" width="40"><br>
                                            Virtual Account Bank Transfer
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <hr>
                    <fieldset>
                        <div class="block" data-voucher="dev/voucher/" data-form="#voucher" data-total="#tableTotal">
                            <div class="flex form-voucher">
                                <input type="text" class="form-input form-input--block" placeholder="Masukkan Kode Voucher" data-name="code" id="inputVoucher">
                                <button class="btn btn--blue btn-apply" type="button" disabled="disabled">Gunakan</button>
                            </div>
                            <span class="msg-container"></span>
                            <button class="btn btn--round btn--pink text--smaller ml-half btn-change-voucher hidden" type="button" data-target="#inputVoucher">
                                Ganti voucher
                            </button>
                        </div>
                        <hr>
                    </fieldset>
                    <div class="block block--inset-small border">
                        <label for="agree_term" class="btn-label">
                            <input type="checkbox" id="agree_term" required class="submit-activator">
                            <span class="btn-label__text">
                                Saya menyetujui seluruh <a href="#">Syarat dan Ketentuan</a> dan <a href="#">Kebijakan Privasi</a> yang berlaku di
                                H.I.S. Travel Indonesia
                            </span>
                        </label>
                    </div>
                    <div class="text-right">
                        <button class="btn btn--round btn--red t-strong text-up btn-submit" type="submit" disabled="disabled">Lanjutkan</button>
                    </div>
                </form>
                <form action="" id="voucher">
                    <!-- <input type="hidden" name="code" value="asdf123"> -->
                    <input type="hidden" name="booking_code" value="lkjv876">
                </form>
            </div>
            <!-- left -->
            <div class="bzg_c" data-col="m4" data-sticky-container>
                <div class="border sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="70">
                    <h3 class="no-space text-center text-up block--inset-small">Rincian Pesanan</h3>
                    <h3 class="no-space text-center text-up block--inset-small fill-yellow">Japan Rail Pass</h3>
                    <div class="block block--inset-small">
                        <div class="info-iconic--left">
                            <div class="label-icon">
                                <span class="fa fa-calendar"></span>
                            </div>
                            Minggu, 6 Agustus 2017
                        </div>
                        <div class="info-iconic--left">
                            <div class="label-icon">
                                <span class="his-train"></span>
                            </div>
                            JR Ordinary Pass
                        </div>
                        <div class="info-iconic--left">
                            <div class="label-icon">
                                <span class="his-alarm"></span>
                            </div>
                            7 Hari
                        </div>
                        <div class="info-iconic--left">
                            <div class="label-icon">
                                <span class="his-travel-bag"></span>
                            </div>
                            JR Pass
                        </div>
                        <div class="info-iconic--left">
                            <div class="label-icon">
                                <span class="his-user-group"></span>
                            </div>
                            2 Travelers
                        </div>
                    </div>
                    <hr class="no-space hr-blue">
                    <h3 class="no-space text-center text-up block--inset-small">Rincian Harga</h3>
                    <hr class="hr-blue">
                    <div class="block--inset-small">
                        <table class="table-total" id="tableTotal">
                            <tbody>
                                <tr>
                                    <td>Dewasa x 2</td>
                                    <td class="text-right">Rp. 3.528.200</td>
                                </tr>
                                <tr>
                                    <td>VAT 1%</td>
                                    <td class="text-right">Rp. 35.282</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td><strong>TOTAL</strong></td>
                                    <td class="text-right text-red t--larger t-strong" data-total="3657000">
                                        Rp. 3.657.000
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
