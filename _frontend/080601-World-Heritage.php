<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">World Heritage</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <h1 class="sr-only">World Heritage</h1>
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block--half text-center">
                <a class="btn btn--round btn--ghost-red is-active" href="#">
                    <strong class="text-up">Jepang</strong>
                </a>
                <a class="btn btn--round btn--ghost-red" href="#">
                    <strong class="text-up">Asia</strong>
                </a>
            </div>
            <hr>
            <div class="inset-on-m">
            <div class="block bzg bzg--small-gutter">
                <?php for ($i=1; $i <= 8; $i++) { ?>
                <div class="block bzg_c" data-col="m4, l3">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                            Tak hanya maju di bidang teknologi dan ekonomi, Jepang didukung oleh kondisi alamnya yang indah serta objek wisata budaya yang selalu dilestarikan
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
            </div>
            </div>
            <hr>
        </div>
    </section>
    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
