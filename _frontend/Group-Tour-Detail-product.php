<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Tour</a></li>
                <li><a href="#">Group Tour</a></li>
                <li><a href="#">Jepang</a></li>
                <li><a href="#">6D4N Mono Sapporo &amp; Tokyo</a></li>
            </ul>
        </div>
    </div>

    
    <div class="container container--smaller">
        <h1 class="block--small">6D4N Mono Sapporo &amp; Tokyo</h1>
        <div class="block bzg">
            <div class="bzg_c" data-col="m7, l8">
                <section class="section-block--smaller">
                    <div class="responsive-media media--3-2">
                        
                    </div>
                </section>
                <section class="section--block one-page-nav-container">
                    
                </section>
            </div>
            <div class="bzg_c" data-col="m5, l4" data-sticky-container>
                <div class="cards sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="120">
                    <div class="card__item">
                        <div class="card-head cf block--inset fill-yellow">
                            <strong class="in-block">mulai dari</strong>
                            <strong class="t--large pull-right">
                                IDR 26.504.500
                            </strong>
                        </div>
                        <div class="block--inset card-content">
                            <form action="" class="form form--line book-form" data-get-items="">
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-pesawat"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            All Nipon Airlines<br>
                                            <img src="//placehold.it/60x25" alt="">
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-alarm"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            3 Hari 2 Malam
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-travel-bag"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            Optional Tour
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="his-alarm"></span>
                                        </label>
                                        <select name="" id="" class="form-input form-input--block selectstyle"
                                        data-tmplist="#qtyUpdater" data-tmplsum="#sumUpdater" data-items="1" data-name="duration">
                                            <option value="1" selected>Durasi 1</option>
                                            <option value="2">Durasi 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label>
                                        <input type="text" class="form-input form-input--block pickaDay" 
                                        data-items="2018/07/12" data-path="dev/" data-tmplist="#qtyUpdater" data-tmplsum="#sumUpdater"
                                        id="departure_start" placeholder="Tanggal Berangkat" value="2018/07/12" data-name="date">
                                    </div>
                                </div>
                                <div class="">
                                    <div class="input-iconic--left block--half">
                                        <label for="" class="label-icon">
                                            <span class="his-user-group"></span>
                                        </label>
                                        <div class="pick-guest fg-1" id="pickJRpassPsg" data-toggle 
                                        data-min="1" data-totalvat="#totalVat_753" data-totalsurcharge="#totalSur_753" 
                                        data-inclusions="0" data-total="#product_753" 
                                        data-sum="#bookSummary">
                                            <button class="form-input form-input--block btn-toggle" type="button">
                                                <strong class="text-ellipsis">
                                                    Jumlah tamu: <span class="total-input--person" id="total_psgjr">0</span>
                                                </strong>
                                            </button>
                                            <div class="toggle-panel t-black fill-white">
                                                <div class="block--inset-small item-container">
                                                    <fieldset class="over-y-auto">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1 line--small">
                                                                <strong>Dewasa- TWN / TRP 2</strong><br>
                                                                IDR 10,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" 
                                                                data-action="minus" type="button" data-age="no-age" data-type="product">
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <!-- button -->
                                                                <input type="number" 
                                                                class="form-input form-input--redline text-center input-product" 
                                                                value="1" min="1" max="8" data-psg="total_psgjr" 
                                                                data-step="1" data-step-item="1" data-price="10000000"
                                                                data-vat="1000000" data-surcharge="10000" 
                                                                data-target="#dewasa_1">
                                                                <!-- input -->
                                                                <button class="btn btn--plain btn--plain-red" 
                                                                data-action="plus" type="button" data-age="no-age" data-type="product">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                                <!-- button -->
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1 line--small">
                                                                <strong>Single- TWN/TRP 2</strong><br>
                                                                IDR 10,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="minus" type="button" data-age="no-age" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-product" 
                                                                value="0" min="0" max="8" data-psg="total_psgjr" 
                                                                data-step="2" data-step-item="1" data-price="10000000"
                                                                data-vat="1000000" data-surcharge="10000" 
                                                                data-target="#anak_1">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="plus" type="button" data-age="no-age">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1 line--small">
                                                                <strong>Anak- Twin Sharing 1</strong><br>
                                                                IDR 10,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="minus" type="button" data-age="no-age" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-product" 
                                                                value="0" min="0" max="8" data-psg="total_psgjr" 
                                                                data-step="1" data-step-item="2" data-price="10000000"
                                                                data-vat="1000000" data-surcharge="10000" 
                                                                data-target="#anak_2">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="plus" type="button" data-age="no-age">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1 line--small">
                                                                <strong>Anak- TWN/TRP 3</strong><br>
                                                                IDR 10,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="minus" type="button" data-age="no-age" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-product" 
                                                                value="0" min="0" max="8" data-psg="total_psgjr" 
                                                                data-step="1" data-step-item="1" data-price="10000000"
                                                                data-vat="1000000" data-surcharge="10000" 
                                                                data-target="#anak_3">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="plus" type="button" data-age="no-age">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1 line--small">
                                                                <strong>Anak- TWN/TRP 1</strong><br>
                                                                IDR 10,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="minus" type="button" data-age="no-age" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-product" 
                                                                value="0" min="0" max="8" data-psg="total_psgjr" 
                                                                data-step="1" data-step-item="1" data-price="10000000"
                                                                data-vat="1000000" data-surcharge="10000" 
                                                                data-target="#anak_4">
                                                                <button class="btn btn--plain btn--plain-red" data-type="product" data-action="plus" type="button" data-age="no-age">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">

                                                    </fieldset>
                                                    <div class="text-right">
                                                        <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                                            Pilih
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- pick-guest -->
                                    </div>
                                </div>

                                <div id="bookSummary" class="book-sum" style="display: block;">
                                    <table class="table-total sum-container">
                                        <tbody class="">
                                            <tr style="display: table-row">
                                                <td>Dewasa- TWN/TRP <small>(<span class="item-total">1</span>)</small></td>
                                                <td class="text-right">Rp. <span id="dewasa_1">10,000,000</span></td>
                                            </tr>
                                            <tr>
                                                <td>Single- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_1">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- Twin Sharing <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_2">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_3">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP <small>(<span class="item-total">0</span>)</small></td>
                                                <td class="text-right">Rp. <span id="anak_4">0</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Vat(1%) <small>(<span class="total-vat">1</span>)</small></td>
                                                <td class="text-right">Rp. <span id="totalVat_753" class="n-total">1,000,000</span></td>
                                            </tr>
                                            <tr>
                                                <td>Surcharge <small>(<span class="total-surcharge">1</span>)</small></td>
                                                <td class="text-right">Rp. <span id="totalSur_753" class="n-total">10,000</span></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="inclusion_1">
                                                        <input type="checkbox" class="check-inclusion" id="inclusion_1" 
                                                        data-include="10" data-includetarget="#total_includes_1">
                                                        Include Something <small>(<span class="total-includes"></span>)</small>
                                                    </label>
                                                </td>
                                                <td class="text-right">Rp. <span id="total_includes_1" class="n-total">0</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><hr class="no-space"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <strong class="pull-left mr-small">TOTAL</strong>
                                                    <div class="text-right text-red t--larger t-strong">
                                                        Rp. <span id="product_753" class="grand-total n-total">11,010,000</span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <hr class="mb-half">
                                <div class="mb-half">
                                    <a href="#">Syarat dan Ketentuan</a>
                                </div>
                                <button class="btn btn--round btn--block btn--red" type="submit">
                                    <b class="text-up">Pesan Sekarang</b>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Produk Terkait
                </h2>
            </div>
            <div class="block content-section">
                <div class="bzg cards cards--blue slide-3">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square">
                                <!-- <img src="assets/img/img-preload.png" data-src="//placehold.it/400x400" alt="" class="item-heavy"> -->
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</main>

<template id="qtyUpdater">
    {{#each this}}
    <div class="block--half flex v-ct qty-input">
        <label for="" class="fg-1 line--small">
            <strong>{{title}}</strong><br>
            IDR {{formatNumber price num}}++
        </label>
        <div class="">
            <button class="btn btn--plain btn--plain-red" data-type="{{type}}" data-action="minus" type="button" data-age="{{age}}" disabled>
                <span class="fa fa-minus"></span>
            </button>
            {{#with input}}
            <input type="number" class="form-input form-input--redline text-center {{type-class}}" 
            value="{{val}}" min="{{min}}" max="{{max}}" data-psg="total_psgjr" 
            data-step="{{step}}"  data-step-item="{{step-item}}" data-price="{{../price}}" 
            data-vat="{{vat}}" data-surcharge="{{surcharge}}" 
            data-target="#{{../id}}">
            {{/with}}
            <button class="btn btn--plain btn--plain-red" data-type="{{type}}" data-action="plus" type="button" data-age="{{age}}">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    </div>
    <hr class="block--half">
    {{/each}}
</template>

<script id="sumUpdater" type="text/html">
    <tbody>
    {{#each this}}
    <tr style="{{#if input.val}}display: table-row;{{/if}}">
        <td>{{title}} <small>(<span class="item-total">0</span>)</small></td>
        <td class="text-right">Rp. <span id="{{id}}">{{formatNumber price}}</span></td>
    </tr>
    {{/each}}
    </tbody>
    <tfoot>
        <tr>
            <td>Vat(1%) <small>(<span class="total-vat">1</span>)</small></td>
            <td class="text-right">
                Rp. <span id="totalVat_753">
                    {{#each this}}
                        {{#if input.val}}
                            {{formatNumber input.vat}}
                        {{/if}}
                    {{/each}}
                </span>
            </td>
        </tr>
        <tr>
            <td>Surcharge <small>(<span class="total-surcharge">1</span>)</small></td>
            <td class="text-right">
                Rp. <span id="totalSur_753">
                    {{#each this}}
                        {{#if input.val}}
                            {{formatNumber input.surcharge}}
                        {{/if}}
                    {{/each}}
                </span>
            </td>
        </tr>
        <tr>
            <td>
                <label for="inclusion_1">
                    <input type="checkbox" class="check-inclusion" id="inclusion_1" 
                    data-include="10" data-includetarget="#total_includes_1">
                    Include Something <small>(<span class="total-includes"></span>)</small>
                </label>
            </td>
            <td class="text-right">Rp. <span id="total_includes_1">0</span></td>
        </tr>
        <tr>
            <td colspan="2"><hr class="no-space"></td>
        </tr>
        <tr>
            <td colspan="2">
                <strong class="pull-left mr-small">TOTAL</strong>
                <div class="text-right text-red t--larger t-strong">
                    Rp. <span id="product_753" class="grand-total n-total">0</span>
                </div>
            </td>
        </tr>
    </tfoot>
</script>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
