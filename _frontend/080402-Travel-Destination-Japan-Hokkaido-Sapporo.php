<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destination</a></li>
                <li><a href="#">Asia</a></li>
                <li><a href="#">Japan</a></li>
                <li><a href="#">Hokkaido</a></li>
                <li><a href="#">Sapporo</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="text-center">
                <h1 class="sr-only">Panduan Wisata di Jepang</h1>
                <h2>SEMUA INFORMASI TEMPAT TERBAIK DI ASIA</h2>
                <p class="t--larger">
                    Jelajahi kota-kota terbaik di Asia, yang penuh dengan budaya, pemandangan alam dan sejarah yang mengesankan bersama H.I.S. Travel Indonesia.<br>
                    Temukan juga berbagai paket tour dari H.I.S. Travel Indonesia yang terdiri dari tempat wisata terkenal, makanan lokal yang lezat, serta transportasi yang nyaman.
                </p>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Destinasi Wisata di Sapporo
                </h2>
            </div>
            <div class="inset-on-m">
            <div class="bzg bzg--small-gutter">
                <div class="block-larger bzg_c" data-col="m4">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up no-space ellipsis-2"><a href="#" class="link-black">Former Hokkaido Government </a></h3>
                            Merupakan salah satu acara musim dingin terbesar di Jepang. Sekitar dua juta orang datang ke Sapporo untuk melihat salju dan es patung di sekitar Odori Park dan sepanjang jalan utama di Susukino. Selama tujuh hari di bulan Februari, Sapporo berubah menjadi Winter Dreamland penuh kristal es dan salju putih
                        </figcaption>
                    </figure>
                </div>
                <?php for ($i=1; $i <= 8; $i++) { ?>
                <div class="block-larger bzg_c" data-col="m4">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up no-space ellipsis-2"><a href="#" class="link-black">Former Hokkaido Government Office</a></h3>
                            Merupakan salah satu tempat wisata di Hokkaido yang terkenal. Odori Park juga merupakan pemisah kota antara kota Sapporo bagian utara dengan bagian selatan. Taman yang sangat asri dan indah ini merupakan tempat favorit untuk bersantai dan bermain
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
            </div>
            </div>
            <hr>
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Makanan Populer di Sapporo
                </h2>
            </div>
            <div class="inset-on-m">
            <div class="bzg bzg--small-gutter">
                <div class="block-larger bzg_c" data-col="m4">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up no-space ellipsis-2"><a href="#" class="link-black">Former Hokkaido Government </a></h3>
                            Merupakan salah satu acara musim dingin terbesar di Jepang. Sekitar dua juta orang datang ke Sapporo untuk melihat salju dan es patung di sekitar Odori Park dan sepanjang jalan utama di Susukino. Selama tujuh hari di bulan Februari, Sapporo berubah menjadi Winter Dreamland penuh kristal es dan salju putih
                        </figcaption>
                    </figure>
                </div>
                <?php for ($i=1; $i <= 2; $i++) { ?>
                <div class="block-larger bzg_c" data-col="m4">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up no-space ellipsis-2"><a href="#" class="link-black">Former Hokkaido Government Office</a></h3>
                            Merupakan salah satu tempat wisata di Hokkaido yang terkenal. Odori Park juga merupakan pemisah kota antara kota Sapporo bagian utara dengan bagian selatan. Taman yang sangat asri dan indah ini merupakan tempat favorit untuk bersantai dan bermain
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
            </div>
            </div>
            <hr>
            <div class="block bzg">
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jtd.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jrm.jpg" alt="">
                    </a>
                </div>
            </div>
            <hr>
        </div>
    </section>

    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
