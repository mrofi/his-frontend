<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pre-Order</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <figure class="">
            <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
        </figure>
        <div class="block text-center">
            <h1 class="h2 no-space">PRE-ORDER</h1>
            <h2 class="no-space text-up">Garuda Indonesia Travel Fair | H.I.S Travel Indonesia</h2>
            <h3>XX - XX September 2018</h3>
        </div>
        <div class="block block--inset-large">
            <h3 class="text-up">Data Pemesan</h3>
            <form action="" class="form baze-form"
            data-empty="Input tidak boleh kosong"
            data-email="Alamat email tidak benar"
            data-number="Input harus berupa angka"
            data-numbermin="Angka minimal "
            data-numbermax="Angka maksimal">
                <div class="form__row">
                    <label class="space-right t-strong" for="">Titel *</label>
                    <label for="title_mr" class="btn-label">
                        <input type="radio" id="title_mr" name="gender" checked data-dest="title_mr1">
                        <span class="btn-label__text">Mr.</span>
                    </label>
                    <label for="title_mrs" class="btn-label">
                        <input type="radio" id="title_mrs" name="gender" data-dest="title_mrs1">
                        <span class="btn-label__text">Mrs.</span>
                    </label>
                    <label for="title_ms" class="btn-label">
                        <input type="radio" id="title_ms" name="gender" data-dest="title_ms1">
                        <span class="btn-label__text">Ms.</span>
                    </label>
                </div>
                <div class="bzg">
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="oriFirstName" class="t-strong">Nama Depan *</label><br>
                        <input type="text" class="form-input form-input--block" id="oriFirstName" required>
                    </div>
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="oriLastName" class="t-strong">Nama Belakang *</label><br>
                        <input type="text" class="form-input form-input--block" id="oriLastName" required>
                    </div>
                </div>
                <div class="bzg">
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="" class="t-strong">Nomor Telepon *</label><br>
                        <div class="bzg bzg--small-gutter">
                            <div class="bzg_c" data-col="x4, l3">
                                <select name="" id="" class="form-input form-input--block selectstyle">
                                    <option value="+61">+61</option>
                                    <option value="+62" selected>+62</option>
                                </select>
                            </div>
                            <div class="bzg_c" data-col="x8, l9">
                                <input type="number" class="form-input form-input--block" required>
                            </div>
                        </div>
                    </div>
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="" class="t-strong">Email</label><br>
                        <input type="email" class="form-input form-input--block" required>
                    </div>
                </div>
                <div class="form__row">
                    <label for="" class="t-strong">Domisili</label><br>
                    <select name="" id="" class="form-input form-input--block selectstyle">
                        <option value="Jakarta">Jakarta</option>
                        <option value="Depok">Depok</option>
                    </select>
                </div>
                <div class="block--half bzg pickdate-range">
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="oriFirstName" class="t-strong">Kota Keberangkatan *</label><br>
                        <select name="" id="" class="form-input form-input--block selectstyle">
                            <option value="JKT">Jakarta</option><option value="SUB">Surabaya</option><option value="BDO">Bandung</option><option value="UPG">Makassar</option><option value="DPS">Denpasar Bali</option>
                        </select>
                    </div>
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="oriLastName" class="t-strong">Tanggal Keberangkatan *</label><br>
                        <div class="input-iconic">
                            <input type="text" class="form-input form-input--block pikaRange pikaRange--start" id="flight_start" placeholder="Tanggal Berangkat">
                            <label for="flight_start" class="label-icon">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="oriFirstName" class="t-strong">Kota Tujuan *</label><br>
                        <select name="" id="" class="form-input form-input--block selectstyle">
                            <option value="TYO">TOKYO</option><option value="NRT">Tokyo, TOKYO NARITA (NRT)</option><option value="HND">Tokyo, TOKYO HANEDA (HND)</option><option value="OSA">OSAKA</option><option value="KIX">Osaka, OSAKA KANSAI INTERNATIONAL (KIX)</option><option value="NGO">NAGOYA</option><option value="FUK">Fukuoka</option><option value="SPK">SAPPORO CHITOSE</option><option value="AKJ">ASAHIKAWA</option><option value="HKD">HAKODATE</option><option value="OBO">OBIHIRO</option><option value="SDJ">Sendai</option><option value="AOJ">AOMORI</option><option value="MSJ">MISAWA</option><option value="AXT">AKITA</option><option value="ONJ">ODATE NOSHIRO</option><option value="HNA">HANAMAKI</option><option value="GAJ">YAMAGATA JUNMACHI</option><option value="SYO">SHONAI</option><option value="FKS">FUKUSHIMA</option><option value="IBR">IBARAKI</option><option value="KIJ">NIIGATA</option><option value="KMQ">KOMATSU</option><option value="TOY">TOYAMA</option><option value="QSZ">SHIZUOKA</option><option value="TAK">TAKAMATSU</option><option value="KCZ">KOCHI</option><option value="MYJ">MATSUYAMA</option><option value="TKS">TOKUSHIMA</option><option value="HIJ">Hiroshima</option><option value="OKJ">OKAYAMA</option><option value="UBJ">UBE</option><option value="YGJ">YONAGO MIHO</option><option value="IZO">IZUMO</option><option value="TTJ">TOTTORI</option><option value="IWJ">IWAMI</option><option value="KKJ">KITA KYUSHU KOKURA</option><option value="HSG">SAGA</option><option value="NGS">NAGASAKI</option><option value="OIT">OITA</option><option value="KMJ">KUMAMOTO</option><option value="KMI">MIYAZAKI</option><option value="KOJ">Kagoshima</option><option value="OKA">Okinawa</option>
                        </select>
                    </div>
                    <div class="form__row bzg_c" data-col="m6">
                        <label for="oriLastName" class="t-strong">Tanggal Kepulangan *</label><br>
                        <div class="input-iconic" id="trip_type-home">
                            <input type="text" class="form-input form-input--block pikaRange pikaRange--end" id="flight_end" placeholder="Tanggal Kembali">
                            <label for="flight_end" class="label-icon">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <h4 class="no-space text-up">Jumlah Penumpang *</h4>
                <div class="block--half bzg">
                    <div class="form__row bzg_c add-person__item" data-col="x4">
                        <label for="" class="t-strong">Dewasa</label><br>
                        <select name="" id="" class="form-input form-input--block selectstyle add-person" data-person="adult">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="form__row bzg_c add-person__item" data-col="x4">
                        <label for="" class="t-strong">Anak</label><br>
                        <select name="" id="" class="form-input form-input--block selectstyle add-person" data-person="child">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="form__row bzg_c add-person__item" data-col="x4">
                        <label for="" class="t-strong">Bayi</label><br>
                        <select name="" id="" class="form-input form-input--block selectstyle add-person" data-person="infant">
                            <option value="0">0</option>
                            <option value="1" disabled>1</option>
                            <option value="2" disabled>2</option>
                            <option value="3" disabled>3</option>
                            <option value="4" disabled>4</option>
                            <option value="5" disabled>5</option>
                            <option value="6" disabled>6</option>
                            <option value="7" disabled>7</option>
                            <option value="8" disabled>8</option>
                            <option value="9" disabled>9</option>
                            <option value="10" disabled>10</option>
                        </select>
                    </div>
                </div>

                <h4 class="no-space block-half text-up">Identitas Penumpang *</h4>
                <div class="person-container" id="adult">
                    <h5 class="no-space text-up">Dewasa</h5>
                    <hr class="no-space">
                    <div class="items">
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m5, l4">
                                <label class="space-right t-strong" for="">Titel *</label><br>
                                <label for="title_mr_adult_0" class="btn-label">
                                    <input type="radio" id="title_mr_adult_0" title="Mr" value="Mr" name="gender_adult[0]" checked >
                                    <span class="btn-label__text">Mr.</span>
                                </label>
                                <label for="title_mrs_adult_0" class="btn-label">
                                    <input type="radio" id="title_mrs_adult_0" title="Mrs" value="Mrs" name="gender_adult[0]">
                                    <span class="btn-label__text">Mrs.</span>
                                </label>
                                <label for="title_ms_adult_0" class="btn-label">
                                    <input type="radio" id="title_ms_adult_0" title="Ms" value="Ms" name="gender_adult[0]">
                                    <span class="btn-label__text">Ms.</span>
                                </label>
                            </div>
                            <div class="form__row bzg_c" data-col="m7, l8">
                                <label for="" class="t-strong">Nama Lengkap *</label><br>
                                <input type="text" class="form-input form-input--block" id="" name="name_adult[0]" required placeholder="Nama Lengkap Sesuai Passport">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="person-container" id="child" style="display: none;">
                    <h5 class="no-space text-up">Anak</h5>
                    <hr class="no-space">
                    <div class="items">
                        
                    </div>
                </div>
                <div class="person-container" id="infant" style="display: none;">
                    <h5 class="no-space text-up">Bayi</h5>
                    <hr class="no-space">
                    <div class="items">
                        
                    </div>
                </div>
                <div class="block-larger">
                    <label for="" class="t-strong">Note</label><br>
                    <textarea name="" id="" rows="9" class="form-input form-input--block"></textarea>
                </div>
                <div class="text-center">
                    <button class="btn btn--round btn--red text-up" type="submit">
                        Pre-Order Now
                    </button>
                </div>
            </form>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
