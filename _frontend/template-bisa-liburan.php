<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Baze is a front-end starter template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#034f98">

    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:image" content="">

    <link rel="apple-touch-icon" href="<?= isset($path) ? $path : '' ?>assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= isset($path) ? $path : '' ?>assets/img/favicon.png">

    <title>H.I.S Tour &amp; Travel</title>

    
    <link rel="stylesheet" href="assets/css/imgGen.css" media="all">
    
</head>
<body>


    <div class="img-gen-container">
        <div class="img-gen">
            <img src="assets/img/dummy-1-kosong.jpg" class="img-gen__bg" alt="">
            <div class="img-gen__content">
                <header class="img-gen__header">
                    <h1 class="title">Liburan Kantor 2020 Bebas Corona</h1>
                    <div class="author">by <span>Rofi Manurung</span></div>
                </header>
                <main class="img-gen__main">
                    <ol class="itinerary-list">
                        <?php for ($i=1; $i <= 5; $i++) { ?>
                        <li id="itineraryDay0<?= $i ?>" class="itinerary-day">
                            <img src="//via.placeholder.com/140?text=140x140_<?= $i ?>" alt="" class="itinerary-img">
                            <table class="itinerary-detail">
                                <tbody>
                                    <tr>
                                        <td class="label-time">09.00</td>
                                        <td>Tiba di Jepang</td>
                                    </tr>
                                    <tr>
                                        <td class="label-time">09.00</td>
                                        <td>Menuju Danau Hakone Sopo</td>
                                    </tr>
                                    <tr>
                                        <td class="label-time">09.00</td>
                                        <td>Berbelanja di Gotemba premium outlet</td>
                                    </tr>
                                    <tr>
                                        <td class="label-time">09.00</td>
                                        <td>Oishi Park, foto bersama di sekitar Oishi Park</td>
                                    </tr>
                                    <tr>
                                        <td class="label-time">19.00</td>
                                        <td>Makan Malam di Angkringan Jepang</td>
                                    </tr>
                                </tbody>
                            </table>
                        </li>
                        <?php } ?>
                    </ol>
                </main>
            </div>
        </div>
    </div>


</body>
</html>
