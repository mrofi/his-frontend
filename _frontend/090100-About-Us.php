<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About Us</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="sr-only">About Us</h1>
        <section class="section-block">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
            </figure>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h3 class="no-space text-up in-block">
                    <span class="title-text text-up t--larger">Press Release</span>
                </h3>
                <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Lainnya</b>
                </a>
            </div>
            <div class="post inset-on-m">
                <div class="block post__item bzg">
                    <div class="bzg_c block--small" data-col="m3">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m9">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger">Cool Japan Travel Fair Kembali Hadir di Jakarta</a>
                        </h3>
                        <div class="block t--indent t--smaller">
                            <span class="text-blue">By:</span> <a href="#" class="link-black">H.I.S Travel Indonesia</a><br>
                            <em>Posted on September 7, 2017 at 12:00 PM</em>
                        </div>
                        Jakarta, 7 September 2017 – Minat serta permintaan masyarakat Indonesia terhadap tur individual semakin 
                        meningkat seiring dengan meningkatnya jumlah perjalanan...
                    </div>
                </div>
                <div class="block post__item bzg">
                    <div class="bzg_c block--small" data-col="m3">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m9">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger">Cool Japan Travel Fair Kembali Hadir di Jakarta</a>
                        </h3>
                        <div class="block t--indent t--smaller">
                            <span class="text-blue">By:</span> <a href="#" class="link-black">H.I.S Travel Indonesia</a><br>
                            <em>Posted on September 7, 2017 at 12:00 PM</em>
                        </div>
                        Jakarta, 7 September 2017 – Minat serta permintaan masyarakat Indonesia terhadap tur individual semakin 
                        meningkat seiring dengan meningkatnya jumlah perjalanan...
                    </div>
                </div>
                <div class="block post__item bzg">
                    <div class="bzg_c block--small" data-col="m3">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m9">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger">Cool Japan Travel Fair Kembali Hadir di Jakarta</a>
                        </h3>
                        <div class="block t--indent t--smaller">
                            <span class="text-blue">By:</span> <a href="#" class="link-black">H.I.S Travel Indonesia</a><br>
                            <em>Posted on September 7, 2017 at 12:00 PM</em>
                        </div>
                        Jakarta, 7 September 2017 – Minat serta permintaan masyarakat Indonesia terhadap tur individual semakin 
                        meningkat seiring dengan meningkatnya jumlah perjalanan...
                    </div>
                </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <h3 class="">
                <span class="title-text text-up t--larger">Kelebihan HIS</span>
            </h3>
            <p>
                MICE H.I.S. Travel Indonesia memberikan pelayanan & mengatur penyelenggaraan program insentif baik perorangan, rombongan, maupun perusahaan Anda. Jadikan kami sebagai mitra Anda dalam perencanaan perjalanan yang Inovatif & informatif dengan berorientasi kepada kepuasan & kepercayaan Anda
            </p>
            <p></p>
            <div class="inset-on-m">
            <div class="block--half bzg cards cards--nostacked">
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="fill-white">
                            <div class="item-img img-4-2 img-4-2--center">
                                <img src="assets/img/call-support-icon.png" alt="" class="">
                            </div>
                            <figcaption class="t--larger block--inset-small"><h4 class="no-space ellipsis-2">Kenyamanan Servis</h4></figcaption>
                        </figure>
                        <div class="item-text">
                            Kami bertanggung jawab untuk mendukung kebutuhan perjalanan Anda mulai pada saat reservasi hingga kembali ke rumah! Silakan bertanya dan konsultasi, staff kami akan melayani Anda.
                        </div>
                    </div>
                </div>
                <!-- items -->
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="fill-white">
                            <div class="item-img img-4-2 img-4-2--center">
                                <img src="assets/img/travel-globe-icon2.png" alt="" class="">
                            </div>
                            <figcaption class="t--larger block--inset-small"><h4 class="no-space ellipsis-2">Global Network Mendukung Keamanan Anda</h4></figcaption>
                        </figure>
                        <div class="item-text">
                            H.I.S. memiliki keuntungan dengan jaringan global yang mendukung perjalanan Anda. Dengan sistem terbaik melayani mulai dari reservasi hingga Anda kembali!
                        </div>
                    </div>
                </div>
                <!-- items -->
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="fill-white">
                            <div class="item-img img-4-2 img-4-2--center">
                                <img src="assets/img/best-price-icon.png" alt="" class="">
                            </div>
                            <figcaption class="t--larger block--inset-small"><h4 class="no-space ellipsis-2">Harga Terbaik Langsung Dari Cabang Luar Negeri</h4></figcaption>
                        </figure>
                        <div class="item-text">
                            Cabang H.I.S. di luar negeri menawarkan harga terbaik untuk hotel dimanapun dan kapanpun Anda butuhkan.
                        </div>
                    </div>
                </div>
                <!-- items -->
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="fill-white">
                            <div class="item-img img-4-2 img-4-2--center">
                                <img src="assets/img/individual-tour-icon.png" alt="" class="">
                            </div>
                            <figcaption class="t--larger block--inset-small"><h4 class="no-space ellipsis-2">Unggul Dalam Individual Tour</h4></figcaption>
                        </figure>
                        <div class="item-text">
                            Kami menyediakan berbagai macam tour dan menangani tour sesuai dengan keinginan Anda. Semua kebutuhan untuk perjalanan dapat Anda serahkan kepada kami!
                        </div>
                    </div>
                </div>
                <!-- items -->
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="fill-white">
                            <div class="item-img img-4-2 img-4-2--center">
                                <img src="assets/img/japan-icon-5.png" alt="" class="">
                            </div>
                            <figcaption class="t--larger block--inset-small"><h4 class="no-space ellipsis-2">Unggul untuk Perjalanan ke Jepang</h4></figcaption>
                        </figure>
                        <div class="item-text">
                            H.I.S. memiliki 279 cabang di Jepang, Anda dapat reservasi perjalanan ke Jepang di kantor cabang lain di luar negeri. Mulai dari cara penggunaan JR PASS hingga penerbangan domestik, serahkan perjalanan ke Jepang Anda pada H.I.S.! 
                        </div>
                    </div>
                </div>
                <!-- items -->
            </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <h3 class="">
                <span class="title-text text-up t--larger">Produk HIS</span>
            </h3>
            <div class="inset-on-m">
            <div class="bzg bzg--big-gutter">
                <div class="block bzg_c" data-col="m4">
                    <a href="#">
                    <figure class="responsive-media square fig-tag fig-tag--blue fig-tag--bold fig-tag--full">
                        <img data-src="//placehold.it/420x420" alt="" class="item-heavy">
                        <figcaption class="text-up">Flight</figcaption>
                    </figure>
                    </a>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <a href="#">
                    <figure class="responsive-media square fig-tag fig-tag--blue fig-tag--bold fig-tag--full">
                        <img data-src="//placehold.it/420x420" alt="" class="item-heavy">
                        <figcaption class="text-up">Individual Tour</figcaption>
                    </figure>
                </a>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <a href="#">
                    <figure class="responsive-media square fig-tag fig-tag--blue fig-tag--bold fig-tag--full">
                        <img data-src="//placehold.it/420x420" alt="" class="item-heavy">
                        <figcaption class="text-up">Group Tour</figcaption>
                    </figure>
                </a>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <a href="#">
                    <figure class="responsive-media square fig-tag fig-tag--blue fig-tag--bold fig-tag--full">
                        <img data-src="//placehold.it/420x420" alt="" class="item-heavy">
                        <figcaption class="text-up">JR Pass</figcaption>
                    </figure>
                </a>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <a href="#">
                    <figure class="responsive-media square fig-tag fig-tag--blue fig-tag--bold fig-tag--full">
                        <img data-src="//placehold.it/420x420" alt="" class="item-heavy">
                        <figcaption class="text-up">Corporate</figcaption>
                    </figure></a>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <a href="#">
                    <figure class="responsive-media square fig-tag fig-tag--blue fig-tag--bold fig-tag--full">
                        <img data-src="//placehold.it/420x420" alt="" class="item-heavy">
                        <figcaption class="text-up">Meeting &amp; Event</figcaption>
                    </figure></a>
                </div>
            </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <h3 class="">
                <span class="title-text text-up t--larger">Cabang HIS</span>
            </h3>
            <div class="branch-map bzg bzg--no-gutter map-container" data-marker="assets/img/marker.png" data-map="dev/network.json">
                <div class="bzg_c" data-col="m4">
                    <nav class="map-nav">
                        <div class="map-search">
                            <div class="form__row">
                                <select name="" id="" class="form-input form-input--block map-search-filter">
                                    <option value="all">Semua Kota</option>
                                </select>
                            </div>
                        </div>

                        <ul class="list-nostyle map-search-result">
                        </ul>

                        <template id="searchList">
                            {{#each this}}
                            <option value="{{area}}">{{area}}</option>
                            {{/each}}
                        </template>

                        <div class="map-location-list">
                            <div class="list-nostyle map-nav-container is-active"></div>
                            <div class="location-details"></div>
                        </div>
                        
                        <template id="mapNavTmpl">
                            {{#each this}}
                                {{#each locations}}
                                    <div class="nav__item">
                                        <a href="#" class="t-black line--small btn-trigger" aria-label="loaction" data-target="#template-{{id}}" data-lat="{{lat}}" data-lng="{{lng}}">
                                            <h4 class="no-space">{{location}}</h4>
                                            <span class="t--smaller in-block block--half">{{address}}</span>
                                            <span class="cf op-5">
                                                <span class="in-block">
                                                    <span class="fa fa-phone"></span> {{#each telephone}}{{this}}{{/each}}
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                {{/each}}
                            {{/each}}
                        </template>
                    </nav>
                </div>
                <div class="bzg_c" data-col="m8">
                    <div class="map-area" id="his_map"></div>
                </div>
            </div>
        </section>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
