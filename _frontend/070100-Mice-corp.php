<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Mice &amp; Corp</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="sr-only">Mice &amp; Corp</h1>
        <section class="section-block">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
            </figure>
        </section>
        <hr>
        <section class="section-block">
            <h3 class="">
                <span class="title-text text-up t--larger">Kelebihan HIS</span>
            </h3>
            <div class="bzg">
                <div class="bzg_c" data-col="m6">
                    <figure class="responsive-media media-3-2">
                        <img src="" data-src="//placehold.it/600x300" alt="" class="item-heavy">
                    </figure>
                </div>
                <div class="block bzg_c" data-col="m6">
                    <p>
                        <strong>1. Harga Terbaik</strong><br>
                        Kami memberikan penawaran dengan harga terbaik
                    </p>
                    <p>
                        <strong>2. Melayani last minute <em>booking</em></strong><br>
                        Dapat mengatur perjalanan Anda satu hari sebelum keberangkatan atau pada hari keberangkatan (ada pengecualian untuk beberapa maskapai)
                    </p>
                    <p>
                        <strong>3. <em>Worldwide network</em></strong><br>
                        Kenyamanan dan keamanan bagi Anda baik di Jepang maupun di negara lainnya. Didukung 105 kantor tesebar di luar Jepang dan total lebih dari 300 kantor cabang. Dengan kemampuan know-how, kami merealisasikan kepercayaan perjalanan bisnis Anda ditambah dengan memberikan harga terbaik. Banyak keuntungan yang akan Anda dapatkan jika perusahaan Anda menjadi bagian corporate kami, salah satunya tiket pesawat termurah yang menjadi keunggulan H.I.S.
                    </p>
                </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <h3 class="">
                <span class="title-text text-up t--larger">Kelebihan HIS</span>
            </h3>
            <p>
                MICE H.I.S. Travel Indonesia memberikan pelayanan & mengatur penyelenggaraan program insentif baik perorangan, rombongan, maupun perusahaan Anda. Jadikan kami sebagai mitra Anda dalam perencanaan perjalanan yang Inovatif & informatif dengan berorientasi kepada kepuasan & kepercayaan Anda
            </p>
            <p></p>
            <div class="block--half bzg cards cards--nostacked">
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="item-img img-3-2 img-3-2--center fill-white">
                            <img src="assets/img/icon-meeting.png" alt="" class="">
                        </figure>
                        <div class="item-text">
                            Konsultasi mengenai kunjungan, loka karya, study tour, perjalanan insentif
                        </div>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="item-img img-3-2 img-3-2--center fill-white">
                            <img src="assets/img/icon-planning.png" alt="" class="">
                        </figure>
                        <div class="item-text">
                            Perencanaan perjalanan karyawan, mengkoordinasikan perjalanan bisnis
                        </div>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item border text-center">
                        <figure class="item-img img-3-2 img-3-2--center fill-white">
                            <img src="assets/img/icon-meeting-2.png" alt="" class="">
                        </figure>
                        <div class="item-text">
                            MICE (Meeting, Incentive, Convention, Exhibition)
                        </div>
                    </div>
                </div>
            </div>
            <div data-toggle="slide">
                <div class="block text-center">
                    <a href="#" class="btn btn--round btn--ghost-red btn-toggle">
                        <strong class="text-up">Hubungi Kami</strong>
                    </a>
                </div>
                <div class="toggle-panel">
                    <div class="block--inset fill-blue">
                        <div class="bzg">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="form-label">Nama *</label>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="form-label">Email *</label>
                                <input type="email" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="form-label">Nama Perusahaan *</label>
                                <input type="text" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for="" class="form-label">Nomor Telepon *</label>
                                <input type="number" class="form-input form-input--block" required>
                            </div>
                            <div class="form__row block bzg_c">
                                <label for="" class="form-label">Pesan Anda *</label>
                                <textarea name="" id="" rows="8" class="form-input form-input--block" required></textarea>
                            </div>
                            <div class="bzg_c">
                                <button class="btn btn--round btn--red">
                                    <strong class="text-up">Kirim</strong>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <h3 class="">
                <span class="title-text text-up t--larger">Layanan Corporate</span>
            </h3>
            <p>
                Program kami telah secara khusus dirancang untuk meningkatkan hubungan kerja sama dengan pelanggan Corporate kami, serta Kami menawarkan penghematan biaya dari anggaran perjalanan Anda dan membantu pengaturan perjalanan Anda. Program Corporate ini tersedia untuk perusahaan-perusahan yang melakukan perjalanan dinas secara rutin atau teratur. 
            </p>
            <p>
                Untuk informasi lebih lanjut tentang H.I.S. Travel Indonesia Corporate Partner Program atau untuk membahas bagaimana kita dapat mengembangkan strategi untuk menggabungkan kebutuhan perjalanan bisnis Anda.
            </p>
            <p></p>
            <div class="block--half bzg cards cards--nostacked">
                <div class="block bzg_c" data-col="m3">
                    <div class="card__item border text-center">
                        <figure class="item-img img-3-2 img-3-2--center fill-white">
                            <img src="assets/img/icon-ticket.png" alt="" class="">
                        </figure>
                        <div class="item-text">
                            Konsultasi mengenai kunjungan, loka karya, study tour, perjalanan insentif
                        </div>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m3">
                    <div class="card__item border text-center">
                        <figure class="item-img img-3-2 img-3-2--center fill-white">
                            <img src="assets/img/icon-transportation.png" alt="" class="">
                        </figure>
                        <div class="item-text">
                            Perencanaan perjalanan karyawan, mengkoordinasikan perjalanan bisnis
                        </div>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m3">
                    <div class="card__item border text-center">
                        <figure class="item-img img-3-2 img-3-2--center fill-white">
                            <img src="assets/img/icon-best-price.png" alt="" class="">
                        </figure>
                        <div class="item-text">
                            MICE (Meeting, Incentive, Convention, Exhibition)
                        </div>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m3">
                    <div class="card__item border text-center">
                        <figure class="item-img img-3-2 img-3-2--center fill-white">
                            <img src="assets/img/icon-flight.png" alt="" class="">
                        </figure>
                        <div class="item-text">
                            MICE (Meeting, Incentive, Convention, Exhibition)
                        </div>
                    </div>
                </div>
            </div>
            <p>
                Berkat kepercayaan dan dukungan dari Anda, saat ini divisi Corporate H.I.S. Jepang telah menjalin kerja sama dengan lebih dari sepuluh ribu perusahaan. Terlebih lagi, divisi inbound di kantor cabang Jakarta ingin memberikan tingkat pelayanan yang sama terkait pelayanan perencanaan hotel, penjemputan, guide, dan perencanaan lainnya dengan harga terbaik.
            </p>
            <div class="text-center">
                <a href="#" class="btn btn--round btn--ghost-red">
                    <strong class="text-up">Hubungi Kami</strong>
                </a>
            </div>
        </section>
        <hr>
        <section class="section-block">
            <h3 class="">
                <span class="title-text text-up t--larger">Cabang HIS</span>
            </h3>
            <div class="branch-map bzg bzg--no-gutter map-container" data-marker="assets/img/marker.png" data-map="dev/network.json">
                <div class="bzg_c" data-col="m4">
                    <nav class="map-nav">
                        <div class="map-search">
                            <div class="form__row">
                                <select name="" id="" class="form-input form-input--block map-search-filter">
                                    <option value="all">Semua Kota</option>
                                </select>
                            </div>
                        </div>

                        <ul class="list-nostyle map-search-result">
                        </ul>

                        <template id="searchList">
                            {{#each this}}
                            <option value="{{area}}">{{area}}</option>
                            {{/each}}
                        </template>

                        <div class="map-location-list">
                            <div class="list-nostyle map-nav-container is-active"></div>
                            <div class="location-details"></div>
                        </div>
                        
                        <template id="mapNavTmpl">
                            {{#each this}}
                                {{#each locations}}
                                    <div class="nav__item">
                                        <a href="#" class="t-black line--small btn-trigger" aria-label="loaction" data-target="#template-{{id}}" data-lat="{{lat}}" data-lng="{{lng}}">
                                            <h4 class="no-space">{{location}}</h4>
                                            <span class="t--smaller in-block block--half">{{address}}</span>
                                            <span class="cf op-5">
                                                <span class="in-block">
                                                    <span class="fa fa-phone"></span> {{#each telephone}}{{this}}{{/each}}
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                {{/each}}
                            {{/each}}
                        </template>
                    </nav>
                </div>
                <div class="bzg_c" data-col="m8">
                    <div class="map-area" id="his_map"></div>
                </div>
            </div>
        </section>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
