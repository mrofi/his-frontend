<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pemesanan</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <ol class="block list-nostyle text-center text-up steps">
            <li class="steps__item">
                <span>Rincian Anda</span>
            </li>
            <li class="steps__item current">
                <span>Pembayaran</span>
            </li>
            <li class="steps__item">
                <span>Selesai</span>
            </li>
        </ol>

        <div class="block bzg">
            <div class="bzg_c" data-col="m8">
                <h2>Ringkasan</h2>
                <div class="border p-16">
                    <h3 class="text-up">Informasi Pemesan</h3>
                    <ul class="mb-2 list-nostyle list-iconic t--larger">
                        <li>
                            <span class="item-icon fa fa-user"></span>
                            <div class="item-text">Mrs. AULIA PUSPA</div>
                        </li>
                        <li>
                            <span class="item-icon fa fa-envelope-open-o"></span>
                            <div class="item-text">
                                <a href="mailto:my.email@gmail.com" class="link-black">my.email@gmail.com</a>
                            </div>
                        </li>
                        <li>
                            <span class="item-icon fa fa-phone"></span>
                            <div class="item-text">
                                <a href="tel:+62-8123456789" class="link-black">+62-8123456789</a>
                            </div>
                        </li>
                    </ul>

                    <h3 class="text-up">Tamu</h3>
                    <div class="bzg">
                        <div class="bzg_c mb" data-col="m6">
                            <h4 class="mb-small">Kamar 1</h4>
                            <table class="no-space table--plain">
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>Mrs. Aulia Puspa</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>27 Jan 1990</td>
                                    </tr>
                                    <tr>
                                        <td>No. Paspor</td>
                                        <td>:</td>
                                        <td>A123546</td>
                                    </tr>
                                    <tr>
                                        <td>Berlaku Paspor</td>
                                        <td>:</td>
                                        <td>31 Des 2022</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>27 Jan 1990</td>
                                    </tr>
                                    <tr>
                                        <td>No. Paspor</td>
                                        <td>:</td>
                                        <td>A123546</td>
                                    </tr>
                                    <tr>
                                        <td>Berlaku Paspor</td>
                                        <td>:</td>
                                        <td>31 Des 2022</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="bzg_c mb" data-col="m6">
                            <h4 class="mb-small">Kamar 2</h4>
                            <table class="no-space table--plain">
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>Mrs. Aulia Puspa</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>27 Jan 1990</td>
                                    </tr>
                                    <tr>
                                        <td>No. Paspor</td>
                                        <td>:</td>
                                        <td>A123546</td>
                                    </tr>
                                    <tr>
                                        <td>Berlaku Paspor</td>
                                        <td>:</td>
                                        <td>31 Des 2022</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="border p-16">
                    <h2 class="block--small text-up">Rincian Biaya</h2>
                    <div class="bzg">
                        <div class="bzg_c" data-col="m9">
                            <table class="no-space table-total">
                                <tbody>
                                    <tr>
                                        <td>Dewasa x 2</td>
                                        <td class="text-right">Rp. 3.528.200</td>
                                    </tr>
                                    <tr>
                                        <td>VAT 1%</td>
                                        <td class="text-right">Rp. 35.282</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td class="text-right text-red t--larger t-strong">
                                            Rp. 3.657.000
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- left -->
            <div class="bzg_c" data-col="m4" data-sticky-container>
                <div class="sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="70">
                    <h3 class="text-center text-up">Rincian Pesanan</h3>
                    <div class="mb">
                        <span>Kode Pesanan</span><br>
                        <div class="text-blue">XXXXXX XXXX XXXX</div>
                    </div>
                    <table class="mb table--border fill-lightblue text-center">
                        <tbody class="text-blue">
                            <tr>
                                <td colspan="2" class="text-up p-small">Mandarin Oriental</td>
                            </tr>
                            <tr>
                                <td class="p-small">
                                    Kamar
                                    <span class="text--larger">1 Kamar</span>
                                </td>
                                <td class="p-small">
                                    Durasi
                                    <span class="text--larger">12 Malam</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right p-small">Harga Kamar</td>
                                <td class="text-left p-small">
                                    <span class="text--larger">IDR 29.060.000</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="mb fill-lightgrey">
                        <h3 class="no-space bg-grey p-smaller text-blue">
                            <span class="fa fa-bed"></span>
                            HOTEL
                        </h3>
                        <div class="p-smaller text-blue">
                            <div class="mb-half text-center">
                                <img src="https://via.placeholder.com/200x150" alt=""><br>
                                <span class="t-yellow t--larger">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </span>
                            </div>
                            <h4 class="no-space">Mandarin Oriental Hotel</h4>
                            <small>
                                Jl. Pejaten Barat II No. 3A Jakarta Selatan 12510 Indonesia
                            </small>
                        </div>
                        <hr class="no-space">
                        <div class="p-smaller text-blue">
                            <table class="table--plain cell--top no-space">
                                <tbody class="text-blue">
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Tipe Kamar</strong></td>
                                        <td>Deluxe Room, no balcony, Single Bed</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Kamar</strong></td>
                                        <td>1 Kamar</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Tamu</strong></td>
                                        <td>Dewasa 1</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Kamar</strong></td>
                                        <td>1 Kamar</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Tamu</strong></td>
                                        <td>Dewasa 1</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Kamar</strong></td>
                                        <td>1 Kamar</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap pr-small"><strong>Jumlah Tamu</strong></td>
                                        <td>Dewasa 1</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- bzg -->
        <!--
        <h3 class="h2 mb-small text-up">Pembayaran</h3>
        <h4 class="fill-blue-yellow text-up"><span class="in-block p-small">Pilih Metode Pembayaran</span></h4>
        <div class="block">
            <nav class="tab-form text-center text-up">
                <label for="credit_card" class="btn-label">
                    <input type="radio" name="payment_method" id="credit_card">
                    <span class="btn-text">Credit Card</span>
                    <span class="btn-bg"></span>
                </label>
                <label for="bank_transfer" class="btn-label">
                    <input type="radio" name="payment_method" id="bank_transfer" checked="checked">
                    <span class="btn-text">Bank Transfer</span>
                    <span class="btn-bg"></span>
                </label>
                <label for="store_payment" class="btn-label">
                    <input type="radio" name="payment_method" id="store_payment">
                    <span class="btn-text">Store Payment</span>
                    <span class="btn-bg"></span>
                </label>
            </nav>
            <div class="tab-form-panel" id="panel_credit_card">

            </div>
            <div class="tab-form-panel" id="panel_bank_transfer">
                <h4 class="no-space fill-blue-yellow text-up">
                    <span class="in-block p-small">Anda memilih bank transfer sebagai metode pembayaran</span>
                </h4>
                <div class="p-smaller fill-lightgrey">
                    <span class="in-block mb-small">Bank Transfer (ATM/Internet Banking)</span>
                    <div class="fill-white p-smaller">
                        <div class="bzg">
                            <div class="bzg_c" data-col="s6, m4, l2">
                                <label for="mandiri" class="btn-label-img">
                                    <input type="radio" id="mandiri" name="bank-transfer">
                                    <img src="https://via.placeholder.com/180x60?text=Mandiri" alt="">
                                </label>
                            </div>
                            <div class="bzg_c" data-col="s6, m4, l2">
                                <label for="bca" class="btn-label-img">
                                    <input type="radio" id="bca" name="bank-transfer">
                                    <img src="https://via.placeholder.com/180x80?text=BCA" alt="">
                                </label>
                            </div>
                            <div class="bzg_c" data-col="s6, m4, l2">
                                <label for="permata" class="btn-label-img">
                                    <input type="radio" id="permata" name="bank-transfer">
                                    <img src="https://via.placeholder.com/180x50?text=Permata" alt="">
                                </label>
                            </div>
                            <div class="bzg_c" data-col="s6, m4, l2">
                                <label for="atm_bersama" class="btn-label-img">
                                    <input type="radio" id="atm_bersama" name="bank-transfer">
                                    <img src="https://via.placeholder.com/180x90?text=ATM-Bersama" alt="">
                                </label>
                            </div>
                            <div class="bzg_c" data-col="s6, m4, l2">
                                <label for="atm_prima" class="btn-label-img">
                                    <input type="radio" id="atm_prima" name="bank-transfer">
                                    <img src="https://via.placeholder.com/180x80?text=Prima" alt="">
                                </label>
                            </div>
                            <div class="bzg_c" data-col="s6, m4, l2">
                                <label for="atm_alto" class="btn-label-img">
                                    <input type="radio" id="atm_alto" name="bank-transfer">
                                    <img src="https://via.placeholder.com/180x80?text=Alto" alt="">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-form-panel" id="panel_store_payment">

            </div>
        </div>
        -->
        <div class="block p-smaller border">
            Dengan klik tombol 'BAYAR', Anda telah menyetujui seluruh <a href="#">Syarat dan Ketentuan</a> yang telah ditetapkan. Serta <a href="#">Syarat dan Ketentuan Pemesanan Tour</a> yang berlaku di H.I.S Travel Indonesia.
        </div>
        <div class="block--double text-center">
            <a href="#" class="btn btn--round btn--red text-up">BAYAR</a>
            <!-- <button class="btn btn--round btn--red text-up">Selesai</button> -->
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
