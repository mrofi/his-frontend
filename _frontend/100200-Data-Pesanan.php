<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Profil Traveler</a></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <h1 class="block--half">Profil Traveler</h1>
        <div class="block block--inset-small border">
            <div class="bzg bzg--no-gutter">
                <div class="bzg_c" data-col="m4, l3">
                    <nav class="side-nav">
                        <ul class="list-nostyle">
                            <li class="side-nav__item is-active">
                                <a href="#">Data Pemesanan</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Profil Saya</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Ubah Password</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Keluar</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="bzg_c" data-col="m8, l9">
                    <div class="profil-main booking-history">
                        <h4 class="no-space title text-up">Daftar Pemesanan</h4>
                        <ul class="list-nostyle" data-category="Booking-History" data-list="dev/booking-history.json" data-tmpl="#booking-template">
                            
                        </ul>
                        <div class="loader">
                            <div class="bounce1"></div>
                            <div class="bounce2"></div>
                            <div class="bounce3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<template id="booking-template">
    {{#each this}}
    {{#each data}}
    <li class="" data-toggle>
        <div class="bzg bzg--inset">
            <div class="bzg_c" data-col="x6, l2">
                <div class="top-label">No. Transaksi</div>
                <div class="content line--small break-all">{{id}}</div>
            </div>
            <div class="bzg_c" data-col="x6, l3">
                <div class="top-label">Time Limit</div>
                <div class="content">{{formatDate date day="numeric" month="long" year="numeric"}} {{formatTime date hour="numeric" minute="numeric" hour12=false}}</div>
            </div>
            <div class="bzg_c" data-col="x6, l2">
                <div class="top-label">Total Harga</div>
                <div class="content">IDR {{formatNumber price num}}</div>
            </div>
            <div class="bzg_c" data-col="x6, l2">
                <div class="top-label">Pembayaran</div>
                <div class="content">IDR {{formatNumber paid num}}</div>
            </div>
            <div class="bzg_c" data-col="x10, l3">
                <div class="top-label">Action</div>
                <div class="content col-action">
                    {{#each action}}
                    {{#if created}}
                    <div class="label-status label-status--red">
                        Pembayaran
                    </div>
                    {{else if paid}}
                    <div class="label-status label-status--green">
                        Terbayar
                    </div>
                    {{else if issued}}
                    <div class="label-status label-status--blue">
                        Issued
                    </div>
                    {{else if canceled}}
                    <div class="label-status">
                        Dibatalkan
                    </div>
                    {{else if rejected}}
                    <div class="label-status label-status--red">
                        Ditolak
                    </div>
                    {{/if}}
                    {{/each}}
                    <span class="btn-toggle btn-action fa fa-caret-down"></span>
                </div>
            </div>
        </div>
        {{#each details}}
        <div class="toggle-panel booking-details">
            <div class="booking-details__item">
                <div class="label">Booking Number</div>
                <div class="input">{{booking_number}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Type</div>
                <div class="input">{{type}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Pergi</div>
                <div class="input">{{depart}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Kembali</div>
                <div class="input">{{return}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Kelas Penerbangan</div>
                <div class="input">{{class}}</div>
            </div>
        </div>
        {{/each}}
    </li>
    {{/each}}
    {{/each}}
</template>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
