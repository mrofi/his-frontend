<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Press Release</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <section class="section-block">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
            </figure>
        </section>
        <hr>
        <section class="section-block">
            <div class="block section-head clearfix">
                <h1 class="no-space text-up in-block">
                    <span class="title-text text-up t--larger">Press Release</span>
                </h1>
            </div>
            <div class="block post inset-on-m">
                <div class="block post__item bzg">
                    <div class="bzg_c block--half" data-col="m4">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m8">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger">Cool Japan Travel Fair Kembali Hadir di Jakarta</a>
                        </h3>
                        <div class="block t--indent t--smaller">
                            <span class="text-blue">By:</span> <a href="#" class="link-black">H.I.S Travel Indonesia</a><br>
                            <em>Posted on September 7, 2017 at 12:00 PM</em>
                        </div>
                        <div class="block">
                        <strong>Jakarta, 23 Agustus 2017</strong> - Cool Japan Travel Fair merupakan acara travel fair tunggal yang diselenggarakan oleh H.I.S. Travel Indonesia. Event yang pertama kali diadakan pada 16 – 18 September 2016 ini berhasil mendapat sambutan positif dari masyarakat Jakarta dan sekitarnya. Hal ini terbukti dengan suksesnya acara ini mendatangkan tidak kurang dari 195.000 pengunjung selama 3 hari acara berlangsung.
                        </div>
                        <div class="block text-right">
                            <a href="#" class="btn btn--round btn--red text-up">Selengkapnya</a>
                        </div>
                    </div>
                    <hr class="hr-small">
                </div>
                <!-- item -->
                <div class="block post__item bzg">
                    <div class="bzg_c block--half" data-col="m4">
                        <figure class="">
                            <a href="#" class="responsive-media media--3-2 fill--lightgrey">
                                <img data-src="//placehold.it/300x200" alt="" class="item-heavy">
                            </a>
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m8">
                        <h3 class="block--small">
                            <a href="#" class="link-black t--larger">Cool Japan Travel Fair Kembali Hadir di Jakarta</a>
                        </h3>
                        <div class="block t--indent t--smaller">
                            <span class="text-blue">By:</span> <a href="#" class="link-black">H.I.S Travel Indonesia</a><br>
                            <em>Posted on September 7, 2017 at 12:00 PM</em>
                        </div>
                        <div class="block">
                        <strong>Jakarta, 23 Agustus 2017</strong> - Cool Japan Travel Fair merupakan acara travel fair tunggal yang diselenggarakan oleh H.I.S. Travel Indonesia. Event yang pertama kali diadakan pada 16 – 18 September 2016 ini berhasil mendapat sambutan positif dari masyarakat Jakarta dan sekitarnya. Hal ini terbukti dengan suksesnya acara ini mendatangkan tidak kurang dari 195.000 pengunjung selama 3 hari acara berlangsung.
                        </div>
                        <div class="block text-right">
                            <a href="#" class="btn btn--round btn--red text-up">Selengkapnya</a>
                        </div>
                    </div>
                    <hr class="hr-small">
                </div>
                <!-- item -->
            </div>
            <ol class="pagination text-right">
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-left"></span>
                    </a>
                </li>
                <li>
                    <a href="#" class="active">
                        1
                    </a>
                </li>
                <li>
                    <a href="#">
                        2
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa fa-chevron-right"></span>
                    </a>
                </li>
            </ol>
        </section>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
