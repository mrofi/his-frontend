<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Press Release</a></li>
                <li><a href="#">Cool Japan Travel Fair Kembali Hadir di Jakarta</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <div class="block">
            <h1 class="no-space text-blue">Cool Japan Travel Fair Kembali Hadir di Jakarta</h1>
            By:<br>
            <a href="#">H.I.S Travel Indonesia</a>
        </div>
        <figure class="responsive-media media--3-1">
            <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
        </figure>
        <hr class="hr-dash">
        <article class="block">
            <p>
                <em>Posted on September 7, 2017 at 12:00 PM</em>
            </p>
            <p>
                Quibusdam quis saepe error atque fugit officiis vel tempore labore, ratione laboriosam eaque, cupiditate, cum voluptate praesentium enim distinctio quo voluptatem exercitationem! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut ut, atque nisi repellendus aliquid omnis amet sapiente earum dolores nesciunt ipsa eaque. Doloribus repellendus amet ipsum, asperiores tempora in impedit.
            </p>
            <p>
                Aliquid doloribus quibusdam officiis natus cupiditate, vero aut, cumque maxime dicta molestias ipsam quidem? Dolores magni dolor laudantium sunt a. Nesciunt, cum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quibusdam itaque debitis impedit consequatur minima pariatur vero id cupiditate, perspiciatis doloribus accusamus quod incidunt corporis asperiores laborum vitae. Molestias, aliquam.
            </p>
            <p>
                Suscipit perferendis tempora vel itaque nesciunt fugiat magni eos cumque a, iste fuga dignissimos, deleniti rerum provident aut? Officiis soluta maxime sed. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse obcaecati quos sequi quae amet dicta neque debitis, nesciunt alias placeat nisi sed modi quibusdam totam. Voluptatibus excepturi modi ipsum.
            </p>
            <p>
                Veritatis aut dolore repudiandae recusandae aspernatur pariatur nisi corporis ipsa sequi velit laborum reprehenderit, iste autem illo placeat, unde tempora asperiores eos. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque doloribus vel ducimus numquam libero ipsam atque, vero unde ipsa consequatur ipsum tenetur deserunt, nam repellendus incidunt quis necessitatibus consequuntur ex.
            </p>
            <p>
                Nemo voluptate voluptatibus, aspernatur officiis saepe! Laborum perferendis illo, numquam distinctio, est sequi provident dignissimos tempora animi? Reiciendis consequatur officiis nostrum error? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur earum ut perferendis labore ipsam quasi maiores consectetur necessitatibus optio dicta id, esse excepturi! Explicabo veniam obcaecati, id deleniti facere, maxime?
            </p>
        </article>
        <hr class="hr-dash">
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
