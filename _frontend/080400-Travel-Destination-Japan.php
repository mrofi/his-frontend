<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destination</a></li>
                <li><a href="#">Asia</a></li>
                <li><a href="#">Japan</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <h1 class="sr-only">Travel Destination - Japan</h1>
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="text-center">
                <h2>SEMUA INFORMASI TEMPAT TERBAIK DI ASIA</h2>
                <p class="t--larger">
                    Jelajahi kota-kota terbaik di Asia, yang penuh dengan budaya, pemandangan alam dan sejarah yang mengesankan bersama H.I.S. Travel Indonesia.<br>
                    Temukan juga berbagai paket tour dari H.I.S. Travel Indonesia yang terdiri dari tempat wisata terkenal, makanan lokal yang lezat, serta transportasi yang nyaman.
                </p>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="text-up in-block">
                    Region Jepang
                </h2>
                <div class="map-index map-index--japan">
                    <nav class="hover-tab map-index-nav text-up">
                        <a href="#" data-target="#map-hokkaido">Hokkaido</a>
                        <a href="#" data-target="#map-tohoku">tohoku</a>
                        <a href="#" data-target="#map-kanto">kanto</a>
                        <a href="#" data-target="#map-chubu">chubu</a>
                        <a href="#" data-target="#map-kansai">kansai</a>
                        <a href="#" data-target="#map-chugoku">chugoku</a>
                        <a href="#" data-target="#map-shikoku">shikoku</a>
                        <a href="#" data-target="#map-kyushu">kyushu</a>
                        <a href="#" data-target="#map-okinawa">okinawa</a>
                    </nav>
                    <figure class="map-index-content">
                        <img src="assets/img/japan-map.png" alt="" class="base-map">
                        <img src="assets/img/japan-hokkaido.png" alt="" id="map-hokkaido">
                        <img src="assets/img/japan-tohoku.png" alt="" id="map-tohoku">
                        <img src="assets/img/japan-kanto.png" alt="" id="map-kanto">
                        <img src="assets/img/japan-chubu.png" alt="" id="map-chubu">
                        <img src="assets/img/japan-kansai.png" alt="" id="map-kansai">
                        <img src="assets/img/japan-chugoku.png" alt="" id="map-chugoku">
                        <img src="assets/img/japan-shikoku.png" alt="" id="map-shikoku">
                        <img src="assets/img/japan-kyushu.png" alt="" id="map-kyushu">
                        <img src="assets/img/japan-okinawa.png" alt="" id="map-okinawa">
                    </figure>
                </div>
            </div>
            <hr>
            <div class="bzg">
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jtd.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jrm.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c">
                    <a href="#">
                        <img src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-block text-center">
        <img data-src="assets/img/img-separator.jpg" alt="" class="item-heavy">
    </section>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Group Tour</a>
                </h2>
                <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Tour Lainnya</b>
                </a>
            </div>
            <div class="block content-section inset-on-m">
                <div class="bzg cards cards--blue">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/420x420" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--round btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <hr>
        </div>
    </section>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Individual Tour</a>
                </h2>
                <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Tour Lainnya</b>
                </a>
            </div>
            <div class="block content-section inset-on-m">
                <div class="bzg cards cards--blue">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/420x420" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <h3>Korea Selatan</h3>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--round btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <hr>
        </div>
    </section>

    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
