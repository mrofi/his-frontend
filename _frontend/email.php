<!DOCTYPE html>
<html>

<head>
    <title>Aktifkan Akun HIS-Travel Anda</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- <style type="text/css" rel="stylesheet" media="all">
    p {
      margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;
  }
  /* Media Queries */
  @media  only screen and (max-width: 500px) {
    .button {
        width: 100% !important;
    }
}
</style> -->
    <style>
        * {
            box-sizing: border-box;
        }
    </style>
</head>



<body style="margin: 0; padding: 0; background-color: #F3F3F3;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 100%; margin: 0; padding: 0; background-color: #F3F3F3; padding: 16px;" align="center">  
                <table width="100%" cellpadding="0" cellspacing="0" style="margin: 0 auto; max-width: 570px; border-radius: 8px; overflow: hidden;">
                    <!-- Logo -->
                    <tr>
                        <td style="padding: 18px 0; text-align: center; background-color: #034f98;">
                            <a style="display: inline-block;" href="http://histravel.local" target="_blank">
                                <img src="assets/img/site-logo.png" width="90">
                            </a>
                        </td>
                    </tr>

                    <!-- Email Body -->
                    <tr>
                        <td style="width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;" width="100%">
                            <table align="center" cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; padding: 24px; margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
                                        <!-- Greeting -->
                                        <h1 style="margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;">
                                            Hello!
                                        </h1>

                                        <!-- Intro -->
                                        <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
                                            Selamat datang dan selamat bergabung di HIS Travel Indonesia. Klik link berikut untuk mengaktivasi akun anda.
                                        </p>
                                        <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
                                            Terima kasih telah bergabung bersama HIS Travel. Silakan klik tombol berikut untuk aktivasi akun Anda.
                                        </p>
                                        
                                        
                                        <!-- Action Button -->
                                        <table style="width: 100%; margin: 30px auto; padding: 0; text-align: center;" align="center" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">

                                                    <a href="http://histravel.local/activateuser/7/c356223c6509f1fe0a958feaca72ad6e"
                                                    style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                                                    background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                                                    text-align: center; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3869D4;"
                                                    class="button"
                                                    target="_blank">
                                                    Aktifkan &amp; Mulai Traveling
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

                                    <!-- Outro -->
                                    <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">

                                    </p>

                                    <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">Jika Anda membutuhkan bantuan, silakan hubungi Customer Service kami melalui email <a href="mailto:callcenter.jkt@his-world.com">callcenter.jkt@his-world.com</a> atau telepon <a href="tel:021-5741701">021-5741701</a>. Mohon tidak mengirimkan balasan ke email ini.</p>

                                    <!-- Salutation -->
                                    <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
                                        Terima Kasih,
                                        <br>HIS Travel
                                    </p>

                                    <!-- Sub Copy -->
                                    <table style="margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;">
                                        <tr>
                                            <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif;">
                                                <p style="margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;">
                                                    If you having trouble clicking the "Aktifkan &amp; Mulai Traveling" button,
                                                    copy and paste the URL below into your web browser:
                                                </p>

                                                <p style="margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;">
                                                    <a style="color: #3869D4; word-break: break-all;" href="http://histravel.local/activateuser/7/c356223c6509f1fe0a958feaca72ad6e" target="_blank">
                                                        http://histravel.local/activateuser/7/c356223c6509f1fe0a958feaca72ad6e
                                                    </a>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- Footer -->
                <tr style="background-color: #666666; color: #FFFFFF; font-family: sans-serif">
                    <td style=" padding: 16px; text-align: center;">
                        <p style="margin-top: 0; font-size: 12px; line-height: 1.5em; color: #FFFFFF;">
                            &copy; 2016 - 2018
                            <a style="color: #FFF;" href="http://histravel.local" target="_blank">HIS Travel - Travel for Everyone</a>.
                            All rights reserved.
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
