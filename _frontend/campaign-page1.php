<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">BISALIBURANLAGI</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block ">
        <div class="container container--smaller">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="lazyload">
                <div class="flex a-bottom j-end">
                    <button class="btn btn--blue pull-left--l block ml-half" type="button">
                        <strong class="text-up">DAFTAR</strong>
                    </button>
                    <button class="btn btn--blue pull-left--l block ml-half mr " type="button">
                        <strong class="text-up">CARA IKUTAN</strong>
                    </button>
                 </div>
            </figure>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block--half t-strong">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into 
                    electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                     and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
            <figure class="flex flex-col-center">
                <img src="" data-src="//placehold.it/720x220" alt="" class="lazyload">
            </figure>
            <div class="block--half text-center t-strong">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
            </div>
            <div class="block--half flex flex-col-center">
                <button class="btn btn--round btn--red space btn-shadow" type="button">
                    <b class="text-up t-strong">BUAT SEKARANG</b>
                </button>
            </div>
            <div class="block--half t-strong">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into 
                    electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                     and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
            <div class="section-block container--smallest">
                <div class="bzg">
                    <?php for ($i=1; $i <= 6; $i++) { ?>
                    <div class="bzg_c block--double" data-col="m6, l4">
                        <div class="card--solid block--inset ">
                            <figure class="fill-white">
                                <img src="" data-src="//placehold.it/450x450" alt="" class="lazyload img-full">
                            </figure>
                            <div class="action flex j-end mt-half">
                                <button class="text-white btn--plain c-blue space-right">
                                    <span class="fa fa-share-alt"></span>
                                </button>
                                <a href="//placehold.it/450x450" download class="text-white btn--plain c-blue">
                                    <span class="fa fa-download "></span>
                                </a>
                            </div>
                            <div class="flex">
                                <span class="fa fa-user-circle user-logo "></span>
                                <div class="title-trip t-strong text-center h-center">
                                    <span>Triana Munad</span>
                                    <br>
                                    <span>"Wonderfull Japan"</span>
                                    <br>
                                    <span >20 Jan 20</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="block--double flex v-center--spread">
                    <button class="btn btn--round btn--red space btn-shadow" type="button">
                        <b class="text-up t-strong">BUAT SEKARANG</b>
                    </button>
                    <button class="btn btn--round btn--red space btn-shadow" type="button">
                        <b class="text-up t-strong">LIHAT SEMUA</b>
                    </button>
                </div>
            </div>

            <div class="text-left space t-strong">
                <h2 class="text-up in-block block--half">
                        SYARAT & KETENTUAN
                </h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into 
                    electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                     and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
