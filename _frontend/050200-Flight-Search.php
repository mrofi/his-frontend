<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Aktivitas</a></li>
                <li><a href="#">Optional Tour</a></li>
                <li><a href="#">Visa</a></li>
            </ul>
        </div>
    </div>
    <section class="section-block">
        <div class="block--inset fill-lightgrey">
            <div class="container container--smaller">
                <h1 class="block--small text-red text-up">
                    <span class="t--smaller">
                        Jakarta (CGK)
                        <span class="fa fa-exchange"></span>
                        Tokyo (HND)
                    </span>
                </h1>
                <p>
                    <strong>Berangkat</strong> :  Minggu, 6 Agustus 2017<br>
                    <strong>Kembali</strong> : Kamis, 10 Agustus 2017<br>
                    <strong>Penumpang</strong> :  1 Dewasa<br>
                    <strong>Kelas Penerbangan</strong> : Ekonomi 
                </p>
                <button class="btn btn--round btn--ghost-red toggle-trigger text-up" type="button" data-target="#research_booking">
                    <strong>Ganti Pencarian</strong>
                </button>
            </div>
        </div>
        <div class="container container--smaller toggle-panel toggle-panel--slideup" id="research_booking">
            <div class="fill-primary block--inset">
                <form action="" class="form">
                    <div class="bzg bzg--lite-gutter">
                        <div class="bzg_c" data-col="m4">
                            <div class="form__row">
                                <select name="" id="" class="form-input form-input--block selectstyle">
                                    <option value="">Asal</option>
                                    <option value="JKT">Jakarta</option><option value="SUB">Surabaya</option><option value="BDO">Bandung</option><option value="UPG">Makassar</option><option value="DPS">Denpasar Bali</option>
                                </select>
                            </div>
                            <div class="form__row">
                                <select name="" id="" class="form-input form-input--block selectstyle">
                                    <option value="">Tujuan</option>
                                    <option value="TYO">TOKYO</option><option value="NRT">Tokyo, TOKYO NARITA (NRT)</option><option value="HND">Tokyo, TOKYO HANEDA (HND)</option><option value="OSA">OSAKA</option><option value="KIX">Osaka, OSAKA KANSAI INTERNATIONAL (KIX)</option><option value="NGO">NAGOYA</option><option value="FUK">Fukuoka</option><option value="SPK">SAPPORO CHITOSE</option><option value="AKJ">ASAHIKAWA</option><option value="HKD">HAKODATE</option><option value="OBO">OBIHIRO</option><option value="SDJ">Sendai</option><option value="AOJ">AOMORI</option><option value="MSJ">MISAWA</option><option value="AXT">AKITA</option><option value="ONJ">ODATE NOSHIRO</option><option value="HNA">HANAMAKI</option><option value="GAJ">YAMAGATA JUNMACHI</option><option value="SYO">SHONAI</option><option value="FKS">FUKUSHIMA</option><option value="IBR">IBARAKI</option><option value="KIJ">NIIGATA</option><option value="KMQ">KOMATSU</option><option value="TOY">TOYAMA</option><option value="QSZ">SHIZUOKA</option><option value="TAK">TAKAMATSU</option><option value="KCZ">KOCHI</option><option value="MYJ">MATSUYAMA</option><option value="TKS">TOKUSHIMA</option><option value="HIJ">Hiroshima</option><option value="OKJ">OKAYAMA</option><option value="UBJ">UBE</option><option value="YGJ">YONAGO MIHO</option><option value="IZO">IZUMO</option><option value="TTJ">TOTTORI</option><option value="IWJ">IWAMI</option><option value="KKJ">KITA KYUSHU KOKURA</option><option value="HSG">SAGA</option><option value="NGS">NAGASAKI</option><option value="OIT">OITA</option><option value="KMJ">KUMAMOTO</option><option value="KMI">MIYAZAKI</option><option value="KOJ">Kagoshima</option><option value="OKA">Okinawa</option>
                                </select>
                            </div>
                        </div>
                        <div class="bzg_c pickdate-range" data-col="m4">
                            <div class="form__row">
                                <div class="input-iconic">
                                    <input type="text" class="form-input form-input--block pikaRange pikaRange--start" id="flight_start" placeholder="Tanggal Berangkat">
                                    <label for="flight_start" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row">
                                <div class="input-iconic" id="trip_type">
                                    <input type="text" class="form-input form-input--block pikaRange pikaRange--end" id="flight_end" placeholder="Tanggal Kembali">
                                    <label for="flight_end" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="m4">
                            <div class="form__row">
                                <select name="" class="form-input form-input--block selectstyle">
                                    <option value="">Kelas Penerbangan</option>
                                    <option value="">Ekonomi</option>
                                </select>
                            </div>
                            <div class="form__row">
                                <div class="pick-guest" id="pickFlightPsg" data-toggle>
                                    <button class="form-input form-input--block btn-toggle" type="button">
                                        <strong class="text-ellipsis">
                                            <span class="total-input--all" id="total_psg">1</span> Traveller(s)
                                        </strong>
                                    </button>
                                    <div class="toggle-panel t-black fill-white">
                                        <div class="block--inset-small">
                                            <div class="form__row flex v-ct qty-input">
                                                <label for="" class="fg-1">
                                                    <span class="total-input">1</span> Adult
                                                </label>
                                                <div class="">
                                                    <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                    <input type="number" class="form-input form-input--redline text-center input-adult" 
                                                    value="1" min="1" max="9" data-psg="total_psg">
                                                    <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <hr class="block--half">
                                            <div class="form__row flex v-ct qty-input">
                                                <label for="" class="fg-1">
                                                    <span class="total-input">0</span> Children
                                                </label>
                                                <div class="">
                                                    <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                    <input type="number" class="form-input form-input--redline text-center" 
                                                    value="0" min="0" max="9" data-psg="total_psg">
                                                    <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <hr class="block--half">
                                            <div class="form__row flex v-ct qty-input">
                                                <label for="" class="fg-1">
                                                    <span class="total-input">0</span> Infant
                                                </label>
                                                <div class="">
                                                    <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="infant" type="button">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                    <input type="number" class="form-input form-input--redline text-center" 
                                                    value="0" min="0" max="9" data-psg="total_psg">
                                                    <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="infant" type="button">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <hr class="block--half">
                                            <div class="text-right">
                                                <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                                    Pilih
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- pick-guest -->
                            </div>
                        </div>
                    </div>
                    <div class="cf">
                        <label for="sekali_jalan" class="form-label--inline">
                            <input type="radio" id="sekali_jalan" name="trip_type" data-trip="one_way">
                            <span class="btn-label__text">Sekali Jalan</span>
                        </label>
                        <label for="pulang_pergi" class="form-label--inline">
                            <input type="radio" id="pulang_pergi" name="trip_type" data-trip="round_trip">
                            <span class="btn-label__text">Pulang Pergi</span>
                        </label>
                        <div class="pull-right text-right">
                            <label for="langsung" class="form-label--inline">
                                <input type="checkbox" id="langsung">
                                <span class="btn-label__text">Hanya penerbangan langsung</span>
                            </label>
                            <button class="btn btn--round btn--red">
                                <strong class="text-up">Cari</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    <section class="section-block">
        <div class="container container--smaller">
            <div class="bzg bzg--fixed flight-result">
                <div class="bzg_c flight-result__item" data-col="m6" data-flight="dev/flight-1.json" data-trip="depart" data-tmpl="#tmpFlight">
                    <div class="block">
                        <h3 class="block--small text-up">Pilih Penerbangan Keberangkatan</h3>
                        <div class="block--small">
                            Jakarta (CGK) ke Tokyo (HND)
                        </div>
                        <time>Minggu, 6 Agustus 2017</time>
                    </div>
                    <nav class="block row-sort">
                        <ul class="list-nostyle">
                            <li class="label-nav">
                                <span class="in-block">Urutkan:</span>
                            </li>
                            <li>
                                <button class="btn btn-sort sort--depart" type="button">
                                    Keberangkatan
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-sort sort--duration" type="button">
                                    Durasi
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-sort sort--price" type="button">
                                    Price
                                </button>
                            </li>
                        </ul>
                    </nav>
                    <div class="data-container">
                
                    </div>
                    <div class="loader">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div>
                <!-- depart -->
                <div class="bzg_c flight-result__item" data-col="m6" data-flight="dev/flight-2.json" data-trip="return" data-tmpl="#tmpFlight">
                    <div class="block">
                        <h3 class="block--small text-up">Pilih Penerbangan Keberangkatan</h3>
                        <div class="block--small">
                            Tokyo (HND) ke Jakarta (CGK)
                        </div>
                        <time>Minggu, 16 Agustus 2017</time>
                    </div>
                    <nav class="block row-sort">
                        <ul class="list-nostyle">
                            <li class="label-nav">
                                <span class="in-block">Urutkan:</span>
                            </li>
                            <li>
                                <button class="btn btn-sort sort--depart" type="button">
                                    Keberangkatan
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-sort sort--duration" type="button">
                                    Durasi
                                </button>
                            </li>
                            <li>
                                <button class="btn btn-sort sort--price" type="button">
                                    Price
                                </button>
                            </li>
                        </ul>
                    </nav>
                    <div class="data-container">
                
                    </div>
                    <div class="loader">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div>
                <!-- return -->
            </div>
        </div>
    </section>
</main>
<div class="flight-summary" data-depart="0" data-return="0">
    <div class="container container--small">
        <div class="block--inset-small">
            <button class="btn-toggle btn btn--smaller btn--blue">
                <span class="fa fa-angle-down"></span>
            </button>
            <div class="flight-summary-container t--smaller">
                <div class="flight-summary--depart">
                    <small>Berangkat</small>
                    <hr class="block--small">
                    <div class="block--half text-center" id="summary_depart">Pilih Penerbangan Berangkat</div>
                </div>
                <div class="flight-summary--return">
                    <small>Kembali</small>
                    <hr class="block--small">
                    <div class="block--half text-center" id="summary_return">Pilih Penerbangan Kembali</div>
                </div>
            </div>
            <hr class="block--half">   
            <div class="bzg">
                <div class="bzg_c" data-col="m6">
                    <h4 class="no-space">Ringkasan Pembelian</h4>
                </div>
                <div class="bzg_c text-right" data-col="m6">
                    <h3 class="block--small text-red in-block">
                        Rp. <span class="total-flight"></span>
                    </h3>
                    &nbsp;
                    <button class="btn btn--round btn--red btn-book-flight" disabled>Book now</button>
                </div>
            </div>
        </div>
    </div>
</div>
<template id="tmpFlight">
    {{#each this}}
        <div class="block--half border flight__item" data-toggle>
            <div class="inner block--inset-small">
                <div class="block--half bzg bzg--small-gutter text-center" id="dom_{{id}}">
                    <div class="bzg_c" data-col="x4, l2">
                        <img src="{{airways_logo}}" alt="{{airways}}" class="img-contain airways-logo"><br>
                        {{airways}}
                    </div>
                    <div class="bzg_c" data-col="x4, l2">
                        <div class="t--larger line--40 t-strong">
                        {{formatTime depart hour="numeric" minute="numeric" hour12=false}}
                        </div>
                        {{#each origin}}
                            {{code}}
                        {{/each}}
                    </div>
                    <div class="bzg_c" data-col="x4, l2">
                        <div class="t--larger line--40 t-strong">
                        {{formatTime arrive hour="numeric" minute="numeric" hour12=false}}
                        </div>
                        {{#each destination}}
                            {{code}}
                        {{/each}}
                    </div>
                    <div class="bzg_c" data-col="x6, l2">
                        <div class="t--larger line--40 t-strong">
                        {{duration_hour}}j {{duration_minute}}m
                        </div>
                        {{#if transit}}
                            Transit
                        {{else}}
                            Direct
                        {{/if}}
                    </div>
                    <div class="bzg_c" data-col="x6, l4">
                        <div class="t--larger line--40 t-strong text-red">
                        Rp. {{formatNumber price num}}
                        </div>
                        Harga per dewasa
                    </div>
                </div>
                <div class="clearfix">
                    <button class="btn-toggle btn--plain in-block"><span>Detail Penerbangan <span class="fa fa-caret-down"></span></span></button>
                    <label for="flight_{{id}}" class="btn btn-check-flight btn--ghost-check btn--round pull-right">
                        <input type="radio" id="flight_{{id}}" name="flight-{{#each origin}}{{city}}{{/each}}" 
                        data-summary="#summary_{{#each origin}}{{city}}{{/each}}" 
                        data-target="#dom_{{id}}" data-price="{{price}}">
                        <span class="bg-label"></span>
                        <strong class="btn-text text-up">Pilih</strong>
                    </label>
                </div>
            </div>
            <div class="toggle-panel">
                <hr class="no-space">
                <div class="block--inset-small">
                {{#if transit}}{{#each transit}}
                    <div class="block--half bzg">
                        <div class="block--half bzg_c" data-col="m7">
                            <div class="block--half line--small">
                                <img src="{{airways_logo}}" alt=""><br>
                                <small>{{airways}}</small><br>
                                <small class="text-grey text-up">{{seat_class}}</small>
                            </div>
                            <ul class="list-nostyle flight-path">
                                <li class="bzg line--small">
                                    <div class="bzg_c" data-col="m4">
                                        {{formatTime depart hour="numeric" minute="numeric" hour12=false}}<br>
                                        <small class="text-grey">{{formatDate depart day="numeric" month="short" year="numeric"}}</small>
                                    </div>
                                    <div class="bzg_c" data-col="m8">
                                        {{#each origin}}
                                            {{city}} ({{code}})<br>
                                            <small class="text-grey">{{airport}}</small>
                                        {{/each}}
                                    </div>
                                </li>
                                <li>
                                    <small class="t--indent">
                                        {{duration_hour}}j {{duration_minute}}m
                                    </small>
                                </li>
                                <li class="bzg line--small">
                                    <div class="bzg_c" data-col="m4">
                                        {{formatTime arrive hour="numeric" minute="numeric" hour12=false}}<br>
                                        <small class="text-grey">{{formatDate arrive day="numeric" month="short" year="numeric"}}</small>
                                    </div>
                                    <div class="bzg_c" data-col="m8">
                                        {{#each destination}}
                                            {{city}} ({{code}})<br>
                                            <small class="text-grey">{{airport}}</small>
                                        {{/each}}
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="bzg_c" data-col="m5">
                            <strong>Aircraft</strong>: {{aircraft}}<br>
                            <strong>Seat Layout</strong>: {{seat_layout}}<br>
                            <strong>Seat Pitch</strong>: {{seat_pitch}}<br>
                            <strong>Cabin Baggage</strong>: {{baggage_size}}<br>
                            <span class="his-koper"></span>
                            {{#if baggage_extra}}
                                {{baggage_extra}}
                            {{eles}}
                                (Buy)
                            {{/if}}
                            <strong>Aircraft</strong>: 
                        </div>
                    </div>
                    {{#if transit_time}}
                        <div class="block--half block--inset-small fill-lightgrey">
                            Transit for {{#each transit_time}}{{hour}}j {{minute}}m {{/each}} in 
                            {{#each destination}}
                                {{city}} ({{code}})
                            {{/each}}
                        </div>
                    {{/if}}
                {{/each}}
                {{else}}
                    <div class="block--half bzg">
                        <div class="block--half bzg_c" data-col="m7">
                            <div class="block--half line--small">
                                <img src="{{airways_logo}}" alt=""><br>
                                <small>{{airways}}</small><br>
                                <small class="text-grey text-up">{{seat_class}}</small>
                            </div>
                            <ul class="list-nostyle flight-path">
                                <li class="bzg line--small">
                                    <div class="bzg_c" data-col="m4">
                                        {{formatTime depart hour="numeric" minute="numeric" hour12=false}}<br>
                                        <small class="text-grey">{{formatDate depart day="numeric" month="short" year="numeric"}}</small>
                                    </div>
                                    <div class="bzg_c" data-col="m8">
                                        {{#each origin}}
                                            {{city}} ({{code}})<br>
                                            <small class="text-grey">{{airport}}</small>
                                        {{/each}}
                                    </div>
                                </li>
                                <li>
                                    <small class="t--indent">
                                        {{duration_hour}}j {{duration_minute}}m
                                    </small>
                                </li>
                                <li class="bzg line--small">
                                    <div class="bzg_c" data-col="m4">
                                        {{formatTime arrive hour="numeric" minute="numeric" hour12=false}}<br>
                                        <small class="text-grey">{{formatDate arrive day="numeric" month="short" year="numeric"}}</small>
                                    </div>
                                    <div class="bzg_c" data-col="m8">
                                        {{#each destination}}
                                            {{city}} ({{code}})<br>
                                            <small class="text-grey">{{airport}}</small>
                                        {{/each}}
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="bzg_c" data-col="m5">
                            <strong>Aircraft</strong>: {{aircraft}}<br>
                            <strong>Seat Layout</strong>: {{seat_layout}}<br>
                            <strong>Seat Pitch</strong>: {{seat_pitch}}<br>
                            <strong>Cabin Baggage</strong>: {{baggage_size}}<br>
                            <span class="his-koper"></span>
                            {{#if baggage_extra}}
                                {{baggage_extra}}
                            {{eles}}
                                (Buy)
                            {{/if}}
                            <strong>Aircraft</strong>: 
                        </div>
                    </div>
                {{/if}}
                </div>
            </div>
            <!-- toggle-panel -->
        </div>
    {{/each}}
</template>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
