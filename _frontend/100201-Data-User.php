<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Profil Traveler</a></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <h1 class="block--half">Profil Traveler</h1>
        <div class="block block--inset-small border">
            <div class="bzg bzg--no-gutter">
                <div class="bzg_c" data-col="m4, l3">
                    <nav class="side-nav">
                        <ul class="list-nostyle">
                            <li class="side-nav__item">
                                <a href="#">Data Pemesanan</a>
                            </li>
                            <li class="side-nav__item is-active">
                                <a href="#">Profil Saya</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Ubah Password</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Keluar</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="bzg_c" data-col="m8, l9">
                    <div class="profil-main">
                    <h4 class="no-space title text-up">Profil Saya</h4>
                    <div class="block--inset">
                        <form action="" class="form baze-form"
                        data-empty="Input tidak boleh kosong"
                        data-email="Alamat email tidak benar"
                        data-number="Input harus berupa angka"
                        data-numbermin="Angka minimal "
                        data-numbermax="Angka maksimal">
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Nama Depan *</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="text" class="form-input form-input--block" required value="Ricky">
                                </div>
                            </div>
                            <!-- row -->
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Nama Belakang</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="text" class="form-input form-input--block" value="Hutasoid">
                                </div>
                            </div>
                            <!-- row -->
                            <div class="bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Tanggal Lahir</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <div class="bzg bzg--small-gutter">
                                        <div class="form__row bzg_c" data-col="m4">
                                            <select name="" class="form-input form-input--block selectstyle">
                                                <option value="1">1</option>
                                                <option value="31" selected>31</option>
                                            </select>
                                        </div>
                                        <div class="form__row bzg_c" data-col="m4">
                                            <select name="" class="form-input form-input--block selectstyle">
                                                <option value="Januari">Januari</option>
                                                <option value="September" selected>September</option>
                                            </select>
                                        </div>
                                        <div class="form__row bzg_c" data-col="m4">
                                            <select name="" class="form-input form-input--block selectstyle">
                                                <option value="1990">1990</option>
                                                <option value="1980" selected>1980</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>No. Telp/HP</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="number" class="form-input form-input--block" value="08123456987">
                                </div>
                            </div>
                            <!-- row -->
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Pekerjaan</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="text" class="form-input form-input--block" value="Karyawan Negara">
                                </div>
                            </div>
                            <!-- row -->
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Domisili</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="text" class="form-input form-input--block" value="Ujung Kulon">
                                </div>
                            </div>
                            <!-- row -->
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Email *</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="email" class="form-input form-input--block" required value="ricky@hotmail.com">
                                </div>
                            </div>
                            <!-- row -->
                            <div class="text-right">
                                <button class="btn btn--round btn--red text-up t-strong" type="submit">Simpan</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
