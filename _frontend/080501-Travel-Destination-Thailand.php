<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destination</a></li>
                <li><a href="#">Asia</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="text-center">
                <h1 class="sr-only">Travel Destination - Thailand</h1>
                <h2>SEMUA INFORMASI TEMPAT TERBAIK DI ASIA</h2>
                <p class="t--larger">
                    Jelajahi kota-kota terbaik di Asia, yang penuh dengan budaya, pemandangan alam dan sejarah yang mengesankan bersama H.I.S. Travel Indonesia.<br>
                    Temukan juga berbagai paket tour dari H.I.S. Travel Indonesia yang terdiri dari tempat wisata terkenal, makanan lokal yang lezat, serta transportasi yang nyaman.
                </p>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Pilih Destinasi
                </h2>
            </div>
            <div class="inset-on-m">
            <div class="block bzg bzg--small-gutter">
                <?php for ($i=1; $i <= 8; $i++) { ?>
                <div class="block bzg_c" data-col="m4, l3">
                    <figure>
                        <a href="#" class="block--small responsive-media media--3-2">
                            <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                        </a>
                        <figcaption class="text-center">
                            <h3 class="text-up block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                        </figcaption>
                    </figure>
                </div>
                <?php } ?>
            </div>
            </div>
            <div class="block bzg">
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jtd.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jrm.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-block text-center">
        <img data-src="assets/img/img-separator.jpg" alt="" class="item-heavy">
    </section>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Group Tour</a>
                </h2>
                <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Tour Lainnya</b>
                </a>
            </div>
            <div class="block content-section">
                <div class="bzg cards cards--blue">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/420x420" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--round btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <hr>
        </div>
    </section>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Individual Tour</a>
                </h2>
                <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Tour Lainnya</b>
                </a>
            </div>
            <div class="block content-section">
                <div class="bzg cards cards--blue">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/420x420" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <h3>Korea Selatan</h3>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--round btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <hr>
        </div>
    </section>

    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
