<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Karir</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="sr-only">Karir</h1>
        <section class="section-block">
            <figure class="responsive-media media--3-1">
                <img src="" data-src="//placehold.it/1080x380" alt="" class="item-heavy">
            </figure>
            <div class="text-center">
                <p class="t--larger">
                    PT. Harum Indah Sari merupakan cabang dari H.I.S. Co. Ltd Jepang yang berada di Indonesia. Kami merupakan perusahaan investasi asing yang berspesialisasi di bidang Tour dan Wisata. Sebagai salah satu pemain di pasar global, kami mempertimbangkan untuk memiliki karyawan yang berkualitas untuk pencapaian kinerja misi perusahaan. Kami mencari individu yang dinamis, termotivasi dan mampu untuk posisi :
                </p>
            </div>
            <hr class="hr-dash">
            <div class="bzg cards cards--nooverlay cards--round-blue">
                <div class="block--double bzg_c" data-col="m6">
                    <div class="card__item">
                        <h3 class="card-title text-up">
                            TRAVEL CONSULTANT / CALL CENTER
                        </h3>
                        <div class="card-content line--small">
                            <h4 class="block--half text-blue">Location:</h4>
                            <p class="block--half"><strong>HIS Bintaro Jaya Xchange, HIS Lippo Kemang, HIS Mall Taman 
                                Anggrek, HIS FX Sudirman, HIS Darmawangsa, HIS BNI 46, HIS 
                                Gandaria City</strong>
                            </p>
                            <hr class="block--half hr-dash">
                            <h4 class="block--half text-blue">Qualification:</h4>
                            <div class="block--small">
                                Job description:
                                <ul>
                                    <li>Give explanation about our product to our guest</li>
                                </ul>
                            </div>
                            <div class="block--small">
                                Requirements :
                                <ul>
                                    <li>Candidate must possess at least a SMIP/Diploma/Bachelor of Degree from Tourism.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- _item -->
                </div>
                <div class="block--double bzg_c" data-col="m6">
                    <div class="card__item">
                        <h3 class="card-title text-up">
                            TRAVEL CONSULTANT / CALL CENTER
                        </h3>
                        <div class="card-content line--small">
                            <h4 class="block--half text-blue">Location:</h4>
                            <p class="block--half"><strong>HIS Bintaro Jaya Xchange, HIS Lippo Kemang, HIS Mall Taman 
                                Anggrek, HIS FX Sudirman, HIS Darmawangsa, HIS BNI 46, HIS 
                                Gandaria City</strong>
                            </p>
                            <hr class="block--half hr-dash">
                            <h4 class="block--half text-blue">Qualification:</h4>
                            <div class="block--small">
                                Job description:
                                <ul>
                                    <li>Give explanation about our product to our guest</li>
                                </ul>
                            </div>
                            <div class="block--small">
                                Requirements :
                                <ul>
                                    <li>Candidate must possess at least a SMIP/Diploma/Bachelor of Degree from Tourism.</li>
                                    <li>Candidate must possess at least a SMIP/Diploma/Bachelor of Degree from Tourism.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- _item -->
                </div>
                <div class="block--double bzg_c" data-col="m6">
                    <div class="card__item">
                        <h3 class="card-title text-up">
                            TRAVEL CONSULTANT / CALL CENTER
                        </h3>
                        <div class="card-content line--small">
                            <h4 class="block--half text-blue">Location:</h4>
                            <p class="block--half"><strong>HIS Bintaro Jaya Xchange, HIS Lippo Kemang, HIS Mall Taman 
                                Anggrek, HIS FX Sudirman, HIS Darmawangsa, HIS BNI 46, HIS 
                                Gandaria City</strong>
                            </p>
                            <hr class="block--half hr-dash">
                            <h4 class="block--half text-blue">Qualification:</h4>
                            <div class="block--small">
                                Job description:
                                <ul>
                                    <li>Give explanation about our product to our guest</li>
                                </ul>
                            </div>
                            <div class="block--small">
                                Requirements :
                                <ul>
                                    <li>Candidate must possess at least a SMIP/Diploma/Bachelor of Degree from Tourism.</li>
                                    <li>Candidate must possess at least a SMIP/Diploma/Bachelor of Degree from Tourism.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- _item -->
                </div>
            </div>
        </section>
    </div>
</main>
<template id="tmpList">
    {{#each data}}
    <section class="section-block">
        <h2 class="text-up">
            {{title}}
        </h2>
        <div class="slideGallery slide-off-arrow bzg bzg--small-gutter">
            {{#each gallery}}
            <div class="slide__item bzg_c" data-col="m3">
                <figure class="responsive-media square fig-tag">
                    <img data-lazy="{{images}}" alt="">
                    <figcaption>{{caption}}</figcaption>
                </figure>
            </div>
            {{/each}}
        </div>
    </section>
    <hr>
    {{/each}}
</template>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
