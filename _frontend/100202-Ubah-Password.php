<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Profil Traveler</a></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <h1 class="block--half">Profil Traveler</h1>
        <div class="block block--inset-small border">
            <div class="bzg bzg--no-gutter">
                <div class="bzg_c" data-col="m4, l3">
                    <nav class="side-nav">
                        <ul class="list-nostyle">
                            <li class="side-nav__item">
                                <a href="#">Data Pemesanan</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Profil Saya</a>
                            </li>
                            <li class="side-nav__item is-active">
                                <a href="#">Ubah Password</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Keluar</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="bzg_c" data-col="m8, l9">
                    <div class="profil-main">
                    <h4 class="no-space title text-up">Ubah Password</h4>
                    <div class="block--inset">
                        <form action="" class="form baze-form"
                        data-empty="Input tidak boleh kosong"
                        data-email="Alamat email tidak benar"
                        data-number="Input harus berupa angka"
                        data-numbermin="Angka minimal "
                        data-numbermax="Angka maksimal">
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Password Lama *</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="password" class="form-input form-input--block" required>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Password Baru *</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="password" class="form-input form-input--block" required>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="form__row bzg">
                                <div class="bzg_c" data-col="m4, l3">
                                    <label for="" class="form-label"><strong>Ulangi Password Baru *</strong></label>
                                </div>
                                <div class="bzg_c" data-col="m8, l9">
                                    <input type="password" class="form-input form-input--block" required>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="text-right">
                                <button class="btn btn--round btn--red text-up t-strong" type="submit">Simpan</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
