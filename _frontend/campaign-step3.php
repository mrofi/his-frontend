<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="responsive-media media--8-1">
        <img src="" data-src="//placehold.it/1600x200" alt="" class="item-heavy">
        <div class="absolute flex j-center a-bottom">
            <ol class="breadcrumb-campaign t-strong ">
                <li class="is-active"><span>PILIH DESIGN</span></li>
                <li class="is-active"><span>BUAT RENCANA PERJALANAN</span></li>
                <li class="is-active"><span>PREVIEW</span></li>
                <li><span>SELESAI & BAGIKAN</span></li>
            </ol>
        </div>
    </div>
    <section class="section-block mt">
        <div class="container container--smaller">
            <div class="bzg">
                <div class="bzg_c" data-col="m5">
                    <h1 class="t-strong text-blue">PREVIEW & SUBMIT</h1>
                    <p class="t-strong block--double ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    <div class="section-block--smaller">
                        <h2 class="t-strong text-blue block--small">JUDUL</h2>
                        <input class="form-input form-input--block" type="text" name="text" placeholder="Text"
                        maxlength="30">
                    </div>
                    <div class="block--half">
                        <button class="btn btn--round btn--red" type="button">
                            <b class="text-up t-strong">SUBMIT</b>
                        </button>
                    </div>
                </div>
                <div class="bzg_c" data-col="m7">
                    <div class="section-block--smaller imgPopup">
                        <figure class="square flex j-center responsive-media media--square">
                            <img src="" data-src="//placehold.it/600x600" alt="Img" class="lazyload">
                            <div class="flex j-end text--huge a-bottom">
                                <a href="assets/img/bg-broken-page-desktop.jpg" class="text-red btn--plain">
                                    <span class="fa fa-search"></span>
                                    <img src="assets/img/img-preload-logo" alt="" class="hidden">
                                </a>
                            </div>
                        </figure>
                    </div>
                    <div class="block--half flex j-end">
                        <button class="btn btn--round btn--red" type="button">
                            <b class="text-up t-strong">EDIT</b>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
