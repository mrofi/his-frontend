<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Profil Traveler</a></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <h1 class="block--half">Profil Traveler</h1>
        <div class="block block--inset-small border">
            <div class="bzg bzg--no-gutter">
                <div class="bzg_c" data-col="m4, l3">
                    <nav class="side-nav">
                        <ul class="list-nostyle">
                            <li class="side-nav__item">
                                <a href="#">Data Pemesanan</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Profil Saya</a>
                            </li>
                            <li class="side-nav__item is-active">
                                <a href="#">Data Penumpang</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Ubah Password</a>
                            </li>
                            <li class="side-nav__item">
                                <a href="#">Keluar</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="bzg_c" data-col="m8, l9">
                    <div class="profil-main booking-history">
                        <h4 class="no-space title text-up">Daftar Pemesanan</h4>
                        <ul class="list-nostyle" data-category="User-Passengers" data-list="dev/userPassengers.json" data-tmpl="#booking-template">
                            
                        </ul>
                        <div class="loader">
                            <div class="bounce1"></div>
                            <div class="bounce2"></div>
                            <div class="bounce3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<template id="booking-template">
    {{#each this}}
    {{#each this}}
    <li class="" data-toggle>
        <div class="bzg bzg--inset">
            <div class="bzg_c" data-col="x6, l5">
                <div class="top-label">Nama Lengkap</div>
                <div class="">{{passenger_name}}</div>
            </div>
            <div class="bzg_c" data-col="x6, l5">
                <div class="top-label">Email</div>
                <div class="break-all">{{passenger_email}}</div>
            </div>
            <div class="bzg_c" data-col="x10, l2">
                <div class="top-label">Action</div>
                <div class="content col-action">
                    <a href="" class="btn btn--smaller btn--green btn--round mr-small" type="button" aria-label="Edit">
                        <span class="fa fa-pencil"></span>
                    </a>
                    <button class="btn btn--smaller btn--round mr-small" type="button" aria-label="Remove">
                        <span class="fa fa-trash"></span>
                    </button>
                    <span class="btn-toggle btn-action btn btn--smaller btn--round btn--ghost">
                        <span class="fa fa-caret-down"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="toggle-panel booking-details">
            <div class="booking-details__item">
                <div class="label">Tanggal Lahir</div>
                <div class="input">{{passenger_birthdate}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Jenis Kelamin</div>
                <div class="input">
                    {{#if (eq passenger_gender 'male')}}
                        laki-laki
                    {{else}}
                        perempuan
                    {{/if}}
                </div>
            </div>
            <div class="booking-details__item">
                <div class="label">Kewarganegaraan</div>
                <div class="input">{{passenger_nationality}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Negara yang Mengeluarkan</div>
                <div class="input">{{passenger_passport_issued_country}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Nomor Paspor</div>
                <div class="input">{{passenger_passport_number}}</div>
            </div>
            <div class="booking-details__item">
                <div class="label">Masa Berlaku Paspor</div>
                <div class="input">{{passenger_passport_expired}}</div>
            </div>
        </div>
    </li>
    {{/each}}
    {{/each}}
</template>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
