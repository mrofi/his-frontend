<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Events</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <h1 class="sr-only">Events</h1>
        <div class="block--half text-center">
            <a class="btn btn--round btn--ghost-red is-active" href="140000-Events.php">
                <strong class="text-up">Event Berlangsung</strong>
            </a>
            <a class="btn btn--round btn--ghost-red" href="140100-Events-ended.php">
                <strong class="text-up">Event Berakhir</strong>
            </a>
        </div>
        <hr>
        
        <div class="inset-on-m">
        <div class="bzg cards cards--blue" data-list="dev/events.json" data-category="events-ongoing" data-no-store="true" data-next="" data-tmpl="#tmpList">
            
        </div>
        </div>
        <div class="loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <div class="block all-data-loaded is-ended text-center" style="display: none;">
            All Ongoing Events Loaded
        </div>
        <div class="block is-blank text-center" style="display: none;">
            Belum ada data
        </div>
    </div>
</main>
<template id="tmpList">
    {{#each this}}
    {{#each data}}
    <div class="block bzg_c" data-col="m4">
        <div class="card__item">
            <figure class="item-img img-square fill-lightgrey">
                <img src="assets/img/img-preload.png" data-src="{{image}}" alt="{{title}}" class="item-heavy">
                <div class="item-detail">
                    <div class="text-center">
                        <a href="{{detail_url}}" class="btn btn--round btn--ghost-red-black">
                            <b class="text-up">Selengkapnya</b>
                        </a>
                    </div>
                </div>
            </figure>
            <div class="item-text">
                <h4 class="no-space">
                    <a href="{{detail_url}}">{{title}}</a>
                </h4>
            </div>
        </div>
    </div>
    {{/each}}
    {{/each}}
</template>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
