<?php include '_partials/head.php'; ?>
<header class="print-header text-center">
    <img src="assets/img/site-logo.png" alt="">
</header>
<main class="site-main">

    <div class="container container--smaller">

        <div class="block--double no-break-inside">
            <h1 class="block--small">Ringkasan</h1>
            <div class="block--inset border line--small">
                Terima kasih Anda telah melakukan reservasi melalui H.I.S. Travel Indonesia<br>
                Staff kami akan segera menghubungi Anda dalam 1 x 24 Jam Hari Kerja atau maksimal 2 x 24 Jam Hari Kerja. Reservasi Anda belum mengikat selama pembayaran belum diterima oleh pihak H.I.S. Travel Indonesia dan kami tidak bertanggung jawab atas perubahan harga ataupun kesalahan cetak selama proses reservasi belum final.<br>
                Harap catat dan simpan kode reservasi Anda untuk reservasi lebih lanjut.<br>
                Terima kasih
            </div>
        </div>

        <div class="block--double no-break-inside">
            <h2 class="block--small text-up">Informasi Pemesan</h2>
            <ul class="list-nostyle list-iconic t--larger">
                <li>
                    <span class="item-icon fa fa-user"></span>
                    <div class="item-text">Mrs. AULIA PUSPA</div>
                </li>
                <li>
                    <span class="item-icon fa fa-envelope-open-o"></span>
                    <div class="item-text">
                        <a href="mailto:my.email@gmail.com" class="link-black">my.email@gmail.com</a>
                    </div>
                </li>
                <li>
                    <span class="item-icon fa fa-phone"></span>
                    <div class="item-text">
                        <a href="tel:+62-8123456789" class="link-black">+62-8123456789</a>
                    </div>
                </li>
            </ul>
        </div>

        <div class="block--double no-break-inside">
            <h2 class="block--small text-up">Peserta Tour</h2>
            <p class="t-strong">Return by Motorcoach</p>
            <div class="bzg list-traveler">
                <div class="block bzg_c" data-col="m6">
                    <div class="block--inset-small border">
                        <div class="title">Dewasa</div>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>Mrs. Aulia Puspa</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>27 Jan 1990</td>
                                </tr>
                                <tr>
                                    <td>No. Paspor</td>
                                    <td>:</td>
                                    <td>A123546</td>
                                </tr>
                                <tr>
                                    <td>Berlaku Paspor</td>
                                    <td>:</td>
                                    <td>31 Des 2022</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m6">
                    <div class="block--inset-small border">
                        <div class="title">Dewasa</div>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>Mrs. Aulia Puspa</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>27 Jan 1990</td>
                                </tr>
                                <tr>
                                    <td>No. Paspor</td>
                                    <td>:</td>
                                    <td>A123546</td>
                                </tr>
                                <tr>
                                    <td>Berlaku Paspor</td>
                                    <td>:</td>
                                    <td>31 Des 2022</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m6">
                    <div class="block--inset-small border">
                        <div class="title">Dewasa</div>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>Mrs. Aulia Puspa</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>27 Jan 1990</td>
                                </tr>
                                <tr>
                                    <td>No. Paspor</td>
                                    <td>:</td>
                                    <td>A123546</td>
                                </tr>
                                <tr>
                                    <td>Berlaku Paspor</td>
                                    <td>:</td>
                                    <td>31 Des 2022</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="block bzg_c" data-col="m6">
                    <div class="block--inset-small border">
                        <div class="title">Dewasa</div>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>Mrs. Aulia Puspa</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>27 Jan 1990</td>
                                </tr>
                                <tr>
                                    <td>No. Paspor</td>
                                    <td>:</td>
                                    <td>A123546</td>
                                </tr>
                                <tr>
                                    <td>Berlaku Paspor</td>
                                    <td>:</td>
                                    <td>31 Des 2022</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="block--double no-break-inside border">
            <h3 class="no-space text-center text-up block--inset-small">Rincian Pesanan</h3>
            <h3 class="no-space text-center text-up block--inset-small fill-yellow">Japan Rail Pass</h3>
            <div class="block--inset-small">
                <div class="info-iconic--left">
                    <div class="label-icon">
                        <span class="fa fa-calendar"></span>
                    </div>
                    Minggu, 6 Agustus 2017
                </div>
                <div class="info-iconic--left">
                    <div class="label-icon">
                        <span class="his-train"></span>
                    </div>
                    JR Ordinary Pass
                </div>
                <div class="info-iconic--left">
                    <div class="label-icon">
                        <span class="his-alarm"></span>
                    </div>
                    7 Hari
                </div>
                <div class="info-iconic--left">
                    <div class="label-icon">
                        <span class="his-travel-bag"></span>
                    </div>
                    JR Pass
                </div>
                <div class="info-iconic--left">
                    <div class="label-icon">
                        <span class="his-user-group"></span>
                    </div>
                    2 Travelers
                </div>
            </div>
        </div>
        <div class="block--double no-break-inside">
            <h2 class="block--small text-up">Rincian Biaya</h2>
                    <div class="border block--inset t--larger">
                        <table class="no-space table-total">
                            <tbody>
                                <tr>
                                    <td>Dewasa x 2</td>
                                    <td class="text-right">Rp. 3.528.200</td>
                                </tr>
                                <tr>
                                    <td>VAT 1%</td>
                                    <td class="text-right">Rp. 35.282</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td><strong>TOTAL</strong></td>
                                    <td class="text-right text-red t--larger t-strong">
                                        Rp. 3.657.000
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
        </div>

        <button class="btn" onclick="self.close();">X</button>
    </div>
</main>
<script>
    window.onload = function() {
        window.print();
        // window.onfocus = function () { 
        //     setTimeout(function () { 
        //         window.close(); 
        //     }, 500); 
        // }

    }
        function windowClose() {
            window.open('','_parent','');
            window.close();
        }
</script>
<?php include '_partials/scripts.php'; ?>
