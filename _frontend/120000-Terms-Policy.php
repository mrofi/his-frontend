<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Terms &amp; Policy</a></li>
            </ul>
        </div>
    </div>

    <div class="container container--smaller">
        <div class="block">
            <h1 class="no-space">Syarat &amp; Ketentuan Penggunaan</h1>
        </div>
        <article class="block">
            <p>
                Quibusdam quis saepe error atque fugit officiis vel tempore labore, ratione laboriosam eaque, cupiditate, cum voluptate praesentium enim distinctio quo voluptatem exercitationem! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut ut, atque nisi repellendus aliquid omnis amet sapiente earum dolores nesciunt ipsa eaque. Doloribus repellendus amet ipsum, asperiores tempora in impedit.
            </p>
            <h4>HEADING 4</h4>
            <p>
                Aliquid doloribus quibusdam officiis natus cupiditate, vero aut, cumque maxime dicta molestias ipsam quidem? Dolores magni dolor laudantium sunt a. Nesciunt, cum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quibusdam itaque debitis impedit consequatur minima pariatur vero id cupiditate, perspiciatis doloribus accusamus quod incidunt corporis asperiores laborum vitae. Molestias, aliquam.
            </p>
            <p>
                Suscipit perferendis tempora vel itaque nesciunt fugiat magni eos cumque a, iste fuga dignissimos, deleniti rerum provident aut? Officiis soluta maxime sed. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse obcaecati quos sequi quae amet dicta neque debitis, nesciunt alias placeat nisi sed modi quibusdam totam. Voluptatibus excepturi modi ipsum.
            </p>
            <p>
                Veritatis aut dolore repudiandae recusandae aspernatur pariatur nisi corporis ipsa sequi velit laborum reprehenderit, iste autem illo placeat, unde tempora asperiores eos. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque doloribus vel ducimus numquam libero ipsam atque, vero unde ipsa consequatur ipsum tenetur deserunt, nam repellendus incidunt quis necessitatibus consequuntur ex.
            </p>
            <p>
                Nemo voluptate voluptatibus, aspernatur officiis saepe! Laborum perferendis illo, numquam distinctio, est sequi provident dignissimos tempora animi? Reiciendis consequatur officiis nostrum error? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur earum ut perferendis labore ipsam quasi maiores consectetur necessitatibus optio dicta id, esse excepturi! Explicabo veniam obcaecati, id deleniti facere, maxime?
            </p>
            <ul>
                <li>
                    Expedita fugit labore ut harum id delectus quam molestias incidunt aspernatur fuga, error et placeat nemo veritatis, aperiam doloremque, eum quidem odit?
                </li>
                <li>
                    Voluptatum consequatur nemo fugit corporis expedita illo tenetur perferendis sunt, ab laudantium vel iusto, delectus amet maiores facilis, suscipit obcaecati blanditiis ipsa.
                </li>
                <li>
                    Nulla, asperiores totam dolorum, aperiam ratione excepturi veritatis enim facere autem quo tempore commodi accusantium, laudantium quibusdam est reiciendis necessitatibus, soluta nemo.
                </li>
                <li>
                    Nulla laudantium nihil accusantium praesentium aperiam voluptatem fugiat perferendis inventore harum iure tempore, cupiditate id repudiandae, necessitatibus qui expedita iusto, deserunt ut!
                </li>
                <li>
                    Error aspernatur quis ad ipsam consequuntur exercitationem sint, accusantium debitis, perspiciatis ratione consequatur amet repellat quaerat laborum veritatis unde laudantium omnis architecto.
                </li>
            </ul>
            <h3>Accordeon Content</h3>
            <ul class="list-nostyle accordeon">
                <li class="is-active">
                    <h3 class="accordeon-trigger no-icon">
                        Untuk Paket Wisata Berbasis Individu
                    </h3>
                    <div class="accordeon-content">
                        Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
                        <ul>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
                            </li>
                            <li>
                                Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <h3 class="accordeon-trigger no-icon">
                        Untuk Paket Wisata Berbasis Individu
                    </h3>
                    <div class="accordeon-content">
                        Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
                        <ul>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
                            </li>
                            <li>
                                Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <h3 class="accordeon-trigger no-icon">
                        Untuk Paket Wisata Berbasis Individu
                    </h3>
                    <div class="accordeon-content">
                        <figure class="responsive-media">
                            <iframe class="video-iframe" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" gesture="media" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe>
                        </figure>

                        <ul>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
                            </li>
                            <li>
                                Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <h3 class="accordeon-trigger no-icon">
                        Untuk Paket Wisata Berbasis Individu
                    </h3>
                    <div class="accordeon-content">
                        <img src="//placehold.it/600x600" alt="">
                    </div>
                </li>
            </ul>
        </article>
        <hr class="hr-dash">
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
