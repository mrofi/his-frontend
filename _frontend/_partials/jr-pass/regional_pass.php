<div id="regional_pass">
	<h3 class="title-group">
		<span class="title-icon fa-1-5x his-train i--blue"></span>
		<span class="title-text text-up t--larger">Regional Pass</span>
	</h3>
	<nav class="block fill-lightgrey block--inset text-center text-up">
		<a href="#west_pass" class="btn btn--round btn--ghost-red-black">West</a>
		<a href="#hokkaido_pass" class="btn btn--round btn--ghost-red-black">Hokkaido</a>
		<a href="#kyushu_pass" class="btn btn--round btn--ghost-red-black">Kyushu</a>
	</nav>
	<div class="block" id="west_pass">
		<h4 class="hl-b sp-b text-up text-blue">West Rail Pass</h4>
		<h4 class="title-group hl-b-ds">
			<span class="title-icon fa-2x his-train"></span>
			<span class="title-text text-up">Tokyo Osaka Hokuriku Pass</span>
		</h4>
		<div class="table-container">
			<table class="table--line">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th>Purchase outside Japan</th>
						<th>Purchase inside Japan</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>7 Hari</td>
						<td><p>Dewasa</p><p>Anak</p></td>
						<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
						<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="accordeon">
			<!-- <div class="accordeon__item is-active">
				<h4 class="title-q accordeon-trigger">
					<span class="title-icon fa fa-map-marker"></span>
					Lihat Rute
				</h4>
				<div class="accordeon-content">
					<php for ($i=1; $i <= 2; $i++) { ?>
					<h4 class="title-group hl-b-ds">
						<span class="title-icon fa-2x his-train"></span>
						<span class="title-text text-up">Hokuriku Area Pass</span>
					</h4>
					<div class="table-container">
						<table class="table--line table--auto">
							<tbody>
								<tr>
									<td>4 Hari<br>Hokuriku Area</td>
									<td><p>Dewasa</p><p>Anak</p></td>
									<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
								</tr>
								<tr>
									<td>7 Hari<br>Kansai - Hokuriku Area</td>
									<td><p>Dewasa</p><p>Anak</p></td>
									<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
								</tr>
							</tbody>
						</table>
					</div>
					<php } ?>
				</div>
			</div> -->
			<div class="accordeon__item">
				<h4 class="title-q accordeon-trigger">
					<span class="title-icon fa fa-map-marker"></span>
					Lihat Rute
				</h4>
				<div class="accordeon-content">
					<figure class="border">
						<img src="assets/img/img-preload.png" data-src="assets/img/jrpass-route-2.jpg" alt="" class="item-heavy">
					</figure>
				</div>
			</div>
		</div>
		<div class="text-center">
			<a href="#" class="btn btn--round btn--ghost-red-black">
				<b class="text-up">Informasi Selengkapnya</b>
			</a>
		</div>
	</div>
	<hr>
	<div class="block" id="hokkaido_pass">
		<h4 class="hl-b sp-b text-up text-blue">Hokkaido Rail Pass</h4>
		<h4 class="title-group hl-b-ds">
			<span class="title-icon fa-2x his-train"></span>
			<span class="title-text text-up">Tokyo Osaka Hokuriku Pass</span>
		</h4>
		<div class="table-container">
			<table class="table--line">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th>Purchase outside Japan</th>
						<th>Purchase inside Japan</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>7 Hari</td>
						<td><p>Dewasa</p><p>Anak</p></td>
						<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
						<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="accordeon">
			<!-- <div class="accordeon__item is-active">
				<h4 class="title-q accordeon-trigger">
					<span class="title-icon fa fa-map-marker"></span>
					Lihat Rute
				</h4>
				<div class="accordeon-content">
					<php for ($i=1; $i <= 2; $i++) { ?>
					<h4 class="title-group hl-b-ds">
						<span class="title-icon fa-2x his-train"></span>
						<span class="title-text text-up">Hokuriku Area Pass</span>
					</h4>
					<div class="table-container">
						<table class="table--line table--auto">
							<tbody>
								<tr>
									<td>4 Hari<br>Hokuriku Area</td>
									<td><p>Dewasa</p><p>Anak</p></td>
									<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
								</tr>
								<tr>
									<td>7 Hari<br>Kansai - Hokuriku Area</td>
									<td><p>Dewasa</p><p>Anak</p></td>
									<td class="text-center"><p>IDR 1.230.000</p><p>IDR 1.230.000</p></td>
								</tr>
							</tbody>
						</table>
					</div>
					<php } ?>
				</div>
			</div> -->
			<div class="accordeon__item">
				<h4 class="title-q accordeon-trigger">
					<span class="title-icon fa fa-map-marker"></span>
					Lihat Rute
				</h4>
				<div class="accordeon-content">
					<figure class="border">
						<img src="assets/img/img-preload.png" data-src="assets/img/jrpass-route-2.jpg" alt="" class="item-heavy">
					</figure>
				</div>
			</div>
		</div>
		<div class="text-center">
			<a href="#" class="btn btn--round btn--ghost-red-black">
				<b class="text-up">Informasi Selengkapnya</b>
			</a>
		</div>
	</div>
	<hr>
</div>