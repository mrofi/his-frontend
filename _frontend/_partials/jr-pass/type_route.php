<div id="type_route">
	<h3 class="title-group">
		<span class="title-icon fa-1-5x his-train i--blue"></span>
		<span class="title-text text-up t--larger">Tipe &amp; Rute</span>
	</h3>
	<p>
		Ada dua macam JAPAN RAIL PASS : Ordinary dan Green. Masing-masing dari tipe tersebut tersedia dalam tiga durasi; 7 hari, 14 hari, dan 21 hari
	</p>
	<div class="block table-container">
		<table class="table--big-gap">
			<thead>
				<tr>
					<th colspan="2" class="hl-b"></th>
					<th>
						<div class="fill-pink-light">
							Ordinary Pass
						</div>
					</th>
					<th>
						<div class="fill-green-light">
							Green Pass
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="hl-b">7 Hari</td>
					<td class="hl-b">
						<div class="block--inset-small">Dewasa</div>
						<div class="block--inset-small">Anak</div>
					</td>
					<td class="text-center">
						<div class="fill-pink-light block--inset-small">
							Rp. 1.780.000
						</div>
						<div class="fill-pink-light block--inset-small">
							Rp. 1.780.000
						</div>
					</td>
					<td class="text-center">
						<div class="fill-green-light block--inset-small">
							Rp. 1.780.000
						</div>
						<div class="fill-green-light block--inset-small">
							Rp. 1.780.000
						</div>
					</td>
				</tr>
				<tr>
					<td class="hl-b">7 Hari</td>
					<td class="hl-b">
						<div class="block--inset-small">Dewasa</div>
						<div class="block--inset-small">Anak</div>
					</td>
					<td class="text-center">
						<div class="fill-pink-light block--inset-small">
							Rp. 1.780.000
						</div>
						<div class="fill-pink-light block--inset-small">
							Rp. 1.780.000
						</div>
					</td>
					<td class="text-center">
						<div class="fill-green-light block--inset-small">
							Rp. 1.780.000
						</div>
						<div class="fill-green-light block--inset-small">
							Rp. 1.780.000
						</div>
					</td>
				</tr>
				<tr>
					<td class="hl-b">7 Hari</td>
					<td class="hl-b">
						<div class="block--inset-small">Dewasa</div>
						<div class="block--inset-small">Anak</div>
					</td>
					<td class="text-center">
						<div class="fill-pink-light block--inset-small">
							Rp. 1.780.000
						</div>
						<div class="fill-pink-light block--inset-small">
							Rp. 1.780.000
						</div>
					</td>
					<td class="text-center">
						<div class="fill-green-light block--inset-small">
							Rp. 1.780.000
						</div>
						<div class="fill-green-light block--inset-small">
							Rp. 1.780.000
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<figure class="border">
		<img src="assets/img/img-preload.png" data-src="assets/img/jrpass-route.jpg" alt="" class="item-heavy">
	</figure>
	<hr>
</div>