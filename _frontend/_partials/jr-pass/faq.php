<div id="faq" class="block--double">
	<h3 class="title-group">
		<span class="title-icon fa-1-5x i--blue">?</span>
		<span class="title-text text-up t--larger">FAQ</span>
	</h3>
	<ul class="list-nostyle accordeon">
		<li class="is-active">
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Penggunaan Japan Rail Pass
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Periode Masa Berlaku Japan Rail PASS			          
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Cara Menggunakan JR Pass
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Pembatalan dan Pengembalian
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
	</ul>
	<hr>
	<h4 class="text-up">Panduan Pengguanaan JR Pass</h4>
	<ul class="list-nostyle accordeon">
		<li class="is-active">
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Penggunaan Japan Rail Pass
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Periode Masa Berlaku Japan Rail PASS			          
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Cara Menggunakan JR Pass
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger accordeon-trigger--grey">
				<span class="title-icon fa fa-question-circle"></span>
				Pembatalan dan Pengembalian
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
	</ul>
</div>