<div id="description">
	<div class="block fill-lightgrey block--inset">
		<h3 class="title-group">
			<span class="title-icon fa fa-1-5x fa-search i--blue"></span>
			<span class="title-text text-up t--larger">Deskripsi</span>
		</h3>
		<h3>JAPAN RAIL PASS & JAPAN AREA PASS</h3>
			Semua bisa! JAPAN RAIL PASS…kunci kenyamanan berwisata di Jepang. Japan Rail Pass merupakan cara paling ekonomis untuk perjalanan mengelilingi Jepang dengan kereta. Namun, ada beberapa batasan, yaitu JR PASS baik Ordinary maupun Green tidak berlaku untuk kereta “NOZOMI” dan “MIZUHO” yaitu jalur Tokaido, Sanyo, dan Kyushu. JR PASS dapat dibeli di seluruh cabang HIS Indonesia (Jakarta, Bandung, Jogjakarta, dan Surabaya).
	</div>

	<h4>Siapa saja yang bisa naik JR PASS?</h4>
	<p>
		JR PASS adalah tiket khusus yang berlaku untuk Wisatawan dari luar negeri yang mengunjungi Jepang untuk wisata. Ketentuan pengguna JR PASS dapat dilihat di bawah ini:
	</p>
	<ul class="list-nostyle bzg bzg--half">
		<li>
			<h4 class="block--half title-group title-line">
				<span class="title-icon fa fa-group"></span>
				<span class="title-text">Wisatawan Asing</span>
			</h4>
			<p>
				Wisatawan Asing yang Datang ke Jepang untuk Wisata dengan Status sebagai Pengunjung Sementara Berdasarkan undang-undang imigrasi Jepang, wisatawan asing dengan status “pengunjung sementara” diperbolehkan untuk tinggal di Jepang selama 15 sampai 90 hari dengan tujuan wisata atau lain-lain. Jika Anda mengajukan permohonan visa untuk wisata ketika masuk Jepang, petugas imigrasi akan mencap paspor sebagai “pengunjung sementara”. Hanya paspor yang ada cap tersebut yang dapat menggunakan JR PASS.
			</p>
		</li>
		<li>
			<h4 class="block--half title-group title-line">
				<span class="title-icon fa fa-group"></span>
				<span class="title-text">Wisatawan Asing</span>
			</h4>
			<p>
				Orang Jepang yang Tinggal di Negara luar<br>
				a. Orang Jepang yang memenuhi syarat untuk tinggal secara permanen di Negara tersebut, atau<br>
				b. Orang Jepang yang menikah dengan non-Jepang yang tinggal di Negara luar selain Jepang.
			</p>
			<p>
				Note:<br>
				* orang Jepang yang memiliki 2 kebangsaan (Jepang dan non-Jepang) dan memiliki paspor non-Jepang, meskipun memiliki bukti izin tinggal permanen di Negara tersebut, tidak dapat menggunakan JR PASS.<br>
				* orang Jepang yang berada dalam kondisi di atas perlu menunjukkan voucher JR PASS pada tempat pembelian JR PASS yang sama.
			</p>
		</li>
	</ul>
	<hr>
</div>