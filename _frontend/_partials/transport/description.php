<div id="tour_point">
	<div class="block fill-lightgrey block--inset">
		<h3 class="title-group">
			<span class="title-icon fa fa-1-5x fa-star i--blue"></span>
			<span class="title-text text-up t--larger">Description</span>
		</h3>
		<article class="clearfix">
			<img src="//placehold.it/300x200" alt="" style="float: left; margin-right: 16px;">
			<p>
				Airport Limousine Bus Melayani langsung door to door dari depan lobi kedatangan di Bandara Narita sampai ke lobi di hotel-hotel besar di Tokyo, tanpa dikenakan biaya bagasi! Anda akan diberikan akses pulang pergi antara bandara Narita dan pusat kota Tokyo yang terpisah jarak sekitar 60 kilometer.
			</p>
			<div class="cf"></div>
			<p>&nbsp;</p>
			<h4>Keuntungan Menggunakan Airport Limousine Bus</h4>
			<p>
				<strong>Akses Terbaik antara Bandara Narita dengan Pusat Kota Tokyo</strong><br>
				Untuk penumpang dari kedatangan Bandara Narita: Konter Airport Limousine terletak di lobi kedatangan (di depan pintu keluar) pada terminal 1 dan 2.
			</p>
			<p>
				<strong>Kenapa Limousine, bukan Kereta?</strong><br>
				Layanan utama service door-to-door langsung antara Bandara dengan hotel-hotel besar di Tokyo tanpa dikenakan biaya bagasi dan mudah diakses. Anda tidak perlu naik turun untuk naik kereta, tapi bisa duduk santai untuk menikmati pemandangan di perjalanan bersama kami. Airport Limousine Bus menyambut Anda dari depan lobi kedatangan di bandara langsung ke tujuan Anda termasuk lobi hotel-hotel besar. Anda akan tepat sampai pada tujuan dan tidak perlu jalan ekstra atau naik taksi ke/dari stasiun kereta.
			</p>
		</article>
	</div>
	<hr>
</div>