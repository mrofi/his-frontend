
    <div class="page-loader">
    <!-- <div class="page-loader is-active"> -->
        <div class="loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <div class="tool-tip"></div>
    <section id="installBanner" class="p-16 banner-install" style="opacity: 0;">
        <div class="block flex">
            <img src="<?= isset($path) ? $path : '' ?>assets/img/launcher-icon-2x.png" width="48" alt="HIS">
            <div class="ml-half">
                <h4 class="no-space">Install HIS App?</h4>
                <small>HIS Travel Progresive Web App</small>
            </div>
        </div>
        <div class="flex">
            <button id="skipInstallBtn" class="btn btn--block mr-small">Lain kali</button>
            <button id="installBtn" class="btn btn--block btn--red">Install</button>
        </div>
    </section>
    <!-- <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch"></script> temporary -->
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/modernizr.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/waypoint.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.counterup.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/headroom.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/baze.validate.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/object-fit-images.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/slick.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/moment.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.jquery.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.selectric.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.unveil.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.nav.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/sticky.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/handlebars.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/handlebars-intl-with-locales.min.js"></script>
    <!-- <script src="<= isset($path) ? $path : '' ?>assets/js/vendor/upup.min.js"></script> -->
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/baguetteBox.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.zoom.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/nouislider.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/select2.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/qrcode.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/nprogress.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDT39m_nMmH9JsWdNQxMbizNX91nKgyUZ8&language=id" defer></script>
    <!-- <script>window.gmapsAPIKey = `AIzaSyDT39m_nMmH9JsWdNQxMbizNX91nKgyUZ8&language=id`</script> -->
    <script src="<?= isset($path) ? $path : '' ?>assets/js/main.min.js"></script>

    <script>
        // window.onload = function() {(
            // Site._popMsg('Situs Bahaya', 'danger', 100000)

            // Site._dialog('Apakah kamu yakin', '//suitmedia.com', 'Yakin dong', 'Gak jadi')
        // )}
    </script>
    <script>
        if ('serviceWorker' in navigator) {
            window.addEventListener('load', () => {
                navigator.serviceWorker.register(cachePath+'baze-sw.js')
            });
        }
    </script>
    <style type="text/css">
        body {
            opacity: 1;
        }
    </style>
</body>
</html>
