
<section class="section-block">
    <div class="container container--smaller">
        <div class="block section-head clearfix">
            <h2 class="no-space text-up in-block">
                Travel Tips / Destinasi Lainnya
            </h2>
        </div>
        <div class="block content-section">
            <div class="bzg cards cards--static cards--static-overlay">
                <?php for ($i=1; $i <= 3; $i++) { ?>
                <div class="block bzg_c" data-col="x6, m3">
                    <div class="card__item">
                        <figure class="item-img img-square fill-lightgrey">
                            <img data-src="//placehold.it/420x420" alt="" class="lazyload">
                        </figure>
                        <div class="item-text text-center">
                            <a href="#" class="lfs-24 no-space fit-cards">
                                Asia
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="block bzg_c" data-col="x6, m3">
                    <div class="card__item cards__plain">
                        <a href="#" class="btn btn--square">
                            <strong class="text-up t--huge">
                                Lihat Lainnya
                                <span class="btn-icon fa fa-caret-right"></span>
                            </strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>