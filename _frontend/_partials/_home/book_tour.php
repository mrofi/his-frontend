<div id="book_tour" class="tab-panel">
    <form action="" class="form">
        <!-- <div class="form__row">
            <label for="check_group_tour" class="form-label--inline">
                <input type="radio" name="book-tour" id="check_group_tour">
                Group Tour
            </label>
            <label for="check_individual_tour" class="form-label--inline">
                <input type="radio" name="book-tour" id="check_individual_tour">
                Individual Tour
            </label>
        </div> -->
        <div class="bzg bzg--lite-gutter">
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Pilih Region</option>
                    <option value="">Asia</option>
                    <option value="">Amerika</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Pilih Negara</option>
                    <option value="" disabled>Korea Utara</option>
                    <option value="">India</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l4" data-offset="m1">
                <div class="input-iconic">
                    <!-- <input type="text" class="form-input form-input--block pickaDay"
                    readonly id="tour_date"> default -->
                    <!-- <input type="text" class="form-input form-input--block pickaDay"
                    data-date='["2018-07-31", "2018-08-20", "2018-08-22", "2018-08-30"]' readonly id="tour_date"> enabled date -->
                    <input type="text" class="form-input form-input--block pickaDay"
                    data-date='["2019-03-06", "2019-03-22", "2019-03-24", "2019-03-25", "2019-03-30"]' data-start="2019-03-06" value="" readonly id="tour_date" data-max-date="2030-12-31">
                    <!-- enabled date -->
                    <!-- <input type="text" class="form-input form-input--block pickaDay"
                    data-start="2018-10-20" value="" readonly id="tour_date" placeholder="Pilih tanggal"> -->
                    <!-- set start date -->
                    <label for="tour_date" class="label-icon">
                        <span class="fa fa-calendar"></span>
                    </label>
                </div>
            </div>
            <div class="form__row bzg_c" data-col="m10, l2" data-offset="m1">
                <button class="btn btn--block btn--red text-up">Cari</button>
            </div>
        </div>
        <!-- <div class="flex--l">
            <div class="form__row fg-1">

            </div>
            <div class="form__row fg-1">

            </div>
            <div class="form__row fg-1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Pilih Jenis Tour</option>
                    <option value="">Group Tour</option>
                    <option value="">Individual Tour</option>
                </select>
            </div>
            <div class="form__row fg-1">

            </div>
            <div>

            </div>
        </div> -->
    </form>
</div>
