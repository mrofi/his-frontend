<section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Theme Park</a>
                </h2>
                <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Lainnya</b>
                </a>
            </div>
            <div class="content-section">
                <div class="bzg cards cards--blue slide-on-mobile coverSlide" data-slide-small="1" data-slide-medium="3" data-slick='{"arrows": false, "dots": true}' data-center="true">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c slides__item" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <a href="#" class="link-shadow" aria-label="KOREA JEJU PLUS HAN"></a>
                                <img src="assets/img/img-preload.png" 
                                data-src="//via.placeholder.com/512?text=TP<?= $i ?>" alt="" class="item-heavy lazyload">
                                <div class="ribbon ribbon--gold">
                                    <span class="ribbon__text">Recommended</span>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2 title">
                                        KOREA JEJU PLUS HAN
                                    </strong>
                                    <div class="cf">
                                        <strong class="t-switch-red-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--round btn--ghost-red">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
