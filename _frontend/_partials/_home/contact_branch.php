<div id="contact_branch" class="tab-panel is-active">
	<form action="" class="form">
		<div class="mb-half m-center">
			Lebih dekat dengan HIS selama WFH. Hubungi cabang terdekat di kotamu!
		</div>
		<div class="bzg">
			<div class="form__row bzg_c" data-col="m6, l4">
				<select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Pilih Kota</option>
                    <option value="">Jakarta</option>
                    <option value="">Pekalongan</option>
                </select>
			</div>
			<div class="form__row bzg_c" data-col="m6, l4">
				<select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Pilih Cabang</option>
                    <option value="">Jakarta</option>
                    <option value="">Pekalongan</option>
                </select>
			</div>
			<div class="form__row bzg_c" data-col="m6, l4" data-offset="m3">
				<button class="btn btn--block btn--red text-up">
					<span class="fa fa-1-5x fa-whatsapp t--top mr-small"></span>
					Hubungi via whatsapp
				</button>
			</div>
		</div>
	</form>
</div>