<section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Kami menerima Pembayaran</a>
                </h2>
                <!-- <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Lainnya</b>
                </a> -->
            </div>
            <div class="block--double content-section">
                <ul class="list-nostyle bzg slide-on-mobile slide-push-arrow" data-slide-small="3" data-slide-medium="5" data-slick='{"arrows": true, "dots": false}' data-center="false">
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-mandiri.png" alt="Logo"></a>
                        </figure>
                    </li>
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-mufg.png" alt="Logo"></a>
                        </figure>
                    </li>
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-bca.png" alt="Logo"></a>
                        </figure>
                    </li>
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-smbc.png" alt="Logo"></a>
                        </figure>
                    </li>
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-mizuho.png" alt="Logo"></a>
                        </figure>
                    </li>
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-bang-resona.png" alt="Logo"></a>
                        </figure>
                    </li>
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/log-anz.png" alt="Logo"></a>
                        </figure>
                    </li>
                    <li class="bzg_c block" data-col="x4, m2, l1">
                        <figure class="no-space block--inset-smallest border border--round">
                            <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-wings.png" alt="Logo"></a>
                        </figure>
                    </li>
                </ul>
            </div>
        </div>
    </section>
