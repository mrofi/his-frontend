<section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Testimonial</a>
                </h2>
                <!-- <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Lainnya</b>
                </a> -->
            </div>
            <div class="block--double content-section">
                <div class="slide-3 slide-push-arrow text-center">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="slide__item block--inset">
                        <figure class="in-block">
                            <img data-src="assets/img/user-150.jpg" width="150" alt="" class="circle size-150 lazyload">
                        </figure>
                        <h3 class="no-space">Hamis - Traveler</h3>
                        <blockquote class="no-space">
                            Perferendis delectus repudiandae vitae ducimus cumque, iusto repellat vero amet consequuntur, suscipit dolores ipsum voluptatum voluptatibus, eos error ipsa quos fugiat quae.
                        </blockquote>
                    </div>
                    <div class="slide__item block--inset">
                        <figure class="in-block">
                            <img data-src="assets/img/user3-150.jpg" width="150" alt="" class="circle size-150 lazyload">
                        </figure>
                        <h3 class="no-space">Nadin - Traveler</h3>
                        <blockquote class="no-space">
                            Lorem voluptates quod nesciunt mollitia quidem sapiente, iusto veniam quisquam commodi quia recusandae quam repellat quae ut est ipsa voluptatibus eligendi alias rerum.
                        </blockquote>
                    </div>
                    <div class="slide__item block--inset">
                        <figure class="in-block">
                            <img data-src="assets/img/user2-150.jpg" width="150" alt="" class="circle size-150 lazyload">
                        </figure>
                        <h3 class="no-space">Via - Celebrity</h3>
                        <blockquote class="no-space">
                            Quos facere ad exercitationem recusandae fuga reprehenderit sit dolorum aliquam optio vero deserunt quis provident, harum est! Neque quaerat expedita iusto aut.
                        </blockquote>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr>
        </div>
    </section>