<div id="book_wifi" class="tab-panel">
    <form action="" class="form">
        <div class="bzg bzg--lite-gutter">
            <div class="form__row bzg_c" data-col="m10, l5" data-offset="m1">
                <input type="text" class="form-input form-input--block" placeholder="Negara Tujuan">
            </div>
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Tipe Wifi</option>
                    <option value="">Tipe Wifi 2</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Durasi</option>
                    <option value="">Durasi 2</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l1" data-offset="m1">
                <button class="btn btn--red btn--block">Cari</button>
            </div>
        </div>
    </form>
</div>