<section class="section-block section-block--inset bg-section" >
    <img data-src="assets/img/bg-counter.jpg" alt="" class="bg-section-img lazyload">
    <div class="container container--smaller text-white content-section">
        <div class="bzg a-center text-center text-up">
            <div class="block bzg_c text-center" data-col="m3">
                <img src="assets/img/icon-around-world.png" alt="" width="120">
            </div>
            <div class="block bzg_c" data-col="x4, m3">
                <h3 class="no-space">
                    <span class="t--huge count-up" data-counter="counter_up" data-value="316" data-start="0">
                        &middot; &middot; &middot;
                    </span>
                </h3>
                <span class="bg-white-capsule">Paket Tour</span>
            </div>
            <div class="block bzg_c" data-col="x4, m3">
                <h3 class="no-space">
                    <span class="t--huge count-up" data-counter="counter_up" data-value="28" data-start="0">
                        &middot; &middot; &middot;
                    </span>
                </h3>
                <span class="bg-white-capsule">Cabang</span>
            </div>
            <div class="block bzg_c" data-col="x4, m3">
                <h3 class="no-space">
                    <span class="t--huge count-up" data-counter="counter_up" data-value="1057" data-start="0">
                        &middot; &middot; &middot;
                    </span>
                </h3>
                <span class="bg-white-capsule">Destinasi</span>
            </div>
        </div>
    </div>
</section>
