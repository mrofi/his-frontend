<section class="section-booking section-booking--hero">
        <div class="container">
            <nav class="tab-nav tab-nav--overflow">
                <ul class="list-nostyle text-up">
                    <li class="tab-nav__item is-active">
                        <a href="#contact_branch">
                            <span class="tab-icon his-branch" aria-hidden="true"></span>
                            Branch
                            <span class="label-warning">NEW</span>
                        </a>
                    </li>
                    <li class="tab-nav__item">
                        <a href="#book_tour">
                            <span class="tab-icon his-koper" aria-hidden="true"></span>
                            Tour
                        </a>
                    </li>
                    <li class="tab-nav__item">
                        <a href="#book_flight">
                            <span class="tab-icon his-pesawat" aria-hidden="true"></span>
                            Tiket Pesawat
                        </a>
                    </li>
                    <li class="tab-nav__item">
                        <a href="#book_hotel">
                            <span class="tab-icon his-hotel" aria-hidden="true"></span>
                            Hotel
                        </a>
                    </li>
                    <li class="tab-nav__item">
                        <a href="#book_activity">
                            <span class="tab-icon his-trees" aria-hidden="true"></span>
                            Aktivitas
                        </a>
                    </li>
                    <li class="tab-nav__item">
                        <a href="#book_wifi">
                            <span class="tab-icon his-wifi" aria-hidden="true"></span>
                            Wifi
                        </a>
                    </li>
                    <li class="tab-nav__item">
                        <a href="#book_jrpass">
                            <span class="tab-icon his-train" aria-hidden="true"></span>
                            JR Pass
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- nav -->
            <div class="tab-panels">
                <?php include 'contact_branch.php'; ?>
                <?php include 'book_tour.php'; ?>
                <?php include 'book_activity.php'; ?>
                <?php include 'book_flight.php'; ?>
                <?php include 'book_hotel.php'; ?>
                <?php include 'book_wifi.php'; ?>
                <?php include 'book_jrpass.php'; ?>
            </div>
        </div>
    </section>
