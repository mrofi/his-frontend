<section class="section-block">
        <div class="container container--smaller">
            <div class="block--double content-section">
                <div class="slide-video">
                    <?php for ($i=1; $i <= 4; $i++) { ?>
                    <div class="block--half slide-video__item">
                        <div class="bzg bzg--v-center l-reverse">
                            <div class="block bzg_c" data-col="m6, l7">
                                <div class="responsive-media">
                                    <!-- <iframe class="video-iframe" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe> -->
                                    <iframe class="video-iframe" src="" data-src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" gesture="media" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="block bzg_c" data-col="m6, l5">
                                <h3 class="text-up">HISGO - Optional Tour</h3>
                                <div class="block t--larger">
                                    Rasakan pengalaman terbaik liburanmu di berbagai tempat-tempat pilihan seperti Gunung Fuji, Desert Safari Dubai, atau Bangkok Wonderful Pearl Dinner Cruise.
                                </div>
                                <a href="#" class="btn btn--round btn--red">
                                    <b class="text-up">Selengkapnya</b>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="block--half slide-video__item">
                        <div class="bzg bzg--v-center l-reverse">
                            <div class="block bzg_c" data-col="m6, l7">
                                <div class="responsive-media">
                                    <!-- <iframe class="video-iframe" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe> -->
                                    <iframe class="video-iframe" src="" data-src="https://www.youtube.com/embed/9Lw2MGgDcMk?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" gesture="media" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="block bzg_c" data-col="m6, l5">
                                <h3 class="text-up">HISGO - Optional Tour</h3>
                                <div class="block t--larger">
                                    Rasakan pengalaman terbaik liburanmu di berbagai tempat-tempat pilihan seperti Gunung Fuji, Desert Safari Dubai, atau Bangkok Wonderful Pearl Dinner Cruise.
                                </div>
                                <a href="#" class="btn btn--round btn--red">
                                    <b class="text-up">Selengkapnya</b>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="no-space">
        </div>
    </section>