<section class="section-block">
        <div class="container container--smaller">
            <!-- <div class="block section-head clearfix"> -->
                <!-- <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Prefecture Japan</a>
                </h2> -->
                <!-- <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Lainnya</b>
                </a> -->
            <!-- </div> -->
            <div class="block content-section inset-on-m">
                <div class="bzg cards cards--static slide-on-mobile coverSlide" data-slide-small="1" data-slide-medium="3" data-slick='{"arrows": false, "dots": true}' data-center="true">
                    <?php for ($i=1; $i <= 4; $i++) { ?>
                    <div class="block bzg_c" data-col="m3">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <img data-src="//placehold.it/420x420" alt="" class="lazyload">
                                <div class="item-detail">
                                    <div class="text-center">
                                        <!-- <h3 class="text-up">
                                            Brunei Darusalam Raya
                                        </h3> -->
                                        <a href="#" class="btn btn--round btn--ghost-red-black">
                                            <b class="text-up">Selengkapnya</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text text-center">
                                <a href="#" class="mfs-36 no-space fit-cards">
                                    <strong class="text-script">
                                        Kagoshima
                                    </strong>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr>
        </div>
    </section>