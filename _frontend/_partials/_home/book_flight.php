<div id="book_flight" class="tab-panel">
    <form action="" class="form">
        <div class="bzg bzg--lite-gutter">
            <div class="form__row bzg_c" data-col="m10, l12" data-offset="m1">
                <label for="sekali_jalan" class="form-label--inline">
                    <input type="radio" id="sekali_jalan" name="trip_type-home" data-trip="one_way">
                    <span class="btn-label__text">Sekali Jalan</span>
                </label>
                <label for="pulang_pergi" class="form-label--inline">
                    <input type="radio" id="pulang_pergi" name="trip_type-home" data-trip="round_trip" checked>
                    <span class="btn-label__text">Pulang Pergi</span>
                </label>
            </div>
            <div class="form__row bzg_c" data-col="m10, l2" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Asal</option>
                    <option value="JKT">Jakarta</option><option value="SUB">Surabaya</option><option value="BDO">Bandung</option><option value="UPG">Makassar</option><option value="DPS">Denpasar Bali</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l2" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Tujuan</option>
                    <option value="TYO">TOKYO</option><option value="NRT">Tokyo, TOKYO NARITA (NRT)</option><option value="HND">Tokyo, TOKYO HANEDA (HND)</option><option value="OSA">OSAKA</option><option value="KIX">Osaka, OSAKA KANSAI INTERNATIONAL (KIX)</option><option value="NGO">NAGOYA</option><option value="FUK">Fukuoka</option><option value="SPK">SAPPORO CHITOSE</option><option value="AKJ">ASAHIKAWA</option><option value="HKD">HAKODATE</option><option value="OBO">OBIHIRO</option><option value="SDJ">Sendai</option><option value="AOJ">AOMORI</option><option value="MSJ">MISAWA</option><option value="AXT">AKITA</option><option value="ONJ">ODATE NOSHIRO</option><option value="HNA">HANAMAKI</option><option value="GAJ">YAMAGATA JUNMACHI</option><option value="SYO">SHONAI</option><option value="FKS">FUKUSHIMA</option><option value="IBR">IBARAKI</option><option value="KIJ">NIIGATA</option><option value="KMQ">KOMATSU</option><option value="TOY">TOYAMA</option><option value="QSZ">SHIZUOKA</option><option value="TAK">TAKAMATSU</option><option value="KCZ">KOCHI</option><option value="MYJ">MATSUYAMA</option><option value="TKS">TOKUSHIMA</option><option value="HIJ">Hiroshima</option><option value="OKJ">OKAYAMA</option><option value="UBJ">UBE</option><option value="YGJ">YONAGO MIHO</option><option value="IZO">IZUMO</option><option value="TTJ">TOTTORI</option><option value="IWJ">IWAMI</option><option value="KKJ">KITA KYUSHU KOKURA</option><option value="HSG">SAGA</option><option value="NGS">NAGASAKI</option><option value="OIT">OITA</option><option value="KMJ">KUMAMOTO</option><option value="KMI">MIYAZAKI</option><option value="KOJ">Kagoshima</option><option value="OKA">Okinawa</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l4" data-offset="m1">
                <div class="bzg bzg--lite-gutter pickdate-range" 
                data-min-start="2019-08-30" 
                data-max-end="2020-06-30">
                    <div class="form__row bzg_c" data-col="l6">
                        <div class="input-iconic">
                            <input type="text" class="form-input form-input--block pikaRange pikaRange--start" id="flight_start" placeholder="Tanggal Berangkat">
                            <label for="flight_start" class="label-icon">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form__row bzg_c" data-col="l6">
                        <div class="input-iconic" id="trip_type-home">
                            <input type="text" class="form-input form-input--block pikaRange pikaRange--end" id="flight_end" placeholder="Tanggal Kembali">
                            <label for="flight_end" class="label-icon">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <!-- pickdate-range -->
            </div>
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <div class="pick-guest" id="pickFlightPsg" data-toggle>
                    <button class="form-input form-input--block btn-toggle" type="button">
                        <strong class="text-ellipsis">
                            <span class="total-input--all" id="total_psg">1</span> Traveller(s)
                            <span class="additional-info" id="psg_add_info">, Ekonomi</span>
                        </strong>
                    </button>
                    <div class="toggle-panel t-black fill-white">
                        <div class="block--inset-small">
                            <div class="form__row flex v-ct qty-input">
                                <label for="" class="fg-1">
                                    <span class="total-input">1</span> Adult
                                </label>
                                <div class="">
                                    <button class="btn btn--plain btn--plain-red" 
                                    data-action="minus" type="button" data-age="adult" data-type="person">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="form-input form-input--redline text-center input-adult" 
                                    value="1" min="1" max="9" data-psg="total_psg" data-step-item="1" data-step="1">
                                    <button class="btn btn--plain btn--plain-red" 
                                    data-action="plus" type="button" data-age="adult" data-type="person">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                            <hr class="block--half">
                            <div class="form__row flex v-ct qty-input">
                                <label for="" class="fg-1">
                                    <span class="total-input">0</span> Children
                                </label>
                                <div class="">
                                    <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="child" type="button" data-type="person">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="form-input form-input--redline text-center" 
                                    value="0" min="0" max="9" data-psg="total_psg" data-step-item="1" data-step="1">
                                    <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="child" type="button" data-type="person">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                            <hr class="block--half">
                            <div class="form__row flex v-ct qty-input">
                                <label for="" class="fg-1">
                                    <span class="total-input">0</span> Infant
                                </label>
                                <div class="">
                                    <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="infant" type="button" data-type="person">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="form-input form-input--redline text-center input-infant" 
                                    value="0" min="0" max="9" data-psg="total_psg" data-step-item="1" data-step="1">
                                    <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="infant" type="button" data-type="person">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                            <hr class="block--half">
                            <div class="form__row">
                                <label for="economy_check" class="btn-check btn-check--radio">
                                    <span class="text-up btn-check-text">Ekonomi</span>
                                    <input type="radio" class="btn-check-input store-val" 
                                    data-val="Ekonomi" data-target="psg_add_info"
                                    name="class_check" checked="checked" id="economy_check">
                                    <span class="input-icon"></span>
                                </label>
                            </div>
                            <div class="form__row">
                                <label for="premium_check" class="btn-check btn-check--radio">
                                    <span class="text-up btn-check-text">Premium Ekonomi</span>
                                    <input type="radio" class="btn-check-input store-val" 
                                    data-val="Premium Ekonomi" data-target="psg_add_info"
                                    name="class_check" id="premium_check">
                                    <span class="input-icon"></span>
                                </label>
                            </div>
                            <div class="form__row">
                                <label for="business_check" class="btn-check btn-check--radio">
                                    <span class="text-up btn-check-text">Bisnis</span>
                                    <input type="radio" class="btn-check-input store-val" 
                                    data-val="Bisnis" data-target="psg_add_info"
                                    name="class_check" id="business_check">
                                    <span class="input-icon"></span>
                                </label>
                            </div>
                            <div class="text-right">
                                <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                    Pilih
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- pick-guest -->
            </div>
            <div class="form__row bzg_c" data-col="m10, l1" data-offset="m1">
                <button class="btn btn--red btn--block">Cari</button>
            </div>
        </div>
    </form>
</div>