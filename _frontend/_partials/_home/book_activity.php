
    <div id="book_activity" class="tab-panel">
        <form action="" class="form">
            
            <div class="bzg bzg--lite-gutter">
                <div class="form__row bzg_c" data-col="m10, l12" data-offset="m1">
                    <label for="check_optional_tour" class="form-label--inline">
                        <input type="radio" name="book-tour" id="check_optional_tour">
                        Optional Tour
                    </label>
                    <label for="check_theme_park" class="form-label--inline">
                        <input type="radio" name="book-tour" id="check_theme_park">
                        Theme Park
                    </label>
                </div>
                <div class="form__row bzg_c" data-col="m8, l10" data-offset="m1">
                    <input type="search" class="form-input form-input--block" placeholder="Cari Kota, Aktivitas yang anda inginkan">
                </div>
                <div class="form__row bzg_c" data-col="m2">
                    <button class="btn btn--block btn--red text-up">Cari</button>                    
                </div>
            </div>
        </form>
    </div>