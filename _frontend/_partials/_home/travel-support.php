<section class="section-block">
    <div class="container container--smaller">
        <div class="block section-head clearfix">
            <h2 class="no-space text-up in-block">
                <a href="#" class="link-black">Travel Support</a>
            </h2>
        </div>
        <div class="content-section">
			<div class="travel-support-slide slide-5 slide-push-arrow cards">
				<?php for ($i=1; $i <= 8; $i++) { ?>
				<div class="card__item">
                    <a href="#">
                        <figure class="item-img img-square fill-lightgrey">
                            <img src="assets/img/img-preload.png" data-src="//placehold.it/300" alt="ratio 1:1" class="item-heavy img-full lazyload">
                        </figure>
                    </a>
                    <!-- <div class="item-text">
                        <a href="#" class="text-up link-black">
                            JR Pass
                        </a>
                        <div class="item-detail">
                            <div class="text-center">
                                <h4 class="block--small text-up line--small">
                                    <span class="">Brunei Darusalam Raya</span>
                                </h4>
                                <a href="#" class="btn btn--round btn--smaller btn--ghost-red">
                                    <b class="text-up">Selengkapnya</b>
                                </a>
                            </div>
                        </div>
                    </div> -->
                </div>
                <?php } ?>
			</div>
        </div>
    </div>
</section>
