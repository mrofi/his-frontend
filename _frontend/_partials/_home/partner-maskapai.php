<section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Partner Maskapai</a>
                </h2>
                <!-- <a href="#" class="btn btn--smaller btn--round btn--ghost-red-black pull-right">
                    <b class="text-up">Lihat Lainnya</b>
                </a> -->
            </div>
            <div class="block--double content-section">
                <ul class="list-nostyle bzg slide-on-mobile slide-push-arrow" data-slide-small="3" data-slide-medium="5" data-slick='{"arrows": true, "dots": false}' data-center="false">
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-sriwijaya.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <?php for ($i=1; $i <= 4; $i++) { ?>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-garuda.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-air-asia.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-citilink.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-lion-air.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-sriwijaya.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-express.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-batik.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <li class="bzg_c block" data-col="l1">
                        <figure class="no-space block--inset-smallest border border--round">
                        <a href="#" class="responsive-media square fill-none"><img class="img-contain lazyload" data-src="assets/img/logo-wings.png" alt="Garuda Indonesia"></a>
                    </figure>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <hr>
        </div>
    </section>
