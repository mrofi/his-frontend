<div id="book_jrpass" class="tab-panel">
    <form action="" class="form">
        <div class="bzg bzg--lite-gutter">
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Tipe JR Pass</option>
                    <option value="">JR Pass Ordinary</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <div class="input-iconic">
                    <input type="text" class="form-input form-input--block pickaDay" id="departure_start" placeholder="Tanggal Berangkat">
                    <label for="departure_start" class="label-icon">
                        <span class="fa fa-calendar"></span>
                    </label>
                </div>
            </div>
            <div class="form__row bzg_c" data-col="m10, l2" data-offset="m1">
                <select name="" id="" class="form-input form-input--block selectstyle">
                    <option value="">Durasi</option>
                    <option value="">Durasi 2</option>
                </select>
            </div>
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <div class="pick-guest" id="pickJRpassPsg" data-min="1" data-toggle>
                    <button class="form-input form-input--block btn-toggle" type="button">
                        <strong class="text-ellipsis">
                            <span class="total-input--all" id="total_psgjr">1</span> Traveller(s)
                        </strong>
                    </button>
                    <div class="toggle-panel t-black fill-white">
                        <div class="block--inset-small">
                            <div class="form__row flex v-ct qty-input">
                                <label for="" class="fg-1">
                                    <span class="total-input">1</span> Adult
                                </label>
                                <div class="">
                                    <button class="btn btn--plain btn--plain-red" data-action="minus" type="button" data-age="adult" data-type="person">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="form-input form-input--redline text-center input-adult" 
                                    value="1" min="1" max="6" data-psg="total_psgjr">
                                    <button class="btn btn--plain btn--plain-red" data-action="plus" type="button" data-age="adult" data-type="person">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                            <hr class="block--half">
                            <div class="form__row flex v-ct qty-input">
                                <label for="" class="fg-1">
                                    <span class="total-input">0</span> Children
                                </label>
                                <div class="">
                                    <button class="btn btn--plain btn--plain-red" data-action="minus" type="button" data-age="child" data-type="person">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="form-input form-input--redline text-center" 
                                    value="0" min="0" max="6" data-psg="total_psgjr">
                                    <button class="btn btn--plain btn--plain-red" data-action="plus" type="button" data-age="child" data-type="person">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                            <hr class="block--half">
                            <div class="form__row flex v-ct qty-input">
                                <label for="" class="fg-1">
                                    <span class="total-input">0</span> Infant
                                </label>
                                <div class="">
                                    <button class="btn btn--plain btn--plain-red" data-action="minus" data-age="infant" type="button" data-type="person">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="form-input form-input--redline text-center input-infant" 
                                    value="0" min="0" max="6" data-psg="total_psgjr">
                                    <button class="btn btn--plain btn--plain-red" data-action="plus" data-age="infant" type="button" data-type="person">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                            <hr class="block--half">
                            <div class="text-right">
                                <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                    Pilih
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- pick-guest -->
            </div>
            <div class="form__row bzg_c" data-col="m10, l1" data-offset="m1">
                <button class="btn btn--red btn--block">Cari</button>
            </div>
        </div>
    </form>
</div>