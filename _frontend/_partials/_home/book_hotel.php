<div id="book_hotel" class="tab-panel">
    <form action="" class="form">
        <div class="bzg bzg--lite-gutter">
            <div class="form__row bzg_c" data-col="m10, l12" data-offset="m1">
                <label for="check_domestic" class="form-label--inline">
                    <input type="radio" name="book-tour" id="check_domestic">
                    Domestic
                </label>
                <label for="check_internatioanal" class="form-label--inline">
                    <input type="radio" name="book-tour" id="check_internatioanal">
                    International
                </label>
            </div>
            <div class="form__row bzg_c" data-col="m10, l4" data-offset="m1">
                <input type="text" class="form-input form-input--block" placeholder="Dimana anda akan menginap?">
            </div>
            <div class="form__row bzg_c" data-col="m10, l4" data-offset="m1">
                <div class="bzg bzg--lite-gutter pickdate-range">
                    <div class="form__row bzg_c" data-col="l6">
                        <div class="input-iconic">
                            <input type="text" class="form-input form-input--block pikaRange pikaRange--start" id="chekin_start" placeholder="Tanggal Check In">
                            <label for="chekin_start" class="label-icon">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form__row bzg_c" data-col="l6">
                        <div class="input-iconic">
                            <input type="text" class="form-input form-input--block pikaRange pikaRange--end" id="chekout_end" placeholder="Tanggal Check Out">
                            <label for="chekout_end" class="label-icon">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <!-- pickdate-range -->
            </div>
            <div class="form__row bzg_c" data-col="m10, l3" data-offset="m1">
                <div class="pick-guest" id="pickHotelGuest" data-toggle>
                    <button class="form-input form-input--block btn-toggle" type="button">
                        <strong class="text-ellipsis">
                            <span class="total-input--all" id="total_guest">0</span> Traveller(s)
                            <span class="additional-info" id="total_room">1</span> Kamar
                        </strong>
                    </button>
                    <div class="toggle-panel t-black fill-white">
                        <div class="block--inset-small">
                            <div class="pick-item" id="pickGuest_1">
                                <div class="form__row flex v-ct qty-input">
                                    <label for="" class="fg-1">
                                        <span class="total-input">0</span> Adult
                                    </label>
                                    <div class="">
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="minus" type="button" data-age="adult" data-type="person">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                        <!-- button -->
                                        <input type="number" class="form-input form-input--redline text-center input-default input-person" 
                                        value="0" min="0" max="6" 
                                        data-psg="total_guest">
                                        <!-- input -->
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="plus" type="button" data-age="adult" data-type="person">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                        <!-- button -->
                                    </div>
                                </div>
                                <hr class="block--half">
                                <div class="form__row flex v-ct qty-input">
                                    <label for="" class="fg-1">
                                        <span class="total-input">0</span> Children
                                    </label>
                                    <div class="">
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="minus" type="button" data-age="child"  data-type="person">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                        <!-- button -->
                                        <input type="number" class="form-input form-input--redline text-center input-person" 
                                        value="0" min="0" max="6" data-psg="total_guest">
                                        <!-- input -->
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="plus" type="button" data-age="child" data-type="person">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                        <!-- button -->
                                    </div>
                                </div>
                                <hr class="block--half">
                            </div>
                            <div class="pick-item" id="pickProduct_1">
                                <div class="form__row flex v-ct qty-input">
                                    <label for="" class="fg-1">
                                        <span class="total-input">1</span> Kamar
                                    </label>
                                    <div class="">
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="minus" type="button" data-age="no-age" data-type="product">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                        <!-- button -->
                                        <input type="number" class="form-input form-input--redline text-center input-product" 
                                        value="1" min="1" max="6" data-psg="total_room">
                                        <!-- input -->
                                        <button class="btn btn--plain btn--plain-red" 
                                        data-action="plus" type="button" data-age="no-age" data-type="product">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                        <!-- button -->
                                    </div>
                                </div>
                                <hr class="block--half">
                            </div>
                            <!-- <input type="hidden" disabled value="0" class="input-infant"> -->
                            <div class="text-right">
                                <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                    Pilih
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- pick-guest -->
            </div>
            <div class="form__row bzg_c" data-col="m10, l1" data-offset="m1">
                <button class="btn btn--red btn--block">Cari</button>
            </div>
        </div>
    </form>
</div>