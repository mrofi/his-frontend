<header class="site-header">
    <div class="site-header__top">
        <div class="container">
            <a href="0001-Home.php" class="site-logo">
                <!-- <svg class="">
                    <image xlink:href="<?= isset($path) ? $path : '' ?>assets/img/site-logo.svg" src="<?= isset($path) ? $path : '' ?>assets/img/site-logo.png"/>
                </svg> -->
                <img src="<?= isset($path) ? $path : '' ?>assets/img/site-logo--new.png" alt="HIS">
                <h1 class="sr-only">H.I.S. Tours & Travel Indonesia | Booking Tiket Pesawat, JR Pass, Visa, Paket Tour, Optional Tour, Hotel,Wifi Rental</h1>
            </a>
            <div class="header-info">
                <div class="office-hour t--smaller">
                    <span class="fa fa-clock-o" aria-hidden="true"></span> 
                    SENIN - JUM’AT 9:00 - 21:00  |   SABTU 9:00 - 18:00
                </div>
                <strong class="location text-up">Indonesia | Jakarta</strong>
            </div>
            <div class="header-contact">
                <p class="block--small header-contact__call">
                    <a href="tel:0215741701" class="in-block t--middle">
                        <span class="fa fa-phone t--middle item-icon" aria-hidden="true"></span>
                        <small class="item-icon--title">Call</small>
                        <span class="t--huge t--middle item-text">021 - 574 1701</span>
                    </a>
                </p>
                <p class="no-space header-contact__mail">
                    <a href="mailto:callcenter.jkt@his-world.com" class="in-block t--middle">
                        <span class="fa fa-envelope t--middle item-icon" aria-hidden="true"></span>
                        <span class="t--middle item-text">callcenter.jkt@his-world.com</span>
                    </a>
                </p>
            </div>
        </div>
    </div>
    <div class="site-header__bottom">
        <div class="container">
            <nav class="main-nav lock-body" data-toggle>
                <button class="btn-toggle" type="button" aria-label="Main Nav">
                    <span class="fa fa-bars icon-open" aria-hidden="true"></span>
                    <span class="fa fa-close icon-close" aria-hidden="true"></span>
                </button>
                <div class="toggle-panel">
                    <ul class="list-nostyle">
                        <li class="main-nav__item on-m">
                            <div class="user-opts flex v-ct">
                                <a href="#" class="circle btn-avatar">
                                    <img src="<?= isset($path) ? $path : '' ?>assets/img/avatar--smallest.jpg" width="50" alt="User">
                                </a>
                                <div class="user-optss-nav">
                                    <a href="#" class="btn btn-enter btn--round btn--ghost text-up">
                                        Daftar
                                    </a>
                                    <a href="#" class="btn btn-enter btn--round btn--ghost text-up">
                                        Masuk
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="main-nav__item">
                            <a href="0001-Home.php"><span class="fa fa-home" aria-hidden="true"></span></a>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Tour</a>
                            <ul class="list-nostyle sub-main-nav">
                                <li class="sub-main-nav__item">
                                    <a href="#">Group Travel</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Individual Travel</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Cruise</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Umrah</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Muslim Tour</a>
                                </li>
                            </ul>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">JR Pass</a>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Aktivitas</a>
                            <ul class="list-nostyle sub-main-nav">
                                <li class="sub-main-nav__item">
                                    <a href="#">Optional Tour</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Theme Park</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Travel Support</a>
                                    <ul class="list-nostyle sub-main-nav">
                                        <li class="sub-main-nav__item">
                                            <a href="#">Wifi</a>
                                            <ul class="list-nostyle sub-main-nav">
                                                <li class="sub-main-nav__item">
                                                    <a href="#">Jepang</a>
                                                </li>
                                                <li class="sub-main-nav__item">
                                                    <a href="#">Global</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="sub-main-nav__item">
                                            <a href="#">Visa</a>
                                        </li>
                                        <li class="sub-main-nav__item">
                                            <a href="#">Asuransi Perjalanan</a>
                                        </li>
                                        <li class="sub-main-nav__item">
                                            <a href="#">Simcard Jepang</a>
                                            <ul class="list-nostyle sub-main-nav">
                                                <li class="sub-main-nav__item">
                                                    <a href="#">GO!GO!SIM</a>
                                                </li>
                                                <li class="sub-main-nav__item">
                                                    <a href="#">DOCOMO</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Tiket Pesawat</a>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Transportasi</a>
                            <ul class="list-nostyle sub-main-nav">
                                <li class="sub-main-nav__item">
                                    <a href="#">LIMOUSINE BUS</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">skyliner</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">osaka city pass</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">osaka amazing pass</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">tokyo subway tiket</a>
                                </li>
                            </ul>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Mice &amp; Corp</a>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Lainnya</a>
                            <ul class="list-nostyle sub-main-nav">
                                <li class="sub-main-nav__item">
                                    <a href="#">sekolah bahasa</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">travel info</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">event</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">galeri</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">karir</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">partnership</a>
                                </li>
                            </ul>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Blog</a>
                        </li>
                        <li class="main-nav__item">
                            <a href="#">Hubungi Kami</a>
                            <ul class="list-nostyle sub-main-nav">
                                <li class="sub-main-nav__item">
                                    <a href="#">Cabang</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Tentang kami</a>
                                    <ul class="list-nostyle sub-main-nav">
                                        <li class="sub-main-nav__item">
                                            <a href="#">Wifi</a>
                                            <ul class="list-nostyle sub-main-nav">
                                                <li class="sub-main-nav__item">
                                                    <a href="#">Jepang</a>
                                                </li>
                                                <li class="sub-main-nav__item">
                                                    <a href="#">Global</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- <li class="main-nav__item">
                            <a href="#">Akun</a>
                            <ul class="list-nostyle sub-main-nav">
                                <li class="sub-main-nav__item">
                                    <a href="#">Register</a>
                                </li>
                                <li class="sub-main-nav__item">
                                    <a href="#">Login</a>
                                    <ul class="list-nostyle sub-main-nav">
                                        <li class="sub-main-nav__item">
                                            <a href="#">Wifi</a>
                                            <ul class="list-nostyle sub-main-nav">
                                                <li class="sub-main-nav__item">
                                                    <a href="#">Jepang</a>
                                                </li>
                                                <li class="sub-main-nav__item">
                                                    <a href="#">Global</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
            </nav>
            <!-- <nav class="user-nav" data-toggle> -->
            <nav class="user-nav <?= isset($notLogin) ? '' : 'is-login' ?>" data-toggle>
                <button class="btn-toggle" type="button">
                    <span class="fa fa-user btn-icon"></span>
                    <span class="btn-text text-ellipsis">
                        <?= isset($notLogin) ? 'MASUK' : 'Hi, Fajar Prasetya' ?>
                    </span>
                    <span class="notif-dot"></span>
                </button>
                <div class="toggle-panel">
                    <?= isset($notLogin) ? '
                    <form action="404.php" method="post" class="form baze-form">
                        <legend class="mb-small t-bold text-up text-blue">Masuk</legend>
                        <hr class="mb-half">
                        <div class="mb-half">
                            <label for="loginEmail" class="text-blue t-bold">
                                E-Mail
                            </label>
                            <input type="email" id="loginEmail" class="form-input form-input--block" required> 
                        </div>
                        <div class="mb-half">
                            <div class="flex">
                                <label for="loginPassword" class="text-blue t-bold fg-1">
                                    Password
                                </label>
                                <a href="#" class="text-blue">Lupa Kata Sandi?</a>
                            </div>
                            <div class="input-peeker">
                                <input type="password" id="loginPassword" class="form-input form-input--block input-peek">
                                <button class="btn-peek" type="button" aria-label="Show" data-input="#loginPassword">
                                    <span class="fa fa-eye"></span>
                                </button>
                            </div>
                        </div>
                        <div class="mb-half flex">
                            <button class="btn btn--red" type="submit">
                                MASUK
                            </button>
                            <div class="ml-small">
                                <small class="t-black">Belum punya account?</small><br>
                                <a href="#" class="text-blue">Daftar Sekarang</a>
                            </div>
                        </div>
                        <div class="text-center">
                            <small class="t-black">Masuk Dengan</small>
                            <div class="flex f-fixed">
                                <button class="btn btn--facebook mr-small" type="button">
                                    <span class="fa fa-facebook mr-small"></span>
                                    Facebook
                                </button>
                                <button class="btn btn--google" type="button">
                                    <span class="fa fa-google mr-small"></span>
                                    Google
                                </button>
                            </div>
                        </div>
                    </form>'
                    :
                    '<ul class="list-nostyle">
                        <li class="user-nav__item">
                            <a href="#">Profil</a>
                        </li>
                        <li class="user-nav__item">
                            <a href="#">Pesanan Saya <span class="notif-dot"></span></a>
                        </li>
                        <li class="user-nav__item">
                            <a href="#">Keluar</a>
                        </li>
                    </ul>'
                    ?>
                </div>
            </nav>
        </div>
    </div>
</header>
<div class="sticky-footer-container">