<footer class="sticky-footer-container-item site-footer">
	<section class="section-block--inset fill-grey-blue section-subscribe">
		<div class="container">
			<div class="bzg bzg--v-center">
				<div class="bzg_c" data-col="m7, l8">
					<h2 class="no-space text-up">
						<img data-src="<?= isset($path) ? $path : '' ?>assets/img/myhisLogo.png" alt="" width="120" class="mr-small lazyload">
	                    Newsletter
	                </h2>
	                <!-- <strong>Daftar sekarang untuk mendapatkan info terkini dan penawaran terbaik dari H.I.S. Tour & Travel.</strong> -->
	                <strong>Mulai berlangganan MY H.I.S Newsletter untuk mendapatkan info terkini dan penawaran terbaik dari H.I.S Tour & Travel</strong>
				</div>
				<div class="bzg_c" data-col="m5, l4">
					<form action="" class="form">
						<div class="bzg bzg--no-gutter">
							<div class="bzg_c" data-col="s7, l8">
								<input type="email" class="form-input form-input--large form-input--block" placeholder="Email anda">
							</div>
							<div class="bzg_c" data-col="s5, l4">
								<button class="btn btn--large btn--block btn--red">
									<b class="t--up">Daftar</b>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- section-subscribe -->

	<section class="section-block--inset fill-blue section-footer-info">
		<div class="container">
			<div class="is-small">
				<div class="block text-center">
					<a href="tel:+6221 574 1701" class="btn btn--ghost-white">
						<span class="text--huge">021 574 1701</span>
						<small class="is-block">Senin - Sabtu | 09.00 - 17.00</small>
					</a>
				</div>
				<address class="block text-center">
					<h3 class="no-space">Mid Plaza Head Office</h3>
					Mid Plaza 1 Building, 1st Floor<br>
					Jl. Jendral Sudirman<br>
					Kav 00 - 00 Jakarta 10210
				</address>
				<div class="block text-center">
					<a href="#" class="btn btn--ghost-white">
						<span class="fa fa-headphones"></span>
						Contact Us
					</a>
				</div>
				<div class="bzg bzg--small-gutter">
					<div class="bzg_c" data-col="x4">
						<img data-src="<?= isset($path) ? $path : '' ?>assets/img/badge-transaction.png" alt="Transaksi terpercaya di H.I.S." class="lazyload">
					</div>
					<div class="bzg_c" data-col="x8">
						<div class="block in-block">
							<a href="#" class="in-block space-right">
								<img data-src="<?= isset($path) ? $path : '' ?>assets/img/logo-iata.png" alt="IATA" height="40" class="lazyload">
							</a>
							<a href="#" class="in-block space-right">
								<img data-src="<?= isset($path) ? $path : '' ?>assets/img/site-logo.png" alt="H.I.S" height="40" class="lazyload">
							</a>
						</div>
						<div class="block--half in-block social-nav">
							<a href="#" class="in-block link-white space-right" title="Youtube">
								<span class="fa fa-2x fa-youtube" aria-hidden="true"></span>
							</a>
							<a href="#" class="in-block link-white space-right" title="Facebook">
								<span class="fa fa-2x fa-facebook" aria-hidden="true"></span>
							</a>
							<a href="#" class="in-block link-white space-right" title="Twitter">
								<span class="fa fa-2x fa-twitter" aria-hidden="true"></span>
							</a>
							<a href="#" class="in-block link-white space-right" title="Instagram">
								<span class="fa fa-2x fa-instagram" aria-hidden="true"></span>
							</a>
							<a href="#" class="in-block link-white space-right" title="Google Plus">
								<span class="fa fa-2x fa-google-plus" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="is-medium">
				<div class="bzg">
					<div class="bzg_c" data-col="m5, l8">
						<div class="bzg bzg--small-gutter">
							<div class="block--half bzg_c" data-col="l3">
								<h4 class="no-space"><span class="t--larger">Produk</span></h4>
								<ul class="list-nostyle footer-nav">
									<li>
										<a href="#">Tiket Pesawat</a>
									</li>
									<li>
										<a href="#">Hotel</a>
									</li>
									<li>
										<a href="#">Tour</a>
									</li>
									<li>
										<a href="#">Aktivitas</a>
									</li>
									<li>
										<a href="#">Theme Park</a>
									</li>
									<li>
										<a href="#">JR Pass</a>
									</li>
									<li>
										<a href="#">Wifi</a>
									</li>
									<li>
										<a href="#">Transportasi</a>
									</li>
								</ul>
							</div>
							<!-- col -->
							<div class="block--half bzg_c" data-col="l3">
								<h4 class="no-space"><span class="t--larger">Cabang</span></h4>
								<ul class="list-nostyle footer-nav">
									<li data-toggle>
										<a href="#" class="btn-toggle">Jabodetabek <span class="fa fa-caret-down"></span></a>
										<ul class="toggle-panel list-nostyle">
											<li>
												<a href="#">Jakarta</a>
											</li>
											<li>
												<a href="#">Bogor</a>
											</li>
											<li>
												<a href="#">Bekasi</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="#">Bandung</a>
									</li>
									<li>
										<a href="#">Yogyakarta</a>
									</li>
									<li data-toggle>
										<a href="#" class="btn-toggle">Surabaya <span class="fa fa-caret-down"></span></a>
										<ul class="toggle-panel list-nostyle">
											<li>
												<a href="#">Sidoarjo</a>
											</li>
											<li>
												<a href="#">Malang</a>
											</li>
											<li>
												<a href="#">Bekasi</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="#">Bali</a>
									</li>
								</ul>
							</div>
							<!-- col -->
							<div class="block--half bzg_c" data-col="l3">
								<h4 class="no-space"><span class="t--larger">Overseas Branches</span></h4>
								<ul class="list-nostyle footer-nav">
									<li>
										<a href="#">Thailand</a>
									</li>
									<li>
										<a href="#">Thailand</a>
									</li>
									<li>
										<a href="#">Thailand</a>
									</li>
									<li>
										<a href="#">Thailand</a>
									</li>
								</ul>
							</div>
							<!-- col -->
							<div class="block--half bzg_c" data-col="l3">
								<h4 class="no-space"><span class="t--larger">Tentang H.I.S</span></h4>
								<ul class="block--half list-nostyle footer-nav">
									<li>
										<a href="#">Tentang Kami</a>
									</li>
									<li>
										<a href="#">Booking Online</a>
									</li>
									<li>
										<a href="#">Karir</a>
									</li>
									<li>
										<a href="#">Gallery</a>
									</li>
									<li>
										<a href="#">Blok</a>
									</li>
									<li>
										<a href="#">Event</a>
									</li>
								</ul>
								<h4 class="no-space">
									<a href="#" class="link-white">
										<span class="t--larger">Japan Website</span>
									</a>
								</h4>
							</div>
							<!-- col -->
						</div>
					</div>

					<div class="bzg_c text-right--l" data-col="m7, l4">
						<div class="block clearfix">
							<img data-src="<?= isset($path) ? $path : '' ?>assets/img/badge-transaction.png" alt="Transaksi terpercaya di H.I.S." class="mr-small pull-left pull-right--l lazyload" width="90">
							<div class="block in-block">
								<a href="#" class="in-block space-right">
									<img data-src="<?= isset($path) ? $path : '' ?>assets/img/logo-iata.png" alt="IATA" height="55" class="lazyload">
								</a>
								<a href="#" class="in-block space-right">
									<img data-src="<?= isset($path) ? $path : '' ?>assets/img/site-logo--new.png" alt="H.I.S" height="55" class="lazyload">
								</a>
							</div>
							<h2 class="no-space in-block h1">
								<a href="tel:021 574 1701" class="in-block link-white">021 574 1701</a>
							</h2>
						</div>
						<div class="block">
							<div class="block--half in-block social-nav">
								<a href="#" class="in-block link-white space-right" title="Youtube">
									<span class="fa fa-2x fa-youtube" aria-hidden="true"></span>
								</a>
								<a href="#" class="in-block link-white space-right" title="Facebook">
									<span class="fa fa-2x fa-facebook" aria-hidden="true"></span>
								</a>
								<a href="#" class="in-block link-white space-right" title="Twitter">
									<span class="fa fa-2x fa-twitter" aria-hidden="true"></span>
								</a>
								<a href="#" class="in-block link-white space-right" title="Instagram">
									<span class="fa fa-2x fa-instagram" aria-hidden="true"></span>
								</a>
								<a href="#" class="in-block link-white space-right" title="Google Plus">
									<span class="fa fa-2x fa-google-plus" aria-hidden="true"></span>
								</a>
							</div>
							<div class="in-block">
								<a href="#" class="in-block">
									<img data-src="<?= isset($path) ? $path : '' ?>assets/img/btn-app-store.png" alt="Download on the App Store" width="122" class="lazyload">
								</a>
								<a href="#" class="in-block">
									<img data-src="<?= isset($path) ? $path : '' ?>assets/img/btn-play-store.png" alt="Download on Google Play" width="122" class="lazyload">
								</a>
							</div>
						</div>
						<strong class="copyrights">
							Copyright(C) 2012-2016 H.I.S. Co., Ltd. All Rights Reserved.
						</strong>
					</div>
				</div>
			</div>
		</div>
	</section>

</footer>
<button class="btn btn-to-top btn--circle btn--blue size-64" type="button" title="Back to top">
	<span class="fa fa-play"></span>
</button>
<template id="notif_tmpl">
	<div class="notif">
		<span class="overlay"></span>
		<div class="notif__box">
		    <button class="btn-close" aria-label="close"><span class="fa fa-close"></span></button>
		    <span class="notif__icon"></span>
		    <div class="notif__text"></div>
	    </div>
    </div>
</template>
<span class="sr-only" data-offline="<?= isset($path) ? $path : '' ?>offline.html" data-chache="v1-2018" data-assets='["<?= isset($path) ? $path : '' ?>assets/css/offline.css", "<?= isset($path) ? $path : '' ?>assets/img/site-logo--new.png", "<?= isset($path) ? $path : '' ?>assets/img/icon-mail-white.png", "<?= isset($path) ? $path : '' ?>assets/img/icon-facebook-white.png", "<?= isset($path) ? $path : '' ?>assets/img/icon-twitter-white.png", "<?= isset($path) ? $path : '' ?>assets/img/icon-instagram-white.png", "<?= isset($path) ? $path : '' ?>assets/img/icon-gplus-white.png", "<?= isset($path) ? $path : '' ?>assets/img/icon-youtube-white.png"]'></span>
