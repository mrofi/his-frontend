<div id="tour_itinerary">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-bus i--blue"></span>
		<span class="title-text text-up t--larger">Itenerary</span>
	</h3>
	<table class="table--line cell--top">
		<tbody>
			<?php for ($i=1; $i <= 6; $i++) { ?>
			<tr>
				<td class="text-center" width="30%">
					<figure class="responsive-media media--3-2">
						<img data-src="//placehold.it/300x200" alt="" class="lazyload">
					</figure>
				</td>
				<td>
					<h4 class="block--half">Hari 1 : Jakarta - Haneda</h4>
					<table class="no-space table--plain">
						<tbody>
							<tr>
								<td>
									<span class="fa fa-plane"></span>
								</td>
								<td>
									Berangkat dari Bandara Soekarno-Hatta
								</td>
							</tr>
							<tr>
								<td><span class="fa fa-cutlery"></span></td>
								<td>Meals : Makan Pagi - Makan Siang - Makan malam</td>
							</tr>
							<tr>
								<td><span class="fa fa-file-text"></span></td>
								<td>Note : Bermalam di Hotel</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<hr>
</div>