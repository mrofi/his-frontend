<div class="cards sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="120">
    <div class="card__item">
        <div class="card-head cf block--inset fill-yellow">
            <strong class="in-block">mulai dari</strong>
            <strong class="t--large pull-right">
                IDR 264.500
            </strong>
        </div>
        <div class="block--inset">
            <form action="" class="form form--line">
                <div class="form__row">
                    <div class="input-iconic--left">
                        <label for="" class="label-icon">
                            <span class="fa fa-credit-card-alt"></span>
                        </label>
                        <select name="" id="" class="form-input form-input--block selectstyle">
                            <option value="">Jenis Visa Jepang</option>
                            <option value="">Single Visa (With H.I.S. Product Purchase)</option>
                            <option value="">Single Visa (Visa Only)</option>
                            <option value="">Multiple Visa (With H.I.S. Product Purchase)</option>
                            <option value="">Multiple Visa (Visa Only)</option>
                        </select>
                    </div>
                </div>
                <div class="form__row">
                    <div class="input-iconic--left block--half">
                        <label for="departure_start" class="label-icon">
                            <span class="fa fa-calendar"></span>
                        </label>
                        <input type="text" class="form-input form-input--block pickaDay" id="departure_start" placeholder="Tanggal Berangkat">
                    </div>
                </div>
                <div class="">
                    <div class="input-iconic--left block--half">
                        <label for="" class="label-icon">
                            <span class="his-user-group"></span>
                        </label>
                        <div class="pick-guest fg-1" id="pickJRpassPsg" data-toggle>
                            <button class="form-input form-input--block btn-toggle" type="button">
                                <strong class="text-ellipsis">
                                    <span class="total-input--all" id="total_psgjr">1</span> Traveller(s)
                                </strong>
                            </button>
                            <div class="toggle-panel t-black fill-white">
                                <div class="block--inset-small">
                                    <div class="form__row flex v-ct qty-input">
                                        <label for="" class="fg-1">
                                            <span class="total-input">1</span> Adult
                                        </label>
                                        <div class="">
                                            <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                            <input type="number" class="form-input form-input--redline text-center input-adult" 
                                            value="1" min="1" max="6" data-psg="total_psgjr">
                                            <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </div>
                                    </div>
                                    <hr class="block--half">
                                    <div class="form__row flex v-ct qty-input">
                                        <label for="" class="fg-1">
                                            <span class="total-input">0</span> Children
                                        </label>
                                        <div class="">
                                            <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                            <input type="number" class="form-input form-input--redline text-center" 
                                            value="0" min="0" max="6" data-psg="total_psgjr">
                                            <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </div>
                                    </div>
                                    <hr class="block--half">
                                    <div class="text-right">
                                        <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                            Pilih
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- pick-guest -->
                    </div>
                </div>
                <hr>
                <button class="btn btn--round btn--block btn--red" type="submit">
                    <b class="text-up">Pesan Sekarang</b>
                </button>
            </form>
        </div>
    </div>
</div>