<div class="cards sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="120">
    <div class="card__item">
        <div class="card-head cf block--inset fill-yellow">
            <strong class="in-block">mulai dari</strong>
            <strong class="t--large pull-right">
                IDR 264.500
            </strong>
        </div>
        <div class="block--inset">
            <form action="" class="form form--line">
                <div class="form__row">
                    <div class="input-iconic--left">
                        <label for="" class="label-icon">
                            <span class="fa his-simcard"></span>
                        </label>
                        <select name="" id="" class="form-input form-input--block selectstyle">
                            <option value="">Type Simcard</option>
                            <option value="">Go! Go! SIMcard Harga Khusus</option>
                            <option value="">Go! Go! SIMcard Harga Normal</option>
                        </select>
                    </div>
                </div>
                <div class="form__row">
                    <div class="input-iconic--left block--half">
                        <label for="departure_start" class="label-icon">
                            <span class="fa fa-calendar"></span>
                        </label>
                        <input type="text" class="form-input form-input--block pickaDay" id="departure_start" placeholder="Tanggal Berangkat">
                    </div>
                </div>
                <div class="">
                    <div class="input-iconic--left block--half">
                        <label for="" class="label-icon">
                            <span class="his-simcard"></span>
                        </label>
                        <div class="pick-guest fg-1" id="pickJRpassPsg" data-toggle>
                            <button class="form-input form-input--block btn-toggle" type="button">
                                <strong class="text-ellipsis">
                                    Jumlah Simcard: <span class="total-input--all" id="total_simcard">1</span>
                                </strong>
                            </button>
                            <div class="toggle-panel t-black fill-white">
                                <div class="block--inset-small">
                                    <div class="flex v-ct qty-input">
                                        <label for="" class="fg-1">
                                            <!-- IDR <span class="total-input" data-total="420000">420000</span> -->
                                            IDR 420.000
                                        </label>
                                        <div class="">
                                            <button class="btn btn--plain btn--plain-red" data-action="minus" type="button">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                            <input type="number" class="form-input form-input--redline text-center" 
                                            value="1" min="1" max="9" data-psg="total_simcard">
                                            <button class="btn btn--plain btn--plain-red" data-action="plus" type="button">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- pick-guest -->
                    </div>
                </div>
                <hr>
                <button class="btn btn--round btn--block btn--red" type="submit">
                    <b class="text-up">Pesan Sekarang</b>
                </button>
            </form>
        </div>
    </div>
</div>