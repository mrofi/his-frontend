<div id="tour_description">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-bus i--blue"></span>
		<span class="title-text text-up t--larger">Deskripsi</span>
	</h3>
	<div class="bzg slide-on-mobile slide-push-arrow" data-slide-small="1" data-slide-medium="1"  data-slick='{"arrows": true, "dots": false}'>
		<div class="bzg_c" data-col="m4">
			<figure>
				<img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full lazyload">
				<figcaption>
					<h4 class="block--half">Gunung Fuji</h4>
					<hr class="block--half">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias nostrum deserunt autem, voluptatibus consequuntur? Sit quis non sed neque id beatae voluptatum! Dolorum earum repudiandae delectus? Odio, illum. Nisi, rerum.
				</figcaption>
			</figure>
		</div>
		<div class="bzg_c" data-col="m4">
			<figure>
				<img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full lazyload">
				<figcaption>
					<h4 class="block--half">Gunung Fuji</h4>
					<hr class="block--half">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias nostrum deserunt autem, voluptatibus consequuntur? Sit quis non sed neque id beatae voluptatum! Dolorum earum repudiandae delectus? Odio, illum. Nisi, rerum.
				</figcaption>
			</figure>
		</div>
		<div class="bzg_c" data-col="m4">
			<figure>
				<img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full lazyload">
				<figcaption>
					<h4 class="block--half">Gunung Fuji</h4>
					<hr class="block--half">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias nostrum deserunt autem, voluptatibus consequuntur? Sit quis non sed neque id beatae voluptatum! Dolorum earum repudiandae delectus? Odio, illum. Nisi, rerum.
				</figcaption>
			</figure>
		</div>
	</div>
	<hr>
</div>