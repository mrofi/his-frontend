<div id="tour_point">
	<div class="block fill-lightgrey block--inset">
		<h3 class="title-group">
			<span class="title-icon fa fa-1-5x fa-star i--blue"></span>
			<span class="title-text text-up t--larger">Tour Point</span>
		</h3>
		<h4>Whats Include</h4>
		<ul>
			<li>Transportasi</li>
			<li>Makan Siang</li>
			<li>Tiket Masuk tempat wisata sesuai itinerary</li>
			<li>Tour Guide berbahasa Inggris</li>
		</ul>
	</div>
	<hr>
</div>