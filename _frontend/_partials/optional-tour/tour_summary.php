<div id="tour_summary">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-suitcase i--blue"></span>
		<span class="title-text text-up t--larger">Rincian Tour</span>
	</h3>
	<div class=" table-container">
		<table class="table--line">
			<tbody>
				<tr>
					<td colspan="2"><strong>Tour Code</strong></td>
					<td>:</td>
					<td>TYO0534</td>
				</tr>
				<tr>
					<td><span class="fa fa-cutlery"></span></td>
					<td><strong>Makan</strong></td>
					<td>:</td>
					<td>1 Makan siang</td>
				</tr>
				<tr>
					<td><span class="fa fa-plane"></span></td>
					<td><strong>Makan</strong></td>
					<td>:</td>
					<td>1 Makan siang</td>
				</tr>
				<tr>
					<td><span class="fa fa-street-view"></span></td>
					<td><strong>Included</strong></td>
					<td>:</td>
					<td>
						<ul>
							<li>Transportasi</li>
							<li>Makan Siang</li>
							<li>Tiket Masuk tempat wisata sesuai itinerary</li>
							<li>Tour Guide berbahasa Inggris</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><strong>Excluded</strong></td>
					<td>:</td>
					<td>
						<ul>
							<li>Transportasi</li>
							<li>Makan Siang</li>
							<li>Tiket Masuk tempat wisata sesuai itinerary</li>
							<li>Tour Guide berbahasa Inggris</li>
						</ul>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<h4 class="hl-b">Catatan</h4>
	<ol>
		<li>
			Apabila jalan menuju jalur Fuji Subaru ditutup, maka tur ke Stasiun ke-5 Gunung Fuji tidak mungkin dilakukan.
		</li>
		<li>
			Apabila ropeway tidak tersedia, kami akan memandu Anda untuk mengunjungi aquarium.
		</li>
		<li>
			Itinerary kemungkinan dapat berubah oleh berbagai faktor.
		</li>
		<li>
			Servis Drop-off ke hotel masing-masing tamu setelah tur tidak tersedia
		</li>
		<li>
			Rute yang akan diambil mungkin akan berubah tergantung kondisi lalu lintas. Waktu kembali mungkin akan tertunda 1-2 jam tergantung kondisi lalu lintas.
		</li>
		<li>
			Peserta diharapkan untuk membawa pakaian hangat mengingat cuaca yang dingin di gunung.
		</li>
		<li>
			Apabila terjadi keterlambatan dikarenakan kondisi lalu lintas, pihak kami tidak akan bertanggung jawab untuk tarif taksi, akomodasi semalam atau biaya lainnya. Kami juga tidak akan mengembalikan biaya tour (refund) apabila tur yang Anda ikuti berangkat lebih cepat daripada jadwal.
		</li>
		<li>
			Kami berhak membatalkan pemesanan apabila jumlah peserta kurang dari 5 orang.
		</li>
	</ol>
	<h4 class="hl-b">Kebijakan Pembatalan</h4>
	<table class="table--line table--auto">
		<tbody>
			<tr>
				<td>10 hari sebelum hari keberangkatan</td>
				<td>:</td>
				<td>20 % Dari Total Biaya</td>
			</tr>
			<tr>
				<td>10 hari sebelum hari keberangkatan</td>
				<td>:</td>
				<td>20 % Dari Total Biaya</td>
			</tr>
			<tr>
				<td>10 hari sebelum hari keberangkatan</td>
				<td>:</td>
				<td>20 % Dari Total Biaya</td>
			</tr>
			<tr>
				<td>10 hari sebelum hari keberangkatan</td>
				<td>:</td>
				<td>20 % Dari Total Biaya</td>
			</tr>
		</tbody>
	</table>
	<hr>
</div>