<div id="tour_location">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-map-marker i--blue"></span>
		<span class="title-text text-up t--larger">Lokasi</span>
	</h3>
	<div class="block">
		<!-- <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1HewlVIqDesVb00TBGLkqbPX_ecn_z1zR" width="320" height="240"></iframe> -->
		<!-- <div class="map-container map-container--responsive border" data-marker="assets/img/marker-dot.png" data-map="dev/destinations.json">
            <div class="map-area" id="his_map"></div>
        </div> -->
		<div class="map-container map-container--responsive border" data-marker="assets/img/marker-dot.png" 
		data-map='{"lat": -6.209465,"lng": 106.820632}' data-title="Gunung Fuji">
            <div class="map-area" id="his_map"></div>
        </div>
	</div>
	<hr>
</div>