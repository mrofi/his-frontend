<div id="tour_itinerary">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-bus i--blue"></span>
		<span class="title-text text-up t--larger">Itenerary</span>
	</h3>
	<table class="table table--white">
		<thead>
			<tr>
				<th class="text-up">Waktu</th>
				<th class="text-up">Aktivitas</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center"><strong>07.00 - 08.30</strong></td>
				<td>Servis penjemputan tersedia di beberapa hotel besar di Tokyo</td>
			</tr>
			<tr>
				<td class="text-center"></td>
				<td>Servis penjemputan tersedia di beberapa hotel besar di Tokyo</td>
			</tr>
			<tr>
				<td class="text-center"><strong>07.00 - 08.30</strong></td>
				<td>Servis penjemputan tersedia di beberapa hotel besar di Tokyo</td>
			</tr>
		</tbody>
	</table>
	<hr>
</div>