<div id="kebijakan">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-info-circle i--blue"></span>
		<span class="title-text text-up t--larger">Kebijakan Pemesanan</span>
	</h3>
	<div class="block" id="west_pass">
		<h4 class="in-block">
			Kebijakan Pemesanan &amp; Pembatalan
			<hr class="no-space">
		</h4>
		<h4 class="block--small">Pemesanan</h4>
		<ul>
			<li>Qui eligendi, blanditiis nisi. Nam, odio? Dignissimos!</li>
			<li>
				tempora, necessitatibus deleniti eos quasi molestias enim perspiciatis. Qui, tempora, facere.
			</li>
			<li>
				possimus, esse! Inventore, necessitatibus asperiores architecto impedit ducimus veniam vitae?
			</li>
			<li>
				nam corporis, temporibus. Ut consequuntur, doloremque aperiam sunt veritatis veniam consequatur nisi?
			</li>
			<li>
				dolor iusto, aut error iure tempora quod. Dolorum, aliquid.
			</li>
			<li>
				numquam ab ducimus, ratione totam distinctio animi saepe commodi beatae!
			</li>
			<li>
				saepe ducimus nesciunt! Iusto cumque excepturi quaerat omnis, obcaecati voluptatum reprehenderit.
			</li>
			<li>
				 fugit eligendi iure adipisci culpa voluptas quibusdam velit suscipit. Corporis, mollitia.
			</li>
			<li>
				possimus quod incidunt. Molestias et beatae, praesentium consectetur totam!
			</li>
			<li>
				temporibus, dignissimos sunt, odio qui officia quo maxime fugit?
			</li>
			<li>
				repellendus ipsam velit, praesentium ullam veniam reprehenderit architecto maxime.
			</li>
		</ul>
		<h4 class="block--small">Pergantian tanggal / Pembatalan / Pengembalian Uang</h4>
		<ul>
			<li>Qui eligendi, blanditiis nisi. Nam, odio? Dignissimos!</li>
			<li>
				tempora, necessitatibus deleniti eos quasi molestias enim perspiciatis. Qui, tempora, facere.
			</li>
		</ul>
	</div>
	<hr>
</div>