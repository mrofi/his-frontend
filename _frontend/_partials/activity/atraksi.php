<div id="atraksi" class="block--double">
	<div class="block">
		<h3 class="title-group">
			<span class="title-icon fa fa-1-5x his-park i--blue"></span>
			<span class="title-text text-up t--larger">Atraksi</span>
		</h3>
	</div>
	<p>
		Huis Ten Bosch menyajikan banyak atraksi menarik yang dapat anda coba bersama keluarga maupun kerabat. Berikut beberapa diantaranya:
	</p>
	<?php for ($i=1; $i <= 4; $i++) { ?>
	<div class="block">
		<h3>Harbor Town</h3>
		<div class="bzg">
			<div class="block--half bzg_c" data-col="m4">
				<img src="//placehold.it/360x240" alt="">
			</div>
			<div class="bzg_c" data-col="m8">
				<strong>Atraksi dan Aktivitas di Harbor Town</strong><br>
				<ul class="list-col-2">
					<li>The Virtual</li>
					<li>GO&GO!! Sniper Game</li>
					<li>The Virtual</li>
					<li>The Virtual</li>
					<li>GO&GO!! Sniper Game Gamess</li>
					<li>The Virtual</li>
					<li>GO&GO!! Sniper Game</li>
					<li>The Virtual</li>
					<li>GO&GO!! Sniper Game</li>
					<li>The Virtual</li>
					<li>GO&GO!! Sniper Game</li>
				</ul>
			</div>
		</div>
	</div>
	<?php } ?>
	<hr>
</div>