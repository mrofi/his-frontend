<div id="regional_pass">
	<h3 class="title-group">
		<span class="title-icon fa-1-5x his-park i--blue"></span>
		<span class="title-text text-up t--larger">Harga</span>
	</h3>
	<div class="block" id="west_pass">
		<h4 class="hl-b sp-b text-up text-blue">Ticker Huis Ten Bosch</h4>
		<div class="table-container">
			<table class="table--line">
				<thead>
					<tr>
						<th></th>
						<th>1 Day Pass</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><p>Dewasa<br>(18th ke atas)</td>
						<td class="text-center"><p>IDR 1.230.000</p></td>
					</tr>
					<tr>
						<td><p>Dewasa<br>(18th ke atas)</td>
						<td class="text-center"><p>IDR 1.230.000</p></td>
					</tr>
					<tr>
						<td><p>Dewasa<br>(18th ke atas)</td>
						<td class="text-center"><p>IDR 1.230.000</p></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<hr>
</div>