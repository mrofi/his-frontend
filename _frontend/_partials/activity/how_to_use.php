<div id="how_to_use" class="block--double">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-map-marker i--blue"></span>
		<span class="title-text text-up t--larger">Akses</span>
	</h3>
	<ul class="list-nostyle accordeon">
		<li class="is-active">
			<h4 class="title-q accordeon-trigger">
				<span class="title-icon fa fa-question-circle"></span>
				Akses ke Huis Ten Bosch dari area Kyushu
			</h4>
			<div class="accordeon-content">
				<strong>Fukuoka Airport</strong>
				<ol class="block list-step-arrow">
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Huis Ten Bosch</span>
					</li>
				</ol>
				<strong>Fukuoka Airport</strong>
				<ol class="block list-step-arrow">
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Huis Ten Bosch</span>
					</li>
				</ol>
				<strong>Fukuoka Airport</strong>
				<ol class="block list-step-arrow">
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Huis Ten Bosch</span>
					</li>
				</ol>
			</div>
		</li>
		<li class="">
			<h4 class="title-q accordeon-trigger">
				<span class="title-icon fa fa-question-circle"></span>
				Akses ke Huis Ten Bosch dari area Kyushu
			</h4>
			<div class="accordeon-content">
				<strong>Fukuoka Airport</strong>
				<ol class="block list-step-arrow">
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Huis Ten Bosch</span>
					</li>
				</ol>
				<strong>Fukuoka Airport</strong>
				<ol class="block list-step-arrow">
					<li>
						<span class="full-v">Terminal Domestik Bandara Fukuoka (subway)</span>
					</li>
					<li>
						<span class="full-v">Huis Ten Bosch</span>
					</li>
				</ol>
			</div>
		</li>
	</ul>
	<hr>
</div>