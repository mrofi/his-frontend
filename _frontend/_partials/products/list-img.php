<div id="list_img" class="block--double">
	<h3 class="title-group">
		<span class="title-icon fa fa-1-5x fa-bus i--blue"></span>
		<span class="title-text text-up t--larger">List Images</span>
	</h3>
	<div class="bzg">
		<div class="block bzg_c" data-col="m4">
			<figure>
				<img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
				<figcaption>
					<h4 class="block--half">Gunung Fuji</h4>
				</figcaption>
			</figure>
		</div>
		<div class="block bzg_c" data-col="m4">
			<figure>
				<img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
				<figcaption>
					<h4 class="block--half">Gunung Fuji</h4>
				</figcaption>
			</figure>
		</div>
		<div class="block bzg_c" data-col="m4">
			<figure>
				<img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
				<figcaption>
					<h4 class="block--half">Gunung Fuji</h4>
				</figcaption>
			</figure>
		</div>
		<div class="block bzg_c" data-col="m4">
			<figure>
				<img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
				<figcaption>
					<h4 class="block--half">Gunung Fuji</h4>
				</figcaption>
			</figure>
		</div>
	</div>
	<hr>
</div>