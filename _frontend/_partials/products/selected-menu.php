<div id="selected_menu" class="block--double">
	<h3 class="title-group">
		<span class="title-icon fa-1-5x his-train i--blue"></span>
		<span class="title-text text-up t--larger">Selected Menu</span>
	</h3>

	<div class="" data-list="dev/stations-line.json" data-category="Stations" data-next="" data-tmpl="#tmpList">
            
    </div>
    <div class="loader">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>

    <template id="tmpList">
		{{#each this}}
    		<nav class="block tab-nav tab-nav--circle">
				<ul class="list-nostyle">
					{{#each data}}
					<li class="tab-nav__item {{#if lineActive}}is-active{{/if}}">
						<a href="#{{lineCode}}_line" class="btn-circle-{{lineColor}}" title="{{lineName}} Line">{{lineCode}}</a>
					</li>
					{{/each}}
				</ul>
			</nav>
			<div class="tab-panels">
				{{#each data}}
				<div id="{{lineCode}}_line" class="tab-panel skin-{{lineColor}} {{#if lineActive}}is-active{{/if}}">
					<div class="block head">
						<h4 class="title-in-block text-up">{{lineName}} Line</h4>
						<div class="title-infos">
							<span class="initial">{{lineCode}}</span>
							<span class="status">
								<span class="t-strong">{{lineName}} Line</span>
								({{lineStations.length}} Stasions)
							</span>
						</div>
					</div>
					<!-- head -->
					<ol class="list-nostyle path-line">
						{{#each lineStations}}
						<li class="path-line__item">
							<span class="item-number">
								<span>
									{{../lineCode}}
									<span class="num"></span>
								</span>
							</span>
							<span class="item-text">{{stationName}}</span>
						</li>
						{{/each}}
					</ol>

					<div class="block list-interest bzg bzg--lite-gutter">
						{{#each lineStations}}
						<dl class="block--half bzg_c" data-col="m4, l3">
							<dt><span>{{../lineCode}}<span class="dl-count"></span> - {{stationName}} Station</span></dt>
							<dd class="no-space">
								<ul class="list-nostyle">
									{{#each interestPlace}}
									<li class="block-small">
										<a href="#" class="btn btn--block btn--ghost">{{this}}</a>
									</li>
									{{/each}}
								</ul>
							</dd>
						</dl>
						{{/each}}
					</div>
				</div>
				{{/each}}
			</div>
    	{{/each}}
    </template>

	<!-- <nav class="block tab-nav tab-nav--circle">
		<ul class="list-nostyle">
			<li class="tab-nav__item is-active">
				<a href="#g_line" class="btn-circle-orange" title="Ginza Line">g</a>
			</li>
			<li class="tab-nav__item">
				<a href="#m_line" class="btn-circle-red" title="M Line">m</a>
			</li>
			<li class="tab-nav__item">
				<a href="#h_line" class="btn-circle-grey" title="H Line">h</a>
			</li>
			<li class="tab-nav__item">
				<a href="#t_line" class="btn-circle-cyan" title="T Line">t</a>
			</li>
			<li class="tab-nav__item">
				<a href="#c_line" class="btn-circle-green" title="C Line">c</a>
			</li>
			<li class="tab-nav__item">
				<a href="#y_line" class="btn-circle-gold" title="Y Line">y</a>
			</li>
			<li class="tab-nav__item">
				<a href="#z_line" class="btn-circle-purple" title="Z Line">z</a>
			</li>
			<li class="tab-nav__item">
				<a href="#n_line" class="btn-circle-tosca" title="N Line">n</a>
			</li>
			<li class="tab-nav__item">
				<a href="#f_line" class="btn-circle-brown" title="F Line">f</a>
			</li>
		</ul>
	</nav> -->
	<!-- tab-nav -->
	<!-- <div class="tab-panels">
		<div id="g_line" class="tab-panel skin-orange is-active">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="m_line" class="tab-panel skin-red">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="h_line" class="tab-panel skin-grey">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="t_line" class="tab-panel skin-cyan">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="c_line" class="tab-panel skin-green">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="y_line" class="tab-panel skin-gold">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="z_line" class="tab-panel skin-purple">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="n_line" class="tab-panel skin-tosca">
			<?php // include 'g-line.php'; ?>
		</div>
		<div id="f_line" class="tab-panel skin-brown">
			<?php // include 'g-line.php'; ?>
		</div>
	</div> -->
	<hr>
</div>