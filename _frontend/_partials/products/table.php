<div id="tables" class="block--double">
	<h3 class="title-group">
		<span class="title-icon fa-1-5x his-park i--blue"></span>
		<span class="title-text text-up t--larger">Harga</span>
	</h3>
	<div class="block" id="west_pass">
		<h4 class="hl-b sp-b text-up text-blue">Ticker Huis Ten Bosch</h4>
		<div class="table-container">
			<table class="table--line">
				<thead>
					<tr>
						<th></th>
						<th>1 Day Pass</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><p>Dewasa<br>(18th ke atas)</td>
						<td class="text-center"><p>IDR 1.230.000</p></td>
					</tr>
					<tr>
						<td><p>Dewasa<br>(18th ke atas)</td>
						<td class="text-center"><p>IDR 1.230.000</p></td>
					</tr>
					<tr>
						<td><p>Dewasa<br>(18th ke atas)</td>
						<td class="text-center"><p>IDR 1.230.000</p></td>
					</tr>
				</tbody>
			</table>
			<table class="table table--white">
				<thead>
					<tr>
						<th class="text-up">Waktu</th>
						<th class="text-up">Aktivitas</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center"><strong>07.00 - 08.30</strong></td>
						<td>Servis penjemputan tersedia di beberapa hotel besar di Tokyo</td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td>Servis penjemputan tersedia di beberapa hotel besar di Tokyo</td>
					</tr>
					<tr>
						<td class="text-center"><strong>07.00 - 08.30</strong></td>
						<td>Servis penjemputan tersedia di beberapa hotel besar di Tokyo</td>
					</tr>
				</tbody>
			</table>

			<table class="table table--zebra">
				<thead class="text-center">
					<tr>
						<th rowspan="2">Dewasa TWN/TRP</th>
						<th colspan="3">ANAK (2 - 11 TAHUN)</th>
						<th rowspan="2">SINGLE SUPP</th>
						<th rowspan="2">VISA JEPANG</th>
					</tr>
					<tr>
						<th>TWN</th>
						<th>EXTRA BED</th>
						<th>NO BED</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
					</tr>
					<tr>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
					</tr>
					<tr>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
						<td>123</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<hr>
</div>