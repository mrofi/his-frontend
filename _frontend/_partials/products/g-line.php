<?php $station = ['Shibuya', 'Omote-sando', 'Gaiemmae', 'Aoyama-itchome', 'Akasaka-mitsuke', 'Tameike-sanno', 'Toranomon', 'Shimbashi', 'Ginza', 'Kyobashi', 'Nihombashi', 'Mitsukoshimae', 'Kanda', 'Suehirocho', 'Ueno-hirokoji', 'Ueno', 'Inaricho', 'Tawaramachi', 'Asakusa' ] ?>
<div class="block head">
	<h4 class="title-in-block text-up">Ginza Line</h4>
	<div class="title-infos">
		<span class="initial">G</span>
		<span class="status">
			<span class="t-strong">Ginza Line</span>
			(19 Stasions)
		</span>
	</div>
</div>
<!-- head -->
<ol class="list-nostyle path-line">
	<?php for ($i=0; $i < sizeof($station); $i++) { ?>
	<li class="path-line__item">
		<span class="item-number">
			<span>
				G
				<span class="num"></span>
			</span>
		</span>
		<span class="item-text"><?=$station[$i]?></span>
	</li>
	<?php } ?>
</ol>

<div class="block list-interest bzg bzg--lite-gutter">
	<?php for ($i=0; $i < sizeof($station); $i++) { ?>
	<dl class="block--half bzg_c" data-col="m4, l3">
		<dt>G01 - <?=$station[$i]?> Station</dt>
		<dd class="no-space">
			<ul class="list-nostyle">
				<li class="block-small">
					<a href="#" class="btn btn--block btn--ghost">Shibuya Crossing</a>
				</li>
				<li class="block-small">
					<a href="#" class="btn btn--block btn--ghost">Hachiko Statue</a>
				</li>
				<li class="block-small">
					<a href="#" class="btn btn--block btn--ghost">Shibuya 109</a>
				</li>
			</ul>
		</dd>
	</dl>
	<?php } ?>
</div>