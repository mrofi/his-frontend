<div id="text-img" class="block--double">
	<div class="block fill-lightgrey block--inset">
		<h3 class="title-group">
			<span class="title-icon fa fa-1-5x fa-search i--blue"></span>
			<span class="title-text text-up t--larger">Deskripsi</span>
		</h3>
		<h3>JAPAN RAIL PASS & JAPAN AREA PASS</h3>
		<p>
			Huis Ten Bosch atau HTB yang merayakan anniversary ke 20 pada 2012 lalu adalah resort terluas di Jepang dengan area 1,520,000 meter persegi yang menyajikan tempat-tempat dengan tampilan Eropa klasik. 
		</p>
		<div class="block bzg">
			<div class="bzg_c block--half" data-col="m6">
				<img src="//placehold.it/360x240" alt="">
			</div>
			<div class="bzg_c block--half" data-col="m6">
				<img src="//placehold.it/360x240" alt="">
			</div>
		</div>
		<h3>Tentang Huis Ten Bosch</h3>
		<ul class="list-nostyle">
			<li class="bzg">
				<div class="bzg_c block" data-col="m6">
					<img src="//placehold.it/360x240" alt="">
				</div>
				<div class="bzg_c" data-col="m6">
					<h4 class="block--small">Arsitektur Eropa</h4>
					Ketika mengelilingi kota yang ada di dalamnya, Anda akan mendapatkan pengalaman di dunia baru yang berbeda dari kehidupan sehari-hari. Huis Ten Bosch menyajikan hiburan untuk semua umur mulai dari kebun penuh dengan bunga-bunga cantik yang mekar setiap musimnya hingga Troy Hercules.
				</div>
			</li>
			<li class="bzg">
				<div class="bzg_c block" data-col="m6">
					<img src="//placehold.it/360x240" alt="">
				</div>
				<div class="bzg_c" data-col="m6">
					<h4 class="block--small">Arsitektur Eropa</h4>
					Ketika mengelilingi kota yang ada di dalamnya, Anda akan mendapatkan pengalaman di dunia baru yang berbeda dari kehidupan sehari-hari. Huis Ten Bosch menyajikan hiburan untuk semua umur mulai dari kebun penuh dengan bunga-bunga cantik yang mekar setiap musimnya hingga Troy Hercules.
				</div>
			</li>
		</ul>
	</div>
	<hr>
</div>