<div id="accordeon" class="block--double">
	<h3 class="title-group">
		<span class="title-icon fa-1-5x i--blue">?</span>
		<span class="title-text text-up t--larger">Accordeon</span>
	</h3>
	<ul class="list-nostyle accordeon">
		<li class="is-active">
			<h4 class="title-q accordeon-trigger">
				<span class="title-icon fa fa-question-circle"></span>
				Berisi teks
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger">
				<span class="title-icon fa fa-question-circle"></span>
				Periode Masa Berlaku Japan Rail PASS			          
			</h4>
			<div class="accordeon-content">
				Jasa transportasi The Japan Rail Pass berlaku untuk kereta api, bus, dan kapal feri seperti yang tertera di bawah ini
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger">
				<span class="title-icon fa fa-question-circle"></span>
				Berisi Video
			</h4>
			<div class="accordeon-content">
				<figure class="responsive-media">
					<iframe class="video-iframe" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" gesture="media" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe>
				</figure>

				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
					<li>
						Earum totam et dolores voluptatem porro tempore temporibus ducimus ipsam, placeat amet, suscipit. Excepturi, dolore! Beatae rem laudantium fugit quibusdam natus veritatis.
					</li>
				</ul>
			</div>
		</li>
		<li>
			<h4 class="title-q accordeon-trigger">
				<span class="title-icon fa fa-question-circle"></span>
				Berisi Image
			</h4>
			<div class="accordeon-content">
				<img src="//placehold.it/600x600" alt="">
			</div>
		</li>
	</ul>
	<hr>
</div>