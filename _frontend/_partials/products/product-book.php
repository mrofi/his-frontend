<fieldset class="over-y-auto">
    <div class="block--half flex v-ct qty-input">
        <label for="" class="fg-1 line--small">
            <strong>Dewasa- TWN / TRP 2</strong><br>
            IDR 10,000,000++
        </label>
        <div class="">
            <button class="btn btn--plain btn--plain-red" 
            data-action="minus" type="button" data-age="single" data-type="person">
            <span class="fa fa-minus"></span>
        </button>
        <!-- button -->
        <input type="number" 
        class="form-input form-input--redline text-center input-person" 
        value="1" min="1" max="8" data-psg="total_psgjr" 
        data-step="1" data-step-item="1" data-price="10000000"
        data-vat="1000000" data-surcharge="10000" 
        data-target="#dewasa_1">
        <!-- input -->
        <button class="btn btn--plain btn--plain-red" 
        data-action="plus" type="button" data-age="single" data-type="person">
        <span class="fa fa-plus"></span>
    </button>
    <!-- button -->
</div>
</div>
<hr class="block--half">
<div class="block--half flex v-ct qty-input">
    <label for="" class="fg-1 line--small">
        <strong>Single- TWN/TRP 2</strong><br>
        IDR 10,000,000++
    </label>
    <div class="">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="single" disabled>
            <span class="fa fa-minus"></span>
        </button>
        <input type="number" 
        class="form-input form-input--redline text-center input-person" 
        value="0" min="0" max="8" data-psg="total_psgjr" 
        data-step="2" data-step-item="1" data-price="10000000"
        data-vat="1000000" data-surcharge="10000" 
        data-target="#anak_1">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="single">
            <span class="fa fa-plus"></span>
        </button>
    </div>
</div>
<div class="block--half flex v-ct qty-input">
    <label for="" class="fg-1 line--small">
        <strong>Anak- Twin Sharing 1</strong><br>
        IDR 10,000,000++
    </label>
    <div class="">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
            <span class="fa fa-minus"></span>
        </button>
        <input type="number" class="form-input form-input--redline text-center input-person" 
        value="0" min="0" max="8" data-psg="total_psgjr" 
        data-step="1" data-step-item="2" data-price="10000000"
        data-vat="1000000" data-surcharge="10000" 
        data-target="#anak_2">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
            <span class="fa fa-plus"></span>
        </button>
    </div>
</div>
<hr class="block--half">
<div class="block--half flex v-ct qty-input">
    <label for="" class="fg-1 line--small">
        <strong>Anak- TWN/TRP 3</strong><br>
        IDR 10,000,000++
    </label>
    <div class="">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
            <span class="fa fa-minus"></span>
        </button>
        <input type="number" class="form-input form-input--redline text-center input-person" 
        value="0" min="0" max="8" data-psg="total_psgjr" 
        data-step="1" data-step-item="1" data-price="10000000"
        data-vat="1000000" data-surcharge="10000" 
        data-target="#anak_3">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
            <span class="fa fa-plus"></span>
        </button>
    </div>
</div>
<hr class="block--half">
<div class="block--half flex v-ct qty-input">
    <label for="" class="fg-1 line--small">
        <strong>Anak- TWN/TRP 1</strong><br>
        IDR 10,000,000++
    </label>
    <div class="">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="single" disabled>
            <span class="fa fa-minus"></span>
        </button>
        <input type="number" class="form-input form-input--redline text-center input-person" 
        value="0" min="0" max="8" data-psg="total_psgjr" 
        data-step="1" data-step-item="1" data-price="10000000"
        data-vat="1000000" data-surcharge="10000" 
        data-target="#anak_4">
        <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="single">
            <span class="fa fa-plus"></span>
        </button>
    </div>
</div>
<hr class="block--half">

</fieldset>