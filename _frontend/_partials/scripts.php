	</div>
    <div class="page-loader">
    <!-- <div class="page-loader is-active"> -->
        <div class="loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- <a href="#" class="btn-whatsapp">
        <picture>
            <source media="(min-width: 768px)" 
                srcset="<= isset($path) ? $path : '' ?>assets/img/logo-whatsapp--full.jpg">
            <source media="(min-width: 480px)" 
                srcset="<= isset($path) ? $path : '' ?>assets/img/logo-whatsapp.jpg">
            <img src="<= isset($path) ? $path : '' ?>assets/img/logo-whatsapp.jpg" alt="Whatsapp">
        </picture>
    </a> -->

    <div class="tool-tip"></div>

    <section id="installBanner" class="p-16 banner-install" style="opacity: 0;">
        <div class="block flex">
            <img src="<?= isset($path) ? $path : '' ?>assets/img/launcher-icon-2x.png" width="48" height="48" alt="HIS">
            <div class="ml-half">
                <h4 class="no-space">Install HIS App?</h4>
                <small>HIS Travel Progresive Web App</small>
            </div>
        </div>
        <div class="flex">
            <button id="skipInstallBtn" class="btn btn--block mr-small">Lain kali</button>
            <button id="installBtn" class="btn btn--block btn--red">Install</button>
        </div>
    </section>

    <a class="bannerhover "id="demo-2-hide" value="Hide Content" onclick="PowerhouseShowHide('demo-2-div=none','demo-2-show=inline','demo-2-hide=none')"><img src="https://i.ibb.co/KjLB2W4/HIS-FAB-bubble.png" id="demo-2-div" style="position: fixed;z-index: 200;position: fixed;bottom: 100px;right: 0;"> </a>

        
    <script type="text/javascript">
    function PowerhouseShowHide()
    {
       // Will Bontrager Software LLC - https://www.willmaster.com/
       if( ! arguments.length ) { return; }
       for(var i=0; i< arguments.length; i++)
       {
          var ta = arguments[i].split("=",2);
          document.getElementById(ta[0]).style.display = ta[1];
       }
    }
    </script>
    
    <div class="fab">
        <label for="revealFAB" class="my-float">
            <span class="fa fa-commenting item-icon"></span>
            <span class="item-text">#TinggalTanya</span>
        </label>
        <input type="checkbox" id="revealFAB" role="button">
        <div id="email">
            <a href="https://api.whatsapp.com/send?phone=6281141606060" target="_blank" class="float-whatsapp">
                <i class="btn-icon fa fa-whatsapp"></i>
            </a>
        </div>
        <input type="checkbox" id="reveal2" role="button">
        <div id="livechat">
            <a class="float-livechat" onClick="myFunction()">
                <p class="btn-icon fa fa-comments"></p>
            </a>
        </div>
    </div>
    
    <!-- <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch"></script> -->
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/modernizr.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/waypoint.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.counterup.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/headroom.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/baze.validate.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/object-fit-images.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/slick.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/moment.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.jquery.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.selectric.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.unveil.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.nav.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/sticky.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/handlebars.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/handlebars-intl-with-locales.min.js"></script>
    <!-- <script src="<= isset($path) ? $path : '' ?>assets/js/vendor/upup.min.js"></script> -->
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/baguetteBox.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.zoom.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/nouislider.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/select2.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/qrcode.min.js"></script>
    <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/lazysizes.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDT39m_nMmH9JsWdNQxMbizNX91nKgyUZ8&language=id"></script>
    <!-- <script>window.gmapsAPIKey = `AIzaSyDT39m_nMmH9JsWdNQxMbizNX91nKgyUZ8&language=id`</script> -->
    <script src="<?= isset($path) ? $path : '' ?>assets/js/main.min.js"></script>

    <script>
        window.onload = function() {
            // Site._popMsg('Situs Bahaya', 'success', 100000)
            

            // Site._dialog('Apakah kamu yakin', '//suitmedia.com', 'Yakin dong', 'Gak jadi')
        }
    </script>
    
    <script>
        function myFunction() {
            document.querySelector('.qcw-trigger-btn').click()
        }
    </script>
    
    <script>
        // document.addEventListener('DOMContentLoaded', function() {
        window.onload = function () {
            var s,t; s = document.createElement('script'); s.type = 'text/javascript';
            s.src = 'https://s3-ap-southeast-1.amazonaws.com/qiscus-sdk/public/qismo/qismo-v2.js'; s.async = true;
            s.onload = s.onreadystatechange = function() { new Qismo("dup-avlebu2wx2zn1ukhg"); }
            t = document.getElementsByTagName('script')[0]; t.parentNode.insertBefore(s, t);
        }
        // });
    </script>

    <script>
        var cachePath = '';
        if ('serviceWorker' in navigator) {
            window.addEventListener('load', () => {
                navigator.serviceWorker.register(cachePath+'baze-sw.js')
                .then(reg => {
                    console.log('😎', reg);
                })
                .catch(err => {
                    console.log('😥', err);
                })
            });
        }
    </script>
    
    <style type="text/css">
        body {
            opacity: 1;
        }
    </style>

    <script>
        // window.onload = function() {


            // socialstory.launch();
        // }
    </script>
</body>
</html>
