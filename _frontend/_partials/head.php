<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Baze is a front-end starter template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#034f98">

    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:image" content="">
    <link rel="manifest" href="<?= isset($path) ? $path : '' ?>manifest.json">

    <link rel="apple-touch-icon" href="<?= isset($path) ? $path : '' ?>assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= isset($path) ? $path : '' ?>assets/img/favicon.png">

    <title>H.I.S Tour &amp; Travel</title>

    <style>body {opacity: 0;}</style>
    <link rel="stylesheet" href="<?= isset($path) ? $path : '' ?>assets/css/main.css" media="all">
    <link href="https://fonts.googleapis.com/css?family=Oleo+Script&display=swap" rel="stylesheet">
    
</head>
<body>
    <!--[if lt IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    