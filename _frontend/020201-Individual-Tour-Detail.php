<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Tour</a></li>
                <li><a href="#">Individual Tour</a></li>
                <li><a href="#">Jepang</a></li>
                <li><a href="#">5D4N TOKYO WAKAI</a></li>
            </ul>
        </div>
    </div>

    
    <div class="container container--smaller">
        <h1 class="block--small">5D4N TOKYO WAKAI</h1>
        <div class="block bzg">
            <div class="bzg_c" data-col="m8">
                <section class="section-block--smaller">
                    <div class="responsive-media media--3-2">
                        <div class="demihero-slider slide-default">
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-1.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-2.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- demihero-slider -->
                    </div>
                </section>
                <section class="section--block one-page-nav-container">
                    <nav class="inpage-nav block fill--overlap fill-lightgrey ">
                        <ul class="list-nostyle navs--inline text-up one-page-nav" id="jrPassNav">
                            <li class="inpage-nav__item nav__item current">
                                <a href="#tour_point">Tour Point</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_itinerary">Itinerary</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_description">Deskripsi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_location">Lokasi</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#tour_summary">Rincian Tour</a>
                            </li>
                            <li class="inpage-nav__item nav__item">
                                <a href="#" class="external">Unduh Flyer</a>
                            </li>
                        </ul>
                    </nav>
                    <article>
                        <div class="block">
                            <?php include '_partials/optional-tour/tour_point.php'; ?>
                            <?php include '_partials/group-tour/tour_itinerary.php'; ?>
                            <?php include '_partials/optional-tour/tour_description.php'; ?>
                            <?php include '_partials/optional-tour/tour_location.php'; ?>
                            <?php include '_partials/group-tour/tour_summary.php'; ?>
                        </div>
                        <hr>
                    </article>
                </section>
            </div>
            <div class="bzg_c" data-col="m4" data-sticky-container>
                <div class="cards sticky" data-sticky-class="is-sticky" data-sticky-for="1152" data-margin-top="120">
                    <div class="card__item">
                        <div class="card-head cf block--inset fill-yellow">
                            <strong class="in-block">mulai dari</strong>
                            <strong class="t--large pull-right">
                                IDR 6.264.500
                            </strong>
                        </div>
                        <div class="block--inset">
                            <form action="" class="form form--line">
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-alarm"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            3 Hari 2 Malam
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left">
                                        <label for="" class="label-icon">
                                            <span class="his-travel-bag"></span>
                                        </label>
                                        <strong class="form-input form-input--block">
                                            Individual Tour
                                        </strong>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="input-iconic--left block--half">
                                        <label for="departure_start" class="label-icon">
                                            <span class="fa fa-calendar"></span>
                                        </label>
                                        <input type="text" class="form-input form-input--block pickaDay" id="departure_start" placeholder="Tanggal Berangkat">
                                    </div>
                                </div>
                                <div class="">
                                    <div class="input-iconic--left block--half">
                                        <label for="" class="label-icon">
                                            <span class="his-user-group"></span>
                                        </label>
                                        <div class="pick-guest fg-1" id="pickJRpassPsg" data-toggle data-total="#product_753" data-sum="#bookSummary">
                                            <button class="form-input form-input--block btn-toggle" type="button">
                                                <strong class="text-ellipsis">
                                                    <span class="total-input--all" id="total_psgjr">1</span> Tamu
                                                    <span class="additional-info" id="psg_add_info">, Ekonomi</span>
                                                </strong>
                                            </button>
                                            <div class="toggle-panel t-black fill-white">
                                                <div class="block--inset-small">
                                                    <fieldset class="over-y-auto">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Dewasa- TWN / TRP</strong><br>
                                                                IDR 50,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="adult">
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-default input-adult input-person" 
                                                                value="1" min="1" max="10" data-psg="total_psgjr" data-price="50000000" data-target="#dewasa_1">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="adult">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Single- TWN/TRP</strong><br>
                                                                IDR 5,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="single" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person" 
                                                                value="0" min="0" max="10" data-psg="total_psgjr" data-price="5000000" data-target="#anak_1">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="single">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Anak- Twin Sharing</strong><br>
                                                                IDR 30,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person" 
                                                                value="0" min="0" max="10" data-psg="total_psgjr" data-price="30000000" data-target="#anak_2">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Anak- TWN/TRP</strong><br>
                                                                IDR 20,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person" 
                                                                value="0" min="0" max="10" data-psg="total_psgjr" data-price="20000000" data-target="#anak_3">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="block--half flex v-ct qty-input">
                                                            <label for="" class="fg-1">
                                                                <strong>Anak- TWN/TRP</strong><br>
                                                                IDR 10,000,000++
                                                            </label>
                                                            <div class="">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="minus" type="button" data-age="child" disabled>
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                                <input type="number" class="form-input form-input--redline text-center input-person" 
                                                                value="0" min="0" max="10" data-psg="total_psgjr" data-price="10000000" data-target="#anak_4">
                                                                <button class="btn btn--plain btn--plain-red" data-type="person" data-action="plus" type="button" data-age="child">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <hr class="block--half">
                                                        <div class="form__row">
                                                            <label for="economy_check" class="btn-check btn-check--radio">
                                                                <span class="text-up btn-check-text">Ekonomi</span>
                                                                <input type="radio" class="btn-check-input store-val" 
                                                                data-val="Ekonomi" data-target="psg_add_info"
                                                                name="class_check" checked="checked" id="economy_check">
                                                                <span class="input-icon"></span>
                                                            </label>
                                                        </div>
                                                        <div class="form__row">
                                                            <label for="premium_check" class="btn-check btn-check--radio">
                                                                <span class="text-up btn-check-text">Standar</span>
                                                                <input type="radio" class="btn-check-input store-val" 
                                                                data-val="Standar" data-target="psg_add_info"
                                                                name="class_check" id="premium_check">
                                                                <span class="input-icon"></span>
                                                            </label>
                                                        </div>
                                                    </fieldset>
                                                    <div class="text-right">
                                                        <button class="btn btn--smaller btn--round btn--ghost-red-black btn-done" type="button">
                                                            Pilih
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- pick-guest -->
                                    </div>
                                </div>
                                <div id="bookSummary" class="book-sum">
                                    <table class="table-total">
                                        <tbody>
                                            <tr>
                                                <td>Dewasa- TWN/TRP</td>
                                                <td class="text-right">Rp. <span id="dewasa_1">50,000,000</span></td>
                                            </tr>
                                            <tr>
                                                <td>Single- TWN/TRP</td>
                                                <td class="text-right">Rp. <span id="anak_1">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- Twin Sharing</td>
                                                <td class="text-right">Rp. <span id="anak_2">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP</td>
                                                <td class="text-right">Rp. <span id="anak_3">0</span></td>
                                            </tr>
                                            <tr>
                                                <td>Anak- TWN/TRP</td>
                                                <td class="text-right">Rp. <span id="anak_4">0</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td><strong>TOTAL</strong></td>
                                                <td class="text-right text-red t--larger t-strong">
                                                    Rp. <span id="product_753">0</span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <hr>
                                <button class="btn btn--round btn--block btn--red" type="submit">
                                    <b class="text-up">Pesan Sekarang</b>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Produk Terkait
                </h2>
            </div>
            <div class="block content-section">
                <div class="bzg cards cards--blue slide-3">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square">
                                <img src="assets/img/img-preload.png" data-src="//placehold.it/400x400" alt="" class="item-heavy lazyload">
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/underFooter.php'; ?>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,promise,fetch"></script>
<!-- <script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/modernizr.min.js"></script> -->
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/waypoint.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/headroom.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/object-fit-images.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/slick.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/moment.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/pikaday.jquery.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.selectric.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/jquery.nav.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/sticky.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/select2.min.js"></script>
<script src="<?= isset($path) ? $path : '' ?>assets/js/vendor/lazysizes.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDT39m_nMmH9JsWdNQxMbizNX91nKgyUZ8&language=id"></script>

<script>
    var cachePath = './';
</script>
<?php include '_partials/mainScripts.php'; ?>
