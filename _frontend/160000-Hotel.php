<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <section class="section-slide-hero">
    	<div class="container container--smaller">
            <h1 class="sr-only">Hotel</h1>
    		<div class="slider-hero">
    			<div class="slider__item">
    				<a href="#">
    					<img src="assets/img/slidebanner-1.jpg" alt="">
    				</a>
    			</div>
    		</div>
    	</div>
    </section>
    <!-- section-slide-hero -->


    <div class="container container--smaller">
        <div class="block--double fill-primary block--inset relative z-4">
            <form action="" class="form">
                <div class="bzg">
                    <div class="form__row bzg_c" data-col="l6">
                        <label for="">Destinasi Pilihan</label>
                        <!-- <input type="text" class="form-input form-input--block" placeholder="Dimana anda akan menginap?"> -->
                        <select name="" id="" class="form-input form-input--block select_2"
                        data-select="dev/city.json"
                        data-placeholder="Select destination">
                            <option value="">Jakarta</option>
                        </select>
                    </div>
                    <div class="form__row bzg_c" data-col="l6">

                        <div class="bzg pickdate-range">
                            <div class="form__row bzg_c" data-col="l6">
                                <label for="">Check In</label>
                                <div class="input-iconic">
                                    <input type="text" class="form-input form-input--block pikaRange pikaRange--start" id="chekin_start" placeholder="Tanggal Check In">
                                    <label for="chekin_start" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="l6">
                                <label for="">Check Out</label>
                                <div class="input-iconic">
                                    <input type="text" class="form-input form-input--block pikaRange pikaRange--end" id="chekout_end" placeholder="Tanggal Check Out">
                                    <label for="chekout_end" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- pickdate-range -->
                    </div>
                </div>
                <div class="bzg" data-room>
                    <div class="mb-small bzg_c" data-col="m4, l2">
                        <div class="flex a-center">
                            <label for="" class="in-block mr-small w-30">
                                <span class="fa fa-bed"></span>
                            </label>
                            <div class="fg-1">
                                <div class="flex add-room">
                                    <button class="btn btn--plain btn--plain-red" type="button" data-action="minus">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                    <input type="number" class="sr-only room-counter" min="1" max="8" value="1">
                                    <div class="fg-1 form-input form-input--redline text-center">1</div>
                                    <button class="btn btn--plain btn--plain-red" type="button" data-action="plus">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                    <template id="roomContent">
                                        <div class="bzg guest-list">
                                            <span class="bzg_c hidden">Kamar <span class="room-index"></span></span>
                                            <div class="mb-small bzg_c" data-col="m6">
                                                <div class="flex a-center">
                                                    <label for="" class="nowrap in-block mr-small w-30">
                                                        <span class="fa fa-male"></span>
                                                        <span class="fa fa-female"></span>
                                                    </label>
                                                    <div class="fg-1">
                                                        <select name="adult_guest[]" id="adult_guest" class="form-input form-input--block selectstyle">
                                                            <option value="1">1 adult</option>
                                                            <option value="2">2 adult</option>
                                                            <option value="3">3 adult</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-small bzg_c" data-col="m6">
                                                <div class="flex a-center">
                                                    <label for="" class="nowrap in-block mr-small w-30 text-center">
                                                        <span class="fa fa-child"></span>
                                                    </label>
                                                    <div class="fg-1">
                                                        <select name="child_guest[]" id="child_guest" class="form-input form-input--block selectstyle child-age-select">
                                                            <option value="0">0 child</option>
                                                            <option value="1">1 child</option>
                                                            <option value="2">2 childs</option>
                                                            <option value="3">3 childs</option>
                                                            <option value="4">4 childs</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-half bzg_c child-age-container">
                                                <div class="">Umur Anak</div>
                                                <div class="bzg bzg--small-gutter child-age__inputs">

                                                </div>
                                                <hr class="no-space">
                                            </div>
                                        </div>
                                    </template>
                                    <!-- roomContent template -->
                                    <script>
                                        function childAgeInput(index, age) {
                                            return '<div class="mb-small bzg_c" data-col="s4">'+
                                            '<select name="child_age['+index+']['+age+']" id="ChildAge['+index+']['+age+']" class="form-input form-input--block">'+ages(18)+
                                            '</select>'+
                                        '</div>'
                                        }
                                        function ages(age) {
                                            var option;
                                            option += '<option value="<1">&lt;1 th</option>'
                                            for(var i = 1; i < age; i++ ) {
                                                option += '<option value="'+i+'">'+i+' th</option>'
                                            }
                                            return option
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-small bzg_c guest-container" data-col="l5">
                        <div class="bzg guest-list">
                            <span class="bzg_c hidden">Kamar <span class="room-index">1</span></span>
                            <div class="mb-small bzg_c" data-col="m6">
                                <div class="flex a-center">
                                    <label for="" class="nowrap in-block mr-small w-30">
                                        <span class="fa fa-male"></span>
                                        <span class="fa fa-female"></span>
                                    </label>
                                    <div class="fg-1">
                                        <select name="adult_guest[0]" id="adult_guest_0" class="form-input form-input--block selectstyle">
                                            <option value="1">1 adult</option>
                                            <option value="2">2 adult</option>
                                            <option value="3">3 adult</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-small bzg_c" data-col="m6">
                                <div class="flex a-center">
                                    <label for="" class="nowrap in-block mr-small w-30 text-center">
                                        <span class="fa fa-child"></span>
                                    </label>
                                    <div class="fg-1">
                                        <select name="child_guest[0]" id="child_guest_0" data-index="0" class="form-input form-input--block selectstyle child-age-select">
                                            <option value="0">0 child</option>
                                            <option value="1">1 child</option>
                                            <option value="2">2 childs</option>
                                            <option value="3">3 childs</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-half bzg_c child-age-container">
                                <div class="">Umur Anak</div>
                                <div class="bzg bzg--small-gutter child-age__inputs">

                                </div>
                                <hr class="no-space">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bzg">
                    <div class="form__row bzg_c" data-col="m9, l7">
                        <div class="toggle-fyout-m" data-toggle>
                            <button class="btn btn-toggle btn--plain link-white">
                                <span class="fa fa-angle-down btn-icon"></span>
                                Pilihan Pencarian Tambahan
                            </button>
                            <div class="toggle-panel fill-pink">
                                <div class="block--inset">
                                    <div class="form__row">
                                        <label for="">Harga per malam</label>
                                        <div class="bzg">
                                            <div class="form__row bzg_c" data-col="s6">
                                                <div class="input-iconic--left fill-white border">
                                                    <label for="" class="label-icon">IDR</label>
                                                    <input type="number" class="form-input form-input--block text-right no-border" placeholder="minimum">
                                                </div>
                                            </div>
                                            <div class="form__row bzg_c" data-col="s6">
                                                <div class="input-iconic--left fill-white border">
                                                    <label for="" class="label-icon">IDR</label>
                                                    <input type="number" class="form-input form-input--block text-right no-border" placeholder="maksimum">
                                                </div>
                                            </div>
                                            <div class="form__row bzg_c" data-col="m5, l6">
                                                <label for="">Star Rating</label>
                                                <div class="add-rating">
                                                    <label class="btn-star" for="star-1">
                                                        <input class="btn-radio" type="radio" name="rating-star" id="star-1">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-2">
                                                        <input class="btn-radio" type="radio" name="rating-star" id="star-2">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-3">
                                                        <input class="btn-radio" type="radio" name="rating-star" id="star-3">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-4">
                                                        <input class="btn-radio" type="radio" name="rating-star" id="star-4">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-5">
                                                        <input class="btn-radio" type="radio" name="rating-star" id="star-5">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                </div>
                                                <!-- add-rating -->
                                            </div>
                                            <div class="form__row bzg_c" data-col="s7, l6">
                                                <label for="">Ketersediaan</label><br>
                                                <label for="available_hotel">
                                                    <input type="checkbox" id="available_hotel">
                                                    <span>
                                                        Hanya menampilkan hotel yang tersedia
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bzg_c" data-col="m3, l2" data-offset="l3">
                        <button class="btn btn--red btn--block">Cari</button>
                    </div>
                </div>
            </form>
        </div>
        <hr>
        <div class="block">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Your Recent Search</a>
                </h2>
            </div>
            <div class="block content-section inset-on-m">
                <div class="bzg cards card-img-toggle">
                    <?php for ($i=1; $i <= 2; $i++) { ?>
                    <div class="block bzg_c" data-col="m6, l3">
                        <div class="card__item">
                            <a href="#">
                                <figure class="item-img img-square fill-lightgrey">
                                    <img src="assets/img/img-preload.png" data-src="http://histravel.suitdev.com/files/tourpackages/336d5ebc5436534e61d16e63ddfca327-20180315090727.jpg" alt="" class="item-heavy">
                                    <figcaption class="text-center">
                                        <div class="title">
                                            <h3 class="no-space text-up">Tokyo Tokyo Tokyo</h3>
                                            <h4 class="no-space">Japan Japan</h4>
                                        </div>
                                        <div class="desc">
                                            28 Desember 2017 - 15 Januari 2018<br>
                                            1 room, 2 adults<br>
                                            5123 properties
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="block bzg_c" data-col="m6, l3">
                        <div class="card__item flex flex-col-center block--inset text-center">
                            <img src="assets/img/icon-hotel.png" width="140" alt="">
                            Pencarianmu selanjutnya akan muncul disini
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="block">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Top Destinations</a>
                </h2>
            </div>
            <div class="block content-section inset-on-m">
                <div class="bzg cards card-img">
                    <?php for ($i=1; $i <= 6; $i++) { ?>
                    <div class="block bzg_c" data-col="m6, l4">
                        <div class="card__item">
                            <a href="#">
                                <figure class="item-img img-square fill-lightgrey">
                                    <img src="assets/img/img-preload.png" data-src="http://histravel.suitdev.com/files/tourpackages/336d5ebc5436534e61d16e63ddfca327-20180315090727.jpg" alt="" class="item-heavy">
                                    <figcaption class="text-center">
                                        <div class="title">
                                            <h3 class="no-space text-up">Tokyo Tokyo Tokyo</h3>
                                            <h4 class="no-space">Japan Japan</h4>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <hr>
        <div class="block">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    <a href="#" class="link-black">Best Deal Product</a>
                </h2>
            </div>
            <div class="block content-section inset-on-m">
                <div class="bzg cards cards--blue">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <div class="card__item">
                            <figure class="item-img img-square fill-lightgrey">
                                <img src="assets/img/img-preload.png" data-src="http://histravel.suitdev.com/files/tourpackages/336d5ebc5436534e61d16e63ddfca327-20180315090727.jpg" alt="" class="item-heavy">
                                <div class="item-detail">
                                    <table>
                                        <tr>
                                            <td><span class="his-alarm"></span></td>
                                            <td>3D/2N</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-calendar"></span></td>
                                            <td>6 April 2017</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-plane"></span></td>
                                            <td>Asiana Airlines</td>
                                        </tr>
                                        <tr>
                                            <td><span class="fa fa-map-marker"></span></td>
                                            <td>Han River Cruise-Nanta Show -Bukchon Hanok Village</td>
                                        </tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--round btn--ghost-red-black">
                                            <b class="text-up">Pesan Sekarang</b>
                                        </a>
                                    </div>
                                </div>
                                <div class="ribbon ribbon--gold">
                                    <span class="ribbon__text">Recommended</span>
                                </div>
                            </figure>
                            <div class="item-text">
                                <a href="#">
                                    <strong class="text-up ellipsis-2">
                                        KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </strong>
                                    <div class="cf">
                                        <strong class="text--larger t-yellow in-block space-right">
                                            IDR 6.698.000++
                                        </strong>
                                        <span class="pull-right">
                                            <span class="fa fa-globe"></span>
                                            Asia
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
