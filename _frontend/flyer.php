<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Baze is a front-end starter template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#034f98">

    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:image" content="">

    <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <title>H.I.S Tour &amp; Travel</title>

    
    <link rel="stylesheet" href="assets/css/flyer.css" media="all">
    
</head>
<body>

    <table class="table-export">
        <thead class="header">
            <tr>
                <th>
                    <a href="#" class="header-main">
                        <img src="assets/img/headerFlyer1_2.png" alt="">
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="main">
                    <div class="text-center">
                        <h3 class="mb-0">
                            GROUP TOUR | Tour Code : HJ-JSSGA-1
                        </h3>
                        <h1 class="h2 text-blue">
                            7D/5N JAPAN SUPERB SHOPPING TOUR
                        </h1>
                    </div>
                    <div class="p-16 mb-8">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="text-center p-8">
                                        <img src="assets/img/600x400.png" alt="" class="mb-4">
                                        <span>Mt Fuji</span>
                                    </td>
                                    <td class="text-center p-8">
                                        <img src="assets/img/600x400.png" alt="" class="mb-4">
                                        <span>Mt Fuji</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="mb-16">
                        <div class="p-8 mb-8 bg-lightblue">
                            <div class="bg-content">
                                <h3 class="mb-0 text-blue">TOUR POINT</h3>
                            </div>
                        </div>
                        <ul class="mb-0">
                            <li>
                                19 Keberangkatan
                            </li>
                            <li>
                                19 Keberangkatan
                            </li>
                            <li>
                                19 Keberangkatan
                            </li>
                            <li>
                                19 Keberangkatan
                            </li>
                            <li>
                                19 Keberangkatan
                            </li>
                            <li>
                                19 Keberangkatan
                            </li>
                        </ul>
                    </div>

                    <div class="mb-16">
                        <div class="p-8 mb-8 bg-lightblue">
                            <div class="bg-content">
                                <h3 class="mb-0 text-blue">ITENERARY</h3>
                            </div>
                        </div>
                        <?php for ($i=1; $i <= 7; $i++) { ?>
                        <table class="mb-8 border-bottom" style="width: 90%;">
                            <tbody>
                                <tr>
                                    <td style="width: 25%" class="p-8">
                                        <img src="assets/img/400x300.png" alt="">
                                    </td>
                                    <td class="p-8">
                                        <h4 class="mb-8">Hari <?= $i ?> : Jakarta - Haneda</h4>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-plane2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-location2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-hotel2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-restaurant2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-file-text2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-camera2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-train2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20px;" class="pr-8">
                                                        <img src="assets/img/icon-building2.png" alt="">
                                                    </td>
                                                    <td>
                                                        Berangkat dari Soekarno Hatta, penerbangan menuju Jepang
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>

                    <div class="mb-36 no-break-inside">
                        <div class="p-8 mb-8 bg-lightblue">
                            <div class="bg-content">
                                <h3 class="mb-0 text-blue">RINCIAN TOUR</h3>
                            </div>
                        </div>
                        <div class="text-center">
                            <h3 class="mb-8">
                                BIAYA TOUR per ORANG dalam IDR RUPIAH (MINIMUM PESERTA: 20 ORANG DEWASA)<br>
                                *Harga tour dan program perjalanan dapat berubah sewaktu-waktu tanpa pemberitahuan
                            </h3>
                            <table class="table-border v-center bold border-bottom">
                                <thead class="line-small">
                                    <tr>
                                        <th rowspan="2">DEWASA<br>TWN/TRIP</th>
                                        <th rowspan="2">SINGLE OCCUPANCY</th>
                                        <th colspan="3">ANAK ANAK (2-11 TAHUN)</th>
                                        <th rowspan="2">APO TAX/FUEL</th>
                                        <th rowspan="2">VISA JEPANG</th>
                                    </tr>
                                    <tr>
                                        <th>TWIN</th>
                                        <th>EXTRA BED</th>
                                        <th>NO BED<br>(<6 Tahun)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>15.678.000</td>
                                        <td>15.678.000</td>
                                        <td>15.678.000</td>
                                        <td>15.678.000</td>
                                        <td>15.678.000</td>
                                        <td>INCLUDE</td>
                                        <td>660.000</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <table class="mb-36">
                        <tbody>
                            <tr class="border-bottom">
                                <td style="width: 24px" class="pb-8 pr-8">
                                    <img src="assets/img/icon-restaurant2.png" alt="">
                                </td>
                                <td class="pb-8 pr-8" style="width: 2px;">
                                    <strong>Makan</strong>
                                </td>
                                <td style="width: 2px" class="pb-8 pr-8">:</td>
                                <td class="pb-8">Included as Itenerary</td>
                            </tr>
                            <tr class="border-bottom">
                                <td style="width: 24px" class="pb-8 pr-8">
                                    <img src="assets/img/icon-plane2.png" alt="">
                                </td>
                                <td class="pb-8 pr-8" style="width: 2px;">
                                    <strong>Transportasi</strong>
                                </td>
                                <td style="width: 2px" class="pb-8 pr-8">:</td>
                                <td class="pb-8">Included as Itenerary</td>
                            </tr>
                            <tr class="border-bottom">
                                <td style="width: 24px" class="pb-8 pr-8">
                                    <img src="assets/img/icon-restaurant2.png" alt="">
                                </td>
                                <td class="pb-8 pr-8" style="width: 2px;">
                                    <strong>Akomodasi</strong>
                                </td>
                                <td style="width: 2px" class="pb-8 pr-8">:</td>
                                <td class="pb-8">Hotel 3 Nights</td>
                            </tr>
                            <tr class="border-bottom">
                                <td style="width: 24px" class="pb-8 pr-8">
                                    &nbsp;
                                </td>
                                <td class="pb-8 pr-8" style="width: 2px;">
                                    &nbsp;
                                </td>
                                <td style="width: 2px" class="pb-8 pr-8"></td>
                                <td class="pb-8">
                                    <ul class="mb-0">
                                        <li>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum ab corrupti architecto expedita voluptas neque sit perspiciatis quae.
                                        </li>
                                        <li>
                                            voluptatum voluptatibus fugit vel.
                                        </li>
                                        <li>
                                            delectus modi ullam architecto praesentium quod voluptates.
                                        </li>
                                        <li>
                                            harum sapiente ipsum accusamus.
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="border-bottom">
                                <td style="width: 24px" class="pb-8 pr-8">
                                    <img src="assets/img/icon-add-circle2.png" alt="">
                                </td>
                                <td class="pb-8 pr-8" style="width: 2px;">
                                    <strong>Included</strong>
                                </td>
                                <td style="width: 2px" class="pb-8 pr-8">:</td>
                                <td class="pb-8">
                                    <ul class="mb-0">
                                        <li>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum ab corrupti architecto expedita voluptas neque sit perspiciatis quae.
                                        </li>
                                        <li>
                                            voluptatum voluptatibus fugit vel.
                                        </li>
                                        <li>
                                            delectus modi ullam architecto praesentium quod voluptates.
                                        </li>
                                        <li>
                                            harum sapiente ipsum accusamus.
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="border-bottom">
                                <td style="width: 24px" class="pb-8 pr-8">
                                    <img src="assets/img/icon-minus-circle2.png" alt="">
                                </td>
                                <td class="pb-8 pr-8" style="width: 2px;">
                                    <strong>Excluded</strong>
                                </td>
                                <td style="width: 2px" class="pb-8 pr-8">:</td>
                                <td class="pb-8">
                                    <ul class="mb-0">
                                        <li>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum ab corrupti architecto expedita voluptas neque sit perspiciatis quae.
                                        </li>
                                        <li>
                                            voluptatum voluptatibus fugit vel.
                                        </li>
                                        <li>
                                            delectus modi ullam architecto praesentium quod voluptates.
                                        </li>
                                        <li>
                                            harum sapiente ipsum accusamus.
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="no-break-inside">
                        <h4 class="border-bottom">
                            Kebijakan dan Pembatalan
                        </h4>
                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam vitae, itaque sit a illo, earum recusandae numquam perspiciatis. Deleniti animi, vitae molestiae maxime quam voluptas dolores cum labore libero minus!</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem quam, quis ipsa beatae voluptatem perspiciatis dolorum necessitatibus iste quo sapiente commodi consectetur placeat quidem maiores dolore omnis eligendi aperiam sit.</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam temporibus, repudiandae cum! Nesciunt dolorem doloremque, officia fuga a placeat sunt ducimus accusamus ipsam, possimus error quaerat voluptas reiciendis velit ab?</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt dolorem laboriosam magni vel, blanditiis rerum obcaecati porro minima tenetur ipsa vitae reprehenderit officia aliquam ut ex unde commodi suscipit quisquam!</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda sequi dolorum, praesentium dolores. Nostrum modi illo consequatur aliquam eaque, asperiores molestiae accusamus soluta veniam. Fuga natus quos, praesentium debitis eaque.</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad dolor fugit, id libero voluptates, aliquid velit, possimus, laborum blanditiis rerum nisi nam! Repellat aliquam rem dignissimos ipsam fuga animi nemo!</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique maiores culpa perferendis animi sequi obcaecati, a repellat consequuntur fugiat, impedit nobis beatae accusantium libero, non dolorum aliquid laborum eligendi atque.</li>
                        </ul>
                    </div>

                    <h4 class="border-bottom">
                        Metode Pembayaran
                    </h4>
                    <ol>
                        <li>Cash</li>
                        <li>Cash</li>
                        <li>Cash</li>
                    </ol>
                </td>
            </tr>
        </tbody>
    </table>


</body>
</html>
