<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Events</a></li>
                <li><a href="#">Events Detail</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="text-center">
                <h1>8D6N Japan Magical Snow Sakura Tour</h1>
                <p class="t--larger">
                    Bagaimana rasanya menikmati pesona salju dan keindahan bunga sakura di Jepang dalam satu waktu yang sama? Rasakan sensasi dua musim terbaik di Jepang bersama Magical Snow Sakura, paket tour dengan itinerary spesial dua musim terbaik di Jepang. Anda bisa merasakan pengalaman bermain ski di Rusutsu Ski Resort serta menikmati mekarnya bunga sakura di Kagoshima.
                </p>
                <img src="assets/img/img-preload.png" data-src="assets/img/img-events.png" alt="" class="item-heavy">
            </div>
            <hr>
        </div>
    </section>
    <!-- section -->
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h3 class="title-group">
                    <span class="title-icon fa fa-1-5x fa-map-marker i--blue"></span>
                    <span class="title-text text-up t--larger">Destination</span>
                </h3>
            </div>
            <div class="inset-on-m">
                <div class="bzg">
                    <?php for ($i=1; $i <= 6; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="">
                                <h3 class="block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                                Tak hanya maju di bidang teknologi dan ekonomi, Jepang didukung oleh kondisi alamnya yang indah serta objek wisata budaya yang selalu dilestarikan
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- section -->
    <section class="section-block--inset fill-lightgrey">
        <div class="container container--small">
            <div class="block--double text-center">
                <h3 class="h1">Orange Shukudo</h3>
                <div class="block responsive-media">
                    <!-- <iframe class="video-iframe" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" frameborder="0" gesture="media" allowscriptaccess="always" allow="encrypted-media" allowfullscreen></iframe> -->
                    <object width="640" height="360">
                        <param name="movie" value="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0"/>
                        <param name="allowFullScreen" value="true"/>
                        <param name="allowscriptaccess" value="always"/>
                        <embed width="640" height="360" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" class="youtube-player" type="text/html" allowscriptaccess="always" allowfullscreen="true" allow="encrypted-media"/>
                    </object>
                </div>
                <p>
                    Memikirkan dimana akan menghabiskan liburan musim dingin yang menyenangkan sepertinya mudah tapi juga sedikit sulit karena banyaknya pilihan ski resort. Tapi, dari sekian banyak ski resort, poin - poin berikut dapat menjadi alasan untuk memilih Rusutsu Resort sebagai tujuan liburan musim dingin!
                </p>
            </div>
        </div>
        <div class="container container--smaller">
            <div class="inset-on-m">
                <div class="bzg">
                    <?php for ($i=1; $i <= 6; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="">
                                <h3 class="block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                                Tak hanya maju di bidang teknologi dan ekonomi, Jepang didukung oleh kondisi alamnya yang indah serta objek wisata budaya yang selalu dilestarikan
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- section -->
    <section class="section-block--inset">
        <div class="container container--small">
            <div class="block--double text-center">
                <h3 class="h1">Rusutsu Winter Season</h3>
                <div class="block responsive-media">
                    <object width="640" height="360">
                        <param name="movie" value="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0"/>
                        <param name="allowFullScreen" value="true"/>
                        <param name="allowscriptaccess" value="always"/>
                        <embed width="640" height="360" src="https://www.youtube.com/embed/09HGlSn-fcI?enablejsapi=1&amp;version=3&amp;rel=0" class="youtube-player" type="text/html" allowscriptaccess="always" allowfullscreen="true" allow="encrypted-media"/>
                    </object>
                </div>
                <p>
                    Memikirkan dimana akan menghabiskan liburan musim dingin yang menyenangkan sepertinya mudah tapi juga sedikit sulit karena banyaknya pilihan ski resort. Tapi, dari sekian banyak ski resort, poin - poin berikut dapat menjadi alasan untuk memilih Rusutsu Resort sebagai tujuan liburan musim dingin!
                </p>
            </div>
        </div>
        <div class="container container--smaller">
            <div class="inset-on-m">
                <h3 class="h2">1. Train Interior</h3>
                <div class="block bzg">
                    <?php for ($i=1; $i <= 6; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="">
                                <h3 class="block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
                <h3 class="h2">2. Lunch Service</h3>
                <div class="block bzg">
                    <?php for ($i=1; $i <= 6; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="">
                                <h3 class="block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- section -->
    <section class="section-block--inset fill-lightgrey">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h3 class="title-group">
                    <span class="title-icon fa fa-1-5x fa-map-marker i--blue"></span>
                    <span class="title-text text-up t--larger">Itenerary</span>
                </h3>
            </div>
            <?php for ($i=1; $i <= 6; $i++) { ?>
            <div class="bzg">
                <div class="block bzg_c" data-col="x3, l3" data-offset="l2">
                    <figure class="responsive-media media--3-2">
                                <img src="//placehold.it/300x200" alt="">
                            </figure>
                </div>
                <div class="block bzg_c" data-col="x9, l5">
                    <h4 class="block--half">Hari 1 : Jakarta - Haneda</h4>
                    <table class="no-space table--plain">
                        <tbody>
                            <tr>
                                <td>
                                    <span class="fa fa-plane"></span>
                                </td>
                                <td>
                                    Berangkat dari Bandara Soekarno-Hatta
                                </td>
                            </tr>
                            <tr>
                                <td><span class="fa fa-cutlery"></span></td>
                                <td>Meals : Makan Pagi - Makan Siang - Makan malam</td>
                            </tr>
                            <tr>
                                <td><span class="fa fa-file-text"></span></td>
                                <td>Note : Bermalam di Hotel</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php } ?>
        </div>
    </section>
    <!-- section -->
    <section class="section-block--inset">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h3 class="title-group">
                    <span class="title-icon fa fa-1-5x fa-building i--blue"></span>
                    <span class="title-text text-up t--larger">Foods, Shops, Hotels</span>
                </h3>
            </div>
            <div class="inset-on-m">
                <h3 class="h2">1. Train Interior</h3>
                <div class="block bzg">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="">
                                <h3 class="block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
                <h3 class="h2">2. Train Interior</h3>
                <div class="block bzg">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="">
                                <h3 class="block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
                <h3 class="h2">3. Train Interior</h3>
                <div class="block bzg">
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="">
                                <h3 class="block--small"><a href="#" class="link-black">Brunei Darussallam</a></h3>
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- section -->
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
