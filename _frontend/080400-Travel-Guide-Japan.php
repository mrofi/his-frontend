<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Travel Destination</a></li>
                <li><a href="#">Asia</a></li>
            </ul>
        </div>
    </div>

    <section class="section-block section-slide">
        <div class="container container--smaller">
            <div class="block slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="text-center">
                <h1 class="h2">Panduan Wisata di Jepang</h1>
                <p class="t--larger">
                    Anda dapat mencatat 6 poin penting di bawah ini, terutama bagi Anda yang baru pertama kali ke Jepang.<br>
                    Selamat berlibur!
                </p>
            </div>
            <hr>
        </div>
    </section>
    <section class="section-block">
        <div class="container container--smaller">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Musim di Jepang
                </h2>
            </div>
            <div class="inset-on-m">
                <div class="bzg bzg--small-gutter">
                    <?php for ($i=1; $i <= 4; $i++) { ?>
                    <div class="block bzg_c" data-col="m6">
                        <h3 class="block--small text-center text-up">Musim Semi</h3>
                        <div class="bzg bzg--small-gutter">
                            <div class="block--small bzg_c" data-col="m6, l7">
                                <figure class="responsive-media square">
                                    <img src="//placehold.it/400x400" alt="">
                                </figure>
                            </div>
                            <div class="bzg_c" data-col="m6, l5">
                                <p>
                                    Musim semi “haru” adalah saat dimana orang Jepang untuk melihat dan berpiknik di bawah bunga sakura yang disebut “Hanami”.
                                </p>
                                <p>
                                    <strong>Cuaca</strong>: cerah berawan, terkadang hujan<br>
                                    <strong>Suhu rata-rata (Tokyo)</strong>: 8.4˚C – 18.4˚C<br>
                                    <strong>Kelembaban</strong>: 64.7%
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr class="block--double">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Makanan di Jepang
                </h2>
            </div>
            <div class="inset-on-m">
                <div class="bzg bzg--small-gutter">
                    <?php for ($i=1; $i <= 8; $i++) { ?>
                    <div class="block bzg_c" data-col="m4, l3">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="text-center">
                                <h3 class="text-up block--small"><a href="#" class="link-black">Okonomiyaki</a></h3>
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr class="block--double">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Tempat - tempat Belanja di Jepang
                </h2>
            </div>
            <div class="inset-on-m">
                <div class="bzg bzg--small-gutter">
                    <div class="block bzg_c" data-col="m4, l3">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="text-center">
                                <h3 class="text-up block--small"><a href="#" class="link-black">ASAKUSA SHIN NAKAMISE STREET</a></h3>
                                Tak hanya maju di bidang teknologi dan ekonomi, Jepang didukung oleh kondisi alamnya yang indah serta objek wisata budaya yang selalu dilestarikan
                            </figcaption>
                        </figure>
                    </div>
                    <?php for ($i=1; $i <= 7; $i++) { ?>
                    <div class="block bzg_c" data-col="m4, l3">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="text-center">
                                <h3 class="text-up block--small"><a href="#" class="link-black">ASAKUSA SHIN NAKAMISE STREET TOGOSHI GINZA (TOKYO)</a></h3>
                                Tak hanya maju di bidang teknologi dan ekonomi, Jepang didukung oleh kondisi alamnya yang indah serta objek wisata budaya yang selalu dilestarikan
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr class="block--double">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Rekomendasi Restoran Halal
                </h2>
            </div>
            <div class="inset-on-m">
                <div class="bzg bzg--small-gutter">
                    <div class="block bzg_c" data-col="m4, l3">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="text-center">
                                <h3 class="text-up block--small"><a href="#" class="link-black">ASAKUSA SHIN NAKAMISE STREET</a></h3>
                                Tak hanya maju di bidang teknologi dan ekonomi, Jepang didukung oleh kondisi alamnya yang indah serta objek wisata budaya yang selalu dilestarikan
                            </figcaption>
                        </figure>
                    </div>
                    <?php for ($i=1; $i <= 3; $i++) { ?>
                    <div class="block bzg_c" data-col="m4, l3">
                        <figure>
                            <a href="#" class="block--small responsive-media media--3-2">
                                <img class="item-heavy" data-src="//placehold.it/300x200" alt="">
                            </a>
                            <figcaption class="text-center">
                                <h3 class="text-up block--small"><a href="#" class="link-black">ASAKUSA SHIN NAKAMISE STREET TOGOSHI GINZA (TOKYO)</a></h3>
                                Tak hanya maju di bidang teknologi dan ekonomi, Jepang didukung oleh kondisi alamnya yang indah serta objek wisata budaya yang selalu dilestarikan
                            </figcaption>
                        </figure>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr class="block--double">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Transaksi di jepang
                </h2>
            </div>
            <div class="inset-on-m">
                <div class="block bzg bzg--small-gutter">
                    <div class="block--small bzg_c" data-col="m6">
                        <figure>
                            <img src="//placehold.it/600x480" class="img-full" alt="">
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m6">
                        <h4 class="no-space">1. UANG TUNAI</h4>
                        <p>Sama seperti negara pada umumnya, transaksi jual beli di Jepang dapat menggunakan uang tunai. Pecahan uang tunai yang berlaku di Jepang yaitu; koin mulai dari 5, 10, 50, 100, dan 500yen, sedangkan uang kertas mulai dari 1000, 5000, dan 10,000yen. Koin dapat digunakan untuk vending machines, bus, dan loker penitipan barang. Uang tunai biasa digunakan untuk membayar tiket masuk tempat wisata.</p>
                        <h4 class="no-space">2. KARTU KREDIT / KARTU DEBIT</h4>
                        <p>Selain uang tunai, Anda juga dapat menggunakan kartu kredit/kartu debit untuk membayar hotel, berbelanja, beberapa restoran dan butik.</p>
                        <h4 class="no-space">3. KARTU IC</h4>
                        <p>Kartu IC, seperti Suica dan Icoca merupakan kartu yang biasa digunakan untuk membayar tiket kereta api dan bus. Kartu ini dapat diisi ulang dan sekarang juga dapat digunakan untuk melakukan transaksi di sejumlah toko, restoran terutama yang berada di sekitar kereta api, vending machines, dan tempat menyewa loker.</p>
                    </div>
                </div>
            </div>
            <hr class="block--double">
            <div class="block section-head clearfix">
                <h2 class="no-space text-up in-block">
                    Visa jepang
                </h2>
            </div>
            <div class="inset-on-m">
                <div class="block bzg bzg--small-gutter">
                    <div class="block--small bzg_c" data-col="m6">
                        <figure>
                            <img src="//placehold.it/600x320" class="img-full" alt="">
                        </figure>
                    </div>
                    <div class="bzg_c" data-col="m6">
                        <p><strong>
                            H.I.S. Tours & Travel merupakan Travel Agent yang Ditunjuk untuk pengajuan Visa Jepang oleh Kedutaan Besar Jepang/Kantor Konsulat Jepang di Indonesia
                        </strong></p>
                        <strong>Kedutaan  Besar Jepang</strong><br>
                        Alamat: Jln. MH. Thamrin No. 24, Menteng – Jakpus 10350 <br>
                        No. t: (+62 21) 3192 4308 <br>
                        Fax: (+62 21) 3192 5460 <br>
                        Website / Email: <a href="www.id.emb-japan.go.jp">www.id.emb-japan.go.jp</a><br>
                        Jam Kerja: 09.00 – 12.30 <br>
                        Untuk syarat dan ketentuan Visa, silahkan klik <a href="#">disini</a>
                    </div>
                </div>
            </div>
            <hr class="block--double">
            <div class="bzg">
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jtd.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c" data-col="m6">
                    <a href="#">
                        <img src="assets/img/banner-jrm.jpg" alt="">
                    </a>
                </div>
                <div class="block--half bzg_c">
                    <a href="#">
                        <img src="assets/img/banner-tic.jpg" alt="">
                    </a>
                </div>
            </div>
            <hr>
        </div>
    </section>

    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
