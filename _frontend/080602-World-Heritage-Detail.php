<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Product</a></li>
            </ul>
        </div>
    </div>

    
    <div class="container container--smaller">
        <h1 class="block--small">Product Content Layout</h1>
        <div class="bzg">
            <div class="bzg_c" data-col="m8">
                
                <section class="section-block--smaller">
                    <div class="responsive-media media--3-2">
                        <div class="demihero-slider slide-default">
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-1.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                            <figure class="slide__item no-space">
                                <img data-lazy="assets/img/shinkansen-2.jpg" alt="">
                                <figcaption class="slide-caption">
                                    <strong>Hokuriku Shinkansen , merupakan kereta cepat</strong>
                                </figcaption>
                            </figure>
                        </div>
                        <!-- demihero-slider -->
                    </div>
                </section>
                <section class="section--block">
                    <article>
                        <div class="block">
                            <div class="block fill-lightgrey block--inset">
                                <h3 class="title-group">
                                    <span class="title-icon fa fa-1-5x fa-search i--blue"></span>
                                    <span class="title-text text-up t--larger">Deskripsi</span>
                                </h3>
                                <p>
                                    Huis Ten Bosch atau HTB yang merayakan anniversary ke 20 pada 2012 lalu adalah resort terluas di Jepang dengan area 1,520,000 meter persegi yang menyajikan tempat-tempat dengan tampilan Eropa klasik. 
                                </p>
                            </div>

                            <div class="block">
                                <h3 class="title-group">
                                    <span class="title-icon fa fa-1-5x fa-map-marker i--blue"></span>
                                    <span class="title-text text-up t--larger">List Images</span>
                                </h3>
                                <div class="bzg">
                                    <div class="block bzg_c" data-col="m6">
                                        <figure>
                                            <img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
                                            <figcaption class="text-center">
                                                <h4 class="block--half">Gunung Fuji</h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic eos sed consectetur quibusdam perspiciatis inventore temporibus soluta assumenda ullam vero officia delectus non maiores numquam, veritatis optio nesciunt velit consequatur.
                                                </p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="block bzg_c" data-col="m6">
                                        <figure>
                                            <img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
                                            <figcaption class="text-center">
                                                <h4 class="block--half">Gunung Fuji</h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nisi minima ex numquam dolore autem qui laudantium aliquid sit dolorem exercitationem ipsa quo, ut quae, magni excepturi soluta, atque fuga.
                                                </p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <hr>
                            </div>

                            <div class="block">
                                <h3 class="title-group">
                                    <span class="title-icon fa fa-1-5x fa-bed i--blue"></span>
                                    <span class="title-text text-up t--larger">Rekomendasi Hotel</span>
                                </h3>
                                <div class="bzg">
                                    <div class="block bzg_c" data-col="m4">
                                        <figure>
                                            <img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
                                            <figcaption class="text-center">
                                                <h4 class="block--half">Yuki Resort Koshinoyu</h4>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="block bzg_c" data-col="m4">
                                        <figure>
                                            <img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
                                            <figcaption class="text-center">
                                                <h4 class="block--half">Gunung Fuji</h4>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="block bzg_c" data-col="m4">
                                        <figure>
                                            <img src="assets/img/img-preload.png" alt="" data-src="//placehold.it/360x260" class="block--half item-heavy img-full">
                                            <figcaption class="text-center">
                                                <h4 class="block--half">Gunung Fuji</h4>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <hr>
                            </div>

                            <div class="block">
                                <h3 class="title-group">
                                    <span class="title-icon fa fa-1-5x fa-bed i--blue"></span>
                                    <span class="title-text text-up t--larger">Rekomendasi Optional Tour</span>
                                </h3>
                                <div class="bzg cards cards--pink">
                                    <?php for ($i=1; $i <= 3; $i++) { ?>
                                    <div class="block bzg_c" data-col="m4">
                                        <div class="card__item">
                                            <figure class="item-img img-square fill-lightgrey">
                                                <img src="assets/img/img-preload.png" data-src="//placehold.it/400x400" alt="" class="item-heavy">
                                            </figure>
                                            <div class="item-text text-center">
                                                <strong class="text-up ellipsis-2 t--smaller">
                                                    Fuji - Hakone Gotemba Special
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
            </div>
            <div class="bzg_c" data-col="m4">
                <div class="cards">
                    <div class="block block--inset-small border">
                        <table class="no-space table--line">
                            <tbody>
                                <tr>
                                    <td><span class="flag-icon flag-icon-90 flag-icon-jp"></span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Lokasi &amp; Wilayah</td>
                                    <td>Kii Penisuila, Mount Koya</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>Kii Penisuila, Mount Koya</td>
                                </tr>
                                <tr>
                                    <td>Tahun didirikan</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>Tahun didaftarkan</td>
                                    <td>2004</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="block">
                        <div class="block--half card-head block--inset-small fill-blue">
                            MAP
                        </div>
                        <div class="responsive-media media--4-3">
                            <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1HewlVIqDesVb00TBGLkqbPX_ecn_z1zR" width="320" height="240"></iframe>
                        </div>
                    </div>
                    <div class="block">
                        <div class="block--half card-head block--inset-small fill-blue">
                            Model Rute Rekomendasi
                        </div>
                        <ol class="block list-step-arrow">
                            <li>
                                <span class="full-v">Jakarta</span>
                            </li>
                            <li>
                                <span class="full-v">Tokyo</span>
                            </li>
                            <li>
                                <span class="full-v">Nara</span>
                            </li>
                            <li>
                                <span class="full-v">Wakayama</span>
                            </li>
                        </ol>
                    </div>
                    <div class="block">
                        <div class="block--half card-head block--inset-small fill-blue">
                            Akses
                        </div>
                        <ul class="list-nostyle list-iconic list-iconic--circle">
                            <li class="block--half">
                                <span class="item-icon fa fa-bus" aria-hidden="true"></span>
                                <div class="item-text line--small">
                                    <h3 class="block--small text-up">Bus</h3>
                                    <ul class="list-inside">
                                        <li>
                                            <strong>Bus Lokal</strong> Komando Bus, Bus Nara Kotsu, Bus Kramat Djati
                                        </li>
                                        <li>
                                            <strong>Bus Lokal</strong> Komando Bus, Bus Nara Kotsu, Bus Kramat Djati
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="block--half">
                                <span class="item-icon fa fa-train" aria-hidden="true"></span>
                                <div class="item-text line--small">
                                    <h3 class="block--small text-up">Kereta</h3>
                                    <ul class="list-inside">
                                        <li>
                                            <strong>Bus Lokal</strong> Komando Bus, Bus Nara Kotsu, Bus Kramat Djati
                                        </li>
                                        <li>
                                            <strong>Bus Lokal</strong> Komando Bus, Bus Nara Kotsu, Bus Kramat Djati
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
    
    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
