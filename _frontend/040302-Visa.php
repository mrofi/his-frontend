<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Aktivitas</a></li>
                <li><a href="#">Optional Tour</a></li>
                <li><a href="#">Visa</a></li>
            </ul>
        </div>
    </div>

    <section class="section-slide">
        <div class="container container--smaller">
            <div class="slider-hero">
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-1.jpg" alt="">
                    </a>
                </div>
                <div class="slider__item">
                    <a href="#">
                        <img src="assets/img/slidebanner-2.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-block section-booking">
        <div class="container container--smaller">
            <h1 class="sr-only">Visa</h1>
            <div class="fill-primary block--inset t--larger">
                <form action="" class="form">
                    <div class="bzg bzg--no-gutter">
                        <div class="bzg_c" data-col="s8, m10">
                            <input type="search" class="form-input form-input--block" placeholder="Search">
                        </div>
                        <div class="bzg_c" data-col="s4, m2">
                            <button class="btn btn--block btn--red">
                                <strong class="text-up">Cari</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section class="section-block text-center">
        <div class="container container--smaller">
            <div class="block bzg">
                <!-- <button class="btn btn--red" type="button">
                    <strong class="text-up">Recommended</strong>
                </button>
                <button class="btn btn--ghost-red-black" type="button">
                    <strong class="text-up">Recommended</strong>
                </button> -->
                <div class="bzg_c" data-col="x6">
                    <a href="#" class="btn-img is-active">
                        <img class="img-full" src="//via.placeholder.com/450x275?text=VISA" alt="">
                    </a>
                </div>
                <div class="bzg_c" data-col="x6">
                    <a href="#" class="btn-img">
                        <img class="img-full" src="//via.placeholder.com/450x275?text=PASPORT" alt="">
                    </a>
                </div>
            </div>
            <hr class="block--half">
            <h3 class="text-up"><span class="t--larger">Filter by Destination</span></h3>
        </div>
        <nav class="fill-lightgrey block--inset-small">
            <div class="container container--small">
                <ul class="list-nostyle list-inblock--small">
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Asia</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Eropa</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Australia</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">America</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Timur Tengah</strong></a></li>
                    <li><a href="#" class="btn btn--round btn--ghost-red-black"><strong class="text-up">Africa</strong></a></li>
                </ul>
            </div>
        </nav>
    </section>

    <section class="section-block">
        <div class="container container--smaller">
            <div class="inset-on-m">
            <div class="bzg cards cards--blue">
                <?php for ($i=1; $i <= 8; $i++) { ?>
                <div class="block bzg_c" data-col="m4">
                    <div class="card__item">
                        <figure class="item-img img-square fill-lightgrey">
                            <img src="assets/img/img-preload.png" data-src="//placehold.it/420x420" alt="" class="item-heavy">
                            <div class="item-detail">
                                <div class="text-center">
                                    <a href="#" class="btn btn--round btn--ghost-red-black">
                                        <b class="text-up">Pesan Sekarang</b>
                                    </a>
                                </div>
                            </div>
                        </figure>
                        <div class="item-text">
                            <a href="#">
                                <strong class="text-up ellipsis-2">
                                    KOREA JEJU PLUS HAN RIVER CRUISE BY GA Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </strong>
                                <div class="cf">
                                    <strong class="text--larger t-yellow in-block space-right">
                                        IDR 6.698.000++
                                    </strong>
                                    <span class="pull-right">
                                        <span class="fa fa-globe"></span>
                                        Asia
                                    </span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            </div>
        </div>
    </section>
    <?php include '_partials/travel-tips.php'; ?>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
