<?php include '_partials/head.php'; ?>
<?php include '_partials/header.php'; ?>

<main class="sticky-footer-container-item --pushed site-main">
    <div class="block">
        <div class="container container--smaller">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Hotel</a></li>
                <li><a href="#">Mandarin Oriental</a></li>
            </ul>
        </div>
    </div>

    <div class="mb-2 container container--smaller">
        <h1 class="no-space">Hasil Pencarian Anda di Jakarta, Indonesia</h1>
        <p>27 Apr 2018 - 30 Apr 2018</p>
    </div>

    <div class="fill-primary block--half" id="research_hotel" data-room="dev/room2.json">
    <!-- <div class="block--double fill-primary block--inset relative z-4"> -->
        <div class="container container--smaller">
            <div class="block--inset-v relative z-4">
            <form id="form-hotel" action="https://histravel.suitdev.com/hotel/search" class="form" data-research="dev/room2.json">
                <input type="hidden" name="city_id" value="339_ID">
                <div class="bzg bzg--lite-gutter">
                    <div class="form__row bzg_c" data-col="l6">
                        <label for=""  class="sr-only"  >Destinasi Pilihan</label>
                        <select required name="city_id" id="city_id" class="form-input form-input--block select_2"
                            data-select="https://histravel.suitdev.com/hotel/destination"
                            data-placeholder="Dimana anda akan menginap?">
                            <option value="7355_SG">Singapore, Singapore</option>
                        </select>
                    </div>
                    <div class="form__row bzg_c" data-col="l6">

                        <div class="bzg bzg--lite-gutter pickdate-range">
                            <div class="form__row bzg_c" data-col="m6">
                                <label for=""  class="sr-only"  >Check In</label>
                                <div class="input-iconic">
                                    <input required name="checkin_date" type="text" class="form-input form-input--block pikaRange pikaRange--start" id="chekin_start" placeholder="Tanggal Check In"
                                     value="2018/12/20"                                     >
                                    <label for="chekin_start" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form__row bzg_c" data-col="m6">
                                <label for=""  class="sr-only"  >Check Out</label>
                                <div class="input-iconic">
                                    <input required name="checkout_date" type="text" class="form-input form-input--block pikaRange pikaRange--end" id="chekout_end" placeholder="Tanggal Check Out"
                                     value="2018/12/24"                                     >
                                    <label for="chekout_end" class="label-icon">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- pickdate-range -->
                    </div>

                </div>

                <div class="bzg">
                    <div class="mb-small bzg_c" data-col="l6">
                        <label for="">Jumlah Kamar</label>
                        <div class="bzg">
                            <div class="bzg_c"data-col="l6">
                                <div class="flex a-center">
                                    <div class="fg-1">
                                        <div class="flex add-room">
                                            <input type="number" class="sr-only room-counter" min="1" max="8" value="1">
                                            <div class="fg-1 form-input form-input-viewer text-center">1</div>
                                            <button class="btn btn--plain" style="color: #fff;" type="button" data-action="minus">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                            <button class="btn btn--plain" style="color: #fff;" type="button" data-action="plus">
                                                <span class="fa fa-plus"></span>
                                            </button>

                                            <template id="roomContent">
                                                <div class="bzg guest-list">
                                                    <span class="bzg_c hidden">Kamar <span class="room-index"></span></span>
                                                    <div class="mb-small bzg_c" data-col="m6">
                                                        <div class="flex a-center">
                                                            <label for="" class="nowrap in-block mr-small w-30">
                                                                <span class="fa fa-male"></span>
                                                                <span class="fa fa-female"></span>
                                                            </label>
                                                            <div class="fg-1">
                                                                <select name="adult_guest[]" id="adult_guest" class="adult-guest form-input form-input--block form-input--person selectstyle" data-value="{{adults}}">
                                                                    <option value="1">1 Dewasa</option>
                                                                    <option value="2">2 Dewasa</option>
                                                                    <option value="3">3 Dewasa</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-small bzg_c" data-col="m6">
                                                        <div class="flex a-center">
                                                            <label for="" class="nowrap in-block mr-small w-30 text-center">
                                                                <span class="fa fa-child"></span>
                                                            </label>
                                                            <div class="fg-1">
                                                                <select name="child_guest[]" id="child_guest" class="form-input form-input--child form-input--block form-input--person selectstyle child-age-select" data-value="{{childs.length}}" data-ages='[{{childs}}]'>
                                                                    <option value="0">0 Anak</option>
                                                                    <option value="1">1 Anak</option>
                                                                    <option value="2">2 Anak</option>
                                                                    <option value="3">3 Anak</option>
                                                                    <option value="4">4 Anak</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-half bzg_c child-age-container">
                                                        <div class="">Umur Anak</div>
                                                        <div class="bzg bzg--small-gutter child-age__inputs">

                                                        </div>
                                                        <hr class="no-space">
                                                    </div>
                                                </div>
                                            </template>
                                            <!-- roomContent template -->
                                            <script>
                                                function childAgeInput(index, age, val = 0) {
                                                    return '<div class="mb-small bzg_c" data-col="s3">'+
                                                    '<select name="child_age['+index+']['+age+']" id="ChildAge['+index+']['+age+']" class="form-input form-input--block" data-value="'+val+'">'+ages(18)+
                                                    '</select>'+
                                                    '</div>'
                                                }
                                                function ages(age) {
                                                    var option;
                                                    option += '<option value="0">&lt;1 Tahun</option>'
                                                    for(var i = 1; i < age; i++ ) {
                                                        option += '<option value="'+i+'">'+i+' Tahun</option>'
                                                    }
                                                    return option
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-small bzg_c guest-container" data-col="l6">
                        loading
                    </div>
                </div>
                <div class="bzg">&nbsp;</div>
                <div class="bzg">
                    <div class="form__row bzg_c" data-col="l6">
                        <div class="toggle-fyout-m" data-toggle>
                            <button class="btn btn-toggle btn--plain link-white">
                                <span class="fa fa-angle-down btn-icon"></span>
                                Pilihan Pencarian Tambahan
                            </button>
                            <div class="toggle-panel fill-pink">
                                <div class="block--inset">
                                    <div class="form__row">
                                        <label for="">Harga per malam</label>
                                        <div class="bzg">
                                            <div class="form__row bzg_c" data-col="s6">
                                                <div class="input-iconic--left fill-white border">
                                                    <label for="" class="label-icon">IDR</label>
                                                    <input name="price_min" type="number" class="form-input form-input--block text-right no-border" placeholder="minimum">
                                                </div>
                                            </div>
                                            <div class="form__row bzg_c" data-col="s6">
                                                <div class="input-iconic--left fill-white border">
                                                    <label for="" class="label-icon">IDR</label>
                                                    <input name="price_max" type="number" class="form-input form-input--block text-right no-border" placeholder="maksimum">
                                                </div>
                                            </div>
                                            <div class="form__row bzg_c" data-col="m5, l6">
                                                <label for="">Star Rating</label>
                                                <div class="add-rating">
                                                    <label class="btn-star" for="star-1">
                                                        <input name="star" value="1" class="btn-radio" type="radio" name="rating-star" id="star-1">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-2">
                                                        <input name="star" value="2" class="btn-radio" type="radio" name="rating-star" id="star-2">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-3">
                                                        <input name="star" value="3" class="btn-radio" type="radio" name="rating-star" id="star-3">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-4">
                                                        <input name="star" value="4" class="btn-radio" type="radio" name="rating-star" id="star-4">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                    <label class="btn-star" for="star-5">
                                                        <input name="star" value="5" class="btn-radio" type="radio" name="rating-star" id="star-5">
                                                        <span class="fa fa-star icon-star"></span>
                                                    </label>
                                                </div>
                                                <!-- add-rating -->
                                            </div>
                                            <div class="form__row bzg_c" data-col="s7, l6">
                                                <label for="">Ketersediaan</label><br>
                                                <label for="available_hotel">
                                                    <input name="is_available_only" value="1" type="checkbox" id="available_hotel">
                                                    <span>
                                                        Hanya menampilkan hotel yang tersedia
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bzg_c" data-col="l6">
                        <div class="bzg">
                            <div class="bzg_c" data-col="l12">
                                <label for="nationality">Kewarganegaraan :</label>
                            </div>
                        </div>
                        <div class="bzg">
                            <div class="bzg_c" data-col="l6">
                                <div class="fg-1">
                                    <select name="NationalityID" id="nationality" class="form-input form-input--block selectstyle">
                                        <option value="75"  selected="selected" >Indonesia</option>
                                        <option value="86" >Singapura</option>
                                    </select>
                                </div>
                                <br>
                            </div>
                            <div class="bzg_c" data-col="l6">
                                <button class="btn btn--red btn--block">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            </div>
        </div>
    </div>

    <div class="container container--smaller">
        <div class="mb-2 bzg">
            <div class="bzg_c" data-col="m4, l3">
                <div class="filter-toggle lock-body" data-toggle>
                    <button class="btn btn-toggle" type="button">
                        <span class="label-open">Filter</span>
                        <span class="label-close">Batal</span>
                    </button>
                    <div class="toggle-panel">
                        <form action="">
                            <div class="field-group">
                                <fieldset>
                                    <legend class="t-strong">Penawaran Kamar</legend>
                                    <div class="mb-small">
                                        <label for="sarapan" class="flex a-center">
                                            <input type="checkbox" id="sarapan" class="mr-small">
                                            <span>Tersedia Sarapan Pagi (457)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="ditempat" class="flex a-center">
                                            <input type="checkbox" id="ditempat" class="mr-small">
                                            <span>Bayar di tempat (457)</span>
                                        </label>
                                    </div>
                                    <hr>
                                </fieldset>
                                <fieldset>
                                    <legend class="t-strong">Hasil Pencarian Untuk</legend>
                                    <div class="mb-small p-small">
                                        <input type="search" class="form-input form-input--block">
                                    </div>
                                    <hr>
                                </fieldset>
                                <fieldset>
                                    <legend class="t-strong">Rekomendasi Penyaringan</legend>
                                    <div class="mb-small">
                                        <label for="swimmingPool" class="flex a-center">
                                            <input type="checkbox" id="swimmingPool" class="mr-small">
                                            <span>Sweeming Pool</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="freeWiFi" class="flex a-center">
                                            <input type="checkbox" id="freeWiFi" class="mr-small">
                                            <span>Free WiFi</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="playGround" class="flex a-center">
                                            <input type="checkbox" id="playGround" class="mr-small">
                                            <span>Playground</span>
                                        </label>
                                    </div>
                                    <hr>
                                </fieldset>
                                <fieldset>
                                    <legend class="t-strong">Harga per malam</legend>
                                    <div class="mb-small">
                                        <div class="nouislider-container">
                                            <div class="mb-half" id="priceSlider" data-start='[0, 1000000]' data-min="0" data-max="20000000"></div>
                                            <div class="">
                                                IDR <span id="lowerPrice"></span> - IDR <span id="upperPrice"></span>
                                                <input type="hidden" id="lowerPriceInput">
                                                <input type="hidden" id="upperPriceInput">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </fieldset>
                                <fieldset>
                                    <legend class="t-strong">Hotel Star Rating</legend>
                                    <div class="mb-small">
                                        <label for="rating_5" class="flex a-center">
                                            <input type="checkbox" id="rating_5" class="mr-small">
                                            <span class="fg-1 t-yellow t--larger">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="rating_4" class="flex a-center">
                                            <input type="checkbox" id="rating_4" class="mr-small">
                                            <span class="fg-1 t-yellow t--larger">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="rating_3" class="flex a-center">
                                            <input type="checkbox" id="rating_3" class="mr-small">
                                            <span class="fg-1 t-yellow t--larger">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="rating_2" class="flex a-center">
                                            <input type="checkbox" id="rating_2" class="mr-small">
                                            <span class="fg-1 t-yellow t--larger">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="rating_1" class="flex a-center">
                                            <input type="checkbox" id="rating_1" class="mr-small">
                                            <span class="fg-1 t-yellow t--larger">
                                                <span class="fa fa-star"></span>
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                                <fieldset class="block">
                                    <legend class="t-strong">Property Facilities</legend>
                                    <div class="mb-small">
                                        <label for="sweemingPool53" class="flex a-center">
                                            <input type="checkbox" id="sweemingPool53" class="mr-small">
                                            <img src="assets/img/icon-swimming-pool.png" alt="" width="24">
                                            <span>Swimming Pool (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="internet53" class="flex a-center">
                                            <input type="checkbox" id="internet53" class="mr-small">
                                            <img src="assets/img/icon-internet.png" alt="" width="24">
                                            <span>internet (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="fitnes53" class="flex a-center">
                                            <input type="checkbox" id="fitnes53" class="mr-small">
                                            <img src="assets/img/icon-gym.png" alt="" width="24">
                                            <span>Gym/Fitnes (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="restaurant123" class="flex a-center">
                                            <input type="checkbox" id="restaurant123" class="mr-small">
                                            <img src="assets/img/icon-restaurant.png" alt="" width="24">
                                            <span>Restaurant (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="spaSauna" class="flex a-center">
                                            <input type="checkbox" id="spaSauna" class="mr-small">
                                            <img src="assets/img/icon-spa.png" alt="" width="24">
                                            <span>Spa/sauna (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="nonSmoking" class="flex a-center">
                                            <input type="checkbox" id="nonSmoking" class="mr-small">
                                            <img src="assets/img/icon-non-smoking.png" alt="" width="24">
                                            <span>Non Smoking (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="familyChild" class="flex a-center">
                                            <input type="checkbox" id="familyChild" class="mr-small">
                                            <img src="assets/img/icon-family.png" alt="" width="24">
                                            <span>Family/child friendly (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="petsAllowed" class="flex a-center">
                                            <input type="checkbox" id="petsAllowed" class="mr-small">
                                            <img src="assets/img/icon-pets.png" alt="" width="24">
                                            <span>Pets Allowed (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="nightCLub" class="flex a-center">
                                            <input type="checkbox" id="nightCLub" class="mr-small">
                                            <img src="assets/img/icon-nightclub.png" alt="" width="24">
                                            <span>Nightclub/bar (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="disabledGuest" class="flex a-center">
                                            <input type="checkbox" id="disabledGuest" class="mr-small">
                                            <img src="assets/img/icon-disabled-guest.png" alt="" width="24">
                                            <span>Facilities for disabled guest (53)</span>
                                        </label>
                                    </div>
                                    <div class="mb-small">
                                        <label for="businessFacilities" class="flex a-center">
                                            <input type="checkbox" id="businessFacilities" class="mr-small">
                                            <img src="assets/img/icon-bussiness.png" alt="" width="24">
                                            <span>Business Facilities (53)</span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                            <button class="btn btn--block btn--blue filter-submit">
                                Apply Filter
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="bzg_c" data-col="m8, l9">
                <h2>Jakarta: 3875 Properties</h2>
                <div class="sort-nav" data-toggle>
                    <button class="btn btn--ghost btn-toggle" type="button">
                        <span class="btn-text">Urutkan hasil berdasarkan:</span>
                        <span class="btn-icon fa fa-angle-down"></span>
                    </button>
                    <div class="toggle-panel">
                        <div class="label-nav">Urutkan hasil berdasarkan:</div>
                        <ul class="list-nostyle">
                            <li class="sort-nav__item">
                                <label for="allRes">
                                    <input type="radio" id="allRes" name="hotel sort" checked>
                                    <span class="label-bg"></span>
                                    <span class="label-text">Semua</span>
                                </label>
                            </li>
                            <li class="sort-nav__item">
                                <label for="recomendRes">
                                    <input type="radio" id="recomendRes" name="hotel sort">
                                    <span class="label-bg"></span>
                                    <span class="label-text">Rekomendasi</span>
                                </label>
                            </li>
                            <li class="sort-nav__item">
                                <label for="priceRes">
                                    <input type="radio" id="priceRes" name="hotel sort">
                                    <span class="label-bg"></span>
                                    <span class="label-text">Harga</span>
                                </label>
                            </li>
                            <li class="sort-nav__item">
                                <label for="ratingRes">
                                    <input type="radio" id="ratingRes" name="hotel sort">
                                    <span class="label-bg"></span>
                                    <span class="label-text">Rating</span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="items">
                    <?php for ($i=1; $i <= 8; $i++) { ?>
                    <div class="block bzg item">
                        <div class="bzg_c" data-col="m5, l4">
                            <a href="#"><img src="http://via.placeholder.com/400" alt=""></a>
                        </div>
                        <div class="bzg_c flex flow-column--spread" data-col="m7, l8">
                            <div class="block bzg">
                                <div class="bzg_c" data-col="l7">
                                    <h3 class="no-space"> <a href="#">Mandarin Oriental</a></h3>
                                    <address class="text-blue flex line--small">
                                        <span class="fs-0 fa fa-map-marker mr-small"></span>
                                        <span class="fg-1">Jl. Pejaten Barat II No. 3A Jakarta Selatan 12510 Indonesia</span>
                                    </address>
                                </div>
                                <div class="bzg_c text-right" data-col="m5">
                                    <span class="t-yellow t--larger">
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="bzg a-bottom">
                                <div class="mb-half bzg_c" data-col="l7">
                                    <div class="block">
                                        <div class="mb-half label-ribbon label--iconic label--ghost-blue">
                                            <div class="label-icon"><img src="assets/img/icon-cup.png" alt=""></div>
                                            <div class="label-text">Best price +5 stars hotel</div>
                                        </div>
                                        <div class="mb-half label-ribbon label--iconic label--ghost-pink">
                                            <div class="label-icon"><img src="assets/img/icon-bars.png" alt=""></div>
                                            <div class="label-text">Free Cancellation</div>
                                        </div>
                                    </div>
                                    <ul class="list-facilities list-nostyle list-inline">
                                        <li class="mr-small">
                                            <img src="assets/img/icon-restaurant.png" alt="" width="30">
                                        </li>
                                        <li class="mr-small">
                                            <img src="assets/img/icon-swimming-pool.png" alt="" width="30">
                                        </li>
                                        <li class="mr-small">
                                            <img src="assets/img/icon-internet.png" alt="" width="30">
                                        </li>
                                        <li class="mr-small">
                                            <img src="assets/img/icon-gym.png" alt="" width="30">
                                        </li>
                                        <li class="mr-small">
                                            <img src="assets/img/icon-restaurant.png" alt="" width="30">
                                        </li>
                                    </ul>
                                </div>
                                <div class="mb-half bzg_c text-center" data-col="l5">
                                    <small class="text-grey">
                                        <span class="fa fa-info-circle"></span>
                                        Harga permalam mulai dari
                                    </small><br>
                                    <div class="h2 mb-small t-strong">IDR 2.345.000</div>
                                    <a href="#" class="btn btn--block btn--round btn--red">
                                        Pilih Kamar
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?php } ?>
                </div>
                <ol class="pagination text-right">
                    <li>
                        <a href="#">
                            <span class="fa fa-chevron-left"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="active">
                            1
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            2
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-chevron-right"></span>
                        </a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</main>

<?php include '_partials/footer.php'; ?>
<?php include '_partials/scripts.php'; ?>
